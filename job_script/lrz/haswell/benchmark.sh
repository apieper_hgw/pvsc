#!/bin/bash
#@ job_name = pvsc_bench
#@ output = pvsc_bench_$(jobid).out
#@ error =  pvsc_bench_$(jobid).err
#@ job_type = parallel
#@ class = test
#@ node = 1
#@ island_count = 1
#@ wall_clock_limit = 4:00:00
# Job classes on SuperMUC Thin Nodes
# test      1-0032 nodes  30min
# micro     1-0032 nodes  48h
# general  33-0512 nodes  48h
# large   513-2048 nodes  48h
# one island has max 512 nodes
#@ total_tasks = 1
#@ node_usage = not_shared
#@ network.MPI = sn_all,not_shared,us
#@ notification=never
#@ notify_user=pieper@physik.uni-greifswald.de
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

#setup of environment

module load likwid
module list

echo $LOADL_STEP_INITDIR

#likwid-topology

EXEPATH=$HOME/pvsc/bin

#for i in `seq 14 1 14`; do
 #for nb in 1 2 4 8; do
   # OMP_NUM_THREADS=$i $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-$(echo "4000 / $nb" | bc)  -dot_mode -nb $nb -Iter 50 -stencil 2d5p
   #echo likwid-perfctr -C S0:0-$i -g FLOPS_DP $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p
   #export OMP_NUM_THREADS=$i
   #likwid-pin -c S0:0-$(echo "$i-1" | bc) $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-8000  -dot_mode -nb $nb -Iter 50 -stencil 2d5p
 #done
#done 

 for nz in 1000 2000 4000 8000 16000 32000 64000 128000; do
   # OMP_NUM_THREADS=$i $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-$(echo "4000 / $nb" | bc)  -dot_mode -nb $nb -Iter 50 -stencil 2d5p
   #echo likwid-perfctr -C S0:0-$i -g FLOPS_DP $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p
   export OMP_NUM_THREADS=14
   likwid-pin -c S0:0-13 $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-$nz  -dot_mode -nb 4 -Iter 50 -stencil 2d5p
 done

