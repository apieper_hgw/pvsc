#!/bin/bash -l

#PBS -l nodes=1:ppn=40:likwid,walltime=00:30:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out

echo 'PBS_ID =' $PBS_ID

echo 'PBS_NUM_PPN =' $PBS_NUM_PPN

# cat $PBS_NODEFILE

np=`wc -l < $PBS_NODEFILE` 
nth=$[$PBS_NUM_PPN/4]
echo "prozesses form nodefile:" $np

np=$[$np*2/$PBS_NUM_PPN]

echo "place prozesses:" $np

cd $PBS_O_WORKDIR



TNP=`wc -l < $PBS_NODEFILE`
N=`bc <<< $TNP"/"$PBS_NUM_PPN`

PPN=2
NP=`bc <<< $PPN"*"$N`
CPP=`bc <<< $PBS_NUM_PPN"*0.5/"$PPN`

cd $PBS_O_WORKDIR

EXEPATH=$HOME/pvsc/bin
#module load intel64 fftw3 likwid
module load intel64 likwid


EXEPATH=$HOME/pvsc/bin
EXEPATH_MPI=$HOME/pvsc/bin_mpi

cp $EXEPATH_MPI/ChebDOS.x .
cp $EXEPATH_MPI/ChebFD.x  .
cp $EXEPATH_MPI/ChebARPES.x  .

#likwid-setFrequencies -l
#likwid-setFrequencies -p
#likwid-setFrequencies -f 2.2
#likwid-setFrequencies -p


STENCIL='-stencil topi'
EIG_RANGE='-eig_range -5.1 5.1'

#STENCIL='-stencil graphene'
#STENCIL='-stencil weyl'
#STENCIL='-stencil graphene_short_var'

#EIG_RANGE='-eig_range -6 6'
#STENCIL='-stencil 3d7p'
#STENCIL='-stencil 2d5p'
#STENCIL='-stencil 2d4p'
#STENCIL='-stencil 2d5pv'


SEARCH_RANGE='-fd_search_range -0.005 0.005'

#EIG_RANGE='-fd_eig_range -3.5 3.5'

#FD_CONF='-fd_subspace_ddf 8'

#SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-400-400 -pbc'
#SYS_SIZE='-mpi_grid 1-2-1 -sys_size 1-200-400 -pbc'


FD_CONF=' -fd_polydegree_factor 6'
FD_CONF=$(echo $FD_CONF ' -fd_overpopulation 3.')
#FD_CONF=$(echo $FD_CONF '-disable_searchspace_reduction')
#FD_CONF=$(echo $FD_CONF '-enable_subspace_reduction')
#FD_CONF=$(echo $FD_CONF '-disable_cheap_init')

#echo $FD_CONF

nth=10

LIKWID_STARTER='likwid-mpirun -omp intel -mpi intelmpi  -np '$np' -pin S0:0-'$[$nth-1]'_S1:0-'$[$nth-1]
#  -g MEM_DP -m
echo run_likwid: $LIKWID_STARTER 


#RUN_LW='likwid-mpirun -omp intel -mpi intelmpi  -np '$NP' -pin S0:0-'`bc <<< $CPP"-1"`'_S1:0-'`bc <<< $CPP"-1"`'  -g MEM_DP -m'
RUN_IH='mpiexec.hydra -np '$NP' -ppn '$PPN' -genv OMP_NUM_THREADS '$CPP
echo run_likwid: $RUN_LW 
echo run_hydra: $RUN_IH 

DISORDER='-disorder 0.0'

SYS_SIZE='-mpi_grid 2-1-1 -sys_size 256-64-8 -pbc -obc_z'
#SYS_SIZE='-mpi_grid 2-2-2 -sys_size 200-200-200 -obc'
#SYS_SIZE='-mpi_grid 2-4-4 -sys_size 200-100-100 -obc'
#likwid-mpirun -omp intel -mpi intelmpi  -np $NP -pin S0:0-$(bc <<< $CPP"-1")_S1:0-$(bc <<< $CPP"-1")

#$LIKWID_STARTER  ./ChebDOS.x $SYS_SIZE $STENCIL -V 0.0 $DISORDER $EIG_RANGE -dos dos.dat -M $[2**14]

K_RANGE='-Nk 128 -k_start -0.5 0. -0. -k_end 0.5 0. 0.'
$LIKWID_STARTER  ./ChebARPES.x $SYS_SIZE $STENCIL -V 0.0 $DISORDER $EIG_RANGE -dos dos.dat -arpes arpes.dat $K_RANGE -M $[2**10]

#$LIKWID_STARTER  ./ChebFD.x $SYS_SIZE $STENCIL  -V 0.0 -disorder 1.2 -dos dos_df.dat $EIG_RANGE $SEARCH_RANGE $FD_CONF -trl


SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-200-200 -pbc'
SEARCH_RANGE='-fd_search_range -0.02 0.02'
##likwid-pin -c S0:0-$(echo "$nth-1" | bc)             $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF
#likwid-perfctr -C S0:0-`bc <<< $CPP"-1"` -g MEM_DP -m $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF

OMP_NUM_THREADS=2

  #4000-$(echo "4000/$nb" | bc)
  #likwid-perfctr -C S0:0-9 -g FLOPS_DP $(EXEPATH)/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p


