#!/bin/bash -l

#PBS -l nodes=32:ppn=40:likwid,walltime=01:00:00
##PBS -l nodes=8:ppn=40:likwid,walltime=01:00:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out

echo 'PBS_NUM_PPN =' $PBS_NUM_PPN

# cat $PBS_NODEFILE

np=`wc -l < $PBS_NODEFILE` 
nth=$[$PBS_NUM_PPN/4]
echo "prozesses form nodefile:" $np

np=$[$np*2/$PBS_NUM_PPN]

echo "place prozesses:" $np

cd $PBS_O_WORKDIR



TNP=`wc -l < $PBS_NODEFILE`
N=`bc <<< $TNP"/"$PBS_NUM_PPN`

PPN=2
NP=`bc <<< $PPN"*"$N`
CPP=`bc <<< $PBS_NUM_PPN"*0.5/"$PPN`

cd $PBS_O_WORKDIR

EXEPATH=$HOME/pvsc/bin
module load intel64 likwid


EXEPATH=$HOME/pvsc/bin
EXEPATH_MPI=$HOME/pvsc/bin_mpi

#likwid-setFrequencies -l
#likwid-setFrequencies -p
likwid-setFrequencies -f 2.2
#likwid-setFrequencies -p



#STENCIL='-stencil graphene_short'
#STENCIL=graphene
STENCIL='-stencil topi'
#STENCIL='-stencil wsm'
#STENCIL=3d7p
#STENCIL=2d5p
#STENCIL=2d4p
#STENCIL=2d5pv


SEARCH_RANGE='-fd_search_range -0.02 0.02'
#SEARCH_RANGE='-fd_search_range -0.05 0.05'

EIG_RANGE='-fd_eig_range -5.5 5.5'

#FD_CONF='-fd_subspace_ddf 8'

#SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-400-400 -pbc'
#SYS_SIZE='-mpi_grid 1-2-1 -sys_size 1-200-400 -pbc'


FD_CONF=' -fd_polydegree_factor 6'
FD_CONF=$(echo $FD_CONF ' -fd_overpopulation 3.')
#FD_CONF=$(echo $FD_CONF '-disable_searchspace_reduction')
#FD_CONF=$(echo $FD_CONF '-enable_subspace_reduction')
#FD_CONF=$(echo $FD_CONF '-disable_cheap_init')

echo $FD_CONF

nth=10

LIKWID_STARTER='likwid-mpirun -omp intel -mpi intelmpi  -np '$np' -pin S0:0-'$[$nth-1]'_S1:0-'$[$nth-1]
#  -g MEM_DP -m
echo run_likwid: $LIKWID_STARTER 


#RUN_LW='likwid-mpirun -omp intel -mpi intelmpi  -np '$NP' -pin S0:0-'`bc <<< $CPP"-1"`'_S1:0-'`bc <<< $CPP"-1"`'  -g MEM_DP -m'
RUN_IH='mpiexec.hydra -np '$NP' -ppn '$PPN' -genv OMP_NUM_THREADS '$CPP
echo run_likwid: $RUN_LW 
echo run_hydra: $RUN_IH 

DISORDER='-disorder 1.2'

#SYS_SIZE='-mpi_grid 4-4-1 -sys_size 40-40-20 -pbc -obc_z'
SYS_SIZE='-mpi_grid 8-8-1 -sys_size 120-120-6 -pbc -obc_z'
#SYS_SIZE='-mpi_grid 1-2-1 -sys_size 1-200-800 -pbc'
#SYS_SIZE='-mpi_grid 1-2-1 -sys_size 1-200-400 -pbc'

#likwid-mpirun -omp intel -mpi intelmpi  -np $NP -pin S0:0-$(bc <<< $CPP"-1")_S1:0-$(bc <<< $CPP"-1")
$LIKWID_STARTER  $EXEPATH_MPI/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE $DISORDER -dos dos.dat -V 0.0 $EIG_RANGE $FD_CONF 
#-trl

SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-200-200 -pbc'
SEARCH_RANGE='-fd_search_range -0.02 0.02'
#likwid-pin -c S0:0-$(echo "$nth-1" | bc)             $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -dos dos.dat -V 0.0 $EIG_RANGE $FD_CONF -trl
#likwid-perfctr -C S0:0-`bc <<< $CPP"-1"` -g MEM_DP -m $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -dos dos.dat -V 0.0 $EIG_RANGE $FD_CONF -trl

OMP_NUM_THREADS=2

  #4000-$(echo "4000/$nb" | bc)
  #likwid-perfctr -C S0:0-9 -g FLOPS_DP $(EXEPATH)/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p


