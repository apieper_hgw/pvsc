#!/bin/bash -l

#PBS -l nodes=1:ppn=40:likwid,walltime=00:25:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out

echo 'PBS_NUM_PPN =' $PBS_NUM_PPN

# cat $PBS_NODEFILE


TNP=`wc -l < $PBS_NODEFILE`
N=`bc <<< $TNP"/"$PBS_NUM_PPN`

PPN=2
NP=`bc <<< $PPN"*"$N`
CPP=`bc <<< $PBS_NUM_PPN"*0.5/"$PPN`

cd $PBS_O_WORKDIR

EXEPATH=$HOME/pvsc/bin
module load intel64 fftw3 likwid


EXEPATH=$HOME/pvsc/bin
EXEPATH_MPI=$HOME/pvsc/bin_mpi

likwid-setFrequencies -l
#likwid-setFrequencies -p
likwid-setFrequencies -f 2.2
likwid-setFrequencies -p



STENCIL='-stencil graphene_short'
#STENCIL=graphene
#STENCIL=topi
#STENCIL=3d7p
#STENCIL=2d5p
#STENCIL=2d4p
#STENCIL=2d5pv
#STENCIL='-stencil 3d7p_n'

#EIG_RANGE='-eig_range -6.1 6.1'
#EIG_RANGE='-fd_eig_range -3.5 3.5'

#SEARCH_RANGE='-fd_search_range -4.84745 -4.8'
#SEARCH_RANGE='-fd_search_range -4.81 -4.0'
#SEARCH_RANGE='-fd_search_range -0.1 0.1'
#SEARCH_RANGE='-fd_search_range -0.1 0.1'


#FD_CONF='-fd_subspace_ddf 8'

#SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-400-400 -pbc'
#SYS_SIZE='-mpi_grid 1-2-1 -sys_size 1-200-400 -pbc'


FD_CONF=' -fd_polydegree_factor 6'
FD_CONF=$(echo $FD_CONF ' -fd_overpopulation 3.')
#FD_CONF=$(echo $FD_CONF '-disable_searchspace_reduction')
#FD_CONF=$(echo $FD_CONF '-enable_subspace_reduction')
#FD_CONF=$(echo $FD_CONF '-disable_cheap_init')

echo $FD_CONF

nth=10


RUN_LW='likwid-mpirun -omp intel -mpi intelmpi  -np '$NP' -pin S0:0-'`bc <<< $CPP"-1"`'_S1:0-'`bc <<< $CPP"-1"`'  -g MEM_DP -m'
RUN_IH='mpiexec.hydra -np '$NP' -ppn '$PPN' -genv OMP_NUM_THREADS '$CPP
echo run_likwid: $RUN_LW 
echo run_hydra: $RUN_IH 


#SYS_SIZE='-mpi_grid 2-1-1 -sys_size 30-60-60 -obc'
#likwid-mpirun -omp intel -mpi intelmpi  -np $NP -pin S0:0-$(bc <<< $CPP"-1")_S1:0-$(bc <<< $CPP"-1")
#$RUN_LW  $EXEPATH_MPI/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF

SYS_SIZE='-mpi_grid 1-1-1 -sys_size 1-200-200 -pbc'
SEARCH_RANGE='-fd_search_range -0.02 0.02'
#SYS_SIZE='-mpi_grid 1-1-1 -sys_size 60-60-60 -obc'
#likwid-pin -c S0:0-$(echo "$nth-1" | bc)             $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF
#likwid-perfctr -C S0:0-`bc <<< $CPP"-1"` -g MEM_DP -m $EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF
#likwid-perfctr -C S0:0-`bc <<< $CPP"-1"` -g MEM_DP -m $EXEPATH/TRLPF.x $SYS_SIZE $STENCIL $EIG_RANGE -dos dos.dat $SEARCH_RANGE
#$EXEPATH/TRLPF.x $SYS_SIZE $STENCIL $EIG_RANGE -dos dos.dat $SEARCH_RANGE
$EXEPATH/ChebFD.x $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF
  #4000-$(echo "4000/$nb" | bc)
  #likwid-perfctr -C S0:0-9 -g FLOPS_DP $(EXEPATH)/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p


