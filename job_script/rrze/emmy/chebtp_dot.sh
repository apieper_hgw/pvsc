#!/bin/bash -l

#PBS -l nodes=8:ppn=40:likwid,walltime=00:45:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out

np=`wc -l < $PBS_NODEFILE` 
nth=$[$PBS_NUM_PPN/4]
echo "prozesses form nodefile:" $np

np=$[$np*2/$PBS_NUM_PPN]

echo "place prozesses:" $np

cd $PBS_O_WORKDIR

module load intel64 fftw3 likwid

EXEPATH=$HOME/pvsc/bin_mpi
EXEPATH_NONMPI=$HOME/pvsc/bin

likwid-setFrequencies -l
#likwid-setFrequencies -p
likwid-setFrequencies -f 2.2
likwid-setFrequencies -p




#EIG_RANGE='-fd_eig_range -3.5 3.5'

#FD_CONF='-fd_subspace_ddf 8'

STENCIL='-stencil graphene_short'
STENCIL='-stencil graphene_short_var'
SYS_SIZE='-mpi_grid 1-8-2 -sys_size 1-1200-1408 -pbc'


FD_CONF=' -fd_polydegree_factor 6'
FD_CONF=$(echo $FD_CONF ' -fd_overpopulation 3.')
#FD_CONF=$(echo $FD_CONF '-disable_searchspace_reduction')
#FD_CONF=$(echo $FD_CONF '-enable_subspace_reduction')
#FD_CONF=$(echo $FD_CONF '-disable_cheap_init')

echo $FD_CONF

DT="-n_dt 4 -dt 2000." 
V_DOT="-V 0.13 -dVz -0.002"

DATA_VEC=chebtp_t8000_V0.13_dV-0.002

LIKWID_STARTER='likwid-mpirun -omp intel -mpi intelmpi  -np '$np' -pin S0:0-'$[$nth-1]'_S1:0-'$[$nth-1]
#likwid-mpirun -omp intel -mpi intelmpi  -np 2 -pin S0:0-9_S1:0-9  -g MEM_DP  $EXEPATH/ChebFD.x  -mpi_grid 1-2-1 $SYS_SIZE $STENCIL $SEARCH_RANGE -V 0.0 $EIG_RANGE $FD_CONF

#mpiexec.hydra -np 16 -ppn 2 -genv OMP_NUM_THREADS 10
$LIKWID_STARTER $EXEPATH/ChebTP_dots.x $SYS_SIZE $STENCIL -nb 1 -damping 0.3 -M 4096 -disorder 0. $DT -n_dt 1 -vec_out $DATA_VEC.vec $V_DOT
 

$EXEPATH_NONMPI/vector_plot.x -v $DATA_VEC.vec -o $DATA_VEC.dat -cS -cO -cV -cC 8


