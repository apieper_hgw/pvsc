#!/bin/bash -l

#PBS -l nodes=1:ppn=40:likwid,walltime=00:10:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out


cd $PBS_O_WORKDIR

module load intel64 fftw3 likwid


#likwid-setFrequencies -l
#likwid-setFrequencies -p
#likwid-setFrequencies -f 2.2
#likwid-setFrequencies -p




#EIG_RANGE='-fd_eig_range -3.5 3.5'

#FD_CONF='-fd_subspace_ddf 8'

EXEPATH_MPI=$HOME/pvsc/bin_mpi
EXEPATH=$HOME/pvsc/bin

MPIRUN='mpiexec.hydra -np 4 -ppn 4 -genv OMP_NUM_THREADS 5'

STENCIL=' -stencil graphene_short'
SYS_SIZE='    -mpi_grid 1-1-1 -sys_size 1-40-40 -obc'
SYS_SIZE_MPI='-mpi_grid 1-2-2 -sys_size 1-20-20 -obc'


$MPIRUN $EXEPATH_MPI/comm_test.x  $SYS_SIZE_MPI -nb 1 -out_x0 x0_mpi.vec -out_x1 x1_mpi.vec $STENCIL
            $EXEPATH/comm_test.x  $SYS_SIZE     -nb 1 -out_x0 x0.vec     -out_x1 x1.vec     $STENCIL

$EXEPATH/vector_plot.x -v x0.vec -d x0_mpi.vec -o x0.dat
$EXEPATH/vector_plot.x -v x1.vec -d x1_mpi.vec -o x1.dat

SYS_SIZE='    -mpi_grid 1-1-1 -sys_size 1-400-400 -pbc'
SYS_SIZE_MPI='-mpi_grid 1-2-2 -sys_size 1-200-200 -pbc'

            $EXEPATH/rr_test.x  $SYS_SIZE     $STENCIL -nb 16 -export tmp.vec
$MPIRUN $EXEPATH_MPI/rr_test.x  $SYS_SIZE_MPI $STENCIL -nb 16 -import tmp.vec

$MPIRUN $EXEPATH_MPI/ChebDOS.x  $SYS_SIZE_MPI $STENCIL -M 16384 -dos dos_mpi.dat -import tmp.vec
            $EXEPATH/ChebDOS.x  $SYS_SIZE     $STENCIL -M 16384 -dos dos_sig.dat -import tmp.vec

$MPIRUN $EXEPATH_MPI/ChebDOS.x  $SYS_SIZE_MPI $STENCIL -M 16384 -dos dos_mpi_f.dat -R 16
            $EXEPATH/ChebDOS.x  $SYS_SIZE     $STENCIL -M 16384 -dos dos_sig_f.dat -R 16
