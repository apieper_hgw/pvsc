#!/bin/bash -l

#PBS -l nodes=1:ppn=40:likwid,walltime=00:30:00
#PBS -e log.err

# #PBS -o pvsc_benchmark.out


cd $PBS_O_WORKDIR

EXEPATH=$HOME/pvsc/bin
module load intel64 fftw3 likwid

EXEPATH=$HOME/pvsc/bin


likwid-setFrequencies -l
likwid-setFrequencies -f 2.2
likwid-setFrequencies -p


#OBS=-dot_mode
#OBS=-z_mode
#OBS=$(echo $OBS ' -z_mode')



#STENCIL=graphene
#STENCIL=graphene_short
#STENCIL=2d5p
#STENCIL=2d4p
#STENCIL=2d5pv

#STENCIL=topi
#STENCIL=3d7p
STENCIL=3d7p_r
#STENCIL=3d7p_n



for nth in `seq 1  10` 20; do
#for nth in 10; do
# for nb in 1 2 4 8 16 32 64 128 256; do
 for nb in  1; do
 
  LIKWID_STARTER='likwid-perfctr -C S0:0-'$(echo "$nth-1" | bc)' -g MEM_DP -m'
  #LIKWID_STARTER='likwid-perfctr -C S0:0-'$(echo "$nth-1" | bc)' -g CACHES -m'
  
  #SYS_SIZE='-sys_size 1-512-512'
  #SYS_SIZE='-sys_size 1-512-'$(echo "512/$nb" | bc)

  SYS_SIZE='-sys_size 512-512-'$(echo "512/$nb" | bc)
  #SYS_SIZE='-sys_size 512-'$(echo "512/$nb" | bc)'-512'
  #SYS_SIZE='-sys_size '$(echo "512/$nb" | bc)'-512-512'
  #SYS_SIZE='-sys_size 256-256-'$(echo "256/$nb" | bc)
  #SYS_SIZE='-sys_size 512-512-128'
  #SYS_SIZE='-sys_size 60-60-60'
  
  #likwid-pin -c S0:0-$(echo "$nth-1" | bc)  $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 512-512-$(echo "512/$nb" | bc)  -nb $nb -Iter 20 -stencil $STENCIL $OBS
  #likwid-perfctr -C S0:0-$(echo "$nth-1" | bc) -g MEM  $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 -sys_size 1-4000-$(echo "8000/$nb" | bc)  -nb $nb -Iter 20 -stencil $STENCIL $OBS
 #$LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil $STENCIL $OBS -rand_simd none
  #likwid-perfctr -C S0:0-9 -g FLOPS_DP $(EXEPATH)/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p

  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p   $OBS -rand_simd none
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd none
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd lgc32
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd xors32
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd xors128
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd xors64s
  $LIKWID_STARTER $EXEPATH/test_benchmark.x -mpi_grid 1-1-1 $SYS_SIZE -nb $nb -Iter 50 -stencil 3d7p_r $OBS -rand_simd xors128p

 done 
done

OBS=' -dot_mode'
OBS+=' -z_mode'
echo $OBS
