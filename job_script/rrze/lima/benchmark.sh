#!/bin/bash -l

#PBS -l nodes=1:ppn=24:likwid,walltime=00:10:00
#PBS -joe
#PBS -o pvsc_benchmark.out


cd $PBS_O_WORKDIR


module load intel64 fftw3 likwid


EXEPATH=~/pvsc/bin

for i in `seq 0 1 9`;
 do
  likwid-perfctr -C S0:0-9 -g FLOPS_DP $(EXEPATH)/test_benchmark.x -mpi_grid 1-1-1 -sys_size 2000-2000  -nb 1 -Iter 10 -stencil 2d5p
 done 


