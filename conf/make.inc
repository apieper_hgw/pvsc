CC  = icc
MPICC  = mpicc


LIBS  += -mkl=parallel
IPATH += -I$(MKLROOT)/include
LIBS  += -lm

RM       = rm -rf
MKDIR_P  = mkdir -p

NONOMP=0
VERBOSE=0
DEBUG=0
LIKWID=1
MMIC=0
FFTW=0


# compiling flags here
CFLAGS += -std=c99
CFLAGS += -Wall
#CFLAGS += -Wcheck
#CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Ofast -xHost -fno-alias
#CFLAGS += -fno-alias
#CFLAGS += -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE


CFLAGS += -D PVSC_HAVE_LONGIDX_GLOBAL
CFLAGS += -D PVSC_HAVE_ERROR_CHECK
CFLAGS += -D PVSC_VERBOSE=3
#CFLAGS += -D PVSC_CACHE_SIZE_CPU=294912
#CFLAGS += -D PVSC_CACHE_SIZE_CORE=26214400

ifeq ($(VERBOSE), 1)
	CFLAGS+= -openmp_report
	CFLAGS+= -qopt-report
	CFLAGS+= -qopt-report-phase=vec
endif

ifneq ($(NONOMP), 1)
	#CFLAGS += -openmp
	CFLAGS += -qopenmp
endif

ifeq ($(MMIC), 1)
	CFLAGS += -mmic
else
	CFLAGS += -xHost
endif

ifeq ($(DEBUG), 1)
	CFLAGS+= -fp-model strict
endif

ifeq ($(DEBUG), 1)
	CFLAGS+= -fp-model strict
	CFLAGS+= -g -debug -O0
endif


ifeq ($(LIKWID), 1)
	IPATH+= -I$(LIKWID_INCDIR) 
	CFLAGS+= -D PVSC_HAVE_LIKWID
	LIBS+= $(LIKWID_LIB) -llikwid
endif

ifeq ($(FFTW), 1)
	CFLAGS+= -D PVSC_HAVE_FFTW
	LIBS  += $(FFTW_LIB)
	IPATH += $(FFTW_INC) 
endif
