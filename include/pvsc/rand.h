#ifndef PVSC_RAND_H
#define PVSC_RAND_H
#include <stdint.h>


#define PVSC_LGC32SIMDW 32
#define PVSC_LGC64SIMDW 16

#define PVSC_XORS32SIMDW 32
#define PVSC_XORS128SIMDW 16
#define PVSC_XORS64STARSIMDW 16
#define PVSC_XORS128PLUSSIMDW 16


#define PVSC_RAND_IDUM32 64
// IDUM32 = max( LGS32, 2*LGS64, XORS32, 4*XORS128, 2*XORS64S, 4*XORS128P )

typedef void (*pvsc_drand_simd)( double *  a, int n, void * s, double b );

#ifdef __cplusplus
extern "C" {
#endif
uint64_t * pvsc_rand_seed128( uint64_t s0, uint64_t s1 );
uint64_t * pvsc_rand_seed64(  uint64_t s );
uint32_t * pvsc_rand_seed32(  uint32_t s );

uint64_t pvsc_rand_l_xorshift128plus(void);
double   pvsc_rand_d_xorshift128plus(void);
uint64_t pvsc_rand_ls_xorshift128plus( uint64_t * s);
double   pvsc_rand_ds_xorshift128plus( uint64_t * s);
void pvsc_rand_ds_xorshift128plus_simd(double * a, int n, void * s, double b);
void pvsc_rand_ds_xorshift128_simd(double *  a, int n, void * s_, double b);
void pvsc_rand_ds_xorshift64star_simd(double * a, int n, void * s, double b);
void pvsc_rand_ds_xorshift32_simd(double * a, int n, void * s, double b);

double pvsc_rand_d_lcg32(void);
double pvsc_rand_d_lcg64(void);
double pvsc_rand_ds_lcg32(uint32_t * s);
double pvsc_rand_ds_lcg64(uint64_t * s);
void pvsc_rand_ds_lgc32_simd(double *  a, int n, void * s, double b);
void pvsc_rand_ds_lgc64_simd(double *  a, int n, void * s, double b);

#ifdef __cplusplus
}
#endif

#endif
