#ifndef PVSC_MEM_MANAG_H
#define PVSC_MEM_MANAG_H
//#include <sys/time.h>


#define PVSC_FREE(p) p=pvsc_free(p,false,NULL);

#ifdef __cplusplus
extern "C" {
#endif

void * pvsc_malloc( void ** ptr, size_t e_size, int n_size, bool aligned, char * comment,unsigned int tag  );
void * pvsc_free( void * ptr, bool lock, char * comment );
int pvsc_mem_manag_clear( );

int pvsc_aligned_check(void * ptr, char * info);

#ifdef __cplusplus
}
#endif


//extern const pvsc_men_info_t PVSC_MEM_INFO_INITIALIZER;

#endif
