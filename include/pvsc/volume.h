#ifndef PVSC_VOLUME_H
#define PVSC_VOLUME_H

#include "pvsc/types.h"

#ifdef __cplusplus
extern "C" {
#endif

int pvsc_vecidx2xyz( pvsc_lidx_t * xyz, pvsc_lidx_t vecidx, pvsc_volume v );
pvsc_volume pvsc_get_sub_volume(pvsc_lidx_t * p,  pvsc_lidx_t * n,  pvsc_volume v );
pvsc_volume pvsc_init_volume( pvsc_lidx_t * n);
int pvsc_global_vecidx2xyz( pvsc_lidx_t * xyz, pvsc_lidx_t vecidx, pvsc_system_geo * sys );
int print_sub_volume( char * fname, pvsc_volume v  );

#ifdef __cplusplus
}
#endif

#endif
