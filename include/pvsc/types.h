#ifndef PVSC_TYPES_H
#define PVSC_TYPES_H
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include "pvsc/rand.h"
#ifdef PVSC_HAVE_MPI
#include <mpi.h>
#else
typedef int64_t MPI_Comm;
#define MPI_COMM_WORLD 0
#endif


#ifdef PVSC_HAVE_LONGIDX_GLOBAL

/**
 * @brief Type for global indices.
 */
typedef int64_t pvsc_gidx_t; 
/**
 * @brief MPI data type for matrix row/column indices
 */
#ifdef PVSC_HAVE_MPI
#define pvsc_mpi_dt_gidx MPI_INT64_T
#endif

/**
 * @brief Macro to print matrix/vector row/column indices depending on index size
 */
#define PRGIDX PRId64
#define PVSC_GIDX_MAX INT64_MAX

#else

/**
 * @brief Type for global indices.
 */
typedef int32_t pvsc_gidx_t; 
/**
 * @brief MPI data type for matrix row/column indices
 */
#ifdef PVSC_HAVE_MPI
#define pvsc_mpi_dt_gidx MPI_INT32_T
#endif
/**
 * @brief Macro to print matrix/vector row/column indices depending on index size
 */
#define PRGIDX PRId32
#define PVSC_GIDX_MAX INT32_MAX

#endif



#ifdef PVSC_HAVE_LONGIDX_LOCAL

/**
 * @brief Type for local indices.
 */
typedef int64_t pvsc_lidx_t; 
/**
 * @brief MPI data type for matrix row/column indices
 */
#ifdef PVSC_HAVE_MPI
#define pvsc_mpi_dt_lidx MPI_INT64_T
#endif
/**
 * @brief Macro to print matrix/vector row/column indices depending on index size
 */
#define PRLIDX PRId64

#ifdef PVSC_HAVE_MKL
/**
 * @brief Type for indices used in BLAS calls
 */
typedef long long int pvsc_blas_idx_t;
#define PRBLASIDX PRId64
#else
typedef int pvsc_blas_idx_t;
#define PRBLASIDX PRId32
#endif

#define PRLIDX PRId64
#define PVSC_LIDX_MAX INT64_MAX

#else

typedef int32_t pvsc_lidx_t;
#ifdef PVSC_HAVE_MPI
#define pvsc_mpi_dt_lidx MPI_INT32_T
#endif
typedef int pvsc_blas_idx_t;
#define PRBLASIDX PRId32

#define PRLIDX PRId32
#define PVSC_LIDX_MAX INT32_MAX

#endif


#ifndef PVSC_OPT_BLOCK_SIZE
#define PVSC_OPT_BLOCK_SIZE 4
#endif

#ifndef PVSC_CACHE_SIZE_CORE
#define PVSC_CACHE_SIZE_CORE ((256+32)*1024)
#endif


#ifndef PVSC_CACHE_SIZE_CPU
#define PVSC_CACHE_SIZE_CPU (25*1024*1024)
#endif

typedef struct pvsc_volume pvsc_volume;
typedef struct pvsc_border_comm pvsc_border_comm;
typedef struct pvsc_system_geo pvsc_system_geo;
typedef struct pvsc_vector pvsc_vector;
typedef struct pvsc_dense_redundant_matrix pvsc_dense_redundant_matrix;
typedef struct pvsc_vec_data_trans_handle pvsc_vec_data_trans_handle;
typedef struct pvsc_matrix pvsc_matrix;
typedef struct pvsc_spmvm_handel_t pvsc_spmvm_handel_t;
typedef struct pvsc_system_hamiltonian pvsc_system_hamiltonian;
typedef struct pvsc_wp_handel_t pvsc_wp_handel_t;

typedef int (*pvsc_spmvm_func_ptr)( pvsc_spmvm_handel_t * p );
typedef int (*pvsc_wp_func_ptr   )( pvsc_wp_handel_t    * p );

extern const pvsc_matrix PVSC_MATRIX_INITIALIZER;
extern const pvsc_vector PVSC_VECTOR_INITIALIZER;
extern const pvsc_dense_redundant_matrix PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER;
extern const pvsc_spmvm_handel_t PVSC_SPMVM_HANDEL_INITIALIZER;
extern const pvsc_wp_handel_t    PVSC_WP_HANDEL_INITIALIZER;
extern const pvsc_vec_data_trans_handle PVSC_VEC_DATA_TRANS_HANDLE_INITIALIZER;

struct pvsc_volume
{
    pvsc_lidx_t offset;
    pvsc_lidx_t n[4];
    pvsc_lidx_t s[4];
};

struct pvsc_border_comm
{
    pvsc_system_geo *sys;
    bool enable_send;
    bool enable_recv;
    int rank_send;
    int rank_recv;
    int tag;
    pvsc_volume v_local;
    pvsc_volume v_remote;
    
};

struct pvsc_system_geo
{
    pvsc_volume v_mem;  //obsolate?
    pvsc_volume v;      //obsolate?
    pvsc_lidx_t n_x;
    pvsc_lidx_t n_y;
    pvsc_lidx_t n_z;
    pvsc_lidx_t b_x;
    pvsc_lidx_t b_y;
    pvsc_lidx_t b_z;
    pvsc_lidx_t s_x; // s = 2*b + n
    pvsc_lidx_t s_y;
    pvsc_lidx_t s_z;
    pvsc_lidx_t n_o; //  Bemerkunk n_o - Dimension obtional nach ganz aussen. koennte Vektorisierung verbessern. Kommunikation wird allerdings aufwendiger
    
    bool pbc_x;
    bool pbc_y;
    bool pbc_z;
    
    MPI_Comm mpi_comm;
    int mpi_size;
    int mpi_size_x;
    int mpi_size_y;
    int mpi_size_z;
    int mpi_rank;
    int mpi_rank_x;
    int mpi_rank_y;
    int mpi_rank_z;
    
    pvsc_border_comm b_xd;
    pvsc_border_comm b_xu;
    pvsc_border_comm b_yd;
    pvsc_border_comm b_yu;
    pvsc_border_comm b_zd;
    pvsc_border_comm b_zu;
    
    pvsc_border_comm b_xd_depend;
    pvsc_border_comm b_xu_depend;
    pvsc_border_comm b_yd_depend;
    pvsc_border_comm b_yu_depend;
    pvsc_border_comm b_zd_depend;
    pvsc_border_comm b_zu_depend;    
    
    bool finalised;
};

struct pvsc_vector 
{
    pvsc_system_geo             *sys;
    pvsc_lidx_t                  nb;
    double                      *val;
    int                          n_data_trans_independent;
    pvsc_vec_data_trans_handle  *data_trans_independent_handle;
    int                          levels_data_trans_dependent;
    int                         *n_data_trans_dependent;
    pvsc_vec_data_trans_handle **data_trans_dependent_handle;
};

struct pvsc_dense_redundant_matrix{
	// COL_MAJOR
	double      * val; 
	pvsc_lidx_t n_row;
	pvsc_lidx_t n_col;
	pvsc_lidx_t ld_col;
	bool        redundant;
	pvsc_dense_redundant_matrix * pernet_matrix;
};

struct pvsc_vec_data_trans_handle
{
	pvsc_vector      *vec;
	bool              inter_porc;
	pvsc_border_comm *bcomm;
	pvsc_lidx_t       n;
	double           *buff_send_local;
	double           *buff_recv_remote;
#ifdef PVSC_HAVE_MPI
	MPI_Request       s_request;
	MPI_Status        s_status;
	MPI_Request       r_request;
	MPI_Status        r_status;
#endif
};

struct pvsc_spmvm_handel_t
{
	pvsc_vector * y;
	pvsc_vector * x;
	pvsc_vector * z;
	

	bool individual_scale_shift;  // TODO
	double * scale_h;
	double * shift_h;
	double * scale_z;
	//double * zscale_y;
	//double * zscale_z;
	bool        dependent_data_trans;  //TODO
	
	pvsc_matrix * mat;     //new!
	//double * mat_scalars;  //obsolate!
	//double * mat_arrays;   //obsolate!
	//pvsc_lidx_t ld_arrays; //obsolate!
	
	bool dot_allreduce;
	double * dot_yy;
	double * dot_xy;
	double * dot_xx;
	
	pvsc_lidx_t nb_block;
	
	bool kacz;
	
	int      call_counter;
	double   call_time;
	double   call_time_compute;
	//double   call_time_datatrans;

};

struct pvsc_wp_handel_t
{
	pvsc_lidx_t n;
	pvsc_lidx_t wp_deep;
	
	pvsc_vector * v0;
	pvsc_vector * v1;
	pvsc_vector * w;

	//bool individual_scale_shift;  // TODO
	double * scale_h;
	double * shift_h;
	double * scale_v;
	double * coeff_w;
	//bool        dependent_data_trans;  //TODO
	
	pvsc_matrix * mat;
	
	bool dot_allreduce;
	double * dot_vv;
	double * dot_vnv;
	
	
	int      call_counter;
	double   call_time;
	double   call_time_compute;
	//double   call_time_datatrans;

};

struct pvsc_system_hamiltonian
{
	pvsc_lidx_t      n_o;
	int              m;
	pvsc_lidx_t     *dx;
	pvsc_lidx_t     *dy;
	pvsc_lidx_t     *dz;
	double           lattice_distance_x;
	double           lattice_distance_y;
	double           lattice_distance_z;
	//new
	pvsc_lidx_t      dim;
	pvsc_spmvm_func_ptr spmvm_kernel;
	pvsc_wp_func_ptr    wp_kernel;
	bool             dependent_data_trans;
	bool             cmp;
	pvsc_lidx_t      n_scalars;
	double          *n_scalars_default;
	pvsc_lidx_t      n_arrays;
	
	int              flop_per_lup;
};

struct pvsc_matrix
{
	pvsc_system_hamiltonian * ham;
	pvsc_system_geo         * sys;
	double                  * ham_scalars;  // todo rename -> ham_vcoeff
	double                  * ham_array;    // todo rename -> ham_ccoeff
	pvsc_drand_simd         rand_simd_func_ptr;
    // todo: funktion nach der ham_array gefuellt wird
};



#ifdef __cplusplus
extern "C" {
#endif

int pvsc_init_matrix_from_args (pvsc_matrix * mat,  MPI_Comm comm, int argc, char * argv[] );
int pvsc_init_system_geo_mpicomm (pvsc_system_geo *sys,  MPI_Comm comm, int mpi_size_x, int mpi_size_y, int mpi_size_z);
int pvsc_finalise_system_geo( pvsc_system_geo *sys );
int pvsc_set_ghostlayer_for_system_hamiltonian( pvsc_system_geo *sys, pvsc_system_hamiltonian * ham, int max_wavefront_iter );
int pvsc_create_matrix( pvsc_matrix * mat, pvsc_system_geo * sys, pvsc_system_hamiltonian * ham );
int pvsc_destroy_matrix( pvsc_matrix * mat );
int pvsc_print_performance_statistic( pvsc_spmvm_handel_t * p );

int pvsc_create_dense_redundant_matrix( pvsc_dense_redundant_matrix * mat, pvsc_lidx_t rows, pvsc_lidx_t cols );
int pvsc_destroy_dense_redundant_matrix( pvsc_dense_redundant_matrix * mat );


#ifdef __cplusplus
}
#endif

#endif
