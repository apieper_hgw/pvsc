#ifndef PVSC_WAVEFUNCTION_H
#define PVSC_WAVEFUNCTION_H




typedef struct 
{
	double kx;
	double ky;
	double kz;
	double deltax;
	double deltay;
	double deltaz;
	bool   enable_dx;
	bool   enable_dy;
	bool   enable_dz;
	double rx;
	double ry;
	double rz;
	double * phase;
	double * ampli;
	
}pvsc_wavepacket;

extern const pvsc_wavepacket PVSC_WAVEPACKET_INITIALIZER;

#ifdef __cplusplus
extern "C" {
#endif

double pvsc_gen_wavepacket_func(pvsc_gidx_t * n, pvsc_lidx_t k, void * p, void * seed );

#ifdef __cplusplus
}
#endif

#endif
