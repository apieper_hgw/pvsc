#ifndef PVSC_KPM_H
#define PVSC_KPM_H

typedef struct pvsc_cheb_fd_handle_t pvsc_cheb_fd_handle_t;

struct pvsc_cheb_fd_handle_t{
	
	pvsc_matrix * mat;
	double        lambda_min;
	double        lambda_max;
	double        res_eps;
	
	pvsc_vector * vec;
	double      * eig;
	double      * res;
	
	char        * dos_export;
	
	int           iter_max;
	bool          cheap_init;
	pvsc_lidx_t   auto_nb;
	
	bool          auto_rescale;
	pvsc_lidx_t   rescale_ld;
	double        scale;
	double        shift;
	
	bool          auto_subspace;
	double        subspace_ddf;
	pvsc_lidx_t   subspace;
	bool          searchspace_reduction;
	bool          subspace_reduction;
	
	bool          auto_polydegree;
	double        polydegree_factor;
	int           polydegree;
	double        overpopulation;
	
};

extern const pvsc_cheb_fd_handle_t PVSC_CHEB_FD_HANDLE_INITIALIZER;



#ifdef __cplusplus
extern "C" {
#endif

int pvsc_cheb_dos( double * mu, pvsc_matrix * mat, int R, int nb, int M, double scale, double shift, pvsc_vector * y);

int pvsc_cheb_filter_block(  pvsc_matrix * mat ,double scale, double shift, int M, double * coeff,  pvsc_lidx_t ldcoeff, pvsc_vector * y, pvsc_vector * y_in, double * mu);
int pvsc_cheb_filter(  pvsc_matrix * mat , double scale, double shift, int M, double * coeff, pvsc_vector * y, pvsc_vector * y_in, bool * apply,  double * mu);

int pvsc_cheb_fd( pvsc_cheb_fd_handle_t * fdh  );

int pvsc_cheb_fd(     pvsc_cheb_fd_handle_t * fdh  );
int pvsc_cheb_fd_trl( pvsc_cheb_fd_handle_t * fdh  );
#ifdef __cplusplus
}
#endif

#endif
