#ifndef PVSC_BENCHMARK_H
#define PVSC_BENCHMARK_H




#ifdef __cplusplus
extern "C" {
#endif

int pvsc_benchmark_mat( pvsc_matrix * mat , int n_nb, pvsc_lidx_t *nb, int Iter, bool dot_mode, bool z_mode);

#ifdef __cplusplus
}
#endif

#endif
