#ifndef PVSC_DOT_ARRAY_H
#define PVSC_DOT_ARRAY_H




typedef struct 
{
	
	double V;
	//double Rx;
	double Ry;
	double Rz;
	//double rx;
	double ry;
	double rz;
	
}pvsc_dot;


typedef struct 
{
	int n_x;
	int n_y;
	int n_z;
	double Rx;
	double Ry;
	double Rz;
	double Dx;
	double Dy;
	double Dz;
	double V;
	double dVz;
	
	double offset_x;
	double offset_y;
	double offset_z;
	
	int n;
	pvsc_dot * dot;
	
}pvsc_dot_array;

extern const pvsc_dot_array PVSC_DOT_ARRAY_INITIALIZER;

#ifdef __cplusplus
extern "C" {
#endif


int pvsc_get_gradually_changing_dot_array(pvsc_dot_array * da, double dx, double dy, double dz, double dVz);
double pvsc_func_dot_array(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);

#ifdef __cplusplus
}
#endif

#endif
