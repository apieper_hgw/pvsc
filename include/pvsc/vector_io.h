#ifndef PVSC_VECTOR_IO_H
#define PVSC_VECTOR_IO_H

#include "pvsc/types.h"


#ifdef __cplusplus
extern "C" {
#endif

int pvsc_vector_write( char * fname, pvsc_vector * vec  );
int pvsc_vector_read(  char * fname, pvsc_vector * vec  );

#ifdef __cplusplus
}
#endif

#endif
