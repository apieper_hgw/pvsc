#ifndef PVSC_CHEB_TP_H
#define PVSC_CHEB_TP_H

#include "pvsc/types.h"


#ifdef __cplusplus
extern "C" {
#endif

int pvsc_cheb_tp( pvsc_vector * x, double dt, pvsc_matrix * mat , double scale, double shift, double ** mu , int * M_mu);

#ifdef __cplusplus
}
#endif

#endif
