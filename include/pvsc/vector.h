#ifndef PVSC_VECTOR_H
#define PVSC_VECTOR_H

#include "pvsc/types.h"


typedef double (*pvsc_vector_func_ptr)(pvsc_gidx_t *, pvsc_lidx_t, void *, void *);

#ifdef __cplusplus
extern "C" {
#endif

int pvsc_create_vector(pvsc_vector * vec, pvsc_lidx_t nb, pvsc_system_geo * sys);
int pvsc_destroy_vector(pvsc_vector * vec);


int pvsc_vector_copy(pvsc_vector * vec_dest, pvsc_vector * vec_src);
int pvsc_vector_copy_select( pvsc_lidx_t n, pvsc_vector * vec_dest, pvsc_lidx_t * select_dest,   pvsc_vector * vec_src, pvsc_lidx_t * select_src );
int pvsc_vector_copy_gather_block( pvsc_vector * vec_dest, pvsc_vector ** vec_src, pvsc_lidx_t n, pvsc_lidx_t src_nb_idx  );
int pvsc_vector_copy_scatter_block( pvsc_vector ** vec_dest, pvsc_lidx_t n,  pvsc_vector * vec_src, pvsc_lidx_t src_nb_idx  );

int pvsc_vector_normalized(pvsc_vector * vec);
int pvsc_vector_scale(pvsc_vector * vec, double * scale);
int pvsc_vector_dot(double * dot, pvsc_vector * x, pvsc_vector * y);
int pvsc_vector_axpby(double * b, pvsc_vector * y, double * a, pvsc_vector * x );
int pvsc_vector_axpy_block(pvsc_vector * y, double * a, pvsc_vector * x, pvsc_lidx_t block );
int pvsc_vector_axpy_complex( pvsc_vector * y, double * coeff, pvsc_vector * x, int n, bool mat_complex);
int pvsc_vector_xApby(double b, pvsc_vector * y,  pvsc_vector * x, double * a, pvsc_lidx_t lda );
int pvsc_vector_M_eq_axy( double * M, pvsc_lidx_t ldM, double a, pvsc_vector * x,  pvsc_vector * y );

int pvsc_vector_class_gram_schmidt( pvsc_vector * x,  pvsc_vector * y, pvsc_lidx_t n );
int pvsc_vector_projection(         pvsc_vector * x,  pvsc_vector * y, pvsc_lidx_t n );

int pvsc_vector_datatransfer(           pvsc_vector * vec );
int pvsc_vector_datatransfer_dependent( pvsc_vector * vec );

int pvsc_vector_from_func_scale(double scale_v, pvsc_vector * vec, double scale_f, pvsc_vector_func_ptr func, void * parm);
#define  pvsc_vector_from_func( v, f, p) pvsc_vector_from_func_scale( 0., v, 1., f, p)
int pvsc_set_array_from_func_scale( double scale_v, pvsc_matrix * mat, double scale_f, pvsc_vector_func_ptr func, void * parm);
#define  pvsc_set_array_from_func( v, f, p) pvsc_set_array_from_func_scale( 0., v, 1., f, p)


double pvsc_zero_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);
double pvsc_rand_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);

double pvsc_hextest_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);
double pvsc_dectest_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);

double pvsc_gen_wave_vec(  pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);
double pvsc_gen_wave_obc_layer_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed);

int print_vector( char * fname, pvsc_vector * vec  );

#ifdef __cplusplus
}
#endif


#endif
