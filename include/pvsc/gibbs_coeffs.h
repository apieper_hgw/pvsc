#ifndef PVSC_GIBBS_COEFFS_H
#define PVSC_GIBBS_COEFFS_H

#include "pvsc/types.h"

double pvsc_kernel_width(double x, double scale, double shift, int M  );
void pvsc_gibbs_Jackson (int M, double * mu, int L);
void pvsc_gibbs_Fejer (int M, double * mu, int L);
void pvsc_gibbs_Lanczos (int M, double P, double * mu, int L);
void pvsc_gibbs_Lorentz (int M,  double lamb, double * mu, int L);

#endif
