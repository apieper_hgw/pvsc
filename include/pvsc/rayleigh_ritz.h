#ifndef PVSC_RAYLEIGH_RITZ_H
#define PVSC_RAYLEIGH_RITZ_H

typedef struct pvsc_rayleigh_ritz_handle_t pvsc_rayleigh_ritz_handle_t;

extern const pvsc_rayleigh_ritz_handle_t PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER;

struct pvsc_rayleigh_ritz_handle_t{
	
	pvsc_matrix * mat;
	
	pvsc_vector * vec_in;
	pvsc_vector * vec_eig;
	pvsc_vector * vec_res;
	double      * eig;
	double      * res;
	pvsc_dense_redundant_matrix * transform_mat;
};





#ifdef __cplusplus
extern "C" {
#endif

int pvsc_rayleigh_ritz( pvsc_rayleigh_ritz_handle_t * rrh );

#ifdef __cplusplus
}
#endif

#endif
