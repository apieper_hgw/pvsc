#ifndef PVSC_KPM_SPEC_RECONSTRUCT_H
#define PVSC_KPM_SPEC_RECONSTRUCT_H

#include "pvsc/types.h"

#ifdef __cplusplus
extern "C" {
#endif

int pvsc_dot_products_2_cheb_moments( double * mu, int M, int N );

int pvsc_kpm_1D_reconstruction_line_level (int const M, double * const mu, int const N, double level, double * const dos );

int pvsc_kpm_1D_reconstruction_line (int const M, double * const mu, int const N, double * const dos );

int pvsc_kpm_print_density(char * file_name, int M, int N, double * mu , double scale, double shift, double * info, void gibbs (int const , double * , int const ) );
int pvsc_kpm_print_density_level(char * file_name, int M, int N, double * mu , double scale, double shift, double level, double label, double * info, void gibbs (int const , double * , int const ) );

int pvsc_kpm_eval_density_at_engegie(double * dense, double omega, int M, int N, double * mu , double scale, double shift, void gibbs (int const , double * , int const ) );

double pvsc_kpm_dos_integal ( double * mu, int M, double a, double b, void gibbs (int const , double * , int const ) );

int pvsc_cheb_toolbox_window_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha );

int pvsc_cheb_toolbox_print_filter(char * file_name, double * coeff, int ldc, int M, double scale, double shift, int n );
double pvsc_cheb_eval_cheb_poly( double * coeff, int M, double x);

int pvsc_cheb_window_coeff( int M, double *e, double *r, double * c, int n, double scale, double shift);
int pvsc_cheb_delta_coeff( int M, double *e, double * c, int n, double scale, double shift);
int pvsc_cheb_toolbox_delta_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha );

int pvsc_cheb_delta_coeff_arg( double * c,  int M, double phi);

#ifdef __cplusplus
}
#endif


#endif
