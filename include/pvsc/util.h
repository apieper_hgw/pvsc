#ifndef PVSC_UTIL_H
#define PVSC_UTIL_H
#include <sys/time.h>
#include "pvsc/types.h"


#ifdef __cplusplus
extern "C" {
#endif

void * pvsc_get_stdout();
void * pvsc_set_stdout( char * name);
void * pvsc_unset_stdout();

int pvsc_print_pvsc_info();

int pvsc_read_args(char * option , int argc, char * argv[]);
int pvsc_read_mpi_grid( int * mpi_xyz, int argc, char * argv[]);
int pvsc_read_sys_conf( pvsc_system_geo * sys, int argc, char * argv[] );

double pvsc_timing_since( struct timeval tp_start );

void pvsc_printf(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif
