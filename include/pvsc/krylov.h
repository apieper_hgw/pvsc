#ifndef PVSC_KRYLOV_H
#define PVSC_KRYLOV_H

typedef enum{
    PVSC_KRYLOV_DEFAULT = 0,
    PVSC_KRYLOV_MIN_EIG_VEC = 1,
    PVSC_KRYLOV_MAX_EIG_VEC = 2,
    PVSC_KRYLOV_SQUARE_MAT = 4
}pvsc_krylov_options;


#ifdef __cplusplus
extern "C" {
#endif

int pvsc_tr_lanczos_arnoldi( pvsc_vector ** vec_eig, pvsc_matrix * mat , double lambda_min, double lambda_max,
	                 double p_scale, double p_shift, double * p_coeff, int p_m, double p_cut,
	                 int MaxIts, pvsc_lidx_t trlz_m );
int pvsc_tr_lanczos( pvsc_vector ** vec_eig, pvsc_matrix * mat , double lambda_min, double lambda_max,
	                 double p_scale, double p_shift, double * p_coeff, int p_m, double p_cut,
	                 int MaxIts, pvsc_lidx_t trlz_m );
int pvsc_lanczos( pvsc_matrix * mat ,  double * shift,  int Iter,  pvsc_vector * x, double * extrem_eig, pvsc_krylov_options obt);
int pvsc_kacz( pvsc_matrix * mat , double * shift, int Iter, pvsc_vector * x_out, pvsc_vector * b, double damping);
int pvsc_cg_min_eig( pvsc_matrix * mat , double * shift,  int Iter, pvsc_vector * x);
int pvsc_cg_eig( pvsc_matrix * mat , double * shift,  int Iter, pvsc_vector * x);
int pvsc_cg( pvsc_matrix * mat , double * shift, int Iter, pvsc_vector * x_out,  pvsc_vector * b, pvsc_krylov_options obt);

#endif
