#ifndef PVSC_TIMING_H
#define PVSC_TIMING_H
#include <sys/time.h>

int pvsc_timing_init(char * name);
double pvsc_timing_end(int i);
int pvsc_timing_print();

typedef struct 
{
	char * name;
	int counter;
	double t_max;
	double t_min;
	double t_acc;
	double t_acc10skip;
	struct timeval current_tp;
}pvsc_timing;

#endif
