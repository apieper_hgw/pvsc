#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pvsc/dot_array.h"


#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int main(int argc, char **argv) {
	
	int rank = 0;
	
	int i,j;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerInit();
	#ifdef _OPENMP
		#pragma omp parallel
		{ likwid_markerThreadInit(); }
	#endif
	#endif
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	if( pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv ) )
	{
		pvsc_printf("pvsc_init_matrix_from_args() retun with error\n");
		pvsc_mem_manag_clear( );
		pvsc_unset_stdout();
		#ifdef PVSC_HAVE_MPI
		MPI_Finalize();
		#endif
		return 1;
	}
	
	
	pvsc_lidx_t nb = 2;
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	int Iter = 10;
	i = pvsc_read_args("-Iter" , argc, argv);
	if(i) Iter = atoi(argv[i+1]);
	
	
	pvsc_vector x[2] = {  PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER };
	
	
	
	
	pvsc_create_vector( x+0, nb, mat->sys);
	pvsc_create_vector( x+1, nb, mat->sys);
	
	pvsc_vector_from_func( x, &pvsc_hextest_vec, NULL);
	
	pvsc_printf(" x0 nb = %d\n", x->nb);
	
	i = pvsc_read_args("-out_x0" , argc, argv);
	if(i) pvsc_vector_write( argv[i+1],  x  );
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	
	spmvm_conf->mat = mat;
	//spmvm_conf->dot_allreduce = false;
	
	spmvm_conf->x = x  ;
	spmvm_conf->y = x+1;
	pvsc_malloc( (void **) &(spmvm_conf->dot_xx), sizeof(double), nb, true, "eig", 0);
	pvsc_malloc( (void **) &(spmvm_conf->dot_xy), sizeof(double), nb, true, "res", 0);
	pvsc_malloc( (void **) &(spmvm_conf->dot_yy), sizeof(double), nb, true, "res", 0);
	mat->ham->spmvm_kernel( spmvm_conf );
	
	for( pvsc_lidx_t i=0; i<nb; i++ ){
		pvsc_printf(" %.14e  %.14e  %.14e\n", spmvm_conf->dot_xx[i], spmvm_conf->dot_xy[i], spmvm_conf->dot_yy[i] );
	}
	
	i = pvsc_read_args("-out_x1" , argc, argv);
	if(i) pvsc_vector_write( argv[i+1], x+1 );
	
	
	pvsc_print_performance_statistic( spmvm_conf );
	
	
	
	PVSC_FREE(spmvm_conf->dot_xx)
	PVSC_FREE(spmvm_conf->dot_xy)
	PVSC_FREE(spmvm_conf->dot_yy)
	
	pvsc_destroy_vector(x  );
	pvsc_destroy_vector(x+1);
	
	
	pvsc_timing_print();
	
	
	pvsc_destroy_matrix( mat );
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerClose();
	#endif
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	
	return EXIT_SUCCESS;
}

