#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>

#include "pvsc/dot_array.h"

#include <math.h>

int main(int argc, char **argv) {
	
	int i,j;
	
	int rank = 0;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	
	pvsc_print_pvsc_info();
		
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	//-----------
	int nb = 1;
	double dt = 1.;
	int n_dt = 1;
	char * vec_in  = NULL;
	char * vec_out = NULL;
	
	i = pvsc_read_args("-vec_in" , argc, argv);
	if(i) vec_in = argv[i+1];
	
	i = pvsc_read_args("-vec_out" , argc, argv);
	if(i) vec_out = argv[i+1];
	//else return EXIT_SUCCESS;
	
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	i = pvsc_read_args("-dt" , argc, argv);
	if(i) dt = atof(argv[i+1]);
	
	i = pvsc_read_args("-n_dt" , argc, argv);
	if(i) n_dt = atoi(argv[i+1]);
	
	double gamma = 0.;
	i = pvsc_read_args("-disorder" , argc, argv);
	if(i) gamma = atof(argv[i+1]);
	
	
	
	double E_min = -4.5;
	double E_max =  4.5;
	
	double shift = 0.5*(E_min+E_max);
	double scale = 0.9*2./(E_max-E_min);
	
	pvsc_dot_array dot_array = PVSC_DOT_ARRAY_INITIALIZER;
	dot_array.n_z = 30;
	dot_array.n_y = 6;
	double D = 40.;
	dot_array.Rz   = 0.2*D/mat->ham->lattice_distance_z;
	dot_array.Ry   = 0.2*D/mat->ham->lattice_distance_y;
	dot_array.Dz   = D/mat->ham->lattice_distance_z;
	dot_array.Dy   = D/mat->ham->lattice_distance_y;
	dot_array.offset_z = 0.5*D/mat->ham->lattice_distance_z;
	dot_array.offset_y = 10.*D/mat->ham->lattice_distance_y;
	dot_array.V   = 0.08;
	dot_array.dVz = 0.002;
	
	
	i = pvsc_read_args("-V" , argc, argv);
	if(i) dot_array.V = atof(argv[i+1]);
	i = pvsc_read_args("-dVz" , argc, argv);
	if(i) dot_array.dVz = atof(argv[i+1]);
	
	
	
	pvsc_get_gradually_changing_dot_array( &dot_array, 0., 0., 0., dot_array.dVz );
	
	
	pvsc_set_array_from_func(mat, &pvsc_func_dot_array, &dot_array);
	
	
	
	
	pvsc_vector vec[0];
	pvsc_vector vec_wp[0];
	vec->sys = mat->sys;
	vec->val = NULL;
	
	pvsc_wavepacket wp;
	
	if (vec_in)
		pvsc_vector_read( vec_in, vec_wp);
	else{
		
		wp.phase = malloc(vec->sys->n_o*sizeof(double));
		wp.ampli = malloc(vec->sys->n_o*sizeof(double));
		for(i=0;i<vec->sys->n_o;i++)
			wp.ampli[i] = 1.;
		
		
		wp.kx = 0.;
		
		//wp.phase[1] = atan((-sin(M_PI*wp.ky)+sin(M_PI*wp.kz))/(1+cos(M_PI*wp.kz)+cos(M_PI*wp.ky)));
		wp.enable_dx = false;
		wp.enable_dy = true;
		//wp.enable_dy = false;
		wp.enable_dz = true;
		//wp.enable_dz = false;
		wp.rx = 0;
		//wp.ry = 0.2*mat->sys->n_y;
		wp.ry = 0.5*dot_array.offset_y;
		wp.rz = 0.5*mat->sys->n_z*mat->sys->mpi_size_z;
		wp.deltax = 0;
		//wp.deltay = 0.2*mat->sys->n_y;
		wp.deltay = 1.5*wp.ry;
		wp.deltaz = 1.*mat->sys->n_z*mat->sys->mpi_size_z;
		
		/*
		pvsc_vector vec_wp[1];
		pvsc_create_vector( vec_wp   , 2, mat->sys);
		
		double phi = M_PI*0.5;  a[4];
		*/
		
				/*
		wp.ky = -2./3.;
		wp.kz = 0.;
		wp.phase[0] =  0./3.;
		wp.phase[1] =  5./3.;
		wp.phase[2] =  2./3.;
		wp.phase[3] =  3./3.;
		*/
		/*
		wp.ky = -2./3.;
		wp.kz = 0.;
		wp.phase[0] = 0./3.;
		wp.phase[1] = 2./3.;
		wp.phase[2] = 2./3.;
		wp.phase[3] = 0./3.;
		*/
		
		/*
		wp.ky = 2./3.;
		wp.kz = 0.;
		wp.phase[0] = 0./3.;
		wp.phase[1] = 1./3.;
		wp.phase[2] = 4./3.;
		wp.phase[3] = 3./3.;
		pvsc_vector_from_func( vec_wp + 0, &pvsc_gen_wavepacket_func, &wp);
		*/
		/*
		pvsc_vector_xApby( 0., vec,  vec_wp, double * a, pvsc_lidx_t lda )
		
		
		wp.ky = 2./3.;
		wp.kz = 0.;
		wp.phase[0] =  0./3.;
		wp.phase[1] = -2./3.;
		wp.phase[2] = -2./3.;
		wp.phase[3] =  0./3.;
		pvsc_vector_from_func( vec_wp + 0, &pvsc_gen_wavepacket_func, &wp);
		
		
		*/
		
		wp.ky = -2./3.;
		wp.kz = 0.;
		wp.phase[0] =  0./3.; wp.ampli[0] = 1.;
		wp.phase[1] =  2./3.; wp.ampli[1] = 1.;
		wp.phase[2] =  2./3.; wp.ampli[2] = 1.;
		wp.phase[3] =     0.; wp.ampli[3] = 1.;
		
		pvsc_create_vector( vec_wp, 2, mat->sys);
		pvsc_vector_from_func( vec_wp, &pvsc_gen_wavepacket_func, &wp);
		
	}
	
	
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	
	double * mu = NULL;
	int M_mu;
	
	pvsc_create_vector( vec, 2, mat->sys);
	
	/*if( !mat->sys->mpi_rank ) printf("Strat cheb tp\n" );
	pvsc_cheb_tp( vec_wp,  dt, mat , scale, shift, NULL, &M_mu);
	if( !mat->sys->mpi_rank ) printf("Strat cheb tp\n" );
	pvsc_cheb_tp( vec,  dt, mat , scale, shift, NULL, &M_mu);
	*/
	
	
	
	pvsc_vector_axpby( NULL, vec, NULL, vec_wp );
	
	pvsc_vector_write( "wp_start.vec", vec);
	
	
	for(int n=0; n<n_dt; n++){
		//if( !mat->sys->mpi_rank ) printf("Strat cheb tp\n" );
		//pvsc_vector_axpby( NULL, vec, NULL, vec_wp );
		pvsc_cheb_tp( vec,  dt, mat , scale, shift, &mu, &M_mu);
		if(vec_out){
			char filename[128];
			sprintf( filename, "%s_%d", vec_out, n);
			pvsc_vector_write( filename, vec);
		}
	}
	
	if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "cheb_tp_spec.dat", M_mu, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	
	
	pvsc_vector pot[0];
	pvsc_create_vector( pot, 1, mat->sys);
	
	pvsc_vector_from_func(pot, &pvsc_func_dot_array, &dot_array);
	pvsc_vector_write( "pot.vec", pot  );
	pvsc_destroy_vector(pot);
	
	if( !mat->sys->mpi_rank ) printf("Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	if( !mat->sys->mpi_rank ) pvsc_timing_print();
	
	PVSC_FREE(mu)
	
	if(vec_out)
		pvsc_vector_write( vec_out, vec);
	
	pvsc_destroy_vector(vec);
	pvsc_destroy_vector(vec_wp);
	pvsc_destroy_matrix( mat );
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	
	return EXIT_SUCCESS;
}


