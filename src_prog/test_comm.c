#include <pvsc.h>
#include <stdio.h>
#include <stdlib.h>



double test_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm, void * seed ){
	
	
	return 10000*n[3]+ 1000*n[2] + 100*n[1] + 10*n[0] + k; 
	//if( (n[3] == 0) &&  (n[2] == 0) && (n[1] == 0) && (n[0] == 0) ) return 1.;
	
	return 0.;
}


int main(int argc, char **argv) {
	
#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
#endif
	pvsc_system_geo sys[1];
	sys->finalised=false;
	pvsc_init_system_geo_mpicomm( sys,  MPI_COMM_WORLD, atoi(argv[1]) , atoi(argv[2]), atoi(argv[3]));
	
	sys->n_x = 4;
	sys->n_y = 4;
	sys->n_z = 4;
	sys->n_o = 1;
	
	sys->pbc_x = true;
	sys->pbc_y = true;
	sys->pbc_z = true;
	
	pvsc_finalise_system_geo( sys );
	
	if (! sys->mpi_rank){
		print_sub_volume( "volume.dat"       , sys->v );
		
		print_sub_volume( "xd.dat"      , sys->b_xd.v_local  );
		print_sub_volume( "xu.dat"      , sys->b_xu.v_local  );
		print_sub_volume( "yd.dat"      , sys->b_yd.v_local  );
		print_sub_volume( "yu.dat"      , sys->b_yu.v_local  );
		print_sub_volume( "zd.dat"      , sys->b_zd.v_local  );
		print_sub_volume( "zu.dat"      , sys->b_zu.v_local  );
		print_sub_volume( "xdr.dat"     , sys->b_xd.v_remote );
		print_sub_volume( "xur.dat"     , sys->b_xu.v_remote );
		print_sub_volume( "ydr.dat"     , sys->b_yd.v_remote );
		print_sub_volume( "yur.dat"     , sys->b_yu.v_remote );
		print_sub_volume( "zdr.dat"     , sys->b_zd.v_remote );
		print_sub_volume( "zur.dat"     , sys->b_zu.v_remote );
		
	}
	
	
	pvsc_lidx_t nb = 2;
	pvsc_vector x[1];
	
	pvsc_create_vector( x, nb, sys);
	
	
	char fname[128];
	pvsc_vector_from_func( x, test_vec, NULL);
	
	sprintf( fname, "vec_x_rank_%d.dat", sys->mpi_rank);
	print_vector( fname, x  );
	
	//pvsc_vector_datatransfer( x );
	pvsc_vector_datatransfer_dependent( x );
	
	
	sprintf( fname, "vec_x_rank_%d_comm.dat", sys->mpi_rank);
	print_vector( fname, x  );
	
#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
#endif
	
	return EXIT_SUCCESS;
}
