#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pvsc/dot_array.h"

#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int main(int argc, char **argv) {
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	int i,j;
	
	int rank = 0;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif

	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_INIT;
	#ifdef _OPENMP
		#pragma omp parallel
		{ LIKWID_MARKER_THREADINIT; }
	#endif
	#endif
	
	
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	
	int nb = PVSC_OPT_BLOCK_SIZE, M=256, R=PVSC_OPT_BLOCK_SIZE;
	
	i = pvsc_read_args("-M" , argc, argv);
	if(i) M = atoi(argv[i+1]);
	
	i = pvsc_read_args("-disorder" , argc, argv);
	if(i) {
		pvsc_printf("  set disorder = %g\n", atof(argv[i+1]));
		pvsc_set_array_from_func_scale(0. , mat, atof(argv[i+1]), &pvsc_rand_vec, NULL);
	}
	
	
	double E_min = -4.;
	double E_max =  4.;
	
	i = pvsc_read_args("-eig_range" , argc, argv);
	if(i){ E_min = atof(argv[i+1]);
	       E_max = atof(argv[i+2]);}
	
	double shift = 0.5*(E_min+E_max);
	double scale = 2./(E_max-E_min);
	
	int Nk = 64;
	double k_start[3] = {0., 0., 0.};
	double k_end[3]   = {1., 1., 1.};
	
	i = pvsc_read_args("-Nk" , argc, argv);
	if(i) Nk = atoi(argv[i+1]);
	
	i = pvsc_read_args("-k_start" , argc, argv);
	if(i){
		k_start[0] = atof(argv[i+1]);
		k_start[1] = atof(argv[i+2]);
		k_start[2] = atof(argv[i+3]);
	}
	
	i = pvsc_read_args("-k_end" , argc, argv);
	if(i){
		k_end[0] = atof(argv[i+1]);
		k_end[1] = atof(argv[i+2]);
		k_end[2] = atof(argv[i+3]);
	}
	
	
	
	double * mu = pvsc_malloc(NULL, sizeof(double),M*Nk, true, "mu", 0);
	
	pvsc_vector vec[1] = {  PVSC_VECTOR_INITIALIZER };
	pvsc_create_vector( vec, mat->sys->n_o, mat->sys);
	
	double *k = malloc(sizeof(double)*3);
	
	int *obc_layer = malloc(sizeof(int)*3);
	
	i = pvsc_read_args("-obc_layer" , argc, argv);
	if(i){
		obc_layer[0] = atoi(argv[i+1]);
		obc_layer[1] = atoi(argv[i+2]);
		obc_layer[2] = atoi(argv[i+3]);
	}else{
		obc_layer[0] = 0;
		obc_layer[1] = 0;
		obc_layer[2] = 0;
	}
	
	void **pram = malloc(sizeof(void *)*2);
	pram[0] = (void *)k;
	pram[1] = (void *)obc_layer;
	
	for(i=0; i<Nk; i++){
		
		for(j=0; j<3; j++)    k[j] = k_start[j] + i*( k_end[j] - k_start[j] )/Nk;
		pvsc_printf(" k = [ %g , %g, %g ]\n", k[0], k[1], k[2] );
		//void pram[2];
		//pram[0] = k;
		//pram[1] = obc_layer;
		/*
		k[2] /= mat->sys->n_x * mat->sys->mpi_size_x;
		k[1] /= mat->sys->n_y * mat->sys->mpi_size_y;
		k[0] /= mat->sys->n_z * mat->sys->mpi_size_z;
		*/
		pvsc_vector_from_func( vec, pvsc_gen_wave_obc_layer_vec, pram);
		
		pvsc_cheb_dos( mu + M*i, mat,  mat->sys->n_o, mat->sys->n_o, M, scale, shift, vec );
	}
	
	free(pram);
	free(obc_layer);
	free(k);
	
	i = pvsc_read_args("-arpes" , argc, argv);
	if( i )  if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( argv[i+1], M, Nk, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	else     if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "arpes.dat", M, Nk, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	i = pvsc_read_args("-fermi_arg" , argc, argv);
	if( i ) { double level = atof(argv[i+2]);
	          double label = atof(argv[i+3]);
	         if( !mat->sys->mpi_rank ) pvsc_kpm_print_density_level( argv[i+1], M, Nk, mu , scale, shift, atof(argv[i+2]), atof(argv[i+3]), NULL, &pvsc_gibbs_Jackson );
	         }
	
	pvsc_vector_from_func( vec, pvsc_rand_vec, NULL);
	pvsc_cheb_dos( mu , mat,  mat->sys->n_o, mat->sys->n_o, M, scale, shift, vec );
	
	i = pvsc_read_args("-dos" , argc, argv);
	if( i )  if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( argv[i+1], M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	else     if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "dos.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	
	
	 pvsc_timing_print();
	
	if(vec) pvsc_destroy_vector( vec );
	pvsc_destroy_matrix( mat );
	PVSC_FREE(mu)
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_CLOSE;
	#endif
	
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	pvsc_printf( "Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	return EXIT_SUCCESS;
}

