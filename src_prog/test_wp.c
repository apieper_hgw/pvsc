#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char **argv) {
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	int i,j;
	
	int rank = 0;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	
	pvsc_wp_handel_t wp_conf[1] = {PVSC_WP_HANDEL_INITIALIZER};
	
	pvsc_vector v[3] = {  PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER}; 
	
	
	
	
	int nb = 1;
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	i = pvsc_read_args("-wd" , argc, argv);
	if(i) wp_conf->wp_deep = atoi(argv[i+1]);
	
	pvsc_create_vector( v  , nb, mat->sys);
	pvsc_create_vector( v+1, nb, mat->sys);
	pvsc_create_vector( v+2, nb, mat->sys);
	
	wp_conf->v0 = v+0;
	wp_conf->v1 = v+1;
	wp_conf->w  = v+2;
	wp_conf->mat = mat;
	
	
	mat->ham->wp_kernel( wp_conf );
	
	
	pvsc_timing_print();
	
	
	pvsc_destroy_matrix( mat );
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	pvsc_printf( "Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	return EXIT_SUCCESS;
}

