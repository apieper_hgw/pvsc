#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pvsc/dot_array.h"

#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

//  OMP_NUM_THREADS=2 ~/pvsc/bin/ChebFD.x -mpi_grid 1-1-1 -sys_size 1-50-50 -pbc -fd_search_range -0.1 0.1 -stencil graphene_short_var -trl
//FILE * pvsc_stdout = stdout;

int main(int argc, char **argv) {
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	int rank = 0;
	
	int i,j;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerInit();
	#ifdef _OPENMP
		#pragma omp parallel
		{ likwid_markerThreadInit(); }
	#endif
	#endif
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	i = pvsc_read_args("-disorder" , argc, argv);
	if(i) {
		pvsc_set_array_from_func_scale(0. , mat, atof(argv[i+1]), &pvsc_rand_vec, NULL);
	}
	
	int nb = PVSC_OPT_BLOCK_SIZE, M=256, R=PVSC_OPT_BLOCK_SIZE;
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	i = pvsc_read_args("-M" , argc, argv);
	if(i) M = atoi(argv[i+1]);
	
	i = pvsc_read_args("-R" , argc, argv);
	if(i) R = atoi(argv[i+1]);
	
	//sys->pbc_z = false;
	

	

	/*
	pvsc_dot_array dot_array = PVSC_DOT_ARRAY_INITIALIZER;
	dot_array.n_z = 7;
	dot_array.n_y = 7;
	double D = 40.;
	dot_array.Rz   = 0.25*D/mat->ham->lattice_distance_z;
	dot_array.Ry   = 0.25*D/mat->ham->lattice_distance_y;
	dot_array.Dz   = D/mat->ham->lattice_distance_z;
	dot_array.Dy   = D/mat->ham->lattice_distance_y;
	dot_array.offset_z = 0.5*D/mat->ham->lattice_distance_z;
	dot_array.offset_y = 0.5*D/mat->ham->lattice_distance_y;
	dot_array.V   = 0.08;
	
	i = pvsc_read_args("-V" , argc, argv);
	if(i) dot_array.V = atof(argv[i+1]);
	
	pvsc_set_array_from_func_scale( 0. ,mat, 1., &pvsc_func_dot_array, &dot_array);
	
	double gamma = 0.;
	i = pvsc_read_args("-disorder" , argc, argv);
	if(i) gamma = atof(argv[i+1]);
	
	pvsc_set_array_from_func_scale(1. ,mat , gamma, &pvsc_rand_vec, NULL);

	
	if (mat->ham_scalars){
		mat->ham_scalars[0] = -1.;
		//mat->ham_scalars[1] = 0.;//-0.1;
		//mat->ham_scalars[2] = 0.;//-0.05;
	}
	//pvsc_lidx_t n_nb[] = {1, 2, 4, 8, 16};
	//pvsc_benchmark_mat( mat , 5, n_nb, 8);
	//return 0;
	*/
	
	double E_min = -4.;
	double E_max =  4.;
	double scale = 0.5*(E_min+E_max);
	double shift = 2./(E_max-E_min);
	
	
	pvsc_cheb_fd_handle_t fdh[1] = {PVSC_CHEB_FD_HANDLE_INITIALIZER};
	fdh->mat = mat;
	//fdh->auto_rescale = false;
	//fdh->auto_subspace = false;
	fdh->lambda_min = -0.02;
	fdh->lambda_max =  0.02;
	
	i = pvsc_read_args("-fd_search_range" , argc, argv);
	if(i){
		fdh->lambda_min = atof(argv[i+1]);
		fdh->lambda_max = atof(argv[i+2]);
	}
	
	
	i = pvsc_read_args("-fd_eig_range" , argc, argv);
	if(i){
		E_min = atof(argv[i+1]);
		E_max = atof(argv[i+2]);
		fdh->auto_rescale = false;
	}
	
	fdh->scale = 2./(E_max-E_min);
	fdh->shift = 0.5*(E_min+E_max);
	scale = fdh->scale;
	shift = fdh->shift;
	
	i = pvsc_read_args("-fd_search_dim" , argc, argv);
	if(i){
		fdh->auto_subspace = false;
		fdh->subspace = atoi(argv[i+1]);
	}
	
	i = pvsc_read_args("-fd_subspace_ddf" , argc, argv);
		if(i){
		fdh->subspace_ddf = atof(argv[i+1]);
	}
	
	i = pvsc_read_args("-fd_polydegree_factor" , argc, argv);
	if(i){
		fdh->polydegree_factor = atof(argv[i+1]);
	}
	
	i = pvsc_read_args("-fd_iter_max" , argc, argv);
	if(i){
		fdh->iter_max = atoi(argv[i+1]);
	}
	
	i = pvsc_read_args("-disable_searchspace_reduction" , argc, argv);
	if(i){
		fdh->searchspace_reduction = false;
	}
	
	i = pvsc_read_args("-enable_subspace_reduction" , argc, argv);
	if(i){
		fdh->subspace_reduction = true;
	}
	
	i = pvsc_read_args("-fd_overpopulation" , argc, argv);
	if(i){
		fdh->overpopulation = atof(argv[i+1]);
	}
	
	i = pvsc_read_args("-disable_cheap_init" , argc, argv);
	if(i){
		fdh->cheap_init = false;
	}
	
	i = pvsc_read_args("-dos" , argc, argv);
	if(i) {
		fdh->dos_export = argv[i+1];
	}
	
	i = pvsc_read_args("-trl" , argc, argv);
	if(i){
		pvsc_cheb_fd_trl( fdh );
	}
	else{
		pvsc_cheb_fd( fdh );
	}
	
	//double * mu = pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
	
	//pvsc_cheb_dos( mu, mat, nb, R, M, fdh->scale, fdh->shift, NULL );
	//if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "dos.dat", M, 1, mu , fdh->scale, fdh->shift, NULL, &pvsc_gibbs_Jackson );
	
	
	
	
	//pvsc_vector x[1] = {PVSC_VECTOR_INITIALIZER};
	//pvsc_create_vector( x, nb, sys);
	//pvsc_vector_from_func( x, pvsc_rand_vec, NULL);
	
	
	//double * shifts = pvsc_malloc(NULL, sizeof(double),nb, true, "shifts_lanczos", 0);
	//for(int i=0; i<nb; i++) shifts[i] = -0.;
	
	
	/*
	int iter = 40;
	//pvsc_lanczos( mat , shifts, 50,  x, NULL, PVSC_KRYLOV_MIN_EIG_VEC|PVSC_KRYLOV_SQUARE_MAT);
	//pvsc_kacz( mat , shifts, 20, NULL, x, 1.0);
	//pvsc_kacz_old( mat , 10,  scale,  shift, NULL, x, 1.0);
	//pvsc_cg_eig( mat , shifts, iter , x);
	pvsc_cg( mat , shifts, iter, NULL,  x, PVSC_KRYLOV_DEFAULT|PVSC_KRYLOV_SQUARE_MAT);
	pvsc_cheb_dos( mu, mat, nb, R, M, scale, shift, x );
	if( !sys->mpi_rank ) pvsc_kpm_print_density( "l_filter_1.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	//pvsc_lanczos( mat , shifts, 50,  x, NULL, PVSC_KRYLOV_MIN_EIG_VEC|PVSC_KRYLOV_SQUARE_MAT);
	//pvsc_kacz( mat , shifts, 20, NULL, x, 1.0);
	//pvsc_kacz_old( mat , 10,  scale,  shift, NULL, x, 1.0);
	//pvsc_cg_eig( mat ,  shifts, iter, x);
	pvsc_cg( mat , shifts, iter, NULL,  x, PVSC_KRYLOV_DEFAULT|PVSC_KRYLOV_SQUARE_MAT);
	pvsc_cheb_dos( mu, mat, nb, R, M, scale, shift, x );
	if( !sys->mpi_rank ) pvsc_kpm_print_density( "l_filter_2.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	//pvsc_lanczos( mat , shifts, 50,  x, NULL, PVSC_KRYLOV_MIN_EIG_VEC|PVSC_KRYLOV_SQUARE_MAT);
	//pvsc_kacz( mat , shifts, 20, NULL, x, 1.0);
	//pvsc_kacz_old( mat , 10,  scale,  shift, NULL, x, 1.0);
	//pvsc_cg_eig( mat ,  shifts, iter, x);
	pvsc_cg( mat , shifts, iter, NULL,  x, PVSC_KRYLOV_DEFAULT|PVSC_KRYLOV_SQUARE_MAT);
	pvsc_cheb_dos( mu, mat, nb, R, M, scale, shift, x );
	if( !sys->mpi_rank ) pvsc_kpm_print_density( "l_filter_3.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	pvsc_vector_write( "min_vec.vec", x  );
	
	pvsc_vector_from_func(x, &pvsc_func_dot_array, &dot_array);
	pvsc_vector_write( "pot.vec", x  );
	
	*/
	
	pvsc_timing_print();
	
	//pvsc_destroy_vector(x);
	
	pvsc_destroy_matrix( mat );
	//PVSC_FREE(shifts)
	//PVSC_FREE(mu)
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerClose();
	#endif
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	pvsc_printf( "Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	return EXIT_SUCCESS;
}

