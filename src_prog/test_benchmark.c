#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pvsc/dot_array.h"


#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int main(int argc, char **argv) {
	
	int rank = 0;
	
	int i,j;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerInit();
	#ifdef _OPENMP
		#pragma omp parallel
		{ likwid_markerThreadInit(); }
	#endif
	#endif
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	if( pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv ) )
	{
		pvsc_printf("pvsc_init_matrix_from_args() retun with error\n");
		pvsc_mem_manag_clear( );
		pvsc_unset_stdout();
		#ifdef PVSC_HAVE_MPI
		MPI_Finalize();
		#endif
		return 1;
	}
	
	
	pvsc_lidx_t n_nb[8] = { 1, 2, 3, 4, 6, 8, 10, 12};
	int         nb_num = 8;
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) {
		nb_num = 0;
		char * pch = strtok( argv[i+1], ",;-");
		while (pch != NULL)
		{
			n_nb[nb_num] = atoi(pch);
			nb_num++;
			pch = strtok (NULL, ",;-");
		}
	}
	
	int Iter = 10;
	i = pvsc_read_args("-Iter" , argc, argv);
	if(i) Iter = atoi(argv[i+1]);
	
	bool dot_mode = false;
	i = pvsc_read_args("-dot_mode" , argc, argv);
	if(i) dot_mode = true;
	
	bool z_mode = false;
	i = pvsc_read_args("-z_mode" , argc, argv);
	if(i) z_mode = true;
	
	pvsc_benchmark_mat( mat , nb_num, n_nb, Iter, dot_mode, z_mode);
	
	
	pvsc_timing_print();
	
	
	pvsc_destroy_matrix( mat );
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerClose();
	#endif
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	
	return EXIT_SUCCESS;
}

