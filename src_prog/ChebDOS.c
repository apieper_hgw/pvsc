#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pvsc/dot_array.h"

#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int main(int argc, char **argv) {
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	int i,j;
	
	int rank = 0;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif

	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_INIT;
	#ifdef _OPENMP
		#pragma omp parallel
		{ LIKWID_MARKER_THREADINIT; }
	#endif
	#endif
	
	
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	
	int nb = PVSC_OPT_BLOCK_SIZE, M=256, R=PVSC_OPT_BLOCK_SIZE;
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	i = pvsc_read_args("-M" , argc, argv);
	if(i) M = atoi(argv[i+1]);
	
	i = pvsc_read_args("-R" , argc, argv);
	if(i) R = atoi(argv[i+1]);
	
	i = pvsc_read_args("-disorder" , argc, argv);
	if(i) {
		pvsc_set_array_from_func_scale(0. , mat, atof(argv[i+1]), &pvsc_rand_vec, NULL);
	}
	
	
	double E_min = -4.;
	double E_max =  4.;
	
	i = pvsc_read_args("-eig_range" , argc, argv);
	if(i){ E_min = atof(argv[i+1]);
	       E_max = atof(argv[i+2]);}
	
	double shift = 0.5*(E_min+E_max);
	double scale = 2./(E_max-E_min);
	
	
	double * mu = pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
	
	
	pvsc_vector *x = NULL;
	
	i = pvsc_read_args("-import" , argc, argv);
	if(i){
		pvsc_vector x_[1] = {  PVSC_VECTOR_INITIALIZER };
		x = x_;
		x_->sys = mat->sys;
		pvsc_vector_read(  argv[i+1], x_ );
	}
	
	
	pvsc_cheb_dos( mu, mat,  R, nb, M, scale, shift, x );
	
	
	i = pvsc_read_args("-dos" , argc, argv);
	if( i )  if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( argv[i+1], M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	else     if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "dos.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	 pvsc_timing_print();
	
	if(x) pvsc_destroy_vector( x );
	pvsc_destroy_matrix( mat );
	PVSC_FREE(mu)
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_CLOSE;
	#endif
	
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	pvsc_printf( "Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	return EXIT_SUCCESS;
}

