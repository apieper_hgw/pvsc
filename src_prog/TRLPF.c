#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "pvsc/dot_array.h"

#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int main(int argc, char **argv) {
	
	struct timeval tp;
	gettimeofday(&tp, NULL);
	
	int i,j;
	
	int rank = 0;
	#ifdef PVSC_HAVE_MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	#endif

	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_INIT;
	#ifdef _OPENMP
		#pragma omp parallel
		{ LIKWID_MARKER_THREADINIT; }
	#endif
	#endif
	
	
	
	char log_out[20];sprintf(log_out,"logfile_rank_%05d", rank );
	if ( rank ) pvsc_set_stdout( log_out );
	else        pvsc_set_stdout( NULL );
	
	pvsc_print_pvsc_info();
	
	pvsc_matrix mat[1] = {PVSC_MATRIX_INITIALIZER};
	
	pvsc_init_matrix_from_args (mat, MPI_COMM_WORLD , argc, argv );
	
	
	int nb = PVSC_OPT_BLOCK_SIZE, M=256, R=PVSC_OPT_BLOCK_SIZE;
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) nb = atoi(argv[i+1]);
	
	i = pvsc_read_args("-M" , argc, argv);
	if(i) M = atoi(argv[i+1]);
	
	i = pvsc_read_args("-R" , argc, argv);
	if(i) R = atoi(argv[i+1]);
	
	
	double E_min = -4.;
	double E_max =  4.;
	
	i = pvsc_read_args("-eig_range" , argc, argv);
	if(i){ E_min = atof(argv[i+1]);
	       E_max = atof(argv[i+2]);}
	
	double shift = 0.5*(E_min+E_max);
	double scale = 2./(E_max-E_min);
	
	
	double * mu = pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
	
	
	pvsc_vector *x = NULL;
	
	i = pvsc_read_args("-import" , argc, argv);
	if(i){
		pvsc_vector x_[1] = {  PVSC_VECTOR_INITIALIZER };
		x = x_;
		x_->sys = mat->sys;
		pvsc_vector_read(  argv[i+1], x_ );
	}
	
	
	pvsc_cheb_dos( mu, mat,  R, nb, M, scale, shift, x );
	
	i = pvsc_read_args("-dos" , argc, argv);
	if( i )  if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( argv[i+1], M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	else     if( !mat->sys->mpi_rank ) pvsc_kpm_print_density( "dos.dat", M, 1, mu , scale, shift, NULL, &pvsc_gibbs_Jackson );
	
	double lambda_min = -0.5;
	double lambda_max =  0.5;
	i = pvsc_read_args("-fd_search_range" , argc, argv);
	if(i){
		lambda_min = atof(argv[i+1]);
		lambda_max = atof(argv[i+2]);
	}
	
	
	
	//double * p_coeff = NULL;
	//int p_m = pvsc_cheb_toolbox_window_coeffs(lambda_min, lambda_max, scale, shift, &p_coeff, 1 );
	
	double x_l = scale*(lambda_min-shift);
	double x_r = scale*(lambda_max-shift);
	double phi_l = acos(x_l);
	double phi_r = acos(x_r);
	double phi_c = 0.5*(phi_l+phi_r);
	double x_c   = cos(phi_c);
	
	int p_m =  (int)( 4.*M_PI/( phi_l - phi_r ));
	double * p_coeff = pvsc_malloc(NULL, sizeof(double), p_m, true, "p_coeff", 0 );
	pvsc_cheb_delta_coeff_arg( p_coeff,  p_m, phi_c );
	
	
	pvsc_gibbs_Lanczos ( p_m, 2., p_coeff, 1);
	//pvsc_gibbs_Jackson ( p_m, p_coeff, 1);
	
	double t[3][4];
	t[0][0] = 1.;
	t[1][0] = 1.;
	t[2][0] = 1.;
	t[0][1] = x_l;
	t[1][1] = x_c;
	t[2][1] = x_r;
	t[0][3] = x_l;
	t[1][3] = x_c;
	t[2][3] = x_r;
	
	for( int i = 0; i<3; i++ ) t[i][2] =   p_coeff[0]*t[i][0] 
	                                     + p_coeff[1]*t[i][1];
	int m;
	
	double p_cut = 0.7;
	
	for( m = 2; m<p_m; m++ ){
		for( int i = 0; i<3; i++ ) { t[i][m&1] = 2*t[i][(m-1)&1]*t[i][3] - t[i][(m-2)&1]; t[i][2] += p_coeff[m]*t[i][m&1];}
		
		if( ( t[0][2] < p_cut*t[1][2] ) && ( t[2][2] < p_cut*t[1][2] ) ) break;
	}
	p_m = m;
	for( m = 0; m<p_m; m++ ) p_coeff[m] /= t[1][2];
	
	//p_cut = t[0][2] < t[2][2] ? t[0][2] : t[2][2];
	
	
	pvsc_printf(" degree %d / %d (%g)\n", m, p_m,  m*1./p_m );
	pvsc_printf(" %g %g %g  %g\n", x_l, phi_l, t[0][2], pvsc_cheb_eval_cheb_poly( p_coeff,  p_m, x_l) );
	pvsc_printf(" %g %g %g  %g\n", x_c, phi_c, t[1][2], pvsc_cheb_eval_cheb_poly( p_coeff,  p_m, x_c) );
	pvsc_printf(" %g %g %g  %g\n", x_r, phi_r, t[2][2], pvsc_cheb_eval_cheb_poly( p_coeff,  p_m, x_r) );
	
	//pvsc_cheb_toolbox_print_filter( "filter.dat", p_coeff, 1, p_m,  scale, shift, 10000 );
	
	//pvsc_tr_lanczos_arnoldi( NULL, mat , lambda_min, lambda_max, scale, shift, p_coeff, p_m, p_cut,  10, 20 );
	pvsc_tr_lanczos(         NULL, mat , lambda_min, lambda_max, scale, shift, p_coeff, p_m, p_cut,  2, 36 );
	
	
	pvsc_timing_print();
	PVSC_FREE(p_coeff)
	if(x) pvsc_destroy_vector( x );
	pvsc_destroy_matrix( mat );
	PVSC_FREE(mu)
	pvsc_mem_manag_clear( );
	
	pvsc_unset_stdout();
	
	#ifdef PVSC_HAVE_LIKWID
	LIKWID_MARKER_CLOSE;
	#endif
	
	#ifdef PVSC_HAVE_MPI
	MPI_Finalize();
	#endif
	pvsc_printf( "Runtime: %g sec\n", pvsc_timing_since(tp) );
	
	return EXIT_SUCCESS;
}

