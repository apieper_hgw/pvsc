#TARGET   = projectname



include conf/make.inc



# change these to set the proper directories where each files shoould be
SRCDIR     = src
PROGSRCDIR = src_prog
PROGSRCDIR_NONMPI = src_prog_nonmpi
STECSRCDIR = src_stencil
INCDIR     = include
IPATH += -I./$(INCDIR)
IPATH += -I/apps/likwid/4.1.0/include
#IPATH += -I/apps/fftw3/fftw-3.1.2_pgi/include

#LIBS += -L/apps/fftw3/fftw-3.1.2_pgi/lib
LIBS += -L/apps/likwid/4.1.0/lib

OBJDIR     = obj
OBJDIR_MPI = obj_mpi
BINDIR     = bin
BINDIR_MPI = bin_mpi

ifeq ($(MMIC), 1)
	OBJDIR     = obj_mic
	OBJDIR_MPI = obj_mpi_mic
	BINDIR     = bin_mic
	BINDIR_MPI = bin_mpi_mic
endif

OUT_DIR := $(OBJDIR) $(BINDIR) $(OBJDIR_MPI) $(BINDIR_MPI)


SOURCES            := $(wildcard $(SRCDIR)/*.c)
PROGSOURCES        := $(wildcard $(PROGSRCDIR)/*.c)
PROGSOURCES_NONMPI := $(wildcard $(PROGSRCDIR_NONMPI)/*.c)

INCLUDES    := $(wildcard $(INCDIR)/*.h)
INCLUDES    += $(wildcard $(INCDIR)/*/*.h)


PROGS          := $(PROGSOURCES:$(PROGSRCDIR)/%.c=$(BINDIR)/%.x)
PROGS_NONMPI   := $(PROGSOURCES_NONMPI:$(PROGSRCDIR_NONMPI)/%.c=$(BINDIR)/%.x)

OBJECTS     := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
PROGOBJECTS := $(PROGSOURCES:$(PROGSRCDIR)/%.c=$(OBJDIR)/%.o)
PROGOBJECTS_NONMPI := $(PROGSOURCES_NONMPI:$(PROGSRCDIR_NONMPI)/%.c=$(OBJDIR)/%.o)


PROGS_MPI   := $(PROGSOURCES:$(PROGSRCDIR)/%.c=$(BINDIR_MPI)/%.x)

OBJECTS_MPI     := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR_MPI)/%.o)
PROGOBJECTS_MPI := $(PROGSOURCES:$(PROGSRCDIR)/%.c=$(OBJDIR_MPI)/%.o)

CFLAGS_MPI := $(CFLAGS) -D PVSC_HAVE_MPI



ifdef MPI
  ifeq ($(MPI),1)
    ALLPROGS = $(PROGS_MPI)
  else
    ALLPROGS = $(PROGS_NONMPI) $(PROGS)
  endif
else
  ALLPROGS = $(PROGS_NONMPI) $(PROGS) $(PROGS_MPI)
endif

.PHONY: all
all: print_vars $(ALLPROGS)

include $(STECSRCDIR)/make.inc

# NONMPI PROGS

$(PROGS_NONMPI): $(BINDIR)/%.x : $(OBJDIR)/%.o $(OBJECTS) $(STECOBJECTS) $(BINDIR)
	@echo "Linked "$@
	@$(CC) -o $@ $(CFLAGS) $< $(OBJECTS) $(STECOBJECTS) $(LIBS)


$(PROGS):        $(BINDIR)/%.x : $(OBJDIR)/%.o $(OBJECTS) $(STECOBJECTS) $(BINDIR)
	@echo "Linked "$@
	@$(CC) -o $@ $(CFLAGS) $< $(OBJECTS) $(STECOBJECTS) $(LIBS)


# NONMPI OBJECTS

$(PROGOBJECTS_NONMPI): $(OBJDIR)/%.o : $(PROGSRCDIR_NONMPI)/%.c $(INCLUDES) $(OBJDIR) $(STEREPOSSRC)
	@echo "Compiled "$@
	@$(CC) $(CFLAGS) $(IPATH) -c $< -o $@

$(PROGOBJECTS):        $(OBJDIR)/%.o : $(PROGSRCDIR)/%.c        $(INCLUDES) $(OBJDIR) $(STEREPOSSRC)
	@echo "Compiled "$@
	@$(CC) $(CFLAGS) $(IPATH) -c $< -o $@

$(OBJECTS):            $(OBJDIR)/%.o : $(SRCDIR)/%.c            $(INCLUDES) $(OBJDIR) $(STEREPOSSRC)
	@echo "Compiled "$@
	@$(CC) $(CFLAGS) $(IPATH) -c $< -o $@




# MPI PROGS

$(PROGS_MPI): $(BINDIR_MPI)/%.x : $(OBJDIR_MPI)/%.o $(OBJECTS_MPI) $(STECOBJECTS_MPI) $(BINDIR_MPI)
	@echo "Linked "$@
	@$(MPICC) -o $@ $(CFLAGS_MPI) $< $(OBJECTS_MPI) $(STECOBJECTS_MPI) $(LIBS)


# MPI OBJECTS

$(PROGOBJECTS_MPI): $(OBJDIR_MPI)/%.o : $(PROGSRCDIR)/%.c $(INCLUDES) $(OBJDIR_MPI) $(STEREPOSSRC)
	@echo "Compiled "$@
	@$(MPICC) $(CFLAGS_MPI) $(IPATH) -c $< -o $@

$(OBJECTS_MPI):     $(OBJDIR_MPI)/%.o : $(SRCDIR)/%.c     $(INCLUDES) $(OBJDIR_MPI) $(STEREPOSSRC)
	@echo "Compiled "$@
	@$(MPICC) $(CFLAGS_MPI) $(IPATH) -c $< -o $@





.PHONY: directories
directories: $(OUT_DIR)
$(OUT_DIR):
	$(MKDIR_P) $@

.PHONEY: clean
clean:
	$(RM) $(OBJDIR_MPI) $(OBJDIR) $(INCDIR_STENCIL_KERNELS) $(SRCDIR_STENCIL_KERNELS)
	echo "Cleanup complete!"

.PHONEY: remove
remove: clean
	$(RM) $(BINDIR) $(BINDIR_MPI)
	echo "Executable removed!"

.PHONY: print_vars
print_vars:
	@echo "Finded sources:" $(SOURCES)
	@echo "Finded sources:" $(STECDATA) "->" $(STECSOURCES) "->" $(STECOBJECTS_MPI)
	@echo "Finded headers:" $(INCLUDES)
	@echo "Finded program sources:" $(PROGSOURCES)
	@echo "Program OBJECTS_MPI:" $(PROGOBJECTS_MPI)
	@echo "CFLAGS:" $(CFLAGS)
	@echo "CFLAGS_MPI:" $(CFLAGS_MPI)
	@echo "LIBS:" $(LIBS)
	@echo "IPATH:" $(IPATH)
