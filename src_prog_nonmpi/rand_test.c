#define _XOPEN_SOURCE 600

#include "pvsc/rand.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include <omp.h>
int nthreads = 1;

void get_walltime_(double* wcTime) {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  *wcTime = (double)(tp.tv_sec + tp.tv_usec/1000000.0);
}


int read_args(char * option , int argc, char * argv[]){
	//============================ Reading commnad line arguments with flags ===============//
	int i=0;
	for ( i = 1; i < argc; i++)  
		if ( !strcmp(argv[i], option ) )
			return i;
	return 0;
}


int rng( double * a, int n, int r, double * time, void (* rand_funk_simd)( double * a, int n, void * s, double b ) ){
	
	double t0, te; 
	
	
	
	#pragma omp parallel
	{
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	uint64_t *  seed;
	posix_memalign((void **)(&seed), 0x40, 64*sizeof(uint64_t));
	for(int i=0; i<64; i++ )	seed[i] = pvsc_rand_l_xorshift128plus();
	
	#pragma omp barrier
	if ( !tid )
	  get_walltime_(&t0);
	
	for(int j=0; j<r; j++){
		rand_funk_simd(a + n*tid, n, (void *)seed, -0.);
		}
	free(seed);
	}
	get_walltime_(&te);
	
	*time = te - t0;
	
	return 0;
}

int libc_rand( double * a, int n, int r, double * time){
	
	double t0, te; 
	double rcp32 = 1. / 0xFFFFFFFFU;
	get_walltime_(&t0);
	
	for(int j=0; j<r; j++){
		for(int i=0; i<n*nthreads; i++){
		  a[i] = rcp32*rand();
		}
	}
	get_walltime_(&te);
	
	*time = te - t0;
	
	return 0;
}

int libc_drand48( double * a, int n, int r, double * time){
	
	double t0, te; 
	double rcp32 = 1. / 0xFFFFFFFFU;
	get_walltime_(&t0);
	
	for(int j=0; j<r; j++){
		for(int i=0; i<n*nthreads; i++){
		  a[i] = drand48();
		}
	}
	get_walltime_(&te);
	
	*time = te - t0;
	
	return 0;
}



int main(int argc, char **argv) {
	
	uint64_t * s = pvsc_rand_seed128( 454765, 5467 );
	printf("seed = %lx %lx\n", s[0], s[1]);
	
	uint64_t sc[2];
	sc[0] = s[0];
	sc[1] = s[1];
	/*
	for(int i=0; i<10; i++) printf("%lx\n", pvsc_rand_l_xorshift128plus());
	for(int i=0; i<10; i++) printf("%.14e\n", pvsc_rand_d_xorshift128plus());
	printf("\n");
	for(int i=0; i<10; i++) printf("%lx\n", pvsc_rand_ls_xorshift128plus(sc));
	for(int i=0; i<10; i++) printf("%.14e\n", pvsc_rand_ds_xorshift128plus(sc));
	
	printf("seed = %lx %lx\n", s[0], s[1]);
	*/
	int n = 2000;
	int i;
	

	#pragma omp parallel
	{
		#pragma omp single
	{
		#ifdef _OPENMP
		nthreads = omp_get_num_threads();
		#endif
	}
	}
	
	i = read_args("-n" , argc, argv);
	if (i) n = atoi(argv[i+1]);
	
	if( n%64 )  n += 64 - n%64;
	
	
	printf("  n/nthreads = %d, nthreads %d \n", n, nthreads );
	double * a;
	posix_memalign((void **)(&a), 0x40, n*nthreads*sizeof(double));
	
	double time;
	int r;
	r=8;do { r *= 2;  libc_rand( a, n, r, &time );           }while (  time < 1. ); printf( "p:  %g rands/sec libs rand()\n",             (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  libc_drand48( a, n, r, &time );           }while (  time < 1. ); printf( "p:  %g rands/sec libs drand48()\n",             (double)(n)*nthreads*r/time );

	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_lgc32_simd );           }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_lgc32_simd\n",             (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_lgc64_simd );           }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_lgc64_simd\n",             (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_xorshift32_simd );      }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_xorshift32_simd  \n",      (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_xorshift64star_simd );      }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_xorshift64star_simd  \n",      (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_xorshift128_simd );     }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_xorshift128_simd  \n",     (double)(n)*nthreads*r/time );
	r=8;do { r *= 2;  rng( a, n, r, &time, pvsc_rand_ds_xorshift128plus_simd ); }while (  time < 4. ); printf( "p:  %g rands/sec pvsc_rand_ds_xorshift128plus_simd  \n", (double)(n)*nthreads*r/time );
	
	
	
	free(a);
	
	return EXIT_SUCCESS;
}
