#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>

typedef enum{
	COLLAPSE_DEFAULT  =  0,
	COLLAPSE_SQUARING =  1<<0,
	COLLAPSE_V        =  1<<1,
	COLLAPSE_V2       =  1<<2,
	COLLAPSE_O        =  1<<3,
	COLLAPSE_O2       =  1<<4,
	COLLAPSE_Z        =  1<<5,
	COLLAPSE_Y        =  1<<6,
	COLLAPSE_X        =  1<<7,
	COLLAPSE_C        =  1<<8
}pvsc_print_collapse;
	

int pvsc_vector_print( char * fname, pvsc_vector * vec, pvsc_print_collapse coll, pvsc_lidx_t c ){
	
	FILE * out = fopen( fname, "w");
	pvsc_volume v = vec->sys->v_mem;
	pvsc_lidx_t i[4],j[4],idx,idx0,xyz[4],k;
	pvsc_lidx_t nb  = vec->nb;
	pvsc_lidx_t ldo = 1;
	pvsc_lidx_t ldnb = 1;
	
	
	if ( coll & COLLAPSE_SQUARING ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx*vec->nb + k ] *= vec->val[ idx*vec->nb + k ];
	}}
	
	if( coll &  COLLAPSE_V2 ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=2)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k+1 ];
		}
		ldnb = 2;
		nb = vec->nb>>1;
	}else 	if ( coll & COLLAPSE_V ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset +    0*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=1; k<nb; k++)
				vec->val[ idx0*vec->nb + 0 ] += vec->val[ idx*vec->nb + k ];
		}
		ldnb = nb;
		nb = 1;
	}
	
	if( coll &  COLLAPSE_O2 ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=2){
			idx  = v.offset + (i[0]+1)*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset +  i[0]   *v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
		}
		ldo = 2;
		v.n[0] = v.n[0]>>1;
	}else if ( coll & COLLAPSE_O ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=1; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset +    0*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
		}
		ldo = v.n[0];
		v.n[0] = 1;
	}
	
	if ( coll & COLLAPSE_Z ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=1; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset + i[0]*v.s[0] +    0*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
		}
		v.n[1] = 1;
	}
	
	if ( coll & COLLAPSE_Y ){
		for( i[3]=0; i[3]<v.n[3]; i[3]++)
		for( i[2]=1; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset + i[0]*v.s[0] + i[1]*v.s[1] +    0*v.s[2] + i[3]*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
		}
		v.n[2] = 1;
	}
	
	if ( coll & COLLAPSE_X ){
		for( i[3]=1; i[3]<v.n[3]; i[3]++)
		for( i[2]=0; i[2]<v.n[2]; i[2]++)
		for( i[1]=0; i[1]<v.n[1]; i[1]++)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
			idx0 = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] +    0*v.s[3];
			for( k=0; k<nb; k+=ldnb)
				vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
		}
		v.n[3] = 1;
	}
	
	if ( coll & COLLAPSE_C ){
		for( j[3]=0; j[3]<v.n[3]; j[3]+=c)
		for( j[2]=0; j[2]<v.n[2]; j[2]+=c)
		for( j[1]=0; j[1]<v.n[1]; j[1]+=c)
		for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
			idx0 = v.offset + i[0]*v.s[0] + j[1]*v.s[1] + j[2]*v.s[2] + j[3]*v.s[3];
			for( i[3]=j[3]; (i[3]<j[3]+c) && (i[3]<v.n[3]); i[3]++)
			for( i[2]=j[2]; (i[2]<j[2]+c) && (i[2]<v.n[2]); i[2]++)
			for( i[1]=j[1]; (i[1]<j[1]+c) && (i[1]<v.n[1]); i[1]++){
				idx  = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
				if( idx - idx0 )
				for( k=0; k<nb; k+=ldnb)
					vec->val[ idx0*vec->nb + k ] += vec->val[ idx*vec->nb + k ];
			}
		}
	}
	
	double sum = 0.;
	
	//double * val
	
	for( i[3]=0; i[3]<v.n[3]; i[3]+=c){
	for( i[2]=0; i[2]<v.n[2]; i[2]+=c){
	for( i[1]=0; i[1]<v.n[1]; i[1]+=c){
	for( i[0]=0; i[0]<v.n[0]; i[0]+=ldo){
		idx = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
		//pvsc_vecidx2xyz( xyz, idx, v );
		pvsc_global_vecidx2xyz( xyz, idx, vec->sys );
		fprintf(out,"%d\t%d\t%d\t%d\t%d", idx, xyz[3], xyz[2], xyz[1], xyz[0]);
		
		if ( coll & COLLAPSE_V){
			fprintf(out,"\t%g\n", vec->val[ idx*vec->nb]);
			
		}else{
			for( k=0; k<nb; k+=ldnb){
				fprintf(out,"\t%g", vec->val[ idx*vec->nb + k ]);
				sum += vec->val[ idx*vec->nb + k ];
			}
			fprintf(out,"\n");
		}
	}
		if(v.n[0] > 1 ) fprintf(out,"\n");
	}
		if(v.n[1] > 1 ) fprintf(out,"\n");
	}
		if(v.n[2] > 1 ) fprintf(out,"\n");
	}
	
	printf("sum = %g\n", sum);
	
	fclose(out);
	return 0;
}


int main(int argc, char **argv) {
	
	int i,j;
	
	char * vector_file_0 = NULL;
	char * vector_file_1 = NULL;
	char * print_file  = NULL;
	char   print_file_default[]  = "data.dat";
	
	
	i = pvsc_read_args("-v" , argc, argv);
	if(i){
		vector_file_0 = argv[i+1];
	}else{
		printf("no path to vector_file\n");
		printf("using: %s -v <vector_file>\n", argv[0]);
		return EXIT_SUCCESS;
	}
	
	
	i = pvsc_read_args("-d" , argc, argv);
	if(i){
		vector_file_1 = argv[i+1];
	}
	
	pvsc_print_collapse coll = COLLAPSE_DEFAULT;

	i = pvsc_read_args("-cX" , argc, argv);
	if(i) coll |= COLLAPSE_X;
	i = pvsc_read_args("-cY" , argc, argv);
	if(i) coll |= COLLAPSE_Y;
	i = pvsc_read_args("-cZ" , argc, argv);
	if(i) coll |= COLLAPSE_Z;
	i = pvsc_read_args("-cO" , argc, argv);
	if(i) coll |= COLLAPSE_O;
	i = pvsc_read_args("-cV" , argc, argv);
	if(i) coll |= COLLAPSE_V;
	
	i = pvsc_read_args("-cS" , argc, argv);
	if(i) coll |= COLLAPSE_SQUARING;
	i = pvsc_read_args("-cV2" , argc, argv);
	if(i) coll |= COLLAPSE_V2;
	i = pvsc_read_args("-cO2" , argc, argv);
	if(i) coll |= COLLAPSE_O2;
	
	pvsc_lidx_t c = 1;
	i = pvsc_read_args("-cC" , argc, argv);
	if(i){ coll |= COLLAPSE_C;
	       c = atoi(argv[i+1]);
	}
	
	pvsc_vector vec[2] = { PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER };
	
	pvsc_vector_read( vector_file_0, vec );
	if( vector_file_1 ) {
		pvsc_vector_read( vector_file_1, vec+1 );
		double * a, *b;
		pvsc_malloc((void**) &(a), sizeof(double), vec->nb, true, "scale a", 0 );
		pvsc_malloc((void**) &(b), sizeof(double), vec->nb, true, "scale b", 0 );
		for(pvsc_lidx_t i=0; i<vec->nb; i++){
			a[i] = -1.;
			b[i] =  1.;
		}
		pvsc_vector_axpby( b, vec, a, vec+1 );
		free(a);
		free(b);
		pvsc_destroy_vector( vec+1 );
	}
	
	printf("vec prob:\n");
	printf(" nb = %d \n", vec->nb);
	printf(" nx = %d \n", vec->sys->n_x);
	printf(" ny = %d \n", vec->sys->n_y);
	printf(" ny = %d \n", vec->sys->n_z);
	printf(" no = %d \n", vec->sys->n_o);
	
	i = pvsc_read_args("-o" , argc, argv);
	if(i)
		print_file = argv[i+1];
	else
		print_file = print_file_default;
	
	pvsc_vector_print( print_file, vec,  coll, c );
	
	pvsc_destroy_vector( vec );
	
	return EXIT_SUCCESS;
}
