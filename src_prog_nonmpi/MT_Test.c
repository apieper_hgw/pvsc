#include <stdio.h>
#include <sys/time.h>

#include "mkl_vsl.h"

#define N 2000
#define REP 10000000

void get_walltime(double* wcTime) {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  *wcTime = (double)(tp.tv_sec + tp.tv_usec/1000000.0);
}

int main()
{
   double s; /* average */
   double wc_start,wc_end;
   int i, j;
    
   /* Initializing */        
   s = 0.0;
   #pragma omp parallel reduction(+:s)
{
   double r[N]; /* buffer for random numbers */
   VSLStreamStatePtr stream;
   vslNewStream( &stream, VSL_BRNG_SFMT19937, 678 );

   #pragma omp single
   get_walltime(&wc_start);    
   /* Generating */        
   #pragma omp for
   for (int i=0; i<REP; i++ )
   {
      // vdRngGaussian( VSL_RNG_METHOD_GAUSSIAN_ICDF, stream, N, r, 5.0, 2.0 );
      vdRngUniform( VSL_RNG_METHOD_UNIFORM_STD, stream, N, r, 0.0, 1.0 );
      for (int j=0; j<N; j++ )
      {
         s += r[j];
      }
   }
   #pragma omp single
   get_walltime(&wc_end);    
   /* Deleting the stream */        
   vslDeleteStream( &stream );
}
   s /= (double)REP*N;
    
    
   /* Printing results */        
   printf( "Sample mean of normal distribution = %.15lf\n", s );
   printf( "Performance: %lf GRN/s\n", (double)REP*N/(wc_end-wc_start)/1.e9 );
    
   return 0;
}




