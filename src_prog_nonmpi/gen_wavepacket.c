#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>

#include <math.h>


typedef struct 
{
	double delta[3];
	double r_s[3];
	double k[3];
	double *phase;
    
}pvsc_wavepacket_conf;


double pvsc_wavepacket_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * p, void * seed  ){
	
	pvsc_wavepacket_conf * parm = (pvsc_wavepacket_conf *)p;
	
	double tmp = exp( -(n[3]-parm->r_s[0])*(n[3]-parm->r_s[0])/(0.25*parm->delta[0]*parm->delta[0]) 
	                  -(n[2]-parm->r_s[1])*(n[2]-parm->r_s[1])/(0.25*parm->delta[1]*parm->delta[1])
	                  -(n[1]-parm->r_s[2])*(n[1]-parm->r_s[2])/(0.25*parm->delta[2]*parm->delta[2]) );
	
	if ( !k&1 ) tmp *= cos( M_PI*( n[3]*parm->k[0] + n[2]*parm->k[1] + n[1]*parm->k[2] + parm->phase[n[0]] ) );
	else        tmp *= sin( M_PI*( n[3]*parm->k[0] + n[2]*parm->k[1] + n[1]*parm->k[2] + parm->phase[n[0]] ) );
	
	return tmp;
}



int main(int argc, char **argv) {
	
	int i;
	
	pvsc_system_geo sys[1];
	pvsc_lidx_t nb;
	char * file  = NULL;
	char   file_default[]  = "wavepacket.vec";
	
	i = pvsc_read_args("-o" , argc, argv);
	if(i) file = argv[i+1];
	else  file = file_default;
	
	sys->finalised=false;
	pvsc_init_system_geo_mpicomm( sys,  MPI_COMM_WORLD, 1,1,1 );
	
	i = pvsc_read_args("-size" , argc, argv);
	if(i) {
		sys->n_x = atoi(argv[i+1]);
		sys->n_y = atoi(argv[i+2]);
		sys->n_z = atoi(argv[i+3]);
	}else{
		return EXIT_SUCCESS;
	}
	
	
	
	i = pvsc_read_args("-n_o" , argc, argv);
	if(i) {
		sys->n_o = atoi(argv[i+1]);
	}else{
		sys->n_o = 1;
	}
	
	pvsc_finalise_system_geo( sys );
	
	i = pvsc_read_args("-nb" , argc, argv);
	if(i) {
		nb = atoi(argv[i+1]);
	}else{
		nb = 1;
	}
	
	pvsc_wavepacket_conf wp_conf;
	
	i = pvsc_read_args("-r_s" , argc, argv);
	if(i) {
		wp_conf.r_s[0] = atof(argv[i+1]);
		wp_conf.r_s[1] = atof(argv[i+2]);
		wp_conf.r_s[2] = atof(argv[i+3]);
	}else{
		wp_conf.r_s[0] = 0.5*sys->n_x;
		wp_conf.r_s[1] = 0.5*sys->n_y;
		wp_conf.r_s[2] = 0.5*sys->n_z;
	}
	
	i = pvsc_read_args("-delta" , argc, argv);
	if(i) {
		wp_conf.delta[0] = atof(argv[i+1]);
		wp_conf.delta[1] = atof(argv[i+2]);
		wp_conf.delta[2] = atof(argv[i+3]);
	}else{
		wp_conf.delta[0] = 0.125*sys->n_x;
		wp_conf.delta[1] = 0.125*sys->n_y;
		wp_conf.delta[2] = 0.125*sys->n_z;
	}
	
	i = pvsc_read_args("-k" , argc, argv);
	if(i) {
		wp_conf.k[0] = atof(argv[i+1]);
		wp_conf.k[1] = atof(argv[i+2]);
		wp_conf.k[2] = atof(argv[i+3]);
	}else{
		wp_conf.k[0] = 0.;
		wp_conf.k[1] = 0.;
		wp_conf.k[2] = 0.;
	}
	
	wp_conf.phase = malloc(sizeof(double)*sys->n_o);
	i = pvsc_read_args("-phase" , argc, argv);
	if(i) {
		for(int j=0; j<sys->n_o; j++)
			wp_conf.phase[j] = atof(argv[i+1+j]);
	}else{
		for(int j=0; j<sys->n_o; j++)
			wp_conf.phase[j] = 0.;
	}
	
	
	//pvsc_finalise_system_geo( sys );
	
	pvsc_vector vec[1];
	pvsc_create_vector( vec, nb, sys);
	
	pvsc_vector_from_func( vec, pvsc_wavepacket_vec, &wp_conf);
	//pvsc_vector_make_wavepacket( vec, delta, r_s );
	
	pvsc_vector_write( file, vec);
	
	return EXIT_SUCCESS;
}
