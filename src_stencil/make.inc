
ifdef (ALLSTENCIL)
  STECDATA    := $(wildcard $(STECSRCDIR)/*.stec)
else
  STECDATA += $(STECSRCDIR)/spmvm_stencil_2dlattice.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_graphene.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_graphene_short.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_graphene_short_rand.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_graphene_short_const.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_3dlattice.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_3dlattice_rng32.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_3dlattice_none.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_topi.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_weyl.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_wsm.stec
  STECDATA += $(STECSRCDIR)/spmvm_stencil_tri.stec
#  STECDATA += $(STECSRCDIR)/spmvm_stencil_2dim4pointfix.stec
#  STECDATA += $(STECSRCDIR)/spmvm_stencil_2dim5point_cv.stec
#  STECDATA += $(STECSRCDIR)/spmvm_stencil_graphene2b.stec
#  STECDATA += $(STECSRCDIR)/spmvm_stencil_3dlattice4b.stec
endif

INCDIR_STENCIL_KERNELS = $(INCDIR)/pvsc_stencil_kernels
SRCDIR_STENCIL_KERNELS = $(SRCDIR)/stencil_kernels


OUT_DIR += $(INCDIR_STENCIL_KERNELS)
OUT_DIR += $(SRCDIR_STENCIL_KERNELS)


STECSOURCES := $(STECDATA:$(STECSRCDIR)/%.stec=$(SRCDIR_STENCIL_KERNELS)/%.c)

#INCLUDES    += $(STECDATA:$(STECSRCDIR)/%.stec=$(INCDIR_STENCIL_KERNELS)/%.h)

STECOBJECTS     := $(STECDATA:$(STECSRCDIR)/%.stec=$(OBJDIR)/%.o)
STECOBJECTS_MPI := $(STECDATA:$(STECSRCDIR)/%.stec=$(OBJDIR_MPI)/%.o)

STEREPOS = stencil_repos
STEREPOSSRC = $(SRCDIR_STENCIL_KERNELS)/$(STEREPOS).c

STECOBJECTS     += $(OBJDIR)/$(STEREPOS).o
STECOBJECTS_MPI += $(OBJDIR_MPI)/$(STEREPOS).o


$(STECOBJECTS): $(OBJDIR)/%.o : $(SRCDIR_STENCIL_KERNELS)/%.c
	@echo "Compiled "$@
	@$(CC) $(CFLAGS) $(IPATH) -c $< -o $@

$(STECOBJECTS_MPI): $(OBJDIR_MPI)/%.o : $(SRCDIR_STENCIL_KERNELS)/%.c
	@echo "Compiled "$@
	@$(MPICC) $(CFLAGS_MPI) $(IPATH) -c $< -o $@


$(STECSOURCES): $(SRCDIR_STENCIL_KERNELS)/%.c : $(STECSRCDIR)/%.stec src_stencil/stencil_gen.py $(INCDIR_STENCIL_KERNELS) $(SRCDIR_STENCIL_KERNELS)
	@echo "CodeGen "$@
	@python src_stencil/stencil_gen.py -s $< -o $@

$(STEREPOSSRC) : $(SRCDIR_STENCIL_KERNELS)/%.c : $(STECSRCDIR)/%.py $(STECDATA)                 $(INCDIR_STENCIL_KERNELS) $(SRCDIR_STENCIL_KERNELS)
	@echo "CodeGen "$@
	@python $< $@ $(STECDATA)

.PHONY: kernels_src kernels

kernels: $(STECOBJECTS)

kernels_src: $(STECSOURCES)
