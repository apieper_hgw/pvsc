

import sys, getopt
import numpy

prog_name = sys.argv[0]
argv = sys.argv[1:]

stencil_file="none"
output_file="none"

name="none"
dim = 3
size= 1

n_scalars = 0
scalar_default = []
n_arrays  = 0

r_arrays = 0
r_scale = []
r_shift = []

try:
	opts, args = getopt.getopt(argv,"s:o:",["help"])
except getopt.GetoptError:
	print prog_name,'getopt Error!'
	sys.exit(2)
for opt, arg in opts:
	if opt == '--help':
		print  prog_name,' ... help'
		sys.exit()
	elif opt in ("-o"):
		output_file = arg
	elif opt in ("-s"):
		stencil_file = arg

rline = stencil_file.split(".")
line = rline[0].split("/")
name = line[-1]


fread  = open(stencil_file,'r')

nn_num = 0
read_nn_vals = False
read_nn_vals_num = 0

hopping_mats_v   = []
reading_mat_v  = []
reading_mat_sca  = []
reading_mat_ary   = []
reading_mat_rnd   = []
links = []
complex_info = 'false'
ldx = '1.'
ldy = '1.'
ldz = '1.'
flop_per_lup = 0




for rline in fread:
	line = rline.split(" ")
	#if   line[0] == 'name':
	#	name = line[1]
	if line[0] == 'dim':
		dim  = int(line[1])
	elif line[0] == 'size':
		size = int(line[1])
	elif line[0] == 'ldx':
		ldx = line[1]
	elif line[0] == 'ldy':
		ldy = line[1]
	elif line[0] == 'ldz':
		ldz = line[1]
	elif line[0] == 'n_scalars': # obsolate
		n_scalars = int(line[1]) # obsolate
	elif line[0] == 'n_coeff_const':
		n_scalars = int(line[1])
	elif line[0] == 'scalar_default': # obsolate
		for i in range(0,n_scalars): # obsolate
			scalar_default.append(float(line[i+1])) # obsolate
	elif line[0] == 'coeff_const_default':
		for i in range(0,n_scalars):
			scalar_default.append(float(line[i+1]))
	elif line[0] == 'n_arrays': # obsolate
		n_arrays = int(line[1]) # obsolate
	elif line[0] == 'n_coeff_variable':
		n_arrays = int(line[1])
	elif line[0] == 'r_arrays': # obsolate
		r_arrays = int(line[1]) # obsolate
	elif line[0] == 'n_coeff_rand':
		r_arrays = int(line[1])
	elif line[0] == 'r_scale':
		for i in range(0,r_arrays):
			r_scale.append(float(line[i+1]))
	elif line[0] == 'r_shift':
		for i in range(0,r_arrays):
			r_shift.append(float(line[i+1]))
	elif line[0] == 'complex':
		complex_info = 'true'
	elif line[0] == 'nn':
		nn_num += 1
		
		if read_nn_vals:
			hopping_mats_v.append([ links ,reading_mat_v, reading_mat_sca, reading_mat_ary, reading_mat_rnd])
			read_nn_vals_num = 0
		
		read_nn_vals = True
		reading_mat_v  = range(0,size)
		reading_mat_sca  = range(0,size)
		reading_mat_ary   = range(0,size)
		reading_mat_rnd   = range(0,size)
		for i in range(0,size):
			reading_mat_v[i]  = []
			reading_mat_sca[i]  = []
			reading_mat_ary[i]   = []
			reading_mat_rnd[i]   = []
			
		links = []
		if dim<2:
			links.append(0)
		if dim<3:
			links.append(0)
		for i in range(0,dim):
			links.append(int(line[i+1]))
	elif read_nn_vals:
		vline = rline.split(";")
		
		#entry_len = len(entry)
		if vline[0][0] == "l":
			linenum = int(vline[0][1:])
			if  linenum >= read_nn_vals_num:
				read_nn_vals_num = linenum
			vline = vline[1:]
		
		for entry in vline:
			
			entry = entry.split("|")
			if entry.__len__() > 1:
				if   entry[1] == 'c':  # c for const
					reading_mat_sca[read_nn_vals_num].append( [ int(entry[0]) , int(entry[2])] )
				elif entry[1] == 'v':  # v for variable
					reading_mat_ary[read_nn_vals_num].append( [ int(entry[0]) , int(entry[2])] )
				elif entry[1] == 'r':  # r for rand
					reading_mat_rnd[read_nn_vals_num].append( [ int(entry[0]) , int(entry[2])] )
				elif entry[1] == 'f':  # f for fixed
					reading_mat_v[read_nn_vals_num].append( [ int(entry[0]) , float(entry[2])] )
				else:
					reading_mat_v[read_nn_vals_num].append( [ int(entry[0]) , float(entry[1])] )
		
		read_nn_vals_num +=1
		#if read_nn_vals_num == size:
		#	hopping_mats_v.append([ links ,reading_mat_v, reading_mat_sca, reading_mat_ary, reading_mat_rnd])
		#	read_nn_vals_num = 0
		#	read_nn_vals = False

if read_nn_vals:
	hopping_mats_v.append([ links ,reading_mat_v, reading_mat_sca, reading_mat_ary, reading_mat_rnd])
	read_nn_vals_num = 0

if len(r_scale) < r_arrays:
	n = r_arrays - len(r_scale)
	for i in range(0,n):
		r_scale.append(1.)

if len(r_shift) < r_arrays:
	n = r_arrays - len(r_shift)
	for i in range(0,n):
		r_shift.append(-0.5)


#print name, dim, size
#print hopping_mats_v[0]
#print hopping_mats_v[0][0]
#print hopping_mats_v[0][1][2][0][1]
#print hopping_mats_i
#print 'nn: ', nn_num

fread.close()

dependent_data_trans = False
for i in range(0,nn_num):
	if hopping_mats_v[i][0][0] != 0 and hopping_mats_v[i][0][1]:
		dependent_data_trans = True
	if hopping_mats_v[i][0][0] != 0 and hopping_mats_v[i][0][2]:
		dependent_data_trans = True
	if hopping_mats_v[i][0][1] != 0 and hopping_mats_v[i][0][2]:
		dependent_data_trans = True
	


str = """include/pvsc_stencil_kernels/{name}.h""".format(name=name, size=size)
fwrite = open( str,'w')
str = """
#ifndef PVSC_{name}_H
#define PVSC_{name}_H

#include "pvsc/types.h"

int pvsc_{name}( pvsc_spmvm_handel_t * p  );

pvsc_system_hamiltonian pvsc_make_stencil_hamiltionian_{name}( );


#endif
""".format(name=name)
fwrite.write(str)
fwrite.close()

#str = """src_stencil/{name}.c""".format(name=name, size=size)
fwrite = open( output_file,'w')

str = """
#define _XOPEN_SOURCE 600
#include "pvsc.h"
#include "pvsc/rand.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif
#ifdef _OPENMP
#include <omp.h>
#endif








int pvsc_{name}( pvsc_spmvm_handel_t * p  ){{
	int timing_num_all = pvsc_timing_init( "pvsc_{name}_all" );
	#if PVSC_VERBOSE>4
		pvsc_printf(" call pvsc_{name} .. \\n");
	#endif
	int nthreads = 1;
	pvsc_lidx_t nb = p->y->nb;
	pvsc_volume      v     = p->y->sys->v;
	pvsc_volume      v_mem = p->y->sys->v_mem;
	pvsc_system_geo * sys = p->y->sys;
	
	double *scale_h, *shift_h, *scale_z;
	if( p->scale_h )  scale_h = p->scale_h; else {{ scale_h = pvsc_malloc(NULL, sizeof(double), nb, true, NULL, 35694 ); for( pvsc_lidx_t i=0; i<nb; i++ ) scale_h[i] = 1.; }}
	if( p->shift_h )  shift_h = p->shift_h; else {{ shift_h = pvsc_malloc(NULL, sizeof(double), nb, true, NULL, 35695 ); for( pvsc_lidx_t i=0; i<nb; i++ ) shift_h[i] = 0.; }}
	if( p->scale_z )  scale_z = p->scale_z; else {{ scale_z = pvsc_malloc(NULL, sizeof(double), nb, true, NULL, 35696 ); for( pvsc_lidx_t i=0; i<nb; i++ ) scale_z[i] = 0.; }}
	
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	int err = 0;
	if( sys->n_o != {size} ) {{
		printf( "pvsc_{name}: error sys->n_o != {size}\\n" );
		err++;
	}}
	
	
	
	//err += pvsc_aligned_check(p->dot_xx, "pvsc_{name}: error: p->dot_xx");
	//err += pvsc_aligned_check(p->dot_xx, "pvsc_{name}: error: p->dot_xy");
	//err += pvsc_aligned_check(p->dot_xx, "pvsc_{name}: error: p->dot_yy");
""".format(name=name, size=size)
fwrite.write(str)

if n_scalars:
	str = """	err += pvsc_aligned_check(p->mat->ham_scalars, "pvsc_{name}: error: p->mat->ham_scalars");\n""".format(name=name)
	fwrite.write(str)
if n_scalars:
	str = """	err += pvsc_aligned_check(p->mat->ham_array, "pvsc_{name}: error: p->mat->ham_array");\n""".format(name=name)
	fwrite.write(str)

str = """
	if (err) return err;
	#endif
""".format(name=name, size=size)
fwrite.write(str)



if dependent_data_trans:
	fwrite.write( "\tpvsc_vector_datatransfer_dependent( p->x );\n" )
else:
	fwrite.write( "\tpvsc_vector_datatransfer(           p->x );\n" )

str = """
	double *              x    = p->x->val;
	pvsc_lidx_t           x_nb = p->x->nb;
	double * restrict     y    = p->y->val;
	pvsc_lidx_t           y_nb = p->y->nb;
	double *              z    = NULL;
	pvsc_lidx_t           z_nb = 0;
	if ( p->z )          {{z    = p->z->val; 
	                       z_nb = p->z->nb;}}
	
	pvsc_lidx_t  no = sys->n_o;
	pvsc_lidx_t ldx = sys->s_x;
	pvsc_lidx_t ldy = sys->s_y;
	pvsc_lidx_t ldz = sys->s_z;
	pvsc_lidx_t ldv  = p->mat->ham->n_arrays;
	
	double * coeff_c = p->mat->ham_scalars;
	double * coeff_v = p->mat->ham_array;
	
	int timing_num_comp;
	double **dot_xx, **dot_xy, **dot_yy;
	
	switch(nb){{
""".format( size=size)
fwrite.write(str)

#for nb in [0]:
#for nb in [1,2,3,4,5,6,7,8,0]:
for nb in [1,2,3,4,8,0]:
	pk0 = ""
	x_nb = "x_nb"
	y_nb = "y_nb"
	z_nb = "z_nb"
	
	if nb == 0:
		fwrite.write( "\tdefault:\n" )
		nb = "k_nb"
		pk0 = "+k0"
		str = """
	p->nb_block = nb;
	//p->nb_block = 2*PVSC_OPT_BLOCK_SIZE;
	// Diese Blocking auf cachlines bringt nicht wirklich was :-( ... war vorher besser, gerade fuer nb=1 
	for( pvsc_lidx_t k0=0; k0<nb; k0+=p->nb_block ){{
	pvsc_lidx_t k_nb = p->nb_block;
	if( k0+k_nb > nb ) k_nb = nb-k0;\n""".format()
		fwrite.write(str)
	else:
		fwrite.write( "\tcase %d:" % ( nb ) )
		x_nb = nb
		y_nb = nb
		z_nb = nb
	
	k_unrolling = False
	if nb in [1,2,3,4]:
		k_unrolling = True
	
	str = """
#pragma omp parallel
	{{
#pragma omp single
{{
	#ifdef _OPENMP
	nthreads = omp_get_num_threads();
	#endif
	if( p->dot_xx || p->dot_xy || p->dot_yy ) {{
		dot_xx = malloc(sizeof(double *) * nthreads);
		dot_xy = malloc(sizeof(double *) * nthreads);
		dot_yy = malloc(sizeof(double *) * nthreads);
	}}
}}	
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	double * restrict y_tmp, * restrict x_tmp; 
	double * restrict xx, * restrict xy, * restrict yy;""".format( size=size)
	fwrite.write(str)
	
	if k_unrolling :
		str = """
	posix_memalign( (void**) &y_tmp, 0x100, sizeof(double) * {nb}*{size}*(sys->n_z+sys->b_z) );
	posix_memalign( (void**) &x_tmp, 0x100, sizeof(double) * {nb}*{size}*(sys->n_z+sys->b_z) );
		""".format( size=size, nb=nb)
		fwrite.write(str)
	else :
		str = """
	posix_memalign( (void**) &y_tmp, 0x100, sizeof(double) * {nb}*{size} );
	posix_memalign( (void**) &x_tmp, 0x100, sizeof(double) * {nb}*{size} );
		""".format( size=size, nb=nb)
		fwrite.write(str)
	
	
	for i in range(0,r_arrays):
		str = """
		double * coeff_r_{i};
		posix_memalign( (void**) &coeff_r_{i}, 0x100, sizeof(double) * (sys->n_z+sys->b_z + (PVSC_RAND_IDUM32>>1))  );
		
		""".format( i=i)
		fwrite.write(str)
	
	str = """
	uint32_t idum32[  PVSC_RAND_IDUM32];
	uint64_t * idum64 = (uint64_t *)idum32;
	uint64_t seed128[2] =  {{ 0x775095e9382798c3ULL, 0xc39cf74baa73568fULL }};
	seed128[0] += 1367*sys->mpi_rank;
	seed128[1] += 217*tid;
	for(pvsc_lidx_t k=0; 2*k<PVSC_RAND_IDUM32; k++) idum64[k] = pvsc_rand_ls_xorshift128plus( seed128 );
	if( PVSC_RAND_IDUM32 & 1 ) idum32[PVSC_RAND_IDUM32-1] = (uint32_t)(pvsc_rand_ls_xorshift128plus( seed128 ));
	double qdrand32_norm = 1. / 0xFFFFFFFFU;
	double qdrand64_norm = 1. / 0xFFFFFFFFFFFFFFFFULL;
	
	if( p->dot_xx || p->dot_xy || p->dot_yy ) {{
		posix_memalign((void**) &xx, 0x100, sizeof(double) * {nb} ); for(pvsc_lidx_t k=0; k<{nb}; k++) xx[k] = 0.; dot_xx[tid] = xx;
		posix_memalign((void**) &xy, 0x100, sizeof(double) * {nb} ); for(pvsc_lidx_t k=0; k<{nb}; k++) xy[k] = 0.; dot_xy[tid] = xy;
		posix_memalign((void**) &yy, 0x100, sizeof(double) * {nb} ); for(pvsc_lidx_t k=0; k<{nb}; k++) yy[k] = 0.; dot_yy[tid] = yy;
	}}
	
#pragma omp single
	timing_num_comp = pvsc_timing_init( "pvsc_{name}_{nb}/comp" );
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerStartRegion("pvsc_{name}_{nb}/comp");
	#endif
	#if PVSC_VERBOSE>4
		pvsc_printf("   start comp pvsc_{name}_{nb}.. \\n");
	#endif
	\n""".format(name=name, size=size, nb=nb)
	fwrite.write(str)
	
	for z_mode in [0,1,2]:
		if z_mode == 0:
			fwrite.write( "\t\tif( !p->z ) ")
		elif z_mode == 1:
			fwrite.write( "\t\telse if( p->z == p->y ) ")
		elif z_mode == 2:
			fwrite.write( "\t\telse ")
		fwrite.write( "{\n")
		
		for dot_mode in [0,1]:
			if dot_mode == 0:
				fwrite.write( "\t\tif( p->dot_xx || p->dot_xy || p->dot_yy ) ")
			else:
				fwrite.write( "\t\telse ")
			fwrite.write( "{\n")
				
			fwrite.write( "\t\tpvsc_lidx_t  i1_0   =          sys->b_z;\n" )
			fwrite.write( "\t\tpvsc_lidx_t  i1_end = sys->n_z+sys->b_z;\n" )
			
			if dim > 2:
				str = """
				pvsc_lidx_t y_block = (pvsc_lidx_t) ( PVSC_CACHE_SIZE_CPU / (1.3*((nthreads*({n_arrays}+4*{size})+3*{size})*{nb}*sys->n_z*sizeof(double))));
				while( (sys->n_y%y_block) && ( sys->n_y%y_block < y_block/2 ) && (sys->n_y > y_block) ) y_block++;
				
				//y_block = sys->n_y;  // this line disable blocking
				#if PVSC_VERBOSE>4
				if( !tid ) pvsc_printf(" y_block = %d  mod = %d\\n", y_block, sys->n_y%y_block );
				#endif
				for(pvsc_lidx_t i2_0=sys->b_y; i2_0<sys->n_y+sys->b_y; i2_0+=y_block ) {{
				pvsc_lidx_t                      i2_end = i2_0+y_block;
				if( i2_end > sys->n_y+sys->b_y ) i2_end = sys->n_y+sys->b_y;
				//#pragma omp barrier\n""".format(nb=nb, size=size, n_arrays=n_arrays )
				fwrite.write(str)
				fwrite.write( "\t\t#pragma omp for collapse(2) schedule(static,1) nowait\n" )
				fwrite.write( "\t\tfor(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)\n" )
			else:
				fwrite.write( "\t\tpvsc_lidx_t const i3=sys->b_x;\n" )
			
			if dim == 2:
				str = """
				pvsc_lidx_t z_block = (pvsc_lidx_t) ( PVSC_CACHE_SIZE_CPU / (1.3*((nthreads*({n_arrays}+2*{size})+2*{size})*{nb}*sizeof(double))));
				while( (sys->n_z%z_block) && ( sys->n_z%z_block < z_block/2 ) && (sys->n_z > z_block) ) z_block++;
				
				//z_block = sys->n_z;  // this line disable blocking
				#if PVSC_VERBOSE>4
				if( !tid ) pvsc_printf(" z_block = %d  mod = %d\\n", z_block, sys->n_z%z_block );
				#endif
				
				//pvsc_printf("z_block  %d\\n",z_block);
				for( i1_0=sys->b_z; i1_0<sys->n_z+sys->b_z; i1_0+=z_block ) {{
				                                 i1_end = i1_0+z_block;
				if( i1_end > sys->n_z+sys->b_z ) i1_end = sys->n_z+sys->b_z;
				//#pragma omp barrier\n""".format(nb=nb, size=size, n_arrays=n_arrays)
				fwrite.write(str)
			
			#if k_unrolling :
				#fwrite.write( "\t\t#pragma omp for nowait\n" )
			#else :
				#fwrite.write( "\t\t#pragma omp for nowait\n" )
				#fwrite.write( "\t\t#pragma omp for collapse(2) nowait\n" )
			if dim >2:
				fwrite.write( "\t\tfor(pvsc_lidx_t i2=i2_0; i2<i2_end; i2++)\n" )
			else:
				fwrite.write( "\t\t#pragma omp for schedule(static,1) nowait\n" )
				fwrite.write( "\t\tfor(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)\n" )
			fwrite.write( "\t\t{\n" )
			
			for i in range(0,r_arrays):
				str = """
				if( p->mat->rand_simd_func_ptr ) p->mat->rand_simd_func_ptr( coeff_r_{i}, i1_end-i1_0, (void *)idum64, 0. );
				else for(int i=0;i< i1_end-i1_0; i++) coeff_r_{i}[i]=0.;\n""".format( i=i)
				fwrite.write(str)
			
			#if k_unrolling :
			if nb == 1 :
				fwrite.write( "\t\t#pragma vector aligned\n")
			str = """\t\tfor(pvsc_lidx_t i1=i1_0; i1<i1_end; i1++){{ 
				pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
				pvsc_lidx_t idx  = {size} * idxa;\n""".format( nb=nb, pk0=pk0, size=size )
			fwrite.write(str)
			
			k_list = "k"
			
			#if k_unrolling :
			if nb == 1 :
				#fwrite.write("\t\t\t#pragma unroll\n", nb)
				str = """\t\t\t#pragma unroll ({nb})\n""".format( nb=nb )
				#fwrite.write(str)
				k_list = range(0,nb)
			else :
				fwrite.write("\t\t\t#pragma vector aligned\n")
				str = """\t\t\tfor(pvsc_lidx_t k=0; k<{nb}; k++) {{\n""".format( nb=nb, pk0=pk0 )
				fwrite.write(str)
			
			for k in k_list:
				flop_per_lup = 0
				
				for i in range(0,size):
					if k_unrolling :
						str = """\t\t\t\t\ty[ (idx+{i})*{y_nb} + {k}{pk0} ] = """.format( i=i, nb=nb, y_nb=y_nb, x_nb=x_nb, size=size, k=k, pk0=pk0 )
						fwrite.write(str)
					else :
						str = """\t\t\t\t\ty[ (idx+{i})*{y_nb} + {k}{pk0} ] = """.format(  i=i, nb=nb, y_nb=y_nb, x_nb=x_nb, size=size, k=k, pk0=pk0 )
						fwrite.write(str)
						
					if z_mode == 1:
						str = """ scale_z[{k}{pk0}] * y[ (idx+{i})*{y_nb} + {k}{pk0}] + """.format( i=i, nb=nb, x_nb=x_nb, y_nb=y_nb, size=size, k=k, pk0=pk0)
						fwrite.write(str)
					if z_mode == 2:
						str = """ scale_z[{k}{pk0}] * z[ (idx+{i})*{z_nb} + {k}{pk0}] + """.format( i=i, nb=nb, x_nb=x_nb, z_nb=z_nb, size=size, k=k, pk0=pk0)
						fwrite.write(str)
					
					str = """scale_h[{k}{pk0}] * ( -shift_h[{k}{pk0}] * x[ (idx+{i})*{x_nb} + {k}{pk0} ]""".format( i=i, nb=nb, x_nb=x_nb, z_nb=z_nb, size=size, k=k, pk0=pk0)
					flop_per_lup += 4
					fwrite.write(str)
					for hm in hopping_mats_v:
						for h in hm[1][i]:
							flop_per_lup += 2
							fwrite.write("\n\t\t\t\t\t\t\t%+f * x[(idx + %d + %d*( %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
							str = """*{x_nb} + {k}{pk0}]""".format( k=k, x_nb=x_nb, pk0=pk0)
							fwrite.write(str)
						for h in hm[2][i]:
							flop_per_lup += 2
							fwrite.write("\n\t\t\t\t\t\t\t+coeff_c[%d] * x[(idx + %d + %d*( %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
							str = """*{x_nb} + {k}{pk0}]""".format( k=k, x_nb=x_nb, pk0=pk0)
							fwrite.write(str)
						for h in hm[3][i]:
							flop_per_lup += 2
							fwrite.write("\n\t\t\t\t\t\t\t+coeff_v[idxa*ldv+%d] * x[(idx + %d + %d*( %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
							str = """*{x_nb} + {k}{pk0}]""".format( k=k, x_nb=x_nb, pk0=pk0)
							fwrite.write(str)
						for h in hm[4][i]:
							flop_per_lup += 2
							fwrite.write("\n\t\t\t\t\t\t\t +(coeff_r_%d[i1-i1_0] %+f)*(%+f) * x[(idx + %d + %d*( %d + ldz*( %d + ldy * (%d) ) ) )" %(  h[1], r_shift[h[1]], r_scale[h[1]], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
							str = """*{x_nb} + {k}{pk0}]""".format( k=k, x_nb=x_nb, pk0=pk0)
							fwrite.write(str)
					fwrite.write(" );\n")
					
					if dot_mode == 0:
						str = """\t\t\t\txx[{k}] +=  x[ (idx+{i})*{x_nb} + {k}{pk0} ] * x[ (idx+{i})*{x_nb} + {k}{pk0} ];\n""".format( k=k, i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb,  pk0=pk0)
						#fwrite.write(str)
						str = """\t\t\t\txy[{k}] +=  x[ (idx+{i})*{x_nb} + {k}{pk0} ] * y[ (idx+{i})*{y_nb} + {k}{pk0} ];\n""".format( k=k, i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb,  pk0=pk0)
						#fwrite.write(str)
						str = """\t\t\t\tyy[{k}] +=  y[ (idx+{i})*{y_nb} + {k}{pk0} ] * y[ (idx+{i})*{y_nb} + {k}{pk0} ];\n""".format( k=k, i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb,  pk0=pk0)
						#fwrite.write(str)
			
			#if not(k_unrolling) :
			if nb != 1 :
				str = """\t\t\t }}  // close k-loop\n""".format()
				fwrite.write(str)
			
			if not(k_unrolling) and dot_mode == 0 :
				
				str = """\t\t\tif( p->dot_xx || p->dot_xy || p->dot_yy ) {{\n\t\t\t#pragma vector aligned\n\t\t\tfor(pvsc_lidx_t k=0; k<{nb}; k++) {{\n""".format( nb=nb, pk0=pk0 )
				fwrite.write(str)
				
				str = """\t\t\t\txx[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ x_tmp[ {nb}*{i} + k ] * x_tmp[ {nb}*{i} + k ]""".format(i=i, size=size, nb=nb)
					str = """ x[ (idx+{i})*{x_nb} + k{pk0} ] * x[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb,  pk0=pk0)
					fwrite.write(str)
				fwrite.write(";\n")
				str = """\t\t\t\txy[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ x_tmp[ {nb}*{i} + k ] * y_tmp[ {nb}*{i} + k ]""".format(i=i, size=size, nb=nb)
					str = """ x[ (idx+{i})*{x_nb} + k{pk0} ] * y[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb, pk0=pk0)
					
					fwrite.write(str)
				fwrite.write(";\n")
				str = """\t\t\t\tyy[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ y_tmp[ {nb}*{i} + k ] * y_tmp[ {nb}*{i} + k ]""".format(i=i, size=size, x_nb=x_nb, y_nb=y_nb, nb=nb)
					str = """ y[ (idx+{i})*{x_nb} + k{pk0} ] * y[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb, pk0=pk0)
					fwrite.write(str)
				fwrite.write(";\n")
				str = """\t\t\t}}  // close k-loop\n\t\t\t}} // close if \n""".format( size=size, nb=nb)
				fwrite.write(str)
			
			str = """
					
					 /*
					if( p->kacz ) {{
					double tmp;
					//#pragma vector aligned
					for(pvsc_lidx_t k=0; k<{nb}; k++) {{ """.format( nb=nb, pk0=pk0, size=size)
			fwrite.write(str)
		
			for i in range(0,size):
				str = """
						tmp = scale_h[k]*scale_h[k]*(shift_h[k]*shift_h[k] """.format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for hm in hopping_mats_v:
					for h in hm[1][i]:
						fwrite.write(" + (%f)*(%f)" %( h[1], h[1] ))
					for h in hm[2][i]:
						fwrite.write(" + p->mat->ham_scalars[%d]*p->mat->ham_scalars[%d]" %( h[1], h[1]))
					for h in hm[3][i]:
						fwrite.write(" + p->mat->ham_array[idxa*p->mat->ham->n_arrays+%d]*p->mat->ham_array[idxa*p->mat->ham->n_arrays+%d]" %( h[1], h[1] ))
				fwrite.write(");")
				
				str = """
						y[ (idx+{i})*{nb} + k ] *= 1./tmp;""".format( i=i, nb=nb)
				fwrite.write(str)
			
			str = """
							}}
						}} */
					}} // close i1 loop \n""".format()
			fwrite.write(str)
			
			if k_unrolling and dot_mode == 0 :
				str = """\t\t\tif( p->dot_xx || p->dot_xy || p->dot_yy ) {{\n""".format()
				fwrite.write(str)
				if nb == 1 :
					fwrite.write("\t\t\t\t#pragma vector aligned\n")
				str = """\t\t\t\tfor(pvsc_lidx_t i1=i1_0; i1<i1_end; i1++) {{
				pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
				pvsc_lidx_t idx  = {size} * idxa;\n""".format(nb=nb, size=size, pk0=pk0)
				fwrite.write(str)
				
				if nb != 1 :
					str = """\t\t\t\t#pragma  vector aligned
					for(pvsc_lidx_t k=0; k<{nb}; k++) {{\n""".format( nb=nb, pk0=pk0 )
					fwrite.write(str)
				else :
					fwrite.write( "\t\t\t\tpvsc_lidx_t k=0;\n" )
				
				str = """\t\t\t\t\txx[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ x_tmp[ {nb}*({size}*i1 + {i}) + k ] * x_tmp[ {nb}*({size}*i1 + {i}) + k ]""".format(i=i, size=size, nb=nb)
					str = """ x[ (idx+{i})*{x_nb} + k{pk0} ] * x[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb, pk0=pk0)
					fwrite.write(str)
				fwrite.write(";\n")
				str = """\t\t\t\t\txy[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ x_tmp[ {nb}*({size}*i1 + {i}) + k ] * y_tmp[ {nb}*({size}*i1 + {i}) + k ]""".format(i=i, size=size, nb=nb)
					str = """ x[ (idx+{i})*{x_nb} + k{pk0} ] * y[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb, pk0=pk0)
					fwrite.write(str)
				fwrite.write(";\n")
				str = """\t\t\t\t\tyy[k] +=""".format(i=i, size=size, nb=nb)
				fwrite.write(str)
				for i in range(0,size):
					if i != 0:
						fwrite.write(" +")
					#str = """ y_tmp[ {nb}*({size}*i1 + {i}) + k ] * y_tmp[ {nb}*({size}*i1 + {i}) + k ]""".format(i=i, size=size, nb=nb)
					str = """ y[ (idx+{i})*{x_nb} + k{pk0} ] * y[ (idx+{i})*{x_nb} + k{pk0} ]""".format(i=i, size=size, nb=nb, x_nb=x_nb, y_nb=y_nb, pk0=pk0)
					fwrite.write(str)
				fwrite.write(";\n")
				if nb != 1 :
					str = """\t\t\t\t}}  // close k-loop\n""".format( size=size, nb=nb)
					fwrite.write(str)
				str = """\t\t\t\t}} // close i1 loop\n\t\t\t}} //close if()\n""".format()
				fwrite.write(str)
				
			fwrite.write( "\t\t} // close i2/i3 loop \n" )
			
			if dim == 2:
				str = """\t\t}} // close z-blocking \n""".format()
				fwrite.write(str)
			
			if dim > 2:
				str = """\t\t}} // close y-blocking \n""".format()
				fwrite.write(str)
			
			fwrite.write( "\t\t} // close if-cond dot\n")
		fwrite.write( "\t\t} // close if-cond y==z\n")
	
	
	for i in range(0,r_arrays):
		str = """
		free(coeff_r_{i});
		""".format( i=i)
		fwrite.write(str)

	str = """
	
		#ifdef PVSC_HAVE_LIKWID
		likwid_markerStopRegion("pvsc_{name}_{nb}/comp");
		#endif
		
		free(y_tmp);
		free(x_tmp);
		
		
		}} // close omp parallel\n""".format(name=name, size=size, nb=nb)
	fwrite.write(str)

	str = """
	p->call_time_compute += pvsc_timing_end(timing_num_comp);
	
	if( p->dot_xx ) {{
		                              for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_xx[k{pk0}]  = 0.;
		for(int j=0; j<nthreads; j++) for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_xx[k{pk0}] += dot_xx[j][k]; }}
	
	
	if( p->dot_xy ) {{
		                              for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_xy[k{pk0}]  = 0.;
		for(int j=0; j<nthreads; j++) for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_xy[k{pk0}] += dot_xy[j][k]; }}
		
	if( p->dot_yy ) {{
		                              for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_yy[k{pk0}]  = 0.;
		for(int j=0; j<nthreads; j++) for(pvsc_lidx_t k=0; k<{nb}; k++) p->dot_yy[k{pk0}] += dot_yy[j][k]; }}
	
	if( p->dot_xx || p->dot_xy || p->dot_yy ) {{
		for(int j=0; j<nthreads; j++) free(dot_xx[j]); free(dot_xx);
		for(int j=0; j<nthreads; j++) free(dot_xy[j]); free(dot_xy);
		for(int j=0; j<nthreads; j++) free(dot_yy[j]); free(dot_yy);
	}}\n""".format(name=name, nb=nb, pk0=pk0, size=size )
	fwrite.write(str)
	
	if nb == "k_nb":
		str = """\t\t}}\n""".format( )
		fwrite.write(str)
		
	str = """
	break; // close case {nb}
	""".format( nb=nb )
	fwrite.write(str)

str = """
	}} // close switch(k_nb)
#ifdef PVSC_HAVE_MPI
	if (p->dot_allreduce){{
		if( p->dot_xx ) MPI_Allreduce( MPI_IN_PLACE, p->dot_xx, nb, MPI_DOUBLE, MPI_SUM, p->x->sys->mpi_comm);
		if( p->dot_yy ) MPI_Allreduce( MPI_IN_PLACE, p->dot_yy, nb, MPI_DOUBLE, MPI_SUM, p->x->sys->mpi_comm);
		if( p->dot_xy ) MPI_Allreduce( MPI_IN_PLACE, p->dot_xy, nb, MPI_DOUBLE, MPI_SUM, p->x->sys->mpi_comm);
	}}
#endif
	
	if( !p->scale_h ) {{ PVSC_FREE(scale_h) }}
	if( !p->shift_h ) {{ PVSC_FREE(shift_h) }}
	if( !p->scale_z ) {{ PVSC_FREE(scale_z) }}
	
	p->call_counter++;
	p->call_time += pvsc_timing_end(timing_num_all);
	return 0;
}}
""".format(name=name, size=size, dim=dim, nn_num=nn_num , ldx=ldx, ldy=ldy, ldz=ldz, n_scalars=n_scalars , n_arrays=n_arrays, complex_info=complex_info )
fwrite.write(str)




str = """
//#ifdef PVSC_WP


#ifdef STENCIL_{name}
#undef STENCIL_{name}
#endif 
#define STENCIL_{name}(i,h) \ \n""".format(name=name)
fwrite.write(str)
str = """\t\t\tfor(pvsc_lidx_t k=0; k<{nb}; k++) {{ \ \n""".format( nb="nb" )
fwrite.write(str)
		
for i in range(0,size):
	str = """\t\t\t\tv0[ ({i} + {size}*(i))*{nb} + {k}] =            \ 
					scale_v[{k}] * v0[ ({i} + {size}*(i))*{nb} + {k}]  \ 
					+ scale_h[{k}] * (                             \ 
						-shift_h[{k}] * v1[ ({i} + {size}*(i))*{nb} + {k}] \ \n""".format( i=i, nb="nb", size=size, k="k" )
	fwrite.write(str)
	for hm in hopping_mats_v:
		for h in hm[1][i]:
			fwrite.write("\t\t\t\t\t\t\t%+f * v1[( %d + %d*( (i) + %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
			str = """*{nb} + {k}] \ \n""".format( k=k,nb="nb")
			fwrite.write(str)
		for h in hm[2][i]:
			fwrite.write("\t\t\t\t\t\t\t+ccoeff[%d] * v1[(%d + %d*( (i) + %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
			str = """*{nb} + {k}] \ \n""".format( k=k,nb="nb")
			fwrite.write(str)
		for h in hm[3][i]:
			#fwrite.write("\t\t\t\t\t\t\t+vcoeff[i*ldvc+%d] * v1[( %d + %d*( (i) + %d + ldz*( %d + ldy * (%d) ) ) )" %( h[1], h[0], size, hm[0][2], hm[0][1], hm[0][0] ))
			str = """*{nb} + {k}] \ \n""".format( k=k,nb="nb")
			#fwrite.write(str)
	fwrite.write("\t\t\t\t\t\t\t\t\t\t\t); \ \n")
	
	
	str = """\t\t\t\tw[ ({i} + {size}*(i))*{nb} + {k}] += coeff_w[(h)*{nb} + {k}] * v0[ ({i} + {size}*(i))*{nb} + {k}];         \ \n""".format( i=i, nb="nb", size=size, k="k" )
	fwrite.write(str)
	str = """\t\t\t\tdot_1[(h)*{nb} + {k}] += v0[ ({i} + {size}*(i))*{nb} + {k}]*v1[ ({i} + {size}*(i))*{nb} + {k}];         \ \n""".format( i=i, nb="nb", size=size, k="k" )
	fwrite.write(str)
	str = """\t\t\t\tdot_0[(h)*{nb} + {k}] += v0[ ({i} + {size}*(i))*{nb} + {k}]*v0[ ({i} + {size}*(i))*{nb} + {k}];         \ \n""".format( i=i, nb="nb", size=size, k="k" )
	fwrite.write(str)
	
str = """\t\t }} \ \n""".format( )
fwrite.write(str)



str = """


int pvsc_wp_{name}_kernel( double ** v, double * restrict w, const double * coeff_w,
					const double * scale_h, const double * shift_h, const double * scale_v,
					double * ccoeff, double * vcoeff, pvsc_lidx_t ldvc,
					pvsc_lidx_t ldx, pvsc_lidx_t ldy, pvsc_lidx_t ldz, pvsc_lidx_t nb,
					pvsc_lidx_t xo,  pvsc_lidx_t yo,  pvsc_lidx_t zo, 
					pvsc_lidx_t xn,                   pvsc_lidx_t zn,
					bool x_pointed,                   bool z_pointed,
					pvsc_lidx_t d,   pvsc_lidx_t yl,
					bool roof, bool groove, bool upwind, bool downwind, int64_t * lups
					  ){{
	//int timing_num_all = pvsc_timing_init( "pvsc_{name}_all" );
	
	pvsc_lidx_t dd = 2*d+2;
	pvsc_lidx_t y_wind_widht = 4*d-2;
	
	int nthreads;
	double ** dot_xy;
	double ** dot_yy;
	
	#pragma omp parallel
	{{
	
	#pragma omp single
	{{
		#ifdef _OPENMP
		nthreads = omp_get_num_threads();
		#endif
		//if( p->dot_xx || p->dot_xy || p->dot_yy ) {{
			dot_xy = malloc(sizeof(double *) * nthreads);
			dot_yy = malloc(sizeof(double *) * nthreads);
		//}}
	}}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	double * restrict dot_1;
	double * restrict dot_0;
	
	posix_memalign((void**) &dot_1, 0x100, sizeof(double) * nb * 2*d ); for(pvsc_lidx_t k=0; k<2*d*nb; k++) dot_1[k] = 0.; dot_xy[tid] = dot_1;
	posix_memalign((void**) &dot_0, 0x100, sizeof(double) * nb * 2*d ); for(pvsc_lidx_t k=0; k<2*d*nb; k++) dot_0[k] = 0.; dot_yy[tid] = dot_0;
	
	//for( pvsc_lidx_t ix = 0; ix < xn; ix++  )
	#pragma omp for
	for( pvsc_lidx_t iz = 0; iz < zn; iz++  ){{
		pvsc_lidx_t index  =  zo+dd*iz + yo*ldz;
		         // index += (xo+dd*ix)*ldy*ldz;
		

		/*
		if( upwind  ){{
			
			pvsc_lidx_t zb = d+1;
			pvsc_lidx_t ze = d+1;
			
			for( pvsc_lidx_t h = 0; h < 2*d-1; h++  ){{
				      double * restrict v0 = v[ h   &1];
				const double * restrict v1 = v[(h+1)&1];
				if ( h<d ){{ zb--; ze++;}}
				if ( h>d ){{ zb++; ze--;}}
				
				for( pvsc_lidx_t y = h; y < y_wind_widht -h; y++  ){{
					pvsc_lidx_t idx = index + (yo + y +1 )*ldz;
					for( pvsc_lidx_t z = zb; z < ze; z++  ){{
						pvsc_lidx_t i = idx - h*ldz + z;
						STENCIL_{name}(i,h)
					}}
				}}
			}}
			
		}}
		*/
		
		if( roof && groove ){{
			
			for( pvsc_lidx_t y = y_wind_widht; y < y_wind_widht + yl; y++  ){{
				pvsc_lidx_t idx = index + (yo + y +1 )*ldz;
				
				pvsc_lidx_t zb = d+1;
				pvsc_lidx_t ze = d+1;
				
				//groove h= 0 ..   d-1
				//roof   h= d .. 2*d-1
				for( pvsc_lidx_t h = 0; h < 2*d; h++  ){{
					      double * restrict v0 = v[ h   &1];
					const double * restrict v1 = v[(h+1)&1];
					if ( h<d ){{ zb--; ze++;}}
					if ( h>d ){{ zb++; ze--;}}
					
					//for( pvsc_lidx_t x = ...
					for( pvsc_lidx_t z = zb; z < ze; z++  ){{
						pvsc_lidx_t i = idx - h*ldz + z;
						STENCIL_{name}(i,h)
					}}
				}}
				
			}}
		}}
		
		/*
		if( downwind ){{
			pvsc_lidx_t zb = d+1 +1;
			pvsc_lidx_t ze = d+1 -1;
			
			for( pvsc_lidx_t h = 1; h < 2*d; h++  ){{
				      double * restrict v0 = v[ h   &1];
				const double * restrict v1 = v[(h+1)&1];
				if ( h<d ){{ zb--; ze++;}}
				if ( h>d ){{ zb++; ze--;}}
				for( pvsc_lidx_t y = y_wind_widht + yl - h; y < y_wind_widht + yl +h; y++  ){{
					pvsc_lidx_t idx = index + (yo + y +1 )*ldz;
					for( pvsc_lidx_t z = zb; z < ze; z++  ){{
						pvsc_lidx_t i = idx - h*ldz + z;
						STENCIL_{name}(i,h)
					}}
				}}
			}}
		}}
		*/
		
""".format(name=name)
fwrite.write(str)





str = """
	}}  // close ix, iz loop
	
	
	}}  // close omp parallel
""".format(name=name, size=size)
fwrite.write(str)



str = """
	
	for(int th=0; th<nthreads; th++ ) free(dot_xy[th]); free(dot_xy);
	for(int th=0; th<nthreads; th++ ) free(dot_yy[th]); free(dot_yy);
	
	int add_lups = 0;
	
	if( groove ) add_lups += d*d + d;
	if(  roof  ) add_lups += d*d + d;
	
	add_lups *= yl;
	
	if(   upwind && groove ) add_lups += 2*d * (  d*d + d );
	if( downwind &&  roof  ) add_lups += 2*d * (  d*d + d );
	
	if(   upwind &&  roof  ) add_lups += 0; // todo
	if( downwind && groove ) add_lups += 0; // todo
	
	add_lups *= nb*zn;
	
	*lups += add_lups;
	
	return 0;
}}


int pvsc_wp_{name}( pvsc_wp_handel_t * p  ){{
	
	int timing_num_all = pvsc_timing_init( "pvsc_wp_{name}_all" );
	pvsc_printf("call pvsc_wp_{name}\\n");
	
	int nthreads = 1;
	pvsc_lidx_t nb = p->v0->nb;
	pvsc_system_geo * sys = p->v0->sys;
	
	double *scale_h, *shift_h, *scale_v, *coeff_w;
	if( p->scale_h )  scale_h = p->scale_h; else {{ scale_h = pvsc_malloc(NULL, sizeof(double),              nb, true, NULL, 35694 ); for( pvsc_lidx_t i=0; i<nb; i++ ) scale_h[i] = 1.; }}
	if( p->shift_h )  shift_h = p->shift_h; else {{ shift_h = pvsc_malloc(NULL, sizeof(double),              nb, true, NULL, 35695 ); for( pvsc_lidx_t i=0; i<nb; i++ ) shift_h[i] = 0.; }}
	if( p->scale_v )  scale_v = p->scale_v; else {{ scale_v = pvsc_malloc(NULL, sizeof(double),              nb, true, NULL, 35696 ); for( pvsc_lidx_t i=0; i<nb; i++ ) scale_v[i] = 0.; }}
	if( p->coeff_w )  coeff_w = p->coeff_w; else {{ coeff_w = pvsc_malloc(NULL, sizeof(double), 2*p->wp_deep*nb, true, NULL, 35697 ); for( pvsc_lidx_t i=0; i<nb; i++ ) coeff_w[i] = 1.; }}
	
	
	for(int i = 0; i<p->mat->ham->n_scalars; i++  ){{
		pvsc_printf(" coeff[%d]  %g \\n", i, p->mat->ham_scalars[i]);
	}}
	
	double * v[2];
	v[0] = p->v0->val;
	v[1] = p->v1->val;
	
	int64_t lups = 0L;
	
	pvsc_lidx_t xn = 1;
	
	//pvsc_lidx_t zn =  sys->n_z                    / (2*p->wp_deep+2);
	pvsc_lidx_t zn = (sys->n_z - p->wp_deep - 1 ) / (2*p->wp_deep+2);
	
	pvsc_lidx_t ldx = sys->s_x;
	pvsc_lidx_t ldy = sys->s_y;
	pvsc_lidx_t ldz = sys->s_z;
	
	pvsc_lidx_t xo = 0;
	pvsc_lidx_t yo = 0;
	pvsc_lidx_t zo = 0;
	
	pvsc_lidx_t yl = sys->n_y - 4*p->wp_deep-2 - 8;
	
	pvsc_printf("call pvsc_wp_{name}_kernel\\n");
	
	for(int i=0; i<10; i++){{
	pvsc_wp_{name}_kernel( v, p->w->val, coeff_w,
					scale_h, shift_h, scale_v,
					p->mat->ham_scalars, p->mat->ham_array, p->mat->ham->n_arrays,
					ldx,  ldy,  ldz, nb,
					xo,   yo,   zo, 
					xn,         zn,
					false, false,
					p->wp_deep  ,  yl,
					true, true, false, false,
					&lups );
	}}
	
	pvsc_printf("end pvsc_wp_{name}_kernel  total lups: %ld \\n", lups);
	
	
	
	
	if( !p->scale_h ) {{ PVSC_FREE(scale_h) }}
	if( !p->shift_h ) {{ PVSC_FREE(shift_h) }}
	if( !p->scale_v ) {{ PVSC_FREE(scale_v) }}
	
	
	pvsc_timing_end(timing_num_all);
	
	return 0;
}}



#undef STENCIL_{name}

//#endif

""".format(name=name, size=size, dim=dim, nn_num=nn_num , ldx=ldx, ldy=ldy, ldz=ldz, n_scalars=n_scalars , n_arrays=n_arrays, complex_info=complex_info )
fwrite.write(str)



str = """
pvsc_system_hamiltonian pvsc_make_stencil_hamiltionian_{name}( ) {{
	
	pvsc_system_hamiltonian H;
	H.n_o = {size};
	H.dim = {dim};
	H.cmp = {complex_info};

	H.lattice_distance_x = {ldx};
	H.lattice_distance_y = {ldy};
	H.lattice_distance_z = {ldz};

	H.m = {nn_num};
	
	H.dx  = malloc(sizeof(pvsc_lidx_t    )*H.m);
	H.dy  = malloc(sizeof(pvsc_lidx_t    )*H.m);
	H.dz  = malloc(sizeof(pvsc_lidx_t    )*H.m);
	H.n_scalars={n_scalars};
	H.n_arrays ={n_arrays};
	H.n_scalars_default = NULL;
	
	if( H.n_scalars ) {{
		H.n_scalars_default = malloc(sizeof(double)*H.n_scalars);
		for(int i=0; i<H.n_scalars; i++) H.n_scalars_default[i] = 0.;
	}} \n""".format(name=name, size=size, dim=dim, nn_num=nn_num , ldx=ldx, ldy=ldy, ldz=ldz, n_scalars=n_scalars , n_arrays=n_arrays, complex_info=complex_info )
fwrite.write(str)

i=0
for v in scalar_default:
	str = """	H.n_scalars_default[{i}]= {v};\n""".format(i=i, v=v )
	fwrite.write(str)
	i+=1


for i in range(0,nn_num):
	str = """	H.dx[{i}] ={dx}; H.dy[{i}] = {dy}; H.dz[{i}] = {dz};\n""".format(i=i, dx=hopping_mats_v[i][0][0], dy=hopping_mats_v[i][0][1], dz=hopping_mats_v[i][0][2])
	fwrite.write(str)

str = """
	
	H.spmvm_kernel = &pvsc_{name};
	H.wp_kernel    = &pvsc_wp_{name};
	
	
	H.flop_per_lup = {flop_per_lup};
	#if PVSC_VERBOSE>0
	pvsc_printf("return stencil_hamiltionian_{name}\\n");
	#endif
	return H;
}}
""".format(name=name, flop_per_lup=flop_per_lup)

fwrite.write(str)



fwrite.close()
