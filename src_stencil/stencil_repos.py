import sys
import numpy

prog_name = sys.argv[0]

rline = sys.argv[0].split(".")
line = rline[0].split("/")
prog_name = line[-1]


#fwrite = open( "stencil_repos.c",'w')
fwrite = open( sys.argv[1],'w')

stencils = []


for stencil_file in sys.argv[2:]:
	rline = stencil_file.split(".")
	line = rline[0].split("/")
	
	name     = line[-1]
	shortname = line[-1]
	
	fread = open( stencil_file,'r')
	for rline in fread:
		line = rline.split(" ")
		if   line[0] == 'name':
			shortname = line[1]
	fread.close()
	
	stencils.append( [ name, shortname] )
	

for name in stencils:
	str = """#include "pvsc_stencil_kernels/{name}.h"\n""".format(name=name[0])
	fwrite.write(str)


str = """
#include <string.h>

#include "pvsc/util.h"


int pvsc_get_hamiltionian( pvsc_system_hamiltonian * ham,  int argc, char * argv[]) {{

	int i;
	
	i = pvsc_read_args("-stencil" , argc, argv);
	if( i ){{""".format()
fwrite.write(str)

		


for name in stencils:
	str = """
		if( (!strcmp(argv[i+1], "{name0}" )) || (!strcmp(argv[i+1], "{name1}" ))  ) *ham = pvsc_make_stencil_hamiltionian_{name0}();
		else""".format(name0=name[0], name1=name[1], )
	fwrite.write(str)

str = """
		return 1;
	}}
	else return 1;
return 0;
}}
""".format()
fwrite.write(str)

fwrite.close()





str = """include/pvsc_stencil_kernels/{prog_name}.h""".format(prog_name=prog_name)
fwrite = open( str,'w')
str = """
#ifndef PVSC_{prog_name}_H
#define PVSC_{prog_name}_H

#include "pvsc/types.h"

#ifdef __cplusplus
extern "C" {{
#endif


int pvsc_get_hamiltionian( pvsc_system_hamiltonian * ham,  int argc, char * argv[]);

#ifdef __cplusplus
}}
#endif

#endif
""".format(prog_name=prog_name)
fwrite.write(str)
fwrite.close()


