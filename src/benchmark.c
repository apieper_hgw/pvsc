
#include "pvsc.h"
//#include <stdlib.h>
//#include <stdio.h>
//#include <math.h>

int pvsc_benchmark_mat( pvsc_matrix * mat , int n_nb, pvsc_lidx_t *nb, int Iter, bool dot_mode, bool z_mode){
	int timing_num = pvsc_timing_init( "pvsc_benchmark_mat" );
	
	if( !mat ) {
		pvsc_printf("error: pvsc_benchmark_mat(): mat is NULL\n");
		return 1;
		}
	
	pvsc_vector x[3] = {  PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->dot_allreduce = false;
	
	for(int k=0; k<n_nb; k++){
		spmvm_conf->mat = mat;
		spmvm_conf->dot_allreduce = false;
		
		pvsc_create_vector( x+0, nb[k], mat->sys);
		pvsc_create_vector( x+1, nb[k], mat->sys);
		if (z_mode) pvsc_create_vector( x+2, nb[k], mat->sys);
		
		pvsc_vector_from_func( x+0, pvsc_rand_vec, NULL);
		pvsc_vector_copy( x+1, x+0);
		
		pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb[k], true, "pvsc_cheb_dos() spmvm_conf->scale_h", 0 );
		pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb[k], true, "pvsc_cheb_dos() spmvm_conf->shift_h", 0 );
		pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb[k], true, "pvsc_cheb_dos() spmvm_conf->scale_z", 0 );
		
		if( dot_mode ){
			pvsc_malloc((void**) &(spmvm_conf->dot_yy), sizeof(double), nb[k], true, "pvsc_benchmark_mat() spmvm_conf->dot_yy", 0 );
			pvsc_malloc((void**) &(spmvm_conf->dot_xx), sizeof(double), nb[k], true, "pvsc_benchmark_mat() spmvm_conf->dot_xx", 0 );
			pvsc_malloc((void**) &(spmvm_conf->dot_xy), sizeof(double), nb[k], true, "pvsc_benchmark_mat() spmvm_conf->dot_xy", 0 );
		}
		
		for(pvsc_lidx_t i=0; i<nb[k]; i++) spmvm_conf->scale_h[i] =  1.;
		for(pvsc_lidx_t i=0; i<nb[k]; i++) spmvm_conf->scale_z[i] = -1.;
		for(pvsc_lidx_t i=0; i<nb[k]; i++) spmvm_conf->shift_h[i] =  0.;
		
		if (z_mode) spmvm_conf->z = x+2;
		
		#ifdef PVSC_HAVE_MPI
		MPI_Barrier( mat->sys->mpi_comm );
		#endif
		for(int j=0; j<Iter; j++){
			spmvm_conf->x = x + ( j   &1);
			spmvm_conf->y = x + ((j+1)&1);
			
			mat->ham->spmvm_kernel( spmvm_conf );
		}
		
		pvsc_print_performance_statistic( spmvm_conf );
		
		pvsc_destroy_vector(x  );
		pvsc_destroy_vector(x+1);
		if (z_mode) pvsc_destroy_vector(x+2);
		
		PVSC_FREE(spmvm_conf->scale_h)
		PVSC_FREE(spmvm_conf->shift_h)
		PVSC_FREE(spmvm_conf->scale_z)
		if( dot_mode ){
			PVSC_FREE(spmvm_conf->dot_yy)
			PVSC_FREE(spmvm_conf->dot_xx)
			PVSC_FREE(spmvm_conf->dot_xy)
		}
		spmvm_conf[0] = PVSC_SPMVM_HANDEL_INITIALIZER;
	}
	
	pvsc_timing_end(timing_num);
	return 0;
}
