//#define _XOPEN_SOURCE 600

#include "pvsc.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <mkl_lapacke.h>

const pvsc_rayleigh_ritz_handle_t PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER = {
	.mat = NULL,
	
	.vec_in        = NULL,
	.vec_eig       = NULL,
	.vec_res       = NULL,
	.eig           = NULL,
	.res           = NULL,
	.transform_mat = NULL
};



int pvsc_rayleigh_ritz( pvsc_rayleigh_ritz_handle_t * rrh ){
	int timing_num = pvsc_timing_init( "pvsc_rayleigh_ritz" );
	
	pvsc_printf("pvsc_rayleigh_ritz start\n");
	
	pvsc_lidx_t nb = rrh->vec_in->nb;
	pvsc_dense_redundant_matrix * a = rrh->transform_mat;
	
	pvsc_dense_redundant_matrix a_mem[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	
	if ( !a ) { a = a_mem; }
	
	pvsc_create_dense_redundant_matrix( a, nb, nb );
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	
	double * eig = NULL;
	if ( rrh->eig ) eig = rrh->eig; 
	else            pvsc_malloc((void **)&eig, sizeof(double), nb, true, NULL, 75694 );
	
	if( rrh->mat ){
		spmvm_conf->mat = rrh->mat;
		
		spmvm_conf->x = rrh->vec_in;
		spmvm_conf->y = rrh->vec_eig;
		
		spmvm_conf->scale_h=NULL;
		spmvm_conf->shift_h=NULL;
		spmvm_conf->scale_z=NULL;
		
		rrh->mat->ham->spmvm_kernel( spmvm_conf );
		
		pvsc_vector_M_eq_axy( a->val, a->ld_col, 1., rrh->vec_in, rrh->vec_eig );
		
		pvsc_dense_redundant_matrix b[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
		pvsc_create_dense_redundant_matrix( b, nb, nb );
		pvsc_vector_M_eq_axy( b->val, b->ld_col, 1., rrh->vec_in, rrh->vec_in  );
		
		pvsc_printf("     call LAPACKE_dsygv\n");
		LAPACKE_dsygv( LAPACK_COL_MAJOR, 1, 'V' , 'U', nb, a->val, a->ld_col, b->val, b->ld_col, eig);
		
		pvsc_destroy_dense_redundant_matrix( b );
	}else{
		
		double * D = pvsc_malloc(NULL, sizeof(double), nb, true, NULL, 468);
		pvsc_vector_M_eq_axy( a->val, a->ld_col, 1., rrh->vec_in, rrh->vec_in  );
		
		for (pvsc_lidx_t i=0; i<nb; i++) {
			if( a->val[i*a->ld_col+i] <= 0. ){
				printf("pvsc_rayleigh_ritz() zero vector in vectorblock  (index %d, value %f\n", i, a->val[i*a->ld_col+i]  );
			}
			D[i] = 1./sqrt(   a->val[i*a->ld_col+i] );
			for(pvsc_lidx_t j=0;j<nb;j++) a->val[i*a->ld_col+j] *= D[i]*D[j];
		}
		
		pvsc_printf("     call LAPACKE_dsyev\n");
		LAPACKE_dsyev( LAPACK_COL_MAJOR, 'V' , 'U', nb, a->val, a->ld_col, eig );
		for (pvsc_lidx_t i=0;i<nb;i++){
			if( eig[i] <= 0. ){
				printf("pvsc_rayleigh_ritz() vector block singular\n");
			}
			eig[i] = 1./sqrt( eig[i] );
			for(pvsc_lidx_t j=0;j<nb;j++) {
				a->val[i*a->ld_col+j] *= D[j]*eig[i];
			}
		}
		
		PVSC_FREE(D);
	}
	
	#ifdef PVSC_HAVE_MPI
		MPI_Bcast(  eig,        nb,  MPI_DOUBLE, 0, rrh->vec_eig->sys->mpi_comm);
		MPI_Bcast(  a->val,  nb*nb,  MPI_DOUBLE, 0, rrh->vec_eig->sys->mpi_comm);
	#endif
	pvsc_vector_xApby( 0, rrh->vec_eig, rrh->vec_in, a->val, a->ld_col );
	
	
	
	if( rrh->mat && rrh->res ){
		
		pvsc_vector   vec_res_mem[1] = {PVSC_VECTOR_INITIALIZER};
		pvsc_vector * vec_res = NULL;
		
		if( !rrh->vec_res ){
			vec_res = vec_res_mem;
			pvsc_create_vector( vec_res, rrh->vec_eig->nb, rrh->vec_eig->sys);
		} else {
			vec_res = rrh->vec_res;
		}
		
		spmvm_conf->x = rrh->vec_eig;
		spmvm_conf->y =      vec_res;
		
		spmvm_conf->shift_h = eig;
		spmvm_conf->dot_yy  = rrh->res;
		
		rrh->mat->ham->spmvm_kernel( spmvm_conf );
		
		for(pvsc_lidx_t j=0;j<nb;j++) rrh->res[j] = sqrt(rrh->res[j]);
		
		if( !rrh->vec_res ){
			pvsc_destroy_vector(vec_res_mem  );
		}
	}
	
	//pvsc_vector_M_eq_axy( a->val, a->ld_col, 1., rrh->vec_eig, rrh->vec_eig  );
	//for(pvsc_lidx_t j=0;j<nb;j++) {
	//	for(pvsc_lidx_t i=0;i<nb;i++) { pvsc_printf(" %g", a->val[i*a->ld_col+j]); }
	//	pvsc_printf("\n");}
	
	
	if ( !rrh->transform_mat ) pvsc_destroy_dense_redundant_matrix( a );
	if ( !rrh->eig           ) { PVSC_FREE(eig) }
	
	pvsc_printf("pvsc_rayleigh_ritz end\n");
	pvsc_timing_end(timing_num);
	return 0;
}




