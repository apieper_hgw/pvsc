#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>

int pvsc_vector_write( char * fname, pvsc_vector * vec  ){
	
	int64_t xyz64[3];
	int32_t hns32[3];
	xyz64[0] = vec->sys->mpi_size_x * vec->sys->n_x;
	xyz64[1] = vec->sys->mpi_size_y * vec->sys->n_y;
	xyz64[2] = vec->sys->mpi_size_z * vec->sys->n_z;
	hns32[0] = vec->sys->n_o;
	hns32[1] = vec->nb;
	hns32[2] = sizeof(double);
	
	pvsc_lidx_t n = hns32[1]*hns32[0]*vec->sys->n_z;
	
	#ifdef PVSC_HAVE_MPI
	MPI_File fh;
	int rank = vec->sys->mpi_rank;
	
	if (!rank ) remove(fname);
	MPI_Barrier(vec->sys->mpi_comm);
	
	MPI_File_open( vec->sys->mpi_comm, fname, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL,  &fh);
	
	MPI_Status status;
	
	MPI_Offset offset_header = 3*sizeof(int32_t) + 3*sizeof(int64_t);
	
	
	if( rank ){
		MPI_File_seek( fh, offset_header, MPI_SEEK_CUR);
	} else {
		//pvsc_printf( "write_vector() calc Filesize: 0x%lx\n", xyz64[0]*xyz64[1]*xyz64[2]*hns32[0]*hns32[1]*hns32[2]+offset_header );
		hns32[2] = sizeof(double);
		
		MPI_File_write(fh, &xyz64, 3, MPI_INT64_T, &status);
		MPI_File_write(fh, &hns32, 3, MPI_INT32_T, &status);
		
	}
	
	MPI_Offset unit = hns32[2]*hns32[1]*hns32[0]*vec->sys->n_z;
	MPI_Offset offset_init =                                unit*( vec->sys->mpi_rank_z
	                        + vec->sys->mpi_size_z*vec->sys->n_y*( vec->sys->mpi_rank_y
	                        + vec->sys->mpi_size_y*vec->sys->n_x*( vec->sys->mpi_rank_x ) ) );
	MPI_Offset offset_y = unit * vec->sys->mpi_size_z;
	MPI_Offset offset_x = unit * vec->sys->mpi_size_z * vec->sys->n_y * vec->sys->mpi_size_y;
	//pvsc_printf( "write_vector()  offset header 0x%lx \n", offset_header );
	//pvsc_printf( "write_vector()  offset init 0x%lx \n",   offset_init );
	//pvsc_printf( "write_vector()  offset y 0x%lx \n",      offset_y );
	//pvsc_printf( "write_vector()  offset x 0x%lx \n",      offset_x );
	#else
	FILE  *out = fopen(fname, "wb");
	fwrite(&xyz64, sizeof(int64_t), 3, out);
	fwrite(&hns32, sizeof(int32_t), 3, out);
	#endif
	
	
	for (pvsc_lidx_t x = 0; x < vec->sys->n_x; x++){
	for (pvsc_lidx_t y = 0; y < vec->sys->n_y; y++){
		double * ptr = vec->val + vec->nb*vec->sys->n_o*(vec->sys->b_z + 
		                                  vec->sys->s_z*(vec->sys->b_y + y + 
		                                  vec->sys->s_y*(vec->sys->b_x + x)));
		
		#ifdef PVSC_HAVE_MPI
		MPI_File_seek( fh, offset_header + offset_init + y*offset_y + x*offset_x , MPI_SEEK_SET);
		MPI_File_write(fh, ptr  , n, MPI_DOUBLE, &status);
		//for(int i=0; i<hns32[1]*hns32[0]*vec->sys->n_z;i++) pvsc_printf( " %6.0f", vec->val[i + vec->nb*vec->sys->n_o*(vec->sys->b_z + vec->sys->s_z*(vec->sys->b_y + y + vec->sys->s_y*( vec->sys->b_x + x)))] );
		//pvsc_printf( "\n");
		//MPI_File_write(fh, dummy, hns32[1]*hns32[0]*vec->sys->n_z, MPI_DOUBLE, &status);
		#else
		fwrite(ptr, sizeof(double), n, out);
		#endif
		}
	}
	#ifdef PVSC_HAVE_MPI
	MPI_File_close(&fh);
	#else
	fclose(out);
	#endif
	
	return 0;
}

int pvsc_vector_read( char * fname, pvsc_vector * vec  ){
	
	int64_t xyz64[3];
	int32_t hns32[3];
	
	
	#ifdef PVSC_HAVE_MPI
	if( !vec->sys ) {
		pvsc_printf( "pvsc_vector_read(): error: vec->sys must be set for mpi mode.\n");
		return 1;
	} 
	MPI_File fh;
	MPI_File_open( vec->sys->mpi_comm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL,  &fh);
	int rank = vec->sys->mpi_rank;
	
	MPI_Status status;
	
	MPI_Offset offset_header = 3*sizeof(int32_t) + 3*sizeof(int64_t);
	
	
	//if( rank ){
	//	MPI_File_seek( fh, offset_header, MPI_SEEK_CUR);
	//} else {
		//pvsc_printf( "write_vector() calc Filesize: 0x%lx\n", xyz64[0]*xyz64[1]*xyz64[2]*hns32[0]*hns32[1]*hns32[2]+offset_header );
		hns32[2] = sizeof(double);
		MPI_File_read( fh, &xyz64, 3, MPI_INT64_T, &status);
		MPI_File_read( fh, &hns32, 3, MPI_INT64_T, &status);
		
	//}
	
	MPI_Offset unit = hns32[2]*hns32[1]*hns32[0]*vec->sys->n_z;
	MPI_Offset offset_init =                                unit*( vec->sys->mpi_rank_z
	                        + vec->sys->mpi_size_z*vec->sys->n_y*( vec->sys->mpi_rank_y
	                        + vec->sys->mpi_size_y*vec->sys->n_x*( vec->sys->mpi_rank_x ) ) );
	MPI_Offset offset_y = unit * vec->sys->mpi_size_z;
	MPI_Offset offset_x = unit * vec->sys->mpi_size_z * vec->sys->n_y * vec->sys->mpi_size_y;

	#else
	
	FILE  *out = fopen(fname, "rb");
	fread(&xyz64, sizeof(int64_t), 3, out);
	fread(&hns32, sizeof(int32_t), 3, out);
	
	#endif
	
	// TODO check file size
	
	if( !vec->sys ){ 
		vec->sys = malloc(sizeof(pvsc_system_geo));
		pvsc_init_system_geo_mpicomm ( vec->sys,  MPI_COMM_WORLD, 1, 1, 1);
		vec->sys->finalised=false;
		vec->sys->n_x = (pvsc_lidx_t)xyz64[0];
		vec->sys->n_y = (pvsc_lidx_t)xyz64[1];
		vec->sys->n_z = (pvsc_lidx_t)xyz64[2];
		vec->sys->n_o = (pvsc_lidx_t)hns32[0];
		pvsc_finalise_system_geo( vec->sys );
	}
	
	if( (vec->val) && (vec->nb != hns32[1]) ){
		pvsc_printf( "pvsc_vector_read(): warning: vector is already alloced with wrong nb\n");
		pvsc_printf( "  Info: only vec->sys need to be set\n");
		pvsc_printf( "  Contiue with destroying vector and recreating ..\n");
		pvsc_system_geo * sys = vec->sys;
		pvsc_destroy_vector( vec );
		pvsc_create_vector( vec, hns32[1], sys);
	}
	
	if( !vec->val ){
		pvsc_create_vector( vec, hns32[1], vec->sys);
	} 
	
	
	
	if( vec->sys ){
		int info = 0;
		if (vec->sys->n_x*vec->sys->mpi_size_x != xyz64[0]) {
			pvsc_printf( "pvsc_vector_read failed: x dimension do not match\n"); 
			info |= 1;
		}
		if (vec->sys->n_y*vec->sys->mpi_size_y != xyz64[1]) {
			pvsc_printf( "pvsc_vector_read failed: y dimension do not match\n"); 
			info |= 2;
		}
		if (vec->sys->n_z*vec->sys->mpi_size_z != xyz64[2]) {
			pvsc_printf( "pvsc_vector_read failed: z dimension do not match\n"); 
			info |= 4;
		}
		if (vec->sys->n_o != hns32[0]) {
			pvsc_printf( "pvsc_vector_read failed: o dimension do not match\n"); 
			info |= 8;
		}
		if (vec->nb != hns32[1]) {
			pvsc_printf( "pvsc_vector_read failed: number of vectors do not match\n"); 
			info |= 16;
		}
		if (sizeof(double) != hns32[2]) {
			pvsc_printf( "pvsc_vector_read failed: elementsize do not match\n"); 
			info |= 32;
		}
		
		if( info )  return 1;
	}
	
	pvsc_lidx_t n = hns32[1]*hns32[0]*vec->sys->n_z;
	
	for (pvsc_lidx_t x = 0; x < vec->sys->n_x; x++){
	for (pvsc_lidx_t y = 0; y < vec->sys->n_y; y++){
		double * ptr = vec->val + vec->nb*vec->sys->n_o*(vec->sys->b_z + vec->sys->s_z*(vec->sys->b_y + y + vec->sys->s_y*( vec->sys->b_x + x)));
		
		#ifdef PVSC_HAVE_MPI
		MPI_File_seek( fh, offset_header + offset_init + y*offset_y + x*offset_x , MPI_SEEK_SET);
		MPI_File_read(fh, ptr  , n, MPI_DOUBLE, &status);
		#else
		fread(ptr, sizeof(double), n, out);
		#endif
		}
	}
	#ifdef PVSC_HAVE_MPI
	MPI_File_close(&fh);
	#else
	fclose(out);
	#endif
	
	
	return 0;
}

