#define _XOPEN_SOURCE 600

#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef PVSC_HAVE_LIKWID
#include <likwid.h>
#endif

int pvsc_create_data_trans_handle(pvsc_vec_data_trans_handle * h, pvsc_vector * vec, pvsc_border_comm * bcomm){
	*h = PVSC_VEC_DATA_TRANS_HANDLE_INITIALIZER;
	h->vec   = vec;
	h->bcomm = bcomm;
	
	h->n = vec->nb * bcomm->v_local.n[0] * bcomm->v_local.n[1] * bcomm->v_local.n[2] * bcomm->v_local.n[3];
	
	if( h->n < 1 ) return 1;
	
	if ( vec->sys->mpi_rank != bcomm->rank_send ) {
		h->inter_porc = true;
		pvsc_malloc((void**) &(h->buff_send_local  ), sizeof(double), h->n, true, NULL, 33 );
		pvsc_malloc((void**) &(h->buff_recv_remote ), sizeof(double), h->n, true, NULL, 34 );
		pvsc_lidx_t i;
		for(i=0;i<h->n;i++) h->buff_send_local[ i] = 0.;
		for(i=0;i<h->n;i++) h->buff_recv_remote[i] = 0.;
	}else{
		h->inter_porc = false;
		pvsc_malloc((void**) &(h->buff_send_local), sizeof(double), h->n, true, NULL, 33 );
		h->buff_recv_remote = h->buff_send_local;
	}
	return 0;
}

int pvsc_destroy_data_trans_handle(pvsc_vec_data_trans_handle * h){
	
	if( h->n < 1 ) return 1;
	
	if ( h->vec->sys->mpi_rank != h->bcomm->rank_send ) {
		PVSC_FREE(h->buff_send_local)
		PVSC_FREE(h->buff_recv_remote)
	}else{
		PVSC_FREE(h->buff_send_local)
	}
	
	return 0;
	
}

int pvsc_create_vector(pvsc_vector * vec, pvsc_lidx_t nb, pvsc_system_geo * sys){
	int timing_num = pvsc_timing_init( "pvsc_create_vector" );
	*vec = PVSC_VECTOR_INITIALIZER;
	vec->sys = sys;
	vec->nb  = nb;
	
	pvsc_malloc((void**) &(vec->val), sizeof(double), nb * sys->n_o * sys->s_z * sys->s_y * sys->s_x, true, NULL, 32 );
	
	pvsc_lidx_t i;
#pragma omp parallel for
	for( i=0; i<nb*sys->n_o*sys->s_z*sys->s_y*sys->s_x; i++ ) vec->val[i] = 0.;
	//for( i=0; i<nb*sys->v_mem.n[0]*sys->v_mem.n[1]*sys->v_mem.n[2]*sys->v_mem.n[3]; i++ ) vec->val[i] = 0.;
	//pvsc_printf( "alloc vektor at 0x%X\n",  vec->val);
	
	vec->n_data_trans_independent = 6;
	pvsc_malloc((void**) &(vec->data_trans_independent_handle), sizeof(pvsc_vec_data_trans_handle), vec->n_data_trans_independent, false, NULL, 35);
	
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 0, vec, &(sys->b_xd) );
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 1, vec, &(sys->b_xu) );
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 2, vec, &(sys->b_yd) );
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 3, vec, &(sys->b_yu) );
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 4, vec, &(sys->b_zd) );
	pvsc_create_data_trans_handle( vec->data_trans_independent_handle + 5, vec, &(sys->b_zu) );
	
	vec->levels_data_trans_dependent = 3;
	pvsc_malloc((void**) &(vec->n_data_trans_dependent), sizeof(int), vec->levels_data_trans_dependent, false, NULL, 36);
	pvsc_malloc((void**) &(vec->data_trans_dependent_handle), sizeof(pvsc_vec_data_trans_handle *), vec->levels_data_trans_dependent, false, NULL, 37);
	vec->n_data_trans_dependent[0] = 2;
	vec->n_data_trans_dependent[1] = 2;
	vec->n_data_trans_dependent[2] = 2;
	
	pvsc_malloc((void**) &(vec->data_trans_dependent_handle[0]), sizeof(pvsc_vec_data_trans_handle), vec->n_data_trans_dependent[0], false,NULL, 38);
	pvsc_malloc((void**) &(vec->data_trans_dependent_handle[1]), sizeof(pvsc_vec_data_trans_handle), vec->n_data_trans_dependent[1], false,NULL, 38);
	pvsc_malloc((void**) &(vec->data_trans_dependent_handle[2]), sizeof(pvsc_vec_data_trans_handle), vec->n_data_trans_dependent[2], false,NULL, 38);
	
	
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[0] + 0, vec, &(sys->b_zd_depend) );
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[0] + 1, vec, &(sys->b_zu_depend) );
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[1] + 0, vec, &(sys->b_yd_depend) );
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[1] + 1, vec, &(sys->b_yu_depend) );
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[2] + 0, vec, &(sys->b_xd_depend) );
	pvsc_create_data_trans_handle( vec->data_trans_dependent_handle[2] + 1, vec, &(sys->b_xu_depend) );
	
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_destroy_vector(pvsc_vector * vec){
	
	PVSC_FREE(vec->val)
	
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 0);
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 1);
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 2);
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 3);
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 4);
	pvsc_destroy_data_trans_handle( vec->data_trans_independent_handle + 5);
	PVSC_FREE(vec->data_trans_independent_handle)
	
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[0] + 0);
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[0] + 1);
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[1] + 0);
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[1] + 1);
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[2] + 0);
	pvsc_destroy_data_trans_handle( vec->data_trans_dependent_handle[2] + 1);
	
	PVSC_FREE(vec->data_trans_dependent_handle[0])
	PVSC_FREE(vec->data_trans_dependent_handle[1])
	PVSC_FREE(vec->data_trans_dependent_handle[2])
	PVSC_FREE(vec->data_trans_dependent_handle)
	PVSC_FREE(vec->n_data_trans_dependent)
	
	*vec = PVSC_VECTOR_INITIALIZER;
	
	return 0;
}

int pvsc_vector_copy_volume2buffer( pvsc_volume v, double * b, pvsc_vector * vec){
	/*
#pragma omp parallel for collapse(4)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++)
	for(pvsc_lidx_t i_0=0; i_0<v.n[0]; i_0++){
		pvsc_lidx_t idx =  vec->nb*(v.offset + i_0*v.s[0] + i_1*v.s[1] + i_2*v.s[2] + i_3*v.s[3]);
		pvsc_lidx_t  n =  vec->nb*(i_0 + v.n[0]*(i_1 + v.n[1]*(i_2 + v.n[2]*i_3)));
#pragma vector aligned
		for(pvsc_lidx_t j=0; j<vec->nb; j++)
			b[n+j] = vec->val[idx+j];
	}
	*/
#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++){
		pvsc_lidx_t idx =  vec->nb*(v.offset + i_1*v.s[1] + i_2*v.s[2] + i_3*v.s[3]);
		pvsc_lidx_t  n =  vec->nb*( v.n[0]*(i_1 + v.n[1]*(i_2 + v.n[2]*i_3)));
//#pragma vector aligned
//		for(pvsc_lidx_t j=0; j<vec->nb*v.n[0]; j++)	b[n+j] = vec->val[idx+j];
		memcpy( b + n, vec->val + idx, sizeof(double)*vec->nb*v.n[0]);
	}
	
	
	return 0;
}

int pvsc_vector_copy_buffer2volume( double * b, pvsc_volume v, pvsc_vector * vec){
	/*
#pragma omp parallel for collapse(4)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++)
	for(pvsc_lidx_t i_0=0; i_0<v.n[0]; i_0++){
		pvsc_lidx_t idx =  vec->nb*(v.offset + i_0*v.s[0] + i_1*v.s[1] + i_2*v.s[2] + i_3*v.s[3]);
		pvsc_lidx_t   n =  vec->nb*(i_0 + v.n[0]*(i_1 + v.n[1]*(i_2 + v.n[2]*i_3)));
#pragma vector aligned
		for(pvsc_lidx_t j=0; j<vec->nb; j++)
			vec->val[idx+j] = b[n+j];
	}
	*/
#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++){
		pvsc_lidx_t idx =  vec->nb*(v.offset + i_1*v.s[1] + i_2*v.s[2] + i_3*v.s[3]);
		pvsc_lidx_t   n =  vec->nb*( v.n[0]*(i_1 + v.n[1]*(i_2 + v.n[2]*i_3)));
//#pragma vector aligned
//		for(pvsc_lidx_t j=0; j<vec->nb*v.n[0]; j++)	vec->val[idx+j] = b[n+j];
		memcpy( vec->val + idx, b + n, sizeof(double)*vec->nb*v.n[0]);
	}
	
	
	return 0;
}

int pvsc_vector_datatransfer( pvsc_vector * vec ){
	int timing_num = pvsc_timing_init( "pvsc_vector_datatransfer" );
	//pvsc_printf("pvsc_vector_datatransfer() ..\n");
	int h;
#ifdef PVSC_HAVE_MPI
	int wait_count_recv=0,wait_count_send=0;
	MPI_Request *  reqs_recv = malloc(sizeof(MPI_Request)*vec->n_data_trans_independent);
	MPI_Request *  reqs_send = malloc(sizeof(MPI_Request)*vec->n_data_trans_independent);
	MPI_Status  * stats      = malloc(sizeof(MPI_Status )*vec->n_data_trans_independent);
	
	for (h=0; h<vec->n_data_trans_independent; h++)
		if( vec->data_trans_independent_handle[h].inter_porc && vec->data_trans_independent_handle[h].bcomm->enable_recv && vec->data_trans_independent_handle[h].n ){
			MPI_Irecv( vec->data_trans_independent_handle[h].buff_recv_remote,
			           vec->data_trans_independent_handle[h].n,
			           MPI_DOUBLE,
			           vec->data_trans_independent_handle[h].bcomm->rank_recv,
			           vec->data_trans_independent_handle[h].bcomm->tag,
			           vec->sys->mpi_comm,
			           reqs_recv + wait_count_recv );
			wait_count_recv++;
		}
#endif
	
	int timing_num_1 = pvsc_timing_init( "pvsc_vector_datatransfer_collect_data_and Isend" );
	for (h=0; h<vec->n_data_trans_independent; h++){
		if( vec->data_trans_independent_handle[h].n && vec->data_trans_independent_handle[h].bcomm->enable_send){
			pvsc_vector_copy_volume2buffer( vec->data_trans_independent_handle[h].bcomm->v_local,
											vec->data_trans_independent_handle[h].buff_send_local, vec);
#ifdef PVSC_HAVE_MPI
			if( vec->data_trans_independent_handle[h].inter_porc ){
				MPI_Isend( vec->data_trans_independent_handle[h].buff_send_local,
				           vec->data_trans_independent_handle[h].n,
				           MPI_DOUBLE,
				           vec->data_trans_independent_handle[h].bcomm->rank_send,
				           vec->data_trans_independent_handle[h].bcomm->tag,
				           vec->sys->mpi_comm,
				           reqs_send + wait_count_send );
				wait_count_send++;
			}
#endif
		}
	}
	pvsc_timing_end(timing_num_1);
	
#ifdef PVSC_HAVE_MPI
	int timing_num_2 = pvsc_timing_init( "pvsc_vector_datatransfer_wait_for_recv" );
	MPI_Waitall(wait_count_recv, reqs_recv, stats);
	pvsc_timing_end(timing_num_2);
#endif
	int timing_num_3 = pvsc_timing_init( "pvsc_vector_datatransfer_scatter_data" );
	for (h=0; h<vec->n_data_trans_independent; h++) if( vec->data_trans_independent_handle[h].bcomm->enable_recv && vec->data_trans_independent_handle[h].n)
		pvsc_vector_copy_buffer2volume( vec->data_trans_independent_handle[h].buff_recv_remote,
		                                vec->data_trans_independent_handle[h].bcomm->v_remote, vec);
	pvsc_timing_end(timing_num_3);
#ifdef PVSC_HAVE_MPI
	int timing_num_4 = pvsc_timing_init( "pvsc_vector_datatransfer_wait_for_reqs_send" );
	MPI_Waitall(wait_count_send, reqs_send, stats);
	pvsc_timing_end(timing_num_4);
	free(reqs_recv);
	free(reqs_send);
	free(stats);
#endif
	//pvsc_printf("pvsc_vector_datatransfer() END\n");
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_datatransfer_dependent( pvsc_vector * vec ){
	int timing_num = pvsc_timing_init( "pvsc_vector_datatransfer_dependent" );
	//pvsc_printf("pvsc_vector_datatransfer_dependent() ..\n");
	int h,l,levels = vec->levels_data_trans_dependent;
#ifdef PVSC_HAVE_MPI
	int * wait_count_recv = malloc(sizeof(int)*levels);
	int * wait_count_send = malloc(sizeof(int)*levels);
	MPI_Request **  reqs_recv = malloc(sizeof(MPI_Request *)*levels);
	MPI_Request **  reqs_send = malloc(sizeof(MPI_Request *)*levels);
	MPI_Status  ** stats      = malloc(sizeof(MPI_Status  *)*levels);
	
	for (l=0;l<levels;l++){
		wait_count_recv[l]=0;
		wait_count_send[l]=0;
		reqs_recv[l] = malloc(sizeof(MPI_Request)*vec->n_data_trans_dependent[l]);
		reqs_send[l] = malloc(sizeof(MPI_Request)*vec->n_data_trans_dependent[l]);
		stats[l]     = malloc(sizeof(MPI_Status )*vec->n_data_trans_dependent[l]);
	}
	
	for (l=0;l<levels;l++){
	for (h=0; h<vec->n_data_trans_dependent[l]; h++)
		if( vec->data_trans_dependent_handle[l][h].inter_porc && vec->data_trans_dependent_handle[l][h].bcomm->enable_recv && vec->data_trans_dependent_handle[l][h].n ){
			MPI_Irecv( vec->data_trans_dependent_handle[l][h].buff_recv_remote,
			           vec->data_trans_dependent_handle[l][h].n,
			           MPI_DOUBLE,
			           vec->data_trans_dependent_handle[l][h].bcomm->rank_recv,
			           vec->data_trans_dependent_handle[l][h].bcomm->tag,
			           vec->sys->mpi_comm,
			           reqs_recv[l] + wait_count_recv[l] );
			wait_count_recv[l]++;
			//pvsc_printf("MPI_Irecv rank %d form %d (tag %d) (size %d)\n", vec->sys->mpi_rank, vec->data_trans_dependent_handle[l][h].bcomm->rank_recv, vec->data_trans_dependent_handle[l][h].bcomm->tag, vec->data_trans_dependent_handle[l][h].n);
		}
	}
#endif
	
	for (l=0;l<levels;l++){
		//pvsc_printf("dt level %d\n",l);
		for (h=0; h<vec->n_data_trans_dependent[l]; h++){
			if( vec->data_trans_dependent_handle[l][h].n && vec->data_trans_dependent_handle[l][h].bcomm->enable_send ){
				pvsc_vector_copy_volume2buffer( vec->data_trans_dependent_handle[l][h].bcomm->v_local,
												vec->data_trans_dependent_handle[l][h].buff_send_local, vec);
#ifdef PVSC_HAVE_MPI
				if( vec->data_trans_dependent_handle[l][h].inter_porc ){
					MPI_Isend( vec->data_trans_dependent_handle[l][h].buff_send_local,
					           vec->data_trans_dependent_handle[l][h].n,
					           MPI_DOUBLE,
					           vec->data_trans_dependent_handle[l][h].bcomm->rank_send,
					           vec->data_trans_dependent_handle[l][h].bcomm->tag,
					           vec->sys->mpi_comm,
					           reqs_send[l] + wait_count_send[l] );
					wait_count_send[l]++;
					//pvsc_printf("MPI_Isend rank %d to %d (tag %d) (size %d)\n", vec->sys->mpi_rank, vec->data_trans_dependent_handle[l][h].bcomm->rank_recv, vec->data_trans_dependent_handle[l][h].bcomm->tag, vec->data_trans_dependent_handle[l][h].n);
				}
#endif
			}
		}
#ifdef PVSC_HAVE_MPI
		MPI_Waitall(wait_count_recv[l], reqs_recv[l], stats[l]);
#endif
		for (h=0; h<vec->n_data_trans_dependent[l]; h++) if( vec->data_trans_dependent_handle[l][h].bcomm->enable_recv && vec->data_trans_dependent_handle[l][h].n)
			pvsc_vector_copy_buffer2volume( vec->data_trans_dependent_handle[l][h].buff_recv_remote,
			                                vec->data_trans_dependent_handle[l][h].bcomm->v_remote, vec);
	}
#ifdef PVSC_HAVE_MPI
	for (l=0;l<levels;l++){
	MPI_Waitall(wait_count_send[l], reqs_send[l], stats[l]);
		free(reqs_recv[l]);
		free(reqs_send[l]);
		free(stats[l]);
	}
	
	free(wait_count_recv);
	free(wait_count_send);
	free(reqs_recv);
	free(reqs_send);
	free(stats);
#endif
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_vector_from_func_scale(double scale_v, pvsc_vector * vec, double scale_f, pvsc_vector_func_ptr func,  void * parm){
	int timing_num = pvsc_timing_init( "pvsc_vector_from_func_scale" );
	
	uint64_t * seeds;
	
	int nthreads = 1;
	
	#pragma  omp parallel
	{
	#pragma omp single
	{
	#ifdef _OPENMP
	nthreads = omp_get_num_threads();
	#endif
	seeds = malloc( sizeof(uint64_t  )*2*nthreads );
	for(int i=0; i<nthreads; i++) {
		seeds[2*i  ] = pvsc_rand_l_xorshift128plus() + i;
		seeds[2*i+1] = pvsc_rand_l_xorshift128plus() + vec->sys->mpi_rank;
	}
	}
	
	int tid = 0;
	#ifdef _OPENMP
	tid      = omp_get_thread_num();
	#endif
	
	uint64_t seed_local[2];
	seed_local[0] = seeds[2*tid  ];
	seed_local[1] = seeds[2*tid+1]; 
	
	#pragma  omp for collapse(4) nowait
	for(pvsc_gidx_t i3=0; i3<vec->sys->n_x; i3++)
	for(pvsc_gidx_t i2=0; i2<vec->sys->n_y; i2++)
	for(pvsc_gidx_t i1=0; i1<vec->sys->n_z; i1++)
	for(pvsc_gidx_t i0=0; i0<vec->sys->n_o; i0++){
		pvsc_gidx_t j[4];
		j[3] = i3 + (pvsc_gidx_t)(vec->sys->n_x) * vec->sys->mpi_rank_x; 
		j[2] = i2 + (pvsc_gidx_t)(vec->sys->n_y) * vec->sys->mpi_rank_y; 
		j[1] = i1 + (pvsc_gidx_t)(vec->sys->n_z) * vec->sys->mpi_rank_z; 
		j[0] = i0;
		double * val =vec->val  + vec->nb      *(i0
			                    + vec->sys->n_o*(i1+vec->sys->b_z 
			                    + vec->sys->s_z*(i2+vec->sys->b_y
			                    + vec->sys->s_y*(i3+vec->sys->b_x))));
		
		for(pvsc_lidx_t k=0; k<vec->nb; k++)
			val[k] = scale_v*val[k] + scale_f * func(j, k, parm, seed_local);
		}
	}
	
	free(seeds);
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_set_array_from_func_scale( double scale_v, pvsc_matrix * mat, double scale_f, pvsc_vector_func_ptr func, void * parm){
	
	pvsc_vector vec;
	pvsc_system_geo sys = *mat->sys;
	
	vec.sys = &sys;
	sys.n_o = 1;
	vec.val = mat->ham_array;
	vec.nb  = mat->ham->n_arrays;
	
	pvsc_vector_from_func_scale(scale_v ,&vec, scale_f, func, parm);
	
	return 0;
}

//  ------  pvsc_vector_func_ptr  -----------------
double pvsc_zero_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	
	return 0.0;
}

double pvsc_rand_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	//return erand48((unsigned short *)seed )-0.5;
	return pvsc_rand_ds_xorshift128plus( (uint64_t *) seed )-0.5;
	//return ((double)rand_r(seed)/RAND_MAX)-0.5;
}

double pvsc_hextest_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	return 16*16*16*16*n[3]+ 16*16*16*n[2] + 16*16*n[1] + 16*n[0] + k;
	//return 42.;
}

double pvsc_dectest_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	return 10*10*10*10*n[3]+ 10*10*10*n[2] + 10*10*n[1] + 10*n[0] + k;
	return 0.;
}

double pvsc_gen_wave_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	double * d_parm = (double *)parm;
	double r = 0.;
	if( n[0] - k == 0 ) r = cos( M_PI * ( d_parm[0]*n[3] + d_parm[1]*n[2] + d_parm[2]*n[1] ) );
	return r;
}

double pvsc_gen_wave_obc_layer_vec(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	
	void ** pparm = (void **)parm;
	double * d_parm = (double *)pparm[0];
	int    * l_parm = (int    *)pparm[1];
	
	double r = 0.;
	
	if( l_parm[0] && n[3] ) return r;
	if( l_parm[1] && n[2] ) return r;
	if( l_parm[2] && n[1] ) return r;
	
	if( n[0] - k == 0 ) r = cos( M_PI * ( d_parm[0]*n[3] + d_parm[1]*n[2] + d_parm[2]*n[1] ) );
	return r;
}

//  ----------------------------------------------------------------------------------

int pvsc_vector_copy(pvsc_vector * vec_dest, pvsc_vector * vec_src){
	int timing_num = pvsc_timing_init( "pvsc_vector_copy" );
	//TODO make some checks
	
	
	
	pvsc_system_geo * sys = vec_dest->sys;
		#ifdef PVSC_HAVE_ERROR_CHECK
	if( vec_dest->nb != vec_src->nb ) { 
		pvsc_printf("ERROR pvsc_vector_copy(): x->nb != y->nb\n");
		return 1; 
		}
	if( vec_dest->sys != vec_src->sys ) { 
		pvsc_printf("ERROR pvsc_vector_copy(): x->sys != y->sys\n");
		return 2; 
		}
	#endif
	
	
	if ( vec_dest->val !=  vec_src->val ){
	#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			#pragma vector aligned
			for(pvsc_lidx_t k=0; k<vec_dest->nb; k++)
				vec_dest->val[(idx+i)*vec_dest->nb + k] = vec_src->val[(idx+i)*vec_src->nb + k];
		}
	}

	//pvsc_volume v = vec_src->sys->v;
	//double * restrict ps = vec_src->val;
	//double * restrict pd = vec_dest->val;
	//
	//if ( ps != pd ){
	//	
	//for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	//for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	//for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++)
	//for(pvsc_lidx_t i_0=0; i_0<v.n[0]; i_0++){
	//	#pragma vector aligned
	//	for(pvsc_lidx_t k=0; k<vec_src->nb; k++)
	//		pd[   (v.offset+i_3*v.s[3]+i_2*v.s[2]+i_1*v.s[1]+i_0*v.s[0])*vec_src->nb + k]
	//		 =  ps[(v.offset+i_3*v.s[3]+i_2*v.s[2]+i_1*v.s[1]+i_0*v.s[0])*vec_src->nb + k];
	//	}
	//}
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_copy_select( pvsc_lidx_t n, pvsc_vector * vec_dest, pvsc_lidx_t * select_dest,  
                                            pvsc_vector * vec_src,  pvsc_lidx_t * select_src ){
	int timing_num = pvsc_timing_init( "pvsc_vector_copy_select" );
	
	//TODO make some checks
	pvsc_system_geo * sys = vec_dest->sys;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	
	for(pvsc_lidx_t i=0; i<n; i++){
		if( (select_dest[i] < 0) || ( select_dest[i] >= vec_dest->nb )){
			pvsc_printf("ERROR pvsc_vector_copy_select(): select_dest[%d] is not in range\n", i);
			return 1;}
		if( (select_src[i] < 0) || ( select_src[i] >= vec_src->nb) ){
			pvsc_printf("ERROR pvsc_vector_copy_select(): select_src[%d] is not in range\n", i);
			return 1;}
	}
	
	if( vec_dest->sys != vec_src->sys ) {
		pvsc_printf("ERROR pvsc_vector_copy_select(): x->sys != y->sys\n");
		return 2; 
		}
	#endif
	
	#pragma omp parallel
	{
	double * restrict x_tmp;
	posix_memalign((void**) &x_tmp, 0x40, sizeof(double) * vec_src->nb*vec_src->sys->n_o );
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			
			memcpy( x_tmp, vec_src->val + idx*vec_src->nb, sizeof(double)*vec_src->nb*vec_src->sys->n_o );
			
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t k=0; k<n; k++)
				vec_dest->val[(idx+i)*vec_dest->nb + select_dest[k] ] = x_tmp       [(    i)*vec_src->nb + select_src[k]];
				//vec_dest->val[(idx+i)*vec_dest->nb + select_dest[k] ] = vec_src->val[(idx+i)*vec_src->nb + select_src[k]];
		}
	free(x_tmp);
	}
	
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_copy_gather_block( pvsc_vector * vec_dest, pvsc_vector ** vec_src, pvsc_lidx_t n, pvsc_lidx_t src_nb_idx  ){
	int timing_num = pvsc_timing_init( "pvsc_vector_copy_gatter_block" );
	
	//TODO make some checks
	pvsc_system_geo * sys = vec_dest->sys;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	
	for(pvsc_lidx_t i=0; i<n; i++)
		if( vec_dest->sys != vec_src[i]->sys ) {
			pvsc_printf("ERROR pvsc_vector_copy_select(): vec_dest->sys != vec_src[%d]->sys\n", i);
			return 2; 
		}
	#endif
	
	#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t k=0; k<n; k++)
				vec_dest->val[(idx+i)*vec_dest->nb + k ] = vec_src[k]->val[(idx+i)*vec_src[k]->nb + src_nb_idx];
		}
	
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_vector_copy_scatter_block( pvsc_vector ** vec_dest, pvsc_lidx_t n,  pvsc_vector * vec_src, pvsc_lidx_t src_nb_idx  ){
	int timing_num = pvsc_timing_init( "pvsc_vector_copy_gatter_block" );
	
	//TODO make some checks
	pvsc_system_geo * sys = vec_src->sys;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	
	for(pvsc_lidx_t i=0; i<n; i++)
		if( vec_src->sys != vec_dest[i]->sys ) {
			pvsc_printf("ERROR pvsc_vector_copy_select(): vec_src->sys != vec_dest[%d]->sys\n", i);
			return 2; 
		}
	#endif
	
	#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t k=0; k<n; k++)
				vec_dest[k]->val[(idx+i)*vec_dest[k]->nb + src_nb_idx] = vec_src->val[(idx+i)*vec_src->nb + k ];
		}
	
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_normalized(pvsc_vector * vec){
	int timing_num = pvsc_timing_init( "pvsc_vector_normalized" );
	
	double * dot = pvsc_malloc(NULL, sizeof(double),vec->nb, true, "mu_sum", 0);
	
	pvsc_vector_dot(dot, vec, vec);
	
	for( pvsc_lidx_t i=0; i<vec->nb; i++ ){
		dot[i] = 1./ sqrt(dot[i]);
	}
	
	pvsc_vector_scale(vec, dot);
	
	PVSC_FREE(dot)
	
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_scale(pvsc_vector * vec, double * scale){
	int timing_num = pvsc_timing_init( "pvsc_vector_scale" );
	
	//TODO make some checks
	
	pvsc_volume v = vec->sys->v;
	double * restrict p = vec->val;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	int err = 0;
	err += pvsc_aligned_check( scale, "pvsc_vector_scale(): scale");
	if( err ) return err;
	#endif
	
	#pragma omp parallel for collapse(4)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++)
	for(pvsc_lidx_t i_0=0; i_0<v.n[0]; i_0++){
		#pragma vector aligned
		for(pvsc_lidx_t k=0; k<vec->nb; k++)
			p[   (v.offset+i_3*v.s[3]+i_2*v.s[2]+i_1*v.s[1]+i_0*v.s[0])*vec->nb + k] *= scale[k];
		}
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_dot(double * dot, pvsc_vector * x, pvsc_vector * y){
	int timing_num = pvsc_timing_init( "pvsc_vector_dot" );
	
	//TODO make some checks
	pvsc_system_geo * sys = x->sys;
	pvsc_volume v = x->sys->v;
	double ** dot_ = NULL;
	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	//pvsc_malloc( (void **)(&dot_), sizeof(double *), nthreads, false, NULL, 65 );
	dot_ = malloc( sizeof(double *)*nthreads );
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	double * restrict dot_local;// = pvsc_malloc( NULL, sizeof(double), x->nb, true, NULL, 64 );
	posix_memalign((void**) &dot_local, 0x40, sizeof(double) * x->nb );
	
	
	for(pvsc_lidx_t k=0; k<x->nb; k++) dot_local[k] = 0.; 
	dot_[tid] = dot_local;
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			#pragma vector aligned
			for(pvsc_lidx_t k=0; k<x->nb; k++)
				dot_local[k] += x->val[(idx+i)*x->nb + k]*y->val[(idx+i)*x->nb + k];
		}
/*	#pragma omp for collapse(4)
	for(pvsc_lidx_t i_3=0; i_3<v.n[3]; i_3++)
	for(pvsc_lidx_t i_2=0; i_2<v.n[2]; i_2++)
	for(pvsc_lidx_t i_1=0; i_1<v.n[1]; i_1++)
	for(pvsc_lidx_t i_0=0; i_0<v.n[0]; i_0++){
		#pragma vector aligned
		for(pvsc_lidx_t k=0; k<x->nb; k++)
			dot_local[k] += 
			   x->val[(v.offset+i_3*v.s[3]+i_2*v.s[2]+i_1*v.s[1]+i_0*v.s[0])*x->nb + k]
			 * y->val[(v.offset+i_3*v.s[3]+i_2*v.s[2]+i_1*v.s[1]+i_0*v.s[0])*y->nb + k];
		}*/
	}
	
	for(pvsc_lidx_t k=0; k<x->nb; k++) dot[k] = 0.;
	for(int i=0; i<nthreads; i++){
		for(pvsc_lidx_t k=0; k<x->nb; k++) dot[k] += dot_[i][k];
		free(dot_[i]);
		//PVSC_FREE(dot_[i])
	}
	free(dot_);
	//PVSC_FREE(dot_)
	
	#ifdef PVSC_HAVE_MPI
		MPI_Allreduce( MPI_IN_PLACE, dot, x->nb, MPI_DOUBLE, MPI_SUM, x->sys->mpi_comm);
	#endif
	
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_axpby(double * b, pvsc_vector * y, double * a, pvsc_vector * x ){
	int timing_num = pvsc_timing_init( "pvsc_vector_axpby" );
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	if( x->nb != y->nb ) { 
		pvsc_printf("ERROR pvsc_vector_axpby(): x->nb != y->nb\n");
		return 1; 
		}
	if( x->sys != y->sys ) { 
		pvsc_printf("ERROR pvsc_vector_axpby(): x->sys != y->sys\n");
		return 2; 
		}
	#endif
	
	double * a_using = NULL;
	double * b_using = NULL;
	
	if( a ) a_using = a;
	else {
		pvsc_malloc((void**) &a_using, sizeof(double), x->nb, true, NULL, 66 );
		for(pvsc_lidx_t i=0; i<x->nb; i++) a_using[i] = 1.;
	}
	if( b ) b_using = b;
	else {
		pvsc_malloc((void**) &b_using, sizeof(double), x->nb, true, NULL, 66 );
		for(pvsc_lidx_t i=0; i<x->nb; i++) b_using[i] = 1.;
	}
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	int err = 0;
	err += pvsc_aligned_check( a_using, "pvsc_vector_axpby(): a_using");
	err += pvsc_aligned_check( b_using, "pvsc_vector_axpby(): b_using");
	if( err ) return err;
	#endif
	
	//TODO make some checks
	
	//pvsc_volume v = x->sys->v;
	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	//#pragma omp for collapse(4)
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
			pvsc_lidx_t idx  = y->sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<y->sys->n_o; i++  ){
			#pragma vector aligned
				for(pvsc_lidx_t k=0; k<y->nb; k++)
					y->val[(idx+i)*x->nb + k] 
					 =  a_using[k] * x->val[(idx+i)*x->nb + k]
					  + b_using[k] * y->val[(idx+i)*y->nb + k];
			}
		}
	}
	
	if( a != a_using ) {PVSC_FREE(a_using)}
	if( b != b_using ) {PVSC_FREE(b_using)}
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_axpy_block(pvsc_vector * y, double * a, pvsc_vector * x, pvsc_lidx_t block ){
	int timing_num = pvsc_timing_init( "pvsc_vector_axpy_block" );
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerStopRegion("pvsc_vector_axpy_block");
	#endif
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	for(pvsc_lidx_t n=0; n<block; n++){
		if( x[n].nb != y->nb ) { 
			pvsc_printf("ERROR pvsc_vector_axpby(): x->nb != y->nb\n");
			return 1; 
			}
		if( x[n].sys != y->sys ) { 
			pvsc_printf("ERROR pvsc_vector_axpby(): x->sys != y->sys\n");
			return 2; 
			}
	}
	#endif
	
	//TODO make some checks
	
	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	//#pragma omp for collapse(4)
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
			pvsc_lidx_t idx  = y->sys->n_o * idxa;
			for(pvsc_lidx_t n=0; n<block; n++)
			for(pvsc_lidx_t i=0; i<y->sys->n_o; i++  ){
			#pragma vector aligned
				for(pvsc_lidx_t k=0; k<y->nb; k++)
					y->val[(idx+i)*y->nb + k] 
					 +=  a[n*y->nb+k] * x[n].val[(idx+i)*y->nb + k];
			}
		}
	}
	
	#ifdef PVSC_HAVE_LIKWID
	likwid_markerStopRegion("pvsc_vector_axpy_block");
	#endif
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_vector_axpy_complex( pvsc_vector * y, double * coeff, pvsc_vector * x, int n, bool mat_complex){
	int timing_num = pvsc_timing_init( "pvsc_vector_axpy_complex" );
	pvsc_lidx_t nb = y->nb;
	pvsc_volume v = x->sys->v;
	
	
	pvsc_lidx_t ppi0 = 1;
	pvsc_lidx_t ppk  = 1;
	pvsc_lidx_t ppim = 0;
	
	if( mat_complex ){
		ppi0 = 2;
		ppim = nb;
		if(y->sys->n_o&1) {
			pvsc_printf("Error pvsc_vector_axpy_complex: n_o is odd\n");
			return 1;
			}
		
		
	}else{
		ppk  = 2;
		ppim = 1;
		if(nb&1) {
			pvsc_printf("Error pvsc_vector_axpy_complex: nb is odd\n");
			return 1;
			}
		
	}
	
	#pragma omp parallel for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
		pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
		pvsc_lidx_t idx  = y->sys->n_o * idxa;
		for(int i=0; i<n; i++){
			for(pvsc_lidx_t i0=0; i0<y->sys->n_o; i0+=ppi0)
			for(pvsc_lidx_t  k=0;  k<y->nb;        k+=ppk ){
				pvsc_lidx_t idx_re = (idx+i0)*nb + k;
				pvsc_lidx_t idx_im = (idx+i0)*nb + k + ppim;
				y->val[idx_re] += coeff[0+2*i]*x[i].val[idx_re] - coeff[1+2*i]*x[i].val[idx_im];
				y->val[idx_im] += coeff[0+2*i]*x[i].val[idx_im] + coeff[1+2*i]*x[i].val[idx_re];
			}
		}
	}
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_xApby(double b, pvsc_vector * y,  pvsc_vector * x, double * a, pvsc_lidx_t lda ){
	int timing_num = pvsc_timing_init( "pvsc_vector_xApby" );
	//  y = b*y + x*A
	//TODO make some checks
	if( !lda ) lda = x->nb;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	if( lda < x->nb ) { 
		pvsc_printf("ERROR pvsc_vector_xApby(): lda < x->nb\n");
		return 1; 
		}
	#endif
	//pvsc_volume v = x->sys->v;
	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	double * restrict x_tmp;
	posix_memalign((void**) &x_tmp, 0x40, sizeof(double) * x->nb*x->sys->n_o );
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
			pvsc_lidx_t idx  = y->sys->n_o * idxa;
			
			memcpy( x_tmp, x->val + idx*x->nb, sizeof(double)*x->nb*x->sys->n_o );
			//for(pvsc_lidx_t i=0; i<x->nb*x->sys->n_o; i++) x_tmp[i] = x->val[ idx*x->nb + i];
			
			for(pvsc_lidx_t i=0; i<y->sys->n_o; i++  )
			for(pvsc_lidx_t k=0; k<y->nb; k++){
				y->val[(idx+i)*y->nb + k] *= b;
				for(pvsc_lidx_t l=0; l<x->nb; l++)
					y->val[(idx+i)*y->nb + k] +=  x_tmp[ (    i)*x->nb + l] * a[lda*k+l];
					//y->val[(idx+i)*y->nb + k] +=  x->val[(idx+i)*x->nb + l] * a[lda*k+l];
			}
		}
	free(x_tmp);
	}
	pvsc_timing_end(timing_num);
	return 0;
}

int pvsc_vector_M_eq_axy( double * M, pvsc_lidx_t ldM, double a, pvsc_vector * x,  pvsc_vector * y ){
	int timing_num = pvsc_timing_init( "pvsc_vector_M_eq_axy" );
	// // M = b*M + a*x^T*y
	//  M = x^T*y
	//TODO make some checks

	#ifdef PVSC_HAVE_ERROR_CHECK
	if( ldM > x->nb ) { 
		pvsc_printf("ERROR pvsc_vector_M_eq_axy(): ldM > x->nb\n");
		return 1; 
		}
	#endif
	//------
		//TODO make some checks
	pvsc_system_geo * sys = x->sys;
	pvsc_volume v = x->sys->v;
	double ** M_ = NULL;
	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	//pvsc_malloc( (void **)(&dot_), sizeof(double *), nthreads, false, NULL, 65 );
	M_ = malloc( sizeof(double *)*nthreads );
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	double * restrict M_local;// = pvsc_malloc( NULL, sizeof(double), x->nb, true, NULL, 64 );
	posix_memalign((void**) &M_local, 0x40, sizeof(double) * x->nb*y->nb );
	for(pvsc_lidx_t k=0; k<x->nb*y->nb; k++) M_local[k] = 0.; 
	M_[tid] = M_local;
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t k=0; k<y->nb; k++)
			#pragma vector aligned
			for(pvsc_lidx_t l=0; l<x->nb; l++)
				M_local[k*x->nb+l] += x->val[(idx+i)*x->nb + l] * y->val[(idx+i)*y->nb + k];
		}
	}
	
	for(pvsc_lidx_t k=0; k<y->nb; k++) for(pvsc_lidx_t l=0; l<x->nb; l++)  M[k*ldM+l] = 0.;
	for(int i=0; i<nthreads; i++){
		for(pvsc_lidx_t k=0; k<y->nb; k++) for(pvsc_lidx_t l=0; l<x->nb; l++)
		                 M[k*ldM+l] += M_[i][k*x->nb+l];
		free(M_[i]);
		//PVSC_FREE(dot_[i])
	}
	free(M_);
	//PVSC_FREE(dot_)
	
	#ifdef PVSC_HAVE_MPI
		if( ldM == x->nb ){
			MPI_Allreduce( MPI_IN_PLACE, M, x->nb*y->nb, MPI_DOUBLE, MPI_SUM, x->sys->mpi_comm);
		}else{
			for(pvsc_lidx_t k=0; k<y->nb; k++)
			  MPI_Allreduce( MPI_IN_PLACE, M + k*ldM, x->nb, MPI_DOUBLE, MPI_SUM, x->sys->mpi_comm);
		}
	#endif
	
	
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_vector_class_gram_schmidt( pvsc_vector * x,  pvsc_vector * y, pvsc_lidx_t n ){
	int timing_num = pvsc_timing_init( "pvsc_vector_class_gram_schmidt" );
	//  x = (1 - y y^T ) x
	//TODO make some checks
	
	if ( n == -1 ) n = y->nb;
	if ( n ==  0 ) return 0;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	if( n > y->nb ) { 
		pvsc_printf("ERROR pvsc_vector_class_gram_schmidt(): n > y->nb\n");
		return 1; 
		}
	#endif
	//------
		//TODO make some checks
	pvsc_system_geo * sys = x->sys;
	//pvsc_volume v = x->sys->v;
	double ** M_ = NULL;

	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	//pvsc_malloc( (void **)(&dot_), sizeof(double *), nthreads, false, NULL, 65 );
	M_ = malloc( sizeof(double *)*nthreads );
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	double * restrict M_local;// = pvsc_malloc( NULL, sizeof(double), x->nb, true, NULL, 64 );
	posix_memalign((void**) &M_local, 0x40, sizeof(double) * x->nb*n );
	for(pvsc_lidx_t k=0; k<x->nb*n; k++) M_local[k] = 0.; 
	M_[tid] = M_local;
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t l=0; l<x->nb; l++)
			#pragma vector aligned
			for(pvsc_lidx_t k=0; k<n; k++)
				M_local[k*x->nb+l] += x->val[(idx+i)*x->nb + l]*y->val[(idx+i)*y->nb + k];
		}
	}
	
	double * M = NULL;
	posix_memalign((void**) &M, 0x40, sizeof(double) * x->nb*n );
	for(pvsc_lidx_t k=0; k<n*x->nb; k++) M[k] = 0.;
	for(int i=0; i<nthreads; i++){
		for(pvsc_lidx_t k=0; k<n*x->nb; k++) M[k] += M_[i][k];
		free(M_[i]);
		//PVSC_FREE(dot_[i])
	}
	free(M_);
	//PVSC_FREE(dot_)
	
	#ifdef PVSC_HAVE_MPI
		MPI_Allreduce( MPI_IN_PLACE, M, x->nb*n, MPI_DOUBLE, MPI_SUM, x->sys->mpi_comm);
	#endif
	
	#pragma omp parallel
	{
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
			pvsc_lidx_t idx  = y->sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<y->sys->n_o; i++  )
			for(pvsc_lidx_t l=0; l<x->nb; l++){
				for(pvsc_lidx_t k=0; k<n; k++)
					x->val[(idx+i)*x->nb + l] -= M[k*x->nb + l] * y->val[(idx+i)*y->nb + k] ;
			}
		}
	}
	
	
	free(M);
	
	pvsc_timing_end(timing_num);
	return 0;
}



int pvsc_vector_projection( pvsc_vector * x,  pvsc_vector * y, pvsc_lidx_t n ){
	int timing_num = pvsc_timing_init( "pvsc_vector_projection" );
	//  x = ( y y^T ) x
	//TODO make some checks
	
	if ( !n ) n = y->nb;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	if( n > y->nb ) { 
		pvsc_printf("ERROR pvsc_vector_projection(): n > y->nb\n");
		return 1; 
		}
	#endif
	//------
		//TODO make some checks
	pvsc_system_geo * sys = x->sys;
	//pvsc_volume v = x->sys->v;
	double ** M_ = NULL;

	
	int nthreads = 1;
	#pragma omp parallel
	{
	#pragma omp single
	{
		#ifdef _OPENMP
			nthreads = omp_get_num_threads();
		#endif
	//pvsc_malloc( (void **)(&dot_), sizeof(double *), nthreads, false, NULL, 65 );
	M_ = malloc( sizeof(double *)*nthreads );
	}
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	double * restrict M_local;// = pvsc_malloc( NULL, sizeof(double), x->nb, true, NULL, 64 );
	posix_memalign((void**) &M_local, 0x40, sizeof(double) * x->nb*n );
	for(pvsc_lidx_t k=0; k<x->nb*n; k++) M_local[k] = 0.; 
	M_[tid] = M_local;
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=sys->b_x; i3<sys->n_x+sys->b_x; i3++)
	for(pvsc_lidx_t i2=sys->b_y; i2<sys->n_y+sys->b_y; i2++)
	for(pvsc_lidx_t i1=sys->b_z; i1<sys->n_z+sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  sys->s_z * ( i2 + sys->s_y * i3);
			pvsc_lidx_t idx  = sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<sys->n_o; i++  )
			for(pvsc_lidx_t l=0; l<x->nb; l++)
			#pragma vector aligned
			for(pvsc_lidx_t k=0; k<n; k++)
				M_local[k*x->nb+l] += x->val[(idx+i)*x->nb + l]*y->val[(idx+i)*y->nb + k];
		}
	}
	
	double * M = NULL;
	posix_memalign((void**) &M, 0x40, sizeof(double) * x->nb*n );
	for(pvsc_lidx_t k=0; k<n*x->nb; k++) M[k] = 0.;
	for(int i=0; i<nthreads; i++){
		for(pvsc_lidx_t k=0; k<n*x->nb; k++) M[k] += M_[i][k];
		free(M_[i]);
		//PVSC_FREE(dot_[i])
	}
	free(M_);
	//PVSC_FREE(dot_)
	
	#ifdef PVSC_HAVE_MPI
		MPI_Allreduce( MPI_IN_PLACE, M, x->nb*n, MPI_DOUBLE, MPI_SUM, x->sys->mpi_comm);
	#endif
	
	#pragma omp parallel
	{
	int tid = 0;
	#ifdef _OPENMP
	tid = omp_get_thread_num();
	#endif
	
	#pragma omp for collapse(3)
	for(pvsc_lidx_t i3=y->sys->b_x; i3<y->sys->n_x+y->sys->b_x; i3++)
	for(pvsc_lidx_t i2=y->sys->b_y; i2<y->sys->n_y+y->sys->b_y; i2++)
	for(pvsc_lidx_t i1=y->sys->b_z; i1<y->sys->n_z+y->sys->b_z; i1++) { 
			pvsc_lidx_t idxa = i1 +  y->sys->s_z * ( i2 + y->sys->s_y * i3);
			pvsc_lidx_t idx  = y->sys->n_o * idxa;
			for(pvsc_lidx_t i=0; i<y->sys->n_o; i++  )
			for(pvsc_lidx_t l=0; l<x->nb; l++){
				x->val[(idx+i)*x->nb + l] = 0.;
				for(pvsc_lidx_t k=0; k<n; k++)
					x->val[(idx+i)*x->nb + l] = M[k*x->nb + l] * y->val[(idx+i)*y->nb + k] ;
			}
		}
	}
	
	
	free(M);
	
	pvsc_timing_end(timing_num);
	return 0;
}




int print_vector( char * fname, pvsc_vector * vec  ){
	
	FILE * out = fopen( fname, "w");
	pvsc_volume v = vec->sys->v_mem;
	pvsc_lidx_t i[4],idx,xyz[4],k;
	
	for( i[3]=0; i[3]<v.n[3]; i[3]++)
	for( i[2]=0; i[2]<v.n[2]; i[2]++)
	for( i[1]=0; i[1]<v.n[1]; i[1]++)
	for( i[0]=0; i[0]<v.n[0]; i[0]++){
		idx = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
		//pvsc_vecidx2xyz( xyz, idx, v );
		pvsc_global_vecidx2xyz( xyz, idx, vec->sys );
		fprintf(out,"%d\t%d\t%d\t%d\t%d\t", idx, xyz[3], xyz[2], xyz[1], xyz[0]);
		for( k=0; k<vec->nb; k++)
			fprintf(out,"\t%g", vec->val[ idx*vec->nb + k ]);
		fprintf(out,"\n");
	}
	
	fclose(out);
	return 0;
}

