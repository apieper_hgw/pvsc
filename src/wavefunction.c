
#include "pvsc.h"
#include "pvsc/wavefunction.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef _OPENMP
#include <omp.h>
#endif


const pvsc_wavepacket PVSC_WAVEPACKET_INITIALIZER = {
	.kx = 0,
	.ky = 0,
	.kz = 0,
	.deltax = 0,
	.deltay = 0,
	.deltaz = 0,
	.enable_dx = false,
	.enable_dy = false,
	.enable_dz = false,
	.rx = 0,
	.ry = 0,
	.rz = 0,
	.phase = NULL,
	.ampli = NULL
	};


double pvsc_gen_wavepacket_func(pvsc_gidx_t * n, pvsc_lidx_t k, void * p , void * seed){
	
	pvsc_wavepacket * parm = p;
	
	double tmp = 1.;
	if( parm->ampli ) tmp = parm->ampli[n[0]];
	
	
	if( parm->enable_dx ) tmp *= exp( -(n[3]-parm->rx)*(n[3]-parm->rx)/(0.25*parm->deltax*parm->deltax));
	if( parm->enable_dy ) tmp *= exp( -(n[2]-parm->ry)*(n[2]-parm->ry)/(0.25*parm->deltay*parm->deltay));
	if( parm->enable_dz ) tmp *= exp( -(n[1]-parm->rz)*(n[1]-parm->rz)/(0.25*parm->deltaz*parm->deltaz));
	
	double            phi  = n[3]*parm->kx + n[2]*parm->ky + n[1]*parm->kz;
	if( parm->phase ) phi += parm->phase[n[0]];
	
	if ( !(k&1) ) tmp *= cos( M_PI*phi );
	else          tmp *= sin( M_PI*phi );
	
	
	
	return tmp;
}

