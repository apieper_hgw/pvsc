
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdarg.h>
//#include <unistd.h>

#include "pvsc/types.h"

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef PVSC_HAVE_MPI
#include <mpi.h>
#endif

static int rank = 0;
static int size = 1;
static int nthreads = 0;

static FILE * pvsc_stdout = NULL;

void * pvsc_get_stdout(){
	return pvsc_stdout;
	}
	
void * pvsc_set_stdout( char * name){
	
	if( name ) pvsc_stdout = fopen( name, "w" );
	else       pvsc_stdout = stdout;
	return pvsc_stdout;
}

void * pvsc_unset_stdout(){
	if( (stdout != pvsc_stdout) && pvsc_stdout ) fclose(pvsc_stdout);
	pvsc_stdout = NULL;
	return pvsc_stdout;
}

void pvsc_printf(const char *format, ...)
{
	if(pvsc_stdout){
		va_list args;
		char buffer[BUFSIZ];
		
		va_start(args, format);
		vsnprintf(buffer, sizeof buffer, format, args);
		va_end(args);
		fprintf( pvsc_stdout ,"%s", buffer);
	}
}

int pvsc_get_global_nthreads(){
	if( !nthreads ){
		#ifdef _OPENMP
			omp_set_dynamic(1);
			
			#pragma omp parallel
			{
				#pragma omp single
				{
					nthreads = omp_get_num_threads();
				}
			}
		#else
			nthreads = 1;
		#endif
	}
	
	return nthreads;
}


int pvsc_print_pvsc_info(){
	
	//int rank = 0;
	//int size = 1;
	
	#ifdef PVSC_HAVE_MPI
		MPI_Comm_rank( MPI_COMM_WORLD, &rank );
		MPI_Comm_size( MPI_COMM_WORLD, &size );
	#endif
	
	pvsc_printf("####### PVSC_INFO #########\n");
	#ifdef PVSC_HAVE_MPI
	pvsc_printf("  MPI run with %d processes\n", size);
	#else
	pvsc_printf("  none MPI \n");
	#endif
	#ifdef _OPENMP
	pvsc_printf("  OMP run with %d (default) threads\n", pvsc_get_global_nthreads() );
	#else
	pvsc_printf("  none OMP (threads = %d)\n", pvsc_get_global_nthreads());
	#endif
	
	pvsc_printf("     Streaming SIMD Extensions:");
	#ifdef __AVX2__
	pvsc_printf(" AVX2");
	#endif
	#ifdef __AVX__
	pvsc_printf(" AVX");
	#endif
	#ifdef __SSSE3__
	pvsc_printf(" SSSE3");
	#endif
	#ifdef __SSE4_2__
	pvsc_printf(" SSE4_2");
	#endif
	#ifdef __SSE4_1__
	pvsc_printf(" SSE4_1");
	#endif
	#ifdef __SSE3__
	pvsc_printf(" SSE3");
	#endif
	pvsc_printf("\n");
	
	//pvsc_printf(" SC_LEVEL1_DCACHE_LINESIZE = %d\n",sysconf(__SC_LEVEL1_DCACHE_LINESIZE);
	//pvsc_printf(" CACHE_LINESIZE = LEVEL1_DCACHE_LINESIZE \n");
	
	#ifdef PVSC_VERBOSE
	pvsc_printf("  verbose level = %d\n", PVSC_VERBOSE);
	#else
	pvsc_printf("  none verbose mode\n");
	#endif
	#ifdef PVSC_HAVE_ERROR_CHECK
	pvsc_printf("  PVSC_HAVE_ERROR_CHECK is enabled\n");
	#else
	pvsc_printf("  PVSC_HAVE_ERROR_CHECK is disabled\n");
	#endif
	
	return 0;
	}


int pvsc_read_args(char * option , int argc, char * argv[]){
	//============================ Reading commnad line arguments with flags ===============//
	int i=0;
	for ( i = 1; i < argc; i++)  
		if ( !strcmp(argv[i], option ) )
			return i;
	return 0;
}

pvsc_read_mpi_grid( int * mpi_xyz, int argc, char * argv[] ){
	
	int i = pvsc_read_args("-mpi_grid" , argc, argv);
	if(i) {
		int n_read[3];
		int n = 0;
		char * pch = strtok( argv[i+1], ",;-");
		while (pch != NULL)
		{
			n_read[n] = atoi(pch);
			n++;
			pch = strtok (NULL, ",;-");
		}
		switch (n){
			case 1: mpi_xyz[0] = 1;         mpi_xyz[1] = 1;         mpi_xyz[2] = n_read[0]; break;
			case 2: mpi_xyz[0] = 1;         mpi_xyz[1] = n_read[0]; mpi_xyz[2] = n_read[1]; break;
			case 3: mpi_xyz[0] = n_read[0]; mpi_xyz[1] = n_read[1]; mpi_xyz[2] = n_read[2]; break;
			default: return 1;
		}
		
	} else {
		mpi_xyz[0] = 1;
		mpi_xyz[1] = 1;
		mpi_xyz[2] = 1;
	}
	
	;
	
	return 0;
}

int pvsc_read_sys_conf( pvsc_system_geo * sys, int argc, char * argv[] ){
	
	int i = pvsc_read_args("-sys_size" , argc, argv);
	if(i) {
		pvsc_lidx_t n_read[3];
		int n = 0;
		char * pch = strtok( argv[i+1], ",;-");
		while (pch != NULL)
		{
			n_read[n] = atoi(pch);
			n++;
			pch = strtok (NULL, ",;-");
		}
		switch (n){
			case 1: sys->n_x = 1;         sys->n_y = 1;         sys->n_z = n_read[0]; break;
			case 2: sys->n_x = 1;         sys->n_y = n_read[0]; sys->n_z = n_read[1]; break;
			case 3: sys->n_x = n_read[0]; sys->n_y = n_read[1]; sys->n_z = n_read[2]; break;
			default: pvsc_printf("error invalid sys_size\n"); return 1;
		}
		
	} else {
		sys->n_x = 8;
		sys->n_y = 8;
		sys->n_z = 8;
	}
	
	sys->pbc_x = true;
	sys->pbc_y = true;
	sys->pbc_z = true;
	
	if( pvsc_read_args("-obc_x" , argc, argv)) sys->pbc_x = false;
	if( pvsc_read_args("-obc_y" , argc, argv)) sys->pbc_y = false;
	if( pvsc_read_args("-obc_z" , argc, argv)) sys->pbc_z = false;
	
	if( pvsc_read_args("-obc" , argc, argv)) { sys->pbc_x = false;
	                                           sys->pbc_y = false;
	                                           sys->pbc_z = false; }
	
	#if PVSC_VERBOSE>0
	pvsc_printf("pvsc_read_sys_conf() success\n");
	#endif
	
	return 0;
}


double pvsc_timing_since( struct timeval tp_start )  // zeitmessung auf Mikrosekunde genau
{
	struct timeval tp;
	
	gettimeofday(&tp, NULL);
	int sec = tp.tv_sec - tp_start.tv_sec;
	if ( sec < 0 ) sec += 86400;
	
	return (double) ( sec + 1.e-6 * (tp.tv_usec - tp_start.tv_usec ) );
}
