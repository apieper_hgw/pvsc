#include "pvsc.h"
#include <math.h>


double cheb_kernel_width(double x, double scale, double shift, int M  ){
	x = scale*(x-shift);
	return  sqrt((M-x*x*(M-1))/(2.*(M+1))*(1-cos(2.*M_PI/(M+1.))))/scale;
}


void pvsc_gibbs_Jackson (int M, double * mu, int L){
	
	int i,l;
	double g, phi=M_PI/(M+1);
	
	for(i=1;i<M;i++) {
		g = ( (M-i)*cos(i*phi) + sin((i+1)*phi)/sin(phi) )/(M+1);
		for(l=0;l<L;l++) mu[L*i+l] *= g;
		}
    
}

void pvsc_gibbs_Fejer (int M, double * mu, int L){
	
	int i,l;
	double g;
	
	for(i=1;i<M;i++){
		g =  1.-(double)(i)/M;
		for(l=0;l<L;l++) mu[L*i+l] *= g;
		}
}

void pvsc_gibbs_Lanczos (int M, double P, double * mu, int L){
	
	int i,l;
	double g;
	
	for(i=1;i<M;i++) {
		g=i*M_PI/M;
		g = pow(sin(g)/g,P);
		for(l=0;l<L;l++) mu[L*i+l] *= g;
		}
}


void pvsc_gibbs_Lorentz (int M,  double lamb, double * mu, int L){
	int i,l;
	double g;
	
	for(i=1;i<M;i++){
		g =  sinh(lamb*(1.-(double)(i)/M))/sinh(lamb);
		for(l=0;l<L;l++) mu[L*i+l] *= g;
		}
}
