#define _XOPEN_SOURCE 600

#include "pvsc.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>



#include <mkl.h>
#ifdef PVSC_HAVE_MPI
#include <mpi.h>
#endif


int pvsc_tr_lanczos_arnoldi( pvsc_vector ** vec_eig, pvsc_matrix * mat , double lambda_min, double lambda_max,
	                 double p_scale, double p_shift, double * p_coeff, int p_m, double p_cut,
	                 int MaxIts, pvsc_lidx_t trlz_m ){
	int timing_num = pvsc_timing_init( "pvsc_tr_lanczos_arnoldi" );
	
	
	pvsc_lidx_t nb_vec_trlz = 1;
	
	
	
	pvsc_spmvm_handel_t spmvm_conf[1] = { PVSC_SPMVM_HANDEL_INITIALIZER };
	spmvm_conf->mat = mat;
	//pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb_vec_trlz, true, "pvsc_lanczos() spmvm_conf->scale_h", 0 );
	//pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb_vec_trlz, true, "pvsc_lanczos() spmvm_conf->scale_h", 0 );
	//pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb_vec_trlz, true, "pvsc_lanczos() spmvm_conf->scale_z", 0 );
	
	//for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) spmvm_conf->scale_h[i] = 1.;// p_scale;
	//for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) spmvm_conf->shift_h[i] = 0.;// p_shift;
	//for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
	
	double *alpha      = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() alpha", 0 );
	double *beta       = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() beta" , 0 );
	double *one        = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() one"   , 0 );
	double *zero       = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() zero"   , 0 );
	double *tmp        = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() tmp" , 0 );
	
	double *trlz_eig   = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_eig"   , 0 );
	double *trlz_res   = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_res"   , 0 );
	double *eig        = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_eig"   , 0 );
	double *res        = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_res"   , 0 );
	
	double *tmp_o      = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() tmp_o"   , 0 );
	double *tmp_z      = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() tmp_z"   , 0 );
	
	pvsc_lidx_t * lock      = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m, true, "pvsc_tr_lanczos() lock"   , 0 );
	pvsc_lidx_t * select    = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m, true, "pvsc_tr_lanczos() select"   , 0 );
	pvsc_lidx_t * dest_idx  = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m,   true, "pvsc_tr_lanczos() dest_idx"   , 0 );
	pvsc_lidx_t select_n;
	pvsc_lidx_t lock_n = 0;
	
	pvsc_vector vec_lock[1] = { PVSC_VECTOR_INITIALIZER };
	pvsc_vector vec_res[ 1] = { PVSC_VECTOR_INITIALIZER };
	
	
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) zero[i] = 0.;
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) one[ i] = 1.;
	
	for(pvsc_lidx_t i=0; i<trlz_m; i++) dest_idx[i] = i;
	
	for(pvsc_lidx_t i=0; i<trlz_m; i++) tmp_o[i]  = 1.;
	
	pvsc_vector **trlz_vec_ptr = pvsc_malloc(NULL, sizeof(pvsc_vector*), trlz_m+1, true, "pvsc_tr_lanczos() vec_trlz", 0 );
	pvsc_vector * trlz_vec     = pvsc_malloc(NULL, sizeof(pvsc_vector), trlz_m+1, true, "pvsc_tr_lanczos() vec_trlz", 0 );
	for(int j=0; j<trlz_m+1; j++) { pvsc_create_vector( trlz_vec + j, nb_vec_trlz, mat->sys);
	                                trlz_vec_ptr[j] = trlz_vec + j;}
	
	
	pvsc_vector trlz_vec_block[    1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block    , trlz_m, mat->sys);
	pvsc_vector trlz_vec_block_eig[1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_eig, trlz_m, mat->sys);
	//pvsc_vector trlz_vec_block_res[1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_res, trlz_m, mat->sys);
	
	
	
	pvsc_vector_from_func( trlz_vec_ptr[0], pvsc_rand_vec, NULL);
	pvsc_vector_dot(beta, trlz_vec_ptr[0], trlz_vec_ptr[0]);
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) beta[i] = 1./sqrt(beta[i]);
	pvsc_vector_scale( trlz_vec, beta);
	
	pvsc_dense_redundant_matrix trlz_t[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	pvsc_dense_redundant_matrix trlz_n[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	pvsc_create_dense_redundant_matrix( trlz_t, trlz_m, trlz_m );
	pvsc_create_dense_redundant_matrix( trlz_n, trlz_m, trlz_m );
	
	pvsc_rayleigh_ritz_handle_t rrh[1] = {PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER};
	rrh->mat = mat;
	
	pvsc_lidx_t trlz_l = 0;
	
	int its = 0;
	while ( its < MaxIts ){
		its++;
		
		pvsc_lidx_t new_vec_count = 0;
		
		{
			pvsc_lidx_t idx_d = trlz_l, idx_s = 0;
			
			pvsc_vector_copy_select( 1, trlz_vec_block,     &idx_d,  trlz_vec_ptr[trlz_l%3],  &idx_s );
			
			pvsc_cheb_filter_block(  mat ,  p_scale, p_shift, p_m, p_coeff,  1, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[trlz_l%3], NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_copy_select( 1, trlz_vec_block_eig, &idx_d,  trlz_vec_ptr[(trlz_l+1)%3],  &idx_s );
			
			pvsc_vector_dot(alpha, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[trlz_l%3]);
			
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = - alpha[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(trlz_l+1)%3], tmp, trlz_vec_ptr[trlz_l%3] ); // obtional da pvsc_vector_class_gram_schmidt als naestes
			
			//            pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  trlz_vec_block, trlz_l+1 );
			//if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock, vec_lock->nb );
			
			pvsc_vector_dot(beta, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[(trlz_l+1)%3]);
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++){ beta[i] = sqrt(beta[i]); tmp[i]  = 1./beta[i];}
			pvsc_vector_scale(trlz_vec_ptr[(trlz_l+1)%3], tmp);
			
			trlz_l++;
		}
		for(int l=trlz_l; l<trlz_m; l++){
			
			pvsc_lidx_t idx_d = l, idx_s = 0;
			pvsc_vector_copy_select( 1, trlz_vec_block,     &idx_d,  trlz_vec_ptr[l%3],  &idx_s );
			
			pvsc_cheb_filter_block(  mat ,  p_scale, p_shift, p_m, p_coeff,  1, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[l%3], NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock, 0 );
			
			pvsc_vector_copy_select( 1, trlz_vec_block_eig, &idx_d,  trlz_vec_ptr[(l+1)%3],  &idx_s );
			
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i]  = -beta[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(l+1)%3], tmp, trlz_vec_ptr[(l-1)%3] );
			pvsc_vector_dot(alpha, trlz_vec_ptr[l%3], trlz_vec_ptr[(l+1)%3]);
			
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = - alpha[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(l+1)%3], tmp, trlz_vec_ptr[l%3] ); // obtional da pvsc_vector_class_gram_schmidt als naestes
			
			//            pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  trlz_vec_block, l+1 );
			//if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_dot(beta, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[(l+1)%3]);
			pvsc_printf("beta %g\n", beta[0]);
			if( (beta[0] < 1.e-8) || (beta[0] > 1.e+3) ){
				pvsc_printf("get new rand vec\n");
				new_vec_count++;
				pvsc_vector_from_func(          trlz_vec_ptr[(l+1)%3], pvsc_rand_vec, NULL);
				if(vec_res->nb -new_vec_count >= 0){ pvsc_vector_projection(         trlz_vec_ptr[(l+1)%3], vec_res, 0 );
					            pvsc_printf("with res_vec projektion\n"); }
				                pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3], trlz_vec_block, l+1 );
				if(lock_n)      pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock,  vec_lock->nb );
				pvsc_vector_dot(tmp, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[(l+1)%3]);
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = 1./sqrt(tmp[i]);
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) beta[i] = 0.;
			} else {
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++){ beta[i] = sqrt(beta[i]); tmp[i]  = 1./beta[i];}
			}
			
			pvsc_vector_scale(trlz_vec_ptr[(l+1)%3], tmp);
			
		}
		
		pvsc_vector_M_eq_axy( trlz_t->val, trlz_t->ld_col, 1., trlz_vec_block_eig, trlz_vec_block  );
		pvsc_vector_M_eq_axy( trlz_n->val, trlz_n->ld_col, 1., trlz_vec_block    , trlz_vec_block  );
		
		//LAPACKE_dsyev (LAPACK_COL_MAJOR,    'V' , 'U', trlz_m, trlz_t->val, trlz_t->ld_col,                              trlz_eig);
		LAPACKE_dsygv( LAPACK_COL_MAJOR, 1, 'V' , 'U', trlz_m, trlz_t->val, trlz_t->ld_col, trlz_n->val, trlz_n->ld_col, trlz_eig);
		
		#ifdef PVSC_HAVE_MPI
			MPI_Bcast(  trlz_t->val,  trlz_m*trlz_m,  MPI_DOUBLE, 0, mat->sys->mpi_comm);
			MPI_Bcast(  trlz_eig,     trlz_m,         MPI_DOUBLE, 0, mat->sys->mpi_comm);
		#endif
		pvsc_vector_xApby( 0, trlz_vec_block_eig, trlz_vec_block, trlz_t->val, trlz_t->ld_col );
		
		select_n = 0;
		for(pvsc_lidx_t i=trlz_m-1; i>=0; i--){
			pvsc_printf("pvsc_tr_lanczos(): #%02d: %.5f\n", i, trlz_eig[i]);
			if( trlz_eig[i] > 0.5*p_cut ) {  select[select_n] = i; select_n++; }
			else break;
		}
		
		pvsc_vector vec_selc[4] = { PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER };
		pvsc_create_vector( vec_selc+0, select_n, mat->sys);
		pvsc_create_vector( vec_selc+1, select_n, mat->sys);
		
		pvsc_vector_copy_select( select_n, vec_selc, dest_idx           ,   trlz_vec_block_eig, select   );
		
		
		rrh->mat = mat;
		rrh->vec_in = vec_selc+0;
		rrh->vec_eig = vec_selc+1;
		rrh->vec_res = vec_selc+0;
		rrh->eig = eig;
		rrh->res = res;
		pvsc_rayleigh_ritz( rrh );
		
		pvsc_lidx_t i=0;
		pvsc_lidx_t select_count = 0;
		pvsc_lidx_t   lock_count = 0;
		for(pvsc_lidx_t i=0; i<select_n; i++){
			pvsc_printf("eig: #%02d: %+.6e %+.6e\n", i, eig[i], res[i] );
			if( res[i] < 1.e-9/p_scale ) {   lock[ lock_count ]  = i;  lock_count++;   }
			else {                          select[ select_count] = i;  select_count++; }
		}
		
		pvsc_printf("\n select %d  lock %d\n\n", select_count, lock_count );
		
		if ( select_count + lock_count + lock_n ) {
			if( (vec_res->nb != select_count + lock_count + lock_n) && vec_res->nb ) pvsc_destroy_vector( vec_res );
			pvsc_create_vector( vec_res, select_count + lock_count + lock_n, mat->sys);
		}
		if ( select_count )  pvsc_vector_copy_select( select_count,   vec_res, dest_idx ,   rrh->vec_res,  select );
		
		//if ( lock_count + lock_n ) {
		//	if( (vec_res->nb != lock_count + lock_n) && vec_res->nb ) pvsc_destroy_vector( vec_res );
		//	pvsc_create_vector( vec_res, lock_count + lock_n, mat->sys);
		//}
		
		pvsc_vector_xApby( 0., trlz_vec_ptr[select_count%3], rrh->vec_res, tmp_o, 0 );
		
		
		if ( lock_count+lock_n > 0 ){
			pvsc_vector vec_lock_tmp[1] = { PVSC_VECTOR_INITIALIZER };
			pvsc_create_vector( vec_lock_tmp, lock_count + lock_n, mat->sys);
			
			if ( lock_count ) pvsc_vector_copy_select( lock_count, vec_lock_tmp, dest_idx, rrh->vec_eig, lock);
			
			if(lock_n){
				pvsc_vector_copy_select( lock_n,   vec_lock_tmp, dest_idx + lock_count,   vec_lock,           dest_idx );
				pvsc_destroy_vector( vec_lock );
			}
			
			
			lock_n += lock_count;
			
			pvsc_create_vector( vec_lock, lock_n, mat->sys);
			
			rrh->vec_in =  vec_lock_tmp;
			rrh->vec_eig = vec_lock;
			rrh->vec_res = vec_lock_tmp;
			rrh->eig = eig;
			rrh->res = res;
			pvsc_rayleigh_ritz( rrh );
			for(pvsc_lidx_t i=0; i<lock_n; i++){
				pvsc_printf("eig lock: #%02d: %+.6e %+.6e\n", i, eig[i], res[i] );
			}
			pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx + select_count ,   rrh->vec_res,  dest_idx );
			//pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx                  ,   rrh->vec_res,  dest_idx );
			pvsc_destroy_vector( vec_lock_tmp );
			
			
			pvsc_vector_xApby( 1., trlz_vec_ptr[select_count%3], rrh->vec_res, tmp_o, 0 );
			
			pvsc_vector_class_gram_schmidt( trlz_vec_ptr[select_count%3],  vec_lock, 0);
		}
		
		if(select_count){
			pvsc_create_vector( vec_selc+2, select_count, mat->sys);
			pvsc_create_vector( vec_selc+3, select_count, mat->sys);
			pvsc_vector_copy_select( select_count, vec_selc+2, dest_idx , rrh->vec_eig, select );
			
			//pvsc_vector_class_gram_schmidt( trlz_vec_ptr[select_count%3],  vec_selc+2, 0);
			
			pvsc_cheb_filter(  mat ,  p_scale, p_shift, p_m, p_coeff,  vec_selc+3, vec_selc+2, NULL, NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_dot(trlz_eig, vec_selc+3, vec_selc+2);
			
			/*
			select_n = 0;
			for(pvsc_lidx_t i=0; i<select_count; i++){
				if( trlz_eig[i] > p_cut ) {
				pvsc_printf("pvsc_tr_lanczos() post: #%02d: %.5f\n", i, trlz_eig[i]);
				              select[select_n] = i; select_n++; }
				
			}
			
			pvsc_vector_copy_select( select_n, trlz_vec_block,     dest_idx,   vec_selc+2, select );
			pvsc_vector_copy_select( select_n, trlz_vec_block_eig, dest_idx,   vec_selc+3, select );
			select_count = select_n;
			*/
			
			pvsc_vector_copy_select( select_count, trlz_vec_block,     dest_idx,   vec_selc+2, dest_idx );
			pvsc_vector_copy_select( select_count, trlz_vec_block_eig, dest_idx,   vec_selc+3, dest_idx );
			
			
			for(pvsc_lidx_t i=0; i<select_count; i++) tmp_z[i] -= trlz_eig[i];
			pvsc_vector_axpby( tmp_o, vec_selc+3, tmp_z, vec_selc+2 );
			pvsc_vector_dot(trlz_res, vec_selc+3, vec_selc+3);
			
			
			for(pvsc_lidx_t i=0; i<select_count; i++){
			pvsc_printf("pvsc_tr_lanczos post(): #%02d: %.6f  %+.6e\n", i, trlz_eig[i], sqrt(trlz_res[i]));
			}
			
			pvsc_destroy_vector( vec_selc+2);
			pvsc_destroy_vector( vec_selc+3);
		}
		
		pvsc_vector_dot(trlz_eig, vec_res, vec_res);
		for(pvsc_lidx_t i=0; i<vec_res->nb; i++) trlz_eig[i] = 1./sqrt(trlz_eig[i]);
		pvsc_vector_scale( vec_res, trlz_eig);
		
		
		pvsc_vector_from_func(  trlz_vec_ptr[select_count%3], pvsc_rand_vec, NULL);
		pvsc_vector_projection( trlz_vec_ptr[select_count%3] , vec_res,  vec_res->nb );
		
		
		pvsc_vector_dot(tmp, trlz_vec_ptr[select_count%3], trlz_vec_ptr[select_count%3]);
		tmp[0] = 1./sqrt(tmp[0]);
		pvsc_vector_scale(trlz_vec_ptr[select_count%3], tmp);
		
		trlz_l = select_count;
		pvsc_printf("restart with %d vectors\n", trlz_l);
		
		
		pvsc_destroy_vector( vec_selc+0);
		pvsc_destroy_vector( vec_selc+1);
	}
	
	
	
	for(int j=0; j<trlz_m+1; j++) pvsc_destroy_vector(trlz_vec + j);
	PVSC_FREE(trlz_vec)
	PVSC_FREE(trlz_vec_ptr)
	
	pvsc_destroy_vector( trlz_vec_block);
	pvsc_destroy_vector( trlz_vec_block_eig);
	//pvsc_destroy_vector( trlz_vec_block_res);
	
	//PVSC_FREE(spmvm_conf->scale_h)
	//PVSC_FREE(spmvm_conf->shift_h)
	
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	PVSC_FREE(zero  )
	PVSC_FREE(one   )
	//PVSC_FREE(mbeta )
	//PVSC_FREE(malpha)
	PVSC_FREE(tmp   )
	PVSC_FREE(trlz_eig   )
	PVSC_FREE(trlz_res   )
	PVSC_FREE(eig   )
	PVSC_FREE(res   )
	PVSC_FREE(tmp_o   )
	PVSC_FREE(tmp_z   )
	
	PVSC_FREE(select)
	PVSC_FREE(lock)
	PVSC_FREE(dest_idx)
	
	pvsc_destroy_dense_redundant_matrix( trlz_t );
	pvsc_destroy_dense_redundant_matrix( trlz_n );
	
	pvsc_timing_end(timing_num);
	return 0;
}



int pvsc_tr_lanczos( pvsc_vector ** vec_eig, pvsc_matrix * mat , double lambda_min, double lambda_max,
	                 double p_scale, double p_shift, double * p_coeff, int p_m, double p_cut,
	                 int MaxIts, pvsc_lidx_t trlz_m ){
	int timing_num = pvsc_timing_init( "pvsc_tr_lanczos_arnoldi" );
	
	
	pvsc_lidx_t nb_vec_trlz = 1;
	
	double *alpha      = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() alpha", 0 );
	double *beta       = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() beta" , 0 );
	double *one        = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() one"   , 0 );
	double *zero       = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() zero"   , 0 );
	double *tmp        = pvsc_malloc(NULL, sizeof(double), nb_vec_trlz, true, "pvsc_tr_lanczos() tmp" , 0 );
	
	double *trlz_eig   = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_eig"   , 0 );
	double *trlz_res   = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_res"   , 0 );
	double *eig        = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_eig"   , 0 );
	double *res        = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() trlz_res"   , 0 );
	
	double *tmp_o      = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() tmp_o"   , 0 );
	double *tmp_z      = pvsc_malloc(NULL, sizeof(double), trlz_m,      true, "pvsc_tr_lanczos() tmp_z"   , 0 );
	
	pvsc_lidx_t * lock      = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m, true, "pvsc_tr_lanczos() lock"   , 0 );
	pvsc_lidx_t * select    = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m, true, "pvsc_tr_lanczos() select"   , 0 );
	pvsc_lidx_t * dest_idx  = pvsc_malloc(NULL, sizeof(pvsc_lidx_t), trlz_m,   true, "pvsc_tr_lanczos() dest_idx"   , 0 );
	pvsc_lidx_t select_n;
	pvsc_lidx_t lock_n = 0;
	
	pvsc_vector vec_lock[1] = { PVSC_VECTOR_INITIALIZER };
	pvsc_vector vec_res[ 1] = { PVSC_VECTOR_INITIALIZER };
	
	
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) zero[i] = 0.;
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) one[ i] = 1.;
	
	for(pvsc_lidx_t i=0; i<trlz_m; i++) dest_idx[i] = i;
	
	for(pvsc_lidx_t i=0; i<trlz_m; i++) tmp_o[i]  = 1.;
	
	pvsc_vector **trlz_vec_ptr = pvsc_malloc(NULL, sizeof(pvsc_vector*), trlz_m+1, true, "pvsc_tr_lanczos() vec_trlz", 0 );
	pvsc_vector * trlz_vec     = pvsc_malloc(NULL, sizeof(pvsc_vector), trlz_m+1, true, "pvsc_tr_lanczos() vec_trlz", 0 );
	for(int j=0; j<trlz_m+1; j++) { pvsc_create_vector( trlz_vec + j, nb_vec_trlz, mat->sys);
	                                trlz_vec_ptr[j] = trlz_vec + j;}
	
	
	pvsc_vector trlz_vec_block[    1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block    , trlz_m, mat->sys);
	pvsc_vector trlz_vec_block_B[  1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_B  , trlz_m, mat->sys);
	pvsc_vector trlz_vec_block_eig[1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_eig, trlz_m, mat->sys);
	pvsc_vector trlz_vec_block_res[1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_res, trlz_m, mat->sys);
	
	pvsc_vector trlz_vec_block_A[      1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_A  , trlz_m, mat->sys);
	pvsc_vector trlz_vec_block_A_res[  1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_A_res  , trlz_m, mat->sys);
	
	pvsc_spmvm_handel_t spmvm_conf[1] = { PVSC_SPMVM_HANDEL_INITIALIZER };
	spmvm_conf->mat = mat;
	spmvm_conf->x = trlz_vec_block_eig;
	spmvm_conf->y = trlz_vec_block_A;
	spmvm_conf->dot_xy = eig;
	
	//pvsc_vector trlz_vec_block_res[1] = { PVSC_VECTOR_INITIALIZER};  pvsc_create_vector( trlz_vec_block_res, trlz_m, mat->sys);
	
	
	
	pvsc_vector_from_func( trlz_vec_ptr[0], pvsc_rand_vec, NULL);
	pvsc_vector_dot(beta, trlz_vec_ptr[0], trlz_vec_ptr[0]);
	for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) beta[i] = 1./sqrt(beta[i]);
	pvsc_vector_scale( trlz_vec, beta);
	
	pvsc_dense_redundant_matrix trlz_t[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	pvsc_dense_redundant_matrix trlz_n[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	pvsc_create_dense_redundant_matrix( trlz_t, trlz_m, trlz_m );
	pvsc_create_dense_redundant_matrix( trlz_n, trlz_m, trlz_m );
	
	pvsc_rayleigh_ritz_handle_t rrh[1] = {PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER};
	rrh->mat = mat;
	
	pvsc_lidx_t trlz_l = 0;
	
	MaxIts = 3;
	int its = 0;
	while ( its < MaxIts ){
		its++;
		
		pvsc_lidx_t new_vec_count = 0;
		
		{
			pvsc_lidx_t idx_d = trlz_l, idx_s = 0;
			
			pvsc_vector_copy_select( 1, trlz_vec_block,     &idx_d,  trlz_vec_ptr[trlz_l%3],  &idx_s );
			
			pvsc_cheb_filter_block(  mat ,  p_scale, p_shift, p_m, p_coeff,  1, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[trlz_l%3], NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_copy_select( 1, trlz_vec_block_B, &idx_d,  trlz_vec_ptr[(trlz_l+1)%3],  &idx_s );
			
			pvsc_vector_dot(alpha, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[trlz_l%3]);
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = - alpha[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(trlz_l+1)%3], tmp, trlz_vec_ptr[trlz_l%3] ); // obtional da pvsc_vector_class_gram_schmidt als naestes
			
			            pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  trlz_vec_block, trlz_l+1 );
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock, vec_lock->nb );
			
			pvsc_vector_dot(beta, trlz_vec_ptr[(trlz_l+1)%3], trlz_vec_ptr[(trlz_l+1)%3]);
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++){ beta[i] = sqrt(beta[i]); tmp[i]  = 1./beta[i];}
			pvsc_vector_scale(trlz_vec_ptr[(trlz_l+1)%3], tmp);
			
			trlz_l++;
		}
		for(int l=trlz_l; l<trlz_m; l++){
			
			pvsc_lidx_t idx_d = l, idx_s = 0;
			pvsc_vector_copy_select( 1, trlz_vec_block,     &idx_d,  trlz_vec_ptr[l%3],  &idx_s );
			
			pvsc_cheb_filter_block(  mat ,  p_scale, p_shift, p_m, p_coeff,  1, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[l%3], NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock, 0 );
			
			pvsc_vector_copy_select( 1, trlz_vec_block_B, &idx_d,  trlz_vec_ptr[(l+1)%3],  &idx_s );
			
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i]  = -beta[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(l+1)%3], tmp, trlz_vec_ptr[(l-1)%3] );
			pvsc_vector_dot(alpha, trlz_vec_ptr[l%3], trlz_vec_ptr[(l+1)%3]);
			for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = - alpha[i];
			pvsc_vector_axpby( one, trlz_vec_ptr[(l+1)%3], tmp, trlz_vec_ptr[l%3] ); // obtional da pvsc_vector_class_gram_schmidt als naestes
			
			            pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  trlz_vec_block, l+1 );
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_dot(beta, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[(l+1)%3]);
			pvsc_printf("beta %g\n", beta[0]);
			if( (beta[0] < 1.e-8) || (beta[0] > 1.e+3) ){
				pvsc_printf("get new rand vec\n");
				new_vec_count++;
				pvsc_vector_from_func(          trlz_vec_ptr[(l+1)%3], pvsc_rand_vec, NULL);
				if(vec_res->nb -new_vec_count >= 0){ pvsc_vector_projection(         trlz_vec_ptr[(l+1)%3], vec_res, 0 );
					            pvsc_printf("with res_vec projektion\n"); }
				                pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3], trlz_vec_block, l+1 );
				if(lock_n)      pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(l+1)%3],  vec_lock,  vec_lock->nb );
				pvsc_vector_dot(tmp, trlz_vec_ptr[(l+1)%3], trlz_vec_ptr[(l+1)%3]);
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) tmp[i] = 1./sqrt(tmp[i]);
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++) beta[i] = 0.;
			} else {
				for(pvsc_lidx_t i=0; i<nb_vec_trlz; i++){ beta[i] = sqrt(beta[i]); tmp[i]  = 1./beta[i];}
			}
			
			pvsc_vector_scale(trlz_vec_ptr[(l+1)%3], tmp);
			
		}
		
		pvsc_vector_M_eq_axy( trlz_t->val, trlz_t->ld_col, 1., trlz_vec_block_B, trlz_vec_block  );
		pvsc_vector_M_eq_axy( trlz_n->val, trlz_n->ld_col, 1., trlz_vec_block    , trlz_vec_block  );
		
		//LAPACKE_dsyev (LAPACK_COL_MAJOR,    'V' , 'U', trlz_m, trlz_t->val, trlz_t->ld_col,                              trlz_eig);
		LAPACKE_dsygv( LAPACK_COL_MAJOR, 1, 'V' , 'U', trlz_m, trlz_t->val, trlz_t->ld_col, trlz_n->val, trlz_n->ld_col, trlz_eig);
		
		#ifdef PVSC_HAVE_MPI
			MPI_Bcast(  trlz_t->val,  trlz_m*trlz_m,  MPI_DOUBLE, 0, mat->sys->mpi_comm);
			MPI_Bcast(  trlz_eig,     trlz_m,         MPI_DOUBLE, 0, mat->sys->mpi_comm);
		#endif
		pvsc_vector_xApby( 0, trlz_vec_block_eig, trlz_vec_block,   trlz_t->val, trlz_t->ld_col );
		pvsc_vector_xApby( 0, trlz_vec_block_res, trlz_vec_block_B, trlz_t->val, trlz_t->ld_col );
		
		for(pvsc_lidx_t i=0; i<trlz_m; i++) tmp_z[i] -= trlz_eig[i];
		pvsc_vector_axpby( tmp_o, trlz_vec_block_res, tmp_z, trlz_vec_block_eig );
		pvsc_vector_dot(trlz_res, trlz_vec_block_res, trlz_vec_block_res);
		
		mat->ham->spmvm_kernel( spmvm_conf );
		pvsc_vector_copy( trlz_vec_block_A_res, trlz_vec_block_eig);
		for(pvsc_lidx_t i=0; i<trlz_m; i++) tmp_z[i] -= eig[i];
		pvsc_vector_axpby( tmp_o, trlz_vec_block_A_res, tmp_z, trlz_vec_block_eig );
		pvsc_vector_dot( res, trlz_vec_block_A_res, trlz_vec_block_A_res);
		
		pvsc_lidx_t select_count = 0;
		pvsc_lidx_t   lock_count = 0;
		select_n = 0;
		for(pvsc_lidx_t i=trlz_m-1; i>=0; i--){
			pvsc_printf("pvsc_tr_lanczos(): #%02d: %.5f +/- %.5f   %+.7e +/- %.2e \n", i, trlz_eig[i], sqrt(trlz_res[i]), eig[i], sqrt(res[i]));
			if( trlz_eig[i] > p_cut ) {
				if( sqrt(res[i]) < 1.e-8 ) { 
					lock[ lock_count ]  = i;
					lock_count++;
				} else {
					select[ select_count] = i;
					select_count++;
				}
			}
			else break;
		}
		
		pvsc_printf("\n select %d  lock %d\n\n", select_count, lock_count );
		
		if ( lock_count > 0 ) {
			pvsc_vector vec_lock_tmp[1] = { PVSC_VECTOR_INITIALIZER };
			pvsc_create_vector( vec_lock_tmp, lock_count + lock_n, mat->sys);
			
			if ( lock_count ) pvsc_vector_copy_select( lock_count, vec_lock_tmp, dest_idx, trlz_vec_block_eig, lock);
			
			if(lock_n){
				pvsc_vector_copy_select( lock_n,   vec_lock_tmp, dest_idx + lock_count,   vec_lock,           dest_idx );
				pvsc_destroy_vector( vec_lock );
			}
			
			
			lock_n += lock_count;
			
			pvsc_create_vector( vec_lock, lock_n, mat->sys);
			
			rrh->vec_in =  vec_lock_tmp;
			rrh->vec_eig = vec_lock;
			rrh->vec_res = vec_lock_tmp;
			rrh->eig = eig;
			rrh->res = res;
			pvsc_rayleigh_ritz( rrh );
			for(pvsc_lidx_t i=0; i<lock_n; i++){
				pvsc_printf("eig lock: #%02d: %+.6e %+.6e\n", i, eig[i], res[i] );
			}
			pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx + select_count ,   rrh->vec_res,  dest_idx );
			//pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx                  ,   rrh->vec_res,  dest_idx );
			pvsc_destroy_vector( vec_lock_tmp );
		}
		
		pvsc_vector_copy_select( select_count , trlz_vec_block ,       dest_idx , trlz_vec_block_eig, select );
		
		pvsc_vector_copy_select( select_count , trlz_vec_block_A_res , dest_idx , trlz_vec_block_res, select );
		
		trlz_l = select_count;
		if( trlz_l%3 != trlz_m%3 )  pvsc_vector_copy( trlz_vec_ptr[trlz_l%3] , trlz_vec_ptr[trlz_m%3] );
		
		//pvsc_vector_class_gram_schmidt( trlz_vec_ptr[trlz_l%3] , trlz_vec_block ,      trlz_l );
		pvsc_vector_projection(         trlz_vec_ptr[trlz_l%3],  trlz_vec_block_A_res, trlz_l );
		
		pvsc_vector_dot( beta , trlz_vec_ptr[trlz_l%3] , trlz_vec_ptr[trlz_l%3] );
		tmp[0] = 1./sqrt(beta[0]);
		pvsc_vector_scale( trlz_vec_ptr[trlz_l%3] , tmp);
		
		/*
		pvsc_vector vec_selc[4] = { PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER };
		pvsc_create_vector( vec_selc+0, select_n, mat->sys);
		pvsc_create_vector( vec_selc+1, select_n, mat->sys);
		
		pvsc_vector_copy_select( select_n, vec_selc, dest_idx           ,   trlz_vec_block_eig, select   );
		
		
		rrh->mat = mat;
		rrh->vec_in = vec_selc+0;
		rrh->vec_eig = vec_selc+1;
		rrh->vec_res = vec_selc+0;
		rrh->eig = eig;
		rrh->res = res;
		pvsc_rayleigh_ritz( rrh );
		
		pvsc_lidx_t i=0;
		pvsc_lidx_t select_count = 0;
		pvsc_lidx_t   lock_count = 0;
		for(pvsc_lidx_t i=0; i<select_n; i++){
			pvsc_printf("eig: #%02d: %+.6e -/+ %.6e\n", i, eig[i], res[i] );
			if( res[i] < 1.e-9/p_scale ) {   lock[ lock_count ]  = i;  lock_count++;   }
			else {                          select[ select_count] = i;  select_count++; }
		}
		
		pvsc_printf("\n select %d  lock %d\n\n", select_count, lock_count );
		
		if ( select_count + lock_count + lock_n ) {
			if( (vec_res->nb != select_count + lock_count + lock_n) && vec_res->nb ) pvsc_destroy_vector( vec_res );
			pvsc_create_vector( vec_res, select_count + lock_count + lock_n, mat->sys);
		}
		if ( select_count )  pvsc_vector_copy_select( select_count,   vec_res, dest_idx ,   rrh->vec_res,  select );
		
		//if ( lock_count + lock_n ) {
		//	if( (vec_res->nb != lock_count + lock_n) && vec_res->nb ) pvsc_destroy_vector( vec_res );
		//	pvsc_create_vector( vec_res, lock_count + lock_n, mat->sys);
		//}
		
		pvsc_vector_xApby( 0., trlz_vec_ptr[select_count%3], rrh->vec_res, tmp_o, 0 );
		
		
		if ( lock_count+lock_n > 0 ){
			pvsc_vector vec_lock_tmp[1] = { PVSC_VECTOR_INITIALIZER };
			pvsc_create_vector( vec_lock_tmp, lock_count + lock_n, mat->sys);
			
			if ( lock_count ) pvsc_vector_copy_select( lock_count, vec_lock_tmp, dest_idx, rrh->vec_eig, lock);
			
			if(lock_n){
				pvsc_vector_copy_select( lock_n,   vec_lock_tmp, dest_idx + lock_count,   vec_lock,           dest_idx );
				pvsc_destroy_vector( vec_lock );
			}
			
			
			lock_n += lock_count;
			
			pvsc_create_vector( vec_lock, lock_n, mat->sys);
			
			rrh->vec_in =  vec_lock_tmp;
			rrh->vec_eig = vec_lock;
			rrh->vec_res = vec_lock_tmp;
			rrh->eig = eig;
			rrh->res = res;
			pvsc_rayleigh_ritz( rrh );
			for(pvsc_lidx_t i=0; i<lock_n; i++){
				pvsc_printf("eig lock: #%02d: %+.6e %+.6e\n", i, eig[i], res[i] );
			}
			pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx + select_count ,   rrh->vec_res,  dest_idx );
			//pvsc_vector_copy_select( lock_n ,   vec_res, dest_idx                  ,   rrh->vec_res,  dest_idx );
			pvsc_destroy_vector( vec_lock_tmp );
			
			
			pvsc_vector_xApby( 1., trlz_vec_ptr[select_count%3], rrh->vec_res, tmp_o, 0 );
			
			pvsc_vector_class_gram_schmidt( trlz_vec_ptr[select_count%3],  vec_lock, 0);
		}
		
		if(select_count){
			pvsc_create_vector( vec_selc+2, select_count, mat->sys);
			pvsc_create_vector( vec_selc+3, select_count, mat->sys);
			pvsc_vector_copy_select( select_count, vec_selc+2, dest_idx , rrh->vec_eig, select );
			
			//pvsc_vector_class_gram_schmidt( trlz_vec_ptr[select_count%3],  vec_selc+2, 0);
			
			pvsc_cheb_filter(  mat ,  p_scale, p_shift, p_m, p_coeff,  vec_selc+3, vec_selc+2, NULL, NULL);
			if(lock_n)  pvsc_vector_class_gram_schmidt( trlz_vec_ptr[(trlz_l+1)%3],  vec_lock,  vec_lock->nb );
			
			pvsc_vector_dot(trlz_eig, vec_selc+3, vec_selc+2);
			
			pvsc_vector_copy_select( select_count, trlz_vec_block,     dest_idx,   vec_selc+2, dest_idx );
			pvsc_vector_copy_select( select_count, trlz_vec_block_eig, dest_idx,   vec_selc+3, dest_idx );
			
			
			for(pvsc_lidx_t i=0; i<select_count; i++) tmp_z[i] -= trlz_eig[i];
			pvsc_vector_axpby( tmp_o, vec_selc+3, tmp_z, vec_selc+2 );
			pvsc_vector_dot(trlz_res, vec_selc+3, vec_selc+3);
			
			
			for(pvsc_lidx_t i=0; i<select_count; i++){
			pvsc_printf("pvsc_tr_lanczos post(): #%02d: %.6f  %+.6e\n", i, trlz_eig[i], sqrt(trlz_res[i]));
			}
			
			pvsc_destroy_vector( vec_selc+2);
			pvsc_destroy_vector( vec_selc+3);
			
		}
		
		pvsc_vector_dot(trlz_eig, vec_res, vec_res);
		for(pvsc_lidx_t i=0; i<vec_res->nb; i++) trlz_eig[i] = 1./sqrt(trlz_eig[i]);
		pvsc_vector_scale( vec_res, trlz_eig);
		
		
		pvsc_vector_from_func(  trlz_vec_ptr[select_count%3], pvsc_rand_vec, NULL);
		pvsc_vector_projection( trlz_vec_ptr[select_count%3] , vec_res,  vec_res->nb );
		
		
		pvsc_vector_dot(tmp, trlz_vec_ptr[select_count%3], trlz_vec_ptr[select_count%3]);
		tmp[0] = 1./sqrt(tmp[0]);
		pvsc_vector_scale(trlz_vec_ptr[select_count%3], tmp);
		
		trlz_l = select_count;
		pvsc_printf("restart with %d vectors\n", trlz_l);
		
		
		pvsc_destroy_vector( vec_selc+0);
		pvsc_destroy_vector( vec_selc+1);
		*/
		
	}
	
	
	
	for(int j=0; j<trlz_m+1; j++) pvsc_destroy_vector(trlz_vec + j);
	PVSC_FREE(trlz_vec)
	PVSC_FREE(trlz_vec_ptr)
	
	pvsc_destroy_vector( trlz_vec_block);
	pvsc_destroy_vector( trlz_vec_block_eig);
	//pvsc_destroy_vector( trlz_vec_block_res);
	
	//PVSC_FREE(spmvm_conf->scale_h)
	//PVSC_FREE(spmvm_conf->shift_h)
	
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	PVSC_FREE(zero  )
	PVSC_FREE(one   )
	//PVSC_FREE(mbeta )
	//PVSC_FREE(malpha)
	PVSC_FREE(tmp   )
	PVSC_FREE(trlz_eig   )
	PVSC_FREE(trlz_res   )
	PVSC_FREE(eig   )
	PVSC_FREE(res   )
	PVSC_FREE(tmp_o   )
	PVSC_FREE(tmp_z   )
	
	PVSC_FREE(select)
	PVSC_FREE(lock)
	PVSC_FREE(dest_idx)
	
	pvsc_destroy_dense_redundant_matrix( trlz_t );
	pvsc_destroy_dense_redundant_matrix( trlz_n );
	
	pvsc_timing_end(timing_num);
	return 0;
}



int pvsc_lanczos( pvsc_matrix * mat , double * shift, int Iter,  pvsc_vector * x, double * extrem_eig,  pvsc_krylov_options obt ){
	int timing_num = pvsc_timing_init( "pvsc_lancoz" );
	
	pvsc_lidx_t nb = x->nb;
	
	pvsc_vector *vp, *vc, *vm;
	pvsc_vector *v = pvsc_malloc(NULL, sizeof(pvsc_vector), Iter+1, false, "pvsc_lanczos() v", 0 );
	//v = malloc((Iter+1)*sizeof(pvsc_vector));
	
	pvsc_vector t[1];
	
	if( (obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC) ){
		for(int j=0; j<Iter+1; j++) pvsc_create_vector( v+j, nb, mat->sys);
	}else {
		pvsc_create_vector( v,   nb, mat->sys);
		pvsc_create_vector( v+1, nb, mat->sys);
	}
	
	pvsc_create_vector( t,   nb, mat->sys);
	
	
	pvsc_spmvm_handel_t spmvm_conf[1] = { PVSC_SPMVM_HANDEL_INITIALIZER };
	spmvm_conf->mat = mat;
	
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xx ), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->dot_xx", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xy ), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->dot_xy", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_yy ), sizeof(double), nb, true, "pvsc_lanczos() spmvm_conf->dot_yy", 0 );
	
	
	if( shift )  for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] =  shift[i];
	else         for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] =  0.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  1.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
	
	
	
	double *alpha  = pvsc_malloc(NULL, sizeof(double), nb*Iter, true, "pvsc_lanczos() alpha", 0 );
	double *beta   = pvsc_malloc(NULL, sizeof(double), nb*Iter, true, "pvsc_lanczos() beta" , 0 );
	double *zero   = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_lanczos() zero"  , 0 );
	double *one    = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_lanczos() one"   , 0 );
	double *mbeta  = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_lanczos() mbeta" , 0 );
	double *malpha = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_lanczos() malpha", 0 );
	double *tmp    = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_lanczos() tmp"   , 0 );
	
	
	pvsc_vector_dot( tmp, x, x);
	for(int i=0; i<nb; i++) tmp[i] = 1./sqrt(tmp[i]);
	for(int i=0; i<nb; i++) zero[i] = 0.;
	for(int i=0; i<nb; i++) one[ i] = 1.;
	
	pvsc_vector_axpby( zero, v, tmp, x);
	
	for(int i=0; i<nb; i++) mbeta[i] = 0.;
	for(int iter=0;iter<Iter;iter++){
		int timing_num_loop = pvsc_timing_init( "pvsc_lancoz_loop" );
		
		spmvm_conf->z = NULL;
		
		if((obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC)){
			vp = v + iter+1;
			vc = v + iter;
			vm = v + iter-1;
			if( !iter ) vm = vp;
		}else{
			vp = v + ((iter+1)&1);
			vc = v + (iter&1);
			vm = v + ((iter+1)&1);
		}
		spmvm_conf->x = vc;
		
		if( (vp == vm) || (obt & PVSC_KRYLOV_SQUARE_MAT) )
			spmvm_conf->y = t;
		else
			spmvm_conf->y = vp;
		
		mat->ham->spmvm_kernel( spmvm_conf );
		
		if( obt & PVSC_KRYLOV_SQUARE_MAT )
			for(int i=0; i<nb; i++) alpha[i+nb*iter] =   spmvm_conf->dot_yy[i];
		else
			for(int i=0; i<nb; i++) alpha[i+nb*iter] =   spmvm_conf->dot_xy[i];
		
		if(iter == Iter-1 ) break;
		
		for(int i=0; i<nb; i++)	malpha[i] = - alpha[i+nb*iter];
		
		if( obt & PVSC_KRYLOV_SQUARE_MAT ){
			spmvm_conf->x = spmvm_conf->y;
			spmvm_conf->y = vp;
			spmvm_conf->z = vm;
			for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  mbeta[i];
			mat->ham->spmvm_kernel( spmvm_conf );
			for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
			spmvm_conf->z = NULL;
		}else{
			if( spmvm_conf->y == t )
				pvsc_vector_axpby( mbeta, vp,    one, t );
			else
				pvsc_vector_axpby( one,   vp,  mbeta, vm );
			}
		pvsc_vector_axpby( one, vp, malpha, vc);
		
		
		#ifdef GHRHH
		if( ((obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC)) && iter ){
			spmvm_conf->x = v+iter  ;
			spmvm_conf->y = v+iter+1;
			vp = v+iter+1;
			
		}else{
			spmvm_conf->y = t;
			spmvm_conf->x = v + (iter&1);
			vp =            v + ((iter+1)&1);
		}
		
		
		mat->ham->spmvm_kernel( spmvm_conf );
		for(int i=0; i<nb; i++) {
			malpha[i]        = - spmvm_conf->dot_xy[i];
			alpha[i+nb*iter] =   spmvm_conf->dot_xy[i];
		}
		
		
		if(iter == Iter-1 ) break;
		if( ((obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC)) && iter ){
			pvsc_vector_axpby( one, v + iter+1,  mbeta, v + iter-1 );
			pvsc_vector_axpby( one, v + iter+1, malpha, v + iter   );
			
		}else{
			pvsc_vector_axpby( mbeta, v + ((iter+1)&1), one, t );
			pvsc_vector_axpby(   one, v + ((iter+1)&1), malpha,  v + (iter&1));
		}
		#endif
		pvsc_vector_dot(   tmp, vp, vp);
		
		for(int i=0; i<nb; i++){
			beta[ i+nb*iter] = sqrt(tmp[i]);
			mbeta[i] = - beta[ i+nb*iter];
			tmp[i] = 1./beta[ i+nb*iter];
		}
		
		pvsc_vector_scale(vp, tmp);
		
		pvsc_timing_end(timing_num_loop);
	}
	
	
	double *dia    = pvsc_malloc(NULL, sizeof(double), Iter,      true, "pvsc_lanczos() dia", 0 );
	double *subdia = pvsc_malloc(NULL, sizeof(double), Iter,      true, "pvsc_lanczos() subdia" , 0 );
	double *eigvec = pvsc_malloc(NULL, sizeof(double), Iter*Iter, true, "pvsc_lanczos() eigvec"  , 0 );
	double *etrvec = pvsc_malloc(NULL, sizeof(double), Iter*nb,   true, "pvsc_lanczos() etrvec"   , 0 );
	
	
	if( !mat->sys->mpi_rank ){
	for(int i=0; i<nb; i++){
		for(int j=0; j<Iter; j++) {
			dia[    j ] = alpha[i+nb*j];
			subdia[ j ] = beta[ i+nb*j];
		}
		//LAPACKE_dsterf (Iter, dia, subdia);
		LAPACKE_dsteqr( LAPACK_COL_MAJOR, 'I', Iter, dia, subdia, eigvec, Iter );
		#if PVSC_VERBOSE > 3
		pvsc_printf("pvsc_lanczos():  min: %+.3e, max: %+.3e\n", dia[0], dia[Iter-1]);
		#endif
		if( extrem_eig ) {
			if( !i ){
				extrem_eig[0] = dia[0];
				extrem_eig[1] = dia[Iter-1];
			}else{
				if( extrem_eig[0] > dia[0     ] ) extrem_eig[0] = dia[0];
				if( extrem_eig[1] < dia[Iter-1] ) extrem_eig[1] = dia[Iter-1];
			}
		}
		
		if( obt & PVSC_KRYLOV_MIN_EIG_VEC ){
			for(int j=0; j<Iter; j++)  etrvec[i+j*nb] = eigvec[j];
		}
		if( obt & PVSC_KRYLOV_MAX_EIG_VEC ){
			for(int j=0; j<Iter; j++)  etrvec[i+j*nb] = eigvec[j+Iter*(Iter-1)];
		}
		
		
		
	}}
	
	#ifdef PVSC_HAVE_MPI
	if( extrem_eig ) MPI_Bcast( extrem_eig, 2, MPI_DOUBLE, 0, x->sys->mpi_comm);
	#endif
	
	if( (obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC) ){
		#ifdef PVSC_HAVE_MPI
		MPI_Bcast( etrvec, Iter*nb, MPI_DOUBLE, 0, x->sys->mpi_comm);
		#endif
		
		for(int i=0; i<nb; i++) tmp[i] = etrvec[i];
		pvsc_vector_axpby( zero, x, tmp, v);
		for(int j=1; j<Iter; j++) {
			for(int i=0; i<nb; i++) tmp[i] = etrvec[i+j*nb];
			pvsc_vector_axpby( one, x, tmp, v+j);
		}
	}
	
	PVSC_FREE(dia );
	PVSC_FREE(subdia);
	PVSC_FREE(eigvec);
	PVSC_FREE(etrvec);
	
	
	//pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );
	
	if( (obt & PVSC_KRYLOV_MIN_EIG_VEC) || (obt & PVSC_KRYLOV_MAX_EIG_VEC) ){
		for(int j=0; j<Iter+1; j++) pvsc_destroy_vector( v+j);
	}else{
		pvsc_destroy_vector( v   );
		pvsc_destroy_vector( v+1 );
	}
	PVSC_FREE(v)
	pvsc_destroy_vector(t);
	
	PVSC_FREE(spmvm_conf->scale_h)
	PVSC_FREE(spmvm_conf->shift_h)
	PVSC_FREE(spmvm_conf->scale_z)
	PVSC_FREE(spmvm_conf->dot_yy)
	PVSC_FREE(spmvm_conf->dot_xx)
	PVSC_FREE(spmvm_conf->dot_xy)
	
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	PVSC_FREE(zero  )
	PVSC_FREE(one   )
	PVSC_FREE(mbeta )
	PVSC_FREE(malpha)
	PVSC_FREE(tmp   )
	
	pvsc_timing_end(timing_num);
	return 0;
	}

int pvsc_kacz( pvsc_matrix * mat , double * shift, int Iter, pvsc_vector * x_out, pvsc_vector * b, double damping){
	int timing_num = pvsc_timing_init( "pvsc_kacz" );
	
	pvsc_vector x_;
	pvsc_vector * x = &x_;
	pvsc_vector r[1];
	
	pvsc_lidx_t nb = b->nb;
	
	int err=0;
	if( !b ) {
		pvsc_printf("pvsc_kacz(): error: vec * b is NULL\n");
		err++;}
	if( !mat ) {
		pvsc_printf("pvsc_kacz(): error: mat * mat is NULL\n");
		err++;}
	
	if(err) return 1;
	
	if( (!x_out) || (b == x_out)  ){
		pvsc_create_vector( x, nb, mat->sys);
		pvsc_vector_copy( x, b);
	}else{
		x = x_out;
	}
	
	pvsc_create_vector( r, nb, mat->sys);
	
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	
	
	if( shift ) spmvm_conf->shift_h = shift;
	else    {    pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->shift_h", 0 );
	             for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] = 0.; }
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->scale_z", 0 );
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  1.;
	
	spmvm_conf->dot_allreduce = true;
	pvsc_malloc((void**) &(spmvm_conf->dot_yy), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->dot_yy", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xx), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->dot_xx", 0 );
	//pvsc_malloc((void**) &(spmvm_conf->dot_xy), sizeof(double), nb, true, "pvsc_kacz() spmvm_conf->dot_xy", 0 );
	
	
	
	
	for(int iter=0;iter<Iter;iter++){
		int timing_num_loop = pvsc_timing_init( "pvsc_kacz_loop" );
		
		for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  -1;
		spmvm_conf->x = x;
		spmvm_conf->y = r;
		spmvm_conf->z = b;
		//spmvm_conf->kacz = true;
		mat->ham->spmvm_kernel( spmvm_conf );
		//pvsc_vector_scale(x, double * scale);
		
		
		pvsc_printf("Iter %02d res: ", iter);
		for(int i=0; i<nb; i++) pvsc_printf(" %.3e", spmvm_conf->dot_yy[i]/spmvm_conf->dot_xx[i]);
		pvsc_printf("\n");
		
		
		for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  1./9;
		spmvm_conf->x = r;
		spmvm_conf->y = x;
		spmvm_conf->z = x;
		spmvm_conf->kacz = false;
		mat->ham->spmvm_kernel( spmvm_conf );
		
		pvsc_vector_copy(b, x);
		
		pvsc_timing_end(timing_num_loop);
	}
	
	
	//pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );
	
	if( (!x_out) || (b == x_out)  ){
		pvsc_vector_copy( b, x);
		pvsc_destroy_vector(x);
	}
	pvsc_destroy_vector(r);
	
	
	PVSC_FREE(spmvm_conf->scale_h)
	if( !shift ) { PVSC_FREE(spmvm_conf->shift_h) }
	PVSC_FREE(spmvm_conf->scale_z)
	PVSC_FREE(spmvm_conf->dot_yy)
	PVSC_FREE(spmvm_conf->dot_xx)
	//PVSC_FREE(spmvm_conf->dot_xy)
	
	pvsc_timing_end(timing_num);
	return 0;
	}


int pvsc_cg_min_eig( pvsc_matrix * mat , double * shift,  int Iter, pvsc_vector * x){
	int timing_num = pvsc_timing_init( "pvsc_cg_min_eig" );
	
	pvsc_lidx_t nb = x->nb;
	
	pvsc_vector r[1];
	pvsc_vector z[1];
	pvsc_create_vector( z, nb, mat->sys);
	pvsc_create_vector( r, nb, mat->sys);
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->individual_scale_shift = false;
	
	if( shift ) spmvm_conf->shift_h = shift;
	else    {    pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->shift_h", 0 );
	             for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] = 0.; }
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->scale_z", 0 );
	
	pvsc_malloc((void**) &(spmvm_conf->dot_yy), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_yy", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xx), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_xx", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xy), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_xy", 0 );
	
	double *alpha = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() alpha", 0 );
	double *beta  = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() beta" , 0 );
	double *one   = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() one"  , 0 );
	
	for(int i=0; i<nb; i++) one[i] =  1.;
	
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  1.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  -1.;
	spmvm_conf->y = r;
	spmvm_conf->z = x;
	spmvm_conf->x = x;
	mat->ham->spmvm_kernel( spmvm_conf );
	
	pvsc_printf("res: ");
	for(int i=0; i<nb; i++) pvsc_printf(" (%.1e %.1e)", spmvm_conf->dot_xx[i], spmvm_conf->dot_xy[i]);
	pvsc_printf("\n");
	
	for(int i=0; i<nb; i++) spmvm_conf->dot_xx[i] = 1./ sqrt(spmvm_conf->dot_xx[i]);
	pvsc_vector_scale( x, spmvm_conf->dot_xx);
	pvsc_vector_scale( r, spmvm_conf->dot_xx);
	
	
	spmvm_conf->x = r;
	spmvm_conf->y = z;
	spmvm_conf->z = NULL;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  1.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
	
	for(int iter=0;iter<Iter;iter++){
		int timing_num_loop = pvsc_timing_init( "pvsc_cg_min_eig_loop" );
		
		mat->ham->spmvm_kernel( spmvm_conf );
		
		for(int i=0; i<nb; i++) alpha[i] = spmvm_conf->dot_xx[i]/spmvm_conf->dot_xy[i];
		pvsc_printf("res: ");
		for(int i=0; i<nb; i++) pvsc_printf(" (%.1e %.1e %.1e)", spmvm_conf->dot_xx[i], spmvm_conf->dot_xy[i], alpha[i]);
		pvsc_printf("\n");
		pvsc_vector_axpby( one, x, alpha, r);
		
		for(int i=0; i<nb; i++) beta[i] = 1. + alpha[i];
		for(int i=0; i<nb; i++) alpha[i] =   - alpha[i];
		pvsc_vector_axpby( beta, r, alpha, z);
		
		
		
		
		pvsc_timing_end(timing_num_loop);
	}
	
	
	//pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );
	pvsc_destroy_vector(r);
	pvsc_destroy_vector(z);
	
	PVSC_FREE(spmvm_conf->scale_h)
	if( !shift ) {PVSC_FREE(spmvm_conf->shift_h)}
	PVSC_FREE(spmvm_conf->scale_z)
	PVSC_FREE(spmvm_conf->dot_yy)
	PVSC_FREE(spmvm_conf->dot_xx)
	PVSC_FREE(spmvm_conf->dot_xy)
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	
	pvsc_timing_end(timing_num);
	return 0;
	}

int pvsc_cg_eig( pvsc_matrix * mat , double * shift,  int Iter, pvsc_vector * x){
	int timing_num = pvsc_timing_init( "pvsc_cg_min_eig" );
	
	//double epsilon = 0.e-1;
	pvsc_lidx_t nb = x->nb;
	
	double * scale = pvsc_malloc( NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() scale", 0 );
	
	
	pvsc_vector r[1];
	pvsc_vector z[1];
	pvsc_create_vector( z  , nb, mat->sys);
	pvsc_create_vector( r, nb, mat->sys);
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->dot_allreduce = true;
	
	if( shift ) spmvm_conf->shift_h = shift;
	else    {    pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->shift_h", 0 );
	             for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] = 0.; }
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->scale_z", 0 );
	
	pvsc_malloc((void**) &(spmvm_conf->dot_yy), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_yy", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xx), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_xx", 0 );
	pvsc_malloc((void**) &(spmvm_conf->dot_xy), sizeof(double), nb, true, "pvsc_cg_min_eig() spmvm_conf->dot_xy", 0 );
	
	double *alpha = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() alpha", 0 );
	double *beta  = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() beta" , 0 );
	double *gamma = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg_min_eig() one"  , 0 );
	
	
	
	for(int i=0; i<nb; i++) scale[i] =  1.;
	
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale[i];
	spmvm_conf->y = z;
	spmvm_conf->z = NULL;
	spmvm_conf->x = x;
	mat->ham->spmvm_kernel( spmvm_conf );
	
	
	//for(int i=0; i<nb; i++) gamma[i] =  0.1;
	for(int i=0; i<nb; i++) gamma[i] =  0.1*sqrt(spmvm_conf->dot_xx[i]/spmvm_conf->dot_yy[i]);
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  -scale[i]*gamma[i]*gamma[i];
	for(int i=0; i<nb; i++) scale[i] = scale[i]*gamma[i];
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  1.;
	
	spmvm_conf->x = z;
	spmvm_conf->y = r;
	spmvm_conf->z = x;
	mat->ham->spmvm_kernel( spmvm_conf );
	
	//pvsc_printf("scale:\n");
	//for(int i=0; i<nb; i++) pvsc_printf(" %.1e, %.5e", scale[i], spmvm_conf->dot_yy[i] );
	//pvsc_printf("\n");
	
	// --------------------
	
	
	for(int iter=0;iter<Iter;iter++){
		
		for(int i=0; i<nb; i++) gamma[i] = 1./sqrt(spmvm_conf->dot_yy[i]) ;
		//for(int i=0; i<nb; i++) gamma[i] = 1.;
		
		int timing_num_loop = pvsc_timing_init( "pvsc_cg_min_eig_loop" );
		spmvm_conf->z = NULL;
		for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale[i]*gamma[i];
		for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
		spmvm_conf->x = r;
		spmvm_conf->y = z;
		mat->ham->spmvm_kernel( spmvm_conf );
		for(int i=0; i<nb; i++) alpha[i] = gamma[i]*gamma[i]*gamma[i]*spmvm_conf->dot_xx[i]/spmvm_conf->dot_yy[i];
		
		//pvsc_printf("res: ");
		//for(int i=0; i<nb; i++) pvsc_printf(" %.3e ", spmvm_conf->dot_xx[i]*alpha[i], );
		//pvsc_printf("\n");
		
		
		pvsc_vector_axpby( gamma, x, alpha, r);
		
		//for(int i=0; i<nb; i++) beta[i] = 1. + alpha[i];
		//for(int i=0; i<nb; i++) alpha[i] =   - alpha[i];
		//pvsc_vector_axpby( beta, r, alpha, z);
		
		for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  -scale[i]*gamma[i]*gamma[i]*spmvm_conf->dot_xx[i]/spmvm_conf->dot_yy[i];
		for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  gamma[i]*(1. + alpha[i]);
		spmvm_conf->z = r;
		spmvm_conf->y = r;
		spmvm_conf->x = z;
		mat->ham->spmvm_kernel( spmvm_conf );
		/*
		for(int i=0; i<nb; i++) beta[i] = 1./sqrt(spmvm_conf->dot_yy[i]);
		pvsc_vector_scale( x, beta);
		pvsc_vector_scale( r, beta);
		*/
		//pvsc_vector_dot( alpha, x, x);
		//pvsc_vector_dot( beta, r, r);
		//for(int i=0; i<nb; i++) pvsc_printf(" %.3e %.3e", alpha[i], beta[i] );
		//pvsc_printf("\n");
		
		pvsc_timing_end(timing_num_loop);
	}
	/*
	pvsc_vector_dot( alpha, x, x);
	for(int i=0; i<nb; i++) alpha[i] = 1./ sqrt(alpha[i]);
	pvsc_vector_scale( x, alpha);
	*/
	
	//pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );
	pvsc_destroy_vector(r);
	pvsc_destroy_vector(z);
	//pvsc_destroy_vector(z+1);
	
	PVSC_FREE(spmvm_conf->scale_h)
	if( !shift ) {PVSC_FREE(spmvm_conf->shift_h)}
	PVSC_FREE(spmvm_conf->scale_z)
	PVSC_FREE(spmvm_conf->dot_yy)
	PVSC_FREE(spmvm_conf->dot_xx)
	PVSC_FREE(spmvm_conf->dot_xy)
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	PVSC_FREE(gamma )
	PVSC_FREE(scale )
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_cg( pvsc_matrix * mat , double * shift, int Iter, pvsc_vector * x_out,  pvsc_vector * b, pvsc_krylov_options obt){
	int timing_num = pvsc_timing_init( "pvsc_cg" );
	
	double scale = 1.;
	double epsilon = 0.;
	
	pvsc_lidx_t nb = b->nb;
	
	pvsc_vector x_;
	pvsc_vector * x = &x_;
	if( (!x_out) || (b == x_out)  ){
		pvsc_create_vector( x, nb, mat->sys);
		pvsc_vector_copy( x, b);
	}else{
		x = x_out;
	}
	pvsc_vector d[2];
	pvsc_vector r[1];
	pvsc_vector z[1];
	pvsc_create_vector( d, nb, mat->sys);
	if( PVSC_KRYLOV_SQUARE_MAT & obt ) pvsc_create_vector( d + 1, nb, mat->sys);
	pvsc_create_vector( r, nb, mat->sys);
	pvsc_create_vector( z, nb, mat->sys);
	
	
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	
	
	if( shift ) spmvm_conf->shift_h = shift;
	else    {    pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cg() spmvm_conf->shift_h", 0 );
	             for(int i=0; i<nb; i++) spmvm_conf->shift_h[i] = 0.; }
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cg() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cg() spmvm_conf->scale_z", 0 );
	
	double *dot_rr = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg() dot_rr", 0 );
	double *dot_dz = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg() dot_dz", 0 );
	double *alpha  = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg() alpha", 0 );
	double *beta   = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg() beta", 0 );
	double *one    = pvsc_malloc(NULL, sizeof(double), nb, true, "pvsc_cg() one", 0 );
	
	
	for(int i=0; i<nb; i++) one[i] =  1.;
	
	spmvm_conf->x = x;
	if (PVSC_KRYLOV_SQUARE_MAT & obt){
		for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
		for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale;
		spmvm_conf->z = NULL;
		spmvm_conf->y = d+1;
		mat->ham->spmvm_kernel( spmvm_conf );
		spmvm_conf->x = d+1;
		}
	spmvm_conf->y = r;
	spmvm_conf->z = b;
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  1.;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  -scale;
	spmvm_conf->dot_yy = dot_rr;
	mat->ham->spmvm_kernel( spmvm_conf );
	
	if( epsilon ){
		for(int i=0; i<nb; i++) beta[i] = -epsilon*epsilon;
		pvsc_vector_axpby( one, r, beta, x);
		}
	
	spmvm_conf->dot_yy = NULL;
	spmvm_conf->z = NULL;
	for(int i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale;
	for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
	pvsc_vector_copy( d, r);
	if( !(PVSC_KRYLOV_SQUARE_MAT & obt) ) spmvm_conf->dot_xy = dot_dz;
	
	
	
	for(int iter=0;iter<Iter;iter++){
		int timing_num_loop = pvsc_timing_init( "pvsc_cg_loop" );
		
		spmvm_conf->x = d;
		if (PVSC_KRYLOV_SQUARE_MAT & obt){
			spmvm_conf->y = d+1;
			spmvm_conf->dot_yy = dot_dz;
			mat->ham->spmvm_kernel( spmvm_conf );
			spmvm_conf->x = d+1;
			spmvm_conf->dot_yy = NULL;
		}
		spmvm_conf->y = z;
		if( epsilon ){
			spmvm_conf->z = d;
			for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] = epsilon*epsilon;}
		mat->ham->spmvm_kernel( spmvm_conf );
		if( epsilon ){
			spmvm_conf->z = NULL;
			for(int i=0; i<nb; i++) spmvm_conf->scale_z[i] = 0.;}
		
		for(int i=0; i<nb; i++) alpha[i] = dot_rr[i]/dot_dz[i];
		
		pvsc_vector_axpby( one, x, alpha, d);
		for(int i=0; i<nb; i++) alpha[i] = -alpha[i];
		pvsc_vector_axpby( one, r, alpha, z);
		for(int i=0; i<nb; i++) beta[i] = 1./dot_rr[i];
		pvsc_vector_dot( dot_rr, r, r);
		for(int i=0; i<nb; i++) beta[i] *= dot_rr[i];
		pvsc_vector_axpby( beta, d, one, r);
		
		
		//pvsc_printf("res: ");
		//for(int i=0; i<nb; i++) pvsc_printf(" %.3e", dot_rr[i]);
		//pvsc_printf("\n");
		
		
		pvsc_timing_end(timing_num_loop);
	}
	
	
	//pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );
	
	if( (!x_out) || (b == x_out)  ){
		pvsc_vector_copy( b, x);
		pvsc_destroy_vector(x);
	}
	
	pvsc_destroy_vector(r);
	pvsc_destroy_vector(z);
	pvsc_destroy_vector(d);
	if( PVSC_KRYLOV_SQUARE_MAT & obt ) pvsc_destroy_vector(d+1);
	
	if( !shift ) { PVSC_FREE(spmvm_conf->shift_h) }
	PVSC_FREE(spmvm_conf->scale_h);
	PVSC_FREE(spmvm_conf->scale_z);
	PVSC_FREE(dot_rr)
	PVSC_FREE(dot_dz)
	PVSC_FREE(alpha )
	PVSC_FREE(beta  )
	PVSC_FREE(one   )
	
	pvsc_timing_end(timing_num);
	return 0;
	}
