#define _XOPEN_SOURCE 600

#include "pvsc.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>

#include <omp.h>


#define ACC 40.0
#define BIGNO 1.0e10
#define BIGNI 1.0e-10
#define BESSEL_EPS_CUT 1.e-17

int my_bessel_Jn(double * const bess_j, int const N, double const x)   //Returns the Bessel function Jn(x) for any real x and n ≥ 2.
{
	if ( N<0 ) return 1;
	int j,jsum,m,n;
	double bj,bjm,bjp,sum,ans;
	double const ax = fabs(x);
	double const tox=2.0/ax;
	int const end = (int)ax + 1;
	if (N >= 0) bess_j[0] = j0(ax);
	if (N >= 1) bess_j[1] = j1(ax);

	if (ax < 1.0e-14)  for (j = 2; j < N; j++) bess_j[j] = 0.;
	else {
		for (j=2;j<N;j++) { bess_j[j]=(j-1)*tox*bess_j[j-1]-bess_j[j-2];  if ( j > end ) break;}    //Upwards recurrence from J0 and J1.
		for (n = j+1; n < N; n++){
			m=2*((n+(int) sqrt(ACC*n))/2);
			jsum=0;			// jsum will alternate between 0 and 1; when it is 1, we accumulate in sum the even terms in
							//   (5.5.16).
			bjp=ans=sum=0.0;
			bj=1.0;
			for (j=m;j>0;j--) {     // The downward recurrence.
				bjm=j*tox*bj-bjp;
				bjp=bj;   bj=bjm;
				if (fabs(bj) > BIGNO) {      //  Renormalize to prevent overflows.
					bj *= BIGNI;  bjp *= BIGNI;  ans *= BIGNI;  sum *= BIGNI;}
				if (jsum) sum += bj;         //Accumulate the sum.
				jsum=!jsum;                  // Change 0 to 1 or vice versa.
				if (j == n) ans=bjp;}        // Save the unnormalized answer.
			sum=2.0*sum-bj;         // Compute (5.5.16)
			bess_j[n] = ans/sum;  // and use it to normalize the answer.
		if ( fabs(bess_j[n]) < BESSEL_EPS_CUT ) break;}
		for (j=n+1;j<N;j++)  bess_j[j] = 0.;
		}
	if ( x < 0. ) for (j = 1; j < N; j +=2) bess_j[j] *= -1.;
return 0;
}


extern void bessel_jn_array_( double *, int *, double * );

void cheb_tp_coeff_single (double complex ** coeff, int * M, double const dt, double const a, double const b,    double const eps ){
	int i,j;
	double tmp = dt/a;
	*M = (int)(2*tmp+10);
	double * Bessel_Tab = (double *)malloc((*M)*sizeof(double));
	my_bessel_Jn( Bessel_Tab, *M, tmp);
	//bessel_jn_array_( Bessel_Tab, M, &tmp);
	j=(int)tmp; do j++; while ( (fabs(Bessel_Tab[j]) > eps) && (j<*M) );
	if( *M == j )   pvsc_printf("Warning: Genauigkeit nicht erreicht\n");
	*M=j;
	
	pvsc_malloc( (void**) coeff, sizeof(double complex), *M, true, "cheb_tp_coeffs", 0  );
	(*coeff)[0] = cexp(-I*b*dt)*Bessel_Tab[0];
	for (j = 1; j < *M; j++) (*coeff)[j] = 2.0 * cexp( -I*( b*dt+M_PI*0.5*j )) * Bessel_Tab[j];
	free( Bessel_Tab );
}

/*
void cheb_toolbox_TP_coeff_multi (int const N, double const * const t, double const a, double const b,  int * M, double complex ** const coeff, double eps ){
  int i,j, M0 = M[0];
  double * Bessel_Tab = (double *)malloc(M0*sizeof(double));
  double tmp;
  for (i = 0; i < N; i++){
               tmp = t[i]/a;
               //my_bessel_Jn( Bessel_Tab, M0, tmp);
               bessel_jn_array_( Bessel_Tab, &M0, &tmp);
               j=0; do j++; while ( ((fabs(Bessel_Tab[j]) > eps) ||  (double)j < tmp ) && (j<M0) );
               M[i]=j;
               coeff[i] = (double complex *)malloc(M[i]*sizeof(double complex));
               coeff[i][0] = cexp(-I*b*t[i])*Bessel_Tab[0];
               for (j = 1; j < M[i]; j++) coeff[i][j] = 2.0 * cexp( -I*( b*t[i]+M_PI*0.5*j )) * Bessel_Tab[j];
       }
  free( Bessel_Tab );
}
*/



int pvsc_cheb_tp( pvsc_vector * x, double dt, pvsc_matrix * mat , double scale, double shift, double ** mu , int * M_mu){
	int timing_num = pvsc_timing_init( "pvsc_ChebTP" );
	double complex * coeff = NULL;
	int M;
	pvsc_lidx_t nb = x->nb;
	cheb_tp_coeff_single ( &coeff, &M, dt, scale, shift,  1.e-7 );
	
	int y_blocksize = 16;
	pvsc_vector *y = pvsc_malloc( NULL, sizeof(pvsc_vector), y_blocksize, false, "pvsc_cheb_tp() y", 0);
	for(int i=0; i<y_blocksize; i++) pvsc_create_vector( y+i, nb, x->sys);
	
	double * mu_tmp = pvsc_malloc( NULL, sizeof(double), nb*2*M, true, "pvsc_cheb_tp() mu_tmp", 0 );
	
	for(pvsc_lidx_t i=0; i<nb*2*M; i++) mu_tmp[i] = 0.;
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->individual_scale_shift = false;
	spmvm_conf->dot_allreduce = false;
	
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cheb_tp() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cheb_tp() spmvm_conf->shift_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cheb_tp() spmvm_conf->scale_z", 0 );
	
	
	for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->shift_h[i] =  shift;
	
	
	//if( !x->sys->mpi_rank ) pvsc_printf(" loop in\n" );
	for(pvsc_lidx_t m=0; m<M; m+=y_blocksize ){
		int block = y_blocksize;
		if( y_blocksize+m > M ) block = M-m;
		for(pvsc_lidx_t mm=0; mm<block; mm++){
			if( !(mm+m) ){
				pvsc_vector_copy( y+0, x);
				pvsc_vector_from_func(x, pvsc_zero_vec, NULL );
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale;
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
				spmvm_conf->z = y+1;
				spmvm_conf->x = y;
				
				continue;
			}
			spmvm_conf->dot_xx = mu_tmp + (2*(mm+m)-2)*nb;
			spmvm_conf->dot_xy = mu_tmp + (2*(mm+m)-1)*nb;
			spmvm_conf->y = y + mm;
			mat->ham->spmvm_kernel( spmvm_conf );
			
			spmvm_conf->z = spmvm_conf->x;
			spmvm_conf->x = spmvm_conf->y;
			
			if( (mm+m)==1 ){
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_h[i] =  2.*scale;
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_z[i] = -1.;        }
		}
		pvsc_vector_axpy_complex( x, (double *)(coeff+m), y, block, mat->ham->cmp);
	}
	
	#if PVSC_VERBOSE>1
	pvsc_print_performance_statistic( spmvm_conf );
	#endif
	
	for(int i=0; i<y_blocksize; i++) pvsc_destroy_vector(y+i);
	PVSC_FREE( y )
	
	if(mu){
		*M_mu = 2*(M-1);
		if(*mu) { PVSC_FREE(*mu) }
		pvsc_malloc((void**) mu, sizeof(double), *M_mu, true, "pvsc_cheb_tp() mu", 0 );
		for(pvsc_lidx_t m=0; m<(*M_mu); m++) {
			                                (*mu)[m]  = 0;
			for(pvsc_lidx_t i=0; i<nb; i++) (*mu)[m] += mu_tmp[m*nb + i];
		}
		
		#ifdef PVSC_HAVE_MPI
		MPI_Allreduce( MPI_IN_PLACE, *mu, *M_mu, MPI_DOUBLE, MPI_SUM, mat->sys->mpi_comm);
		#endif
		
		pvsc_dot_products_2_cheb_moments( *mu, *M_mu, 1 );
	}
	
	PVSC_FREE(spmvm_conf->scale_z)
	PVSC_FREE(spmvm_conf->scale_h)
	PVSC_FREE(spmvm_conf->shift_h)
	
	PVSC_FREE(mu_tmp)
	PVSC_FREE(coeff)
	
	pvsc_timing_end(timing_num);
	return 0;
	}
