#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "pvsc.h"

#ifdef PVSC_HAVE_FFTW
#include <fftw3.h>

#define FFT_HANDLE  fftw_plan
#define FFT_FREE    fftw_destroy_plan
#define FFT_EXE     fftw_execute
#define FFT_C_R2R_1D(h,ptr,m,n) h=fftw_plan_r2r_1d( 2*m , ptr, ptr + 2*m,  FFTW_REDFT01 , FFTW_ESTIMATE);
/*
#else

#include <ipps.h>

#define FFT_HANDLE  DFTI_DESCRIPTOR_HANDLE
#define FFT_FREE    DftiFreeDescriptor
#define FFT_EXE     DftiComputeForward
//#define FFT_EXE     DftiComputeBackward
#define  FFT_C_R2R_1D(h,n)  DftiCreateDescriptor( h, DFTI_DOUBLE, DFTI_REAL, 1, n ); \
*/                            
#endif

//#include <ipps.h>

int pvsc_dot_products_2_cheb_moments( double * mu, int M, int N ){
	int n,m;
	double tmp;
	int err = 0;
	
	for(n=0;n<N;n++){
		if( isnormal( mu[0 + n] ) ){
			tmp = 1./mu[0 + n];
			mu[  n]  = 1.;
			mu[N+n] *= tmp;
			for (m=2;m<M-1;m+=2){ mu[(m  )*N+n] = 2.*mu[(m  )*N+n]*tmp - mu[  n];
			                      mu[(m+1)*N+n] = 2.*mu[(m+1)*N+n]*tmp - mu[N+n];  
			                      if( fabs( mu[(m  )*N+n] ) > 1.  ) err++;
			                      if( fabs( mu[(m+1)*N+n] ) > 1.  ) err++;
			                  }
			if(M&1)           { mu[(M-1)*N+n] = 2.*mu[(M-1)*N+n]*tmp - mu[  n];
			                    if( fabs( mu[(M-1)*N+n] ) > 1.  ) err++;
			                  }
		} else {
			pvsc_printf( "Warning pvsc_dot_products_2_cheb_moments(): mu[0] is \n" );
			switch (fpclassify(mu[0 + n])) {
				case FP_INFINITE:  pvsc_printf ("infinite");  break;
				case FP_NAN:       pvsc_printf ("NaN");       break;
				case FP_ZERO:      pvsc_printf ("zero");      break;
				case FP_SUBNORMAL: pvsc_printf ("subnormal"); break;
				case FP_NORMAL:    pvsc_printf ("normal");    break;
			}
			if (signbit(mu[0 + n])) pvsc_printf (" negative\n");
			else pvsc_printf (" positive or unsigned\n");
			err++;
		}
	}
	
	if( err ){
		pvsc_printf( "Warning pvsc_dot_products_2_cheb_moments(): cheb_moments are not valide (error: %d) \n", err );
		pvsc_printf( "   probably wrong resacling in chebyshev iteration\n" );
		}
	
	return err;
}


int my_dct_III(double * out, double * in,   int M ){
	
	for(int i=0; i<M; i++){
		
		out[i] = in[0];
		double phi0 = M_PI*(i+0.5)/M;
		for(int j=1; j<M; j++) 
			out[i] += 2.0 * in[j] * cos(phi0*j);
	}
	return 0;
}

int ipp_dct_III(double * out, double * in,   int M ){
	
	for(int i=0; i<M; i++){
		
		out[i] = in[0];
		double phi0 = M_PI*(i+0.5)/M;
		for(int j=1; j<M; j++) 
			out[i] += 2.0 * in[j] * cos(phi0*j);
	}
	return 0;
}

int pvsc_kpm_1D_reconstruction_line_level (int const M, double * const mu, int const N, double level, double * const dos ){
	
	double phi = acos(level);
	
	for(int i=0; i<N; i++)	dos[i] = mu[i*M];
	
	for(int j=1; j<M; j++){
		double cos_p = cos( j*phi );
		for(int i=0; i<N; i++)
			dos[i] += 2.*cos_p*mu[i*M+j];
		}
	
	double tmp = 1./(M_PI*sqrt(1 - level*level));
	
	for(int i=0; i<N; i++)	dos[i] *= tmp;
		
		
	return 0;
}

int pvsc_kpm_eval_density_at_engegie(double * dense, double omega, int M, int N, double * mu , double scale, double shift, void gibbs (int const , double * , int const ) ){
    int m,n;
    double * g = (double *)malloc(sizeof(double)*M);
    for(m=0;m<M;m++) g[m]=1.;
    gibbs(M, g, 1);
    
    omega = scale*(omega-shift);
    scale = scale/(M_PI*sqrt(1-omega*omega));
    
    double P[2];
    P[0] = 1.;
    P[1] = omega;
    
    for(n=0;n<N;n++) dense[n]  =   scale*g[0]*mu[0+n*M]*P[0];
    for(n=0;n<N;n++) dense[n] += 2*scale*g[1]*mu[1+n*M]*P[1];
    
    for(m=2;m<M;m++)
    {
        P[m&1] = 2*omega*P[(m-1)&1] - P[(m-2)&1];
        for(n=0;n<N;n++) dense[n] += 2*scale*g[m]*mu[m+n*M]*P[m&1];
    }
    free(g);
    
    return 0;
}

int pvsc_kpm_1D_reconstruction_line (int const M, double * const mu, int const N, double * const dos )
{
    //        mu[i] = g[i]*mu[i]
    //
    //                DCT ( mu[i] )_om             DCT-III  i =                    { 0      , 1,  ..  M-1    }
    // dos[i]  = -------------------------             ->  om = cos( PI*(i+0.5)/M) { 0.99.. ,  ..... -0.99.. }
    //             PI * sqrt( 1 - om^2 )
    static int M_old=0;
    static int init=1;
    static double  *a;
#ifdef PVSC_HAVE_FFTW 
    static FFT_HANDLE  dct_mu2f;
#endif
    if ( (init == 0) && (  M_old != M  )   ){
        free(a);
#ifdef PVSC_HAVE_FFTW        
         FFT_FREE( dct_mu2f );
#endif
         M_old = 0; init=1;
        if ( N == 0 ) return -1;
    }
    if ( init == 1 ){
        M_old = M; init = 0;
        a = (double *) malloc ( 4*M*sizeof(double));
#ifdef PVSC_HAVE_FFTW
        dct_mu2f = fftw_plan_r2r_1d( 2*M , a, a + 2*M,  FFTW_REDFT01 , FFTW_ESTIMATE);
        //  REDFT01 (DCT-III):
        //                       M-1  /                                \ .
        //    f[i] = mu[0] + 2 * SUM  | mu[j] * cos( PI*j*(i+0.5) /M ) | .
        //                       j=1  \                                / .
#endif
        }
        int i,j,l;
        for (i = 0; i < M;     i++)      a[i] = mu[i];
        for (i = M; i < 2*M; i++)  a[i] = 0.0;
#ifdef PVSC_HAVE_FFTW
        fftw_execute( dct_mu2f );
#else
        my_dct_III( a+2*M, a,  2*M);
#endif
        for(i = 0; i< N; i++) dos[i] = 0.0;
        double const dE = 2.0/N;
        double * E  = (double *)malloc((2*M+2)*sizeof(double));
        double * Ec = (double *)malloc((2*M+2)*sizeof(double));
        for( i = 0; i<2*M+1; i++) E[i] = - cos(i*M_PI/(2*M)); E[2*M+1]= 2.0;
        for( i = 0; i<2*M+2; i++) Ec[i] = E[i];

        int x0=0,x=1;
        for ( j = 0; j < N; j++){
            while ( (-1.0+(j+1)*dE) > E[x] )  x++;
            E[x0  ]= -1.0+ j   *dE;
            E[x   ]= -1.0+(j+1)*dE;
            for ( l = x0; l < x; l++) dos[j] += a[4*M-1-l]*(E[l+1]-E[l])/(Ec[l+1]-Ec[l]);
            dos[j] /= 2*M;
            E[x0] = Ec[x0];
            E[x ] = Ec[x ];
            x0=x-1;
        }
        free(Ec);
        free(E);

        return 0;
//#else
//	pvsc_printf("pvsc_kpm_1D_reconstruction_line: error: pvsc is builed without fftw lib\n");
//	return 1;
//#endif
}

int pvsc_kpm_print_density(char * file_name, int M, int N, double * mu , double scale, double shift, double * info, void gibbs (int const , double * , int const ) ){
    int m,n;
    FILE * out = fopen(file_name,"w");
    double * spc = (double *)malloc(sizeof(double)*M);
    double E = 1./scale;
    double scal_spc = 0.5/E;
    

             
    double * mu_tmp = (double *)malloc(sizeof(double)*M);
    
    for ( n=0;n<N;n++ ){
        
        //for(m=0;m<M;m++) mu_tmp[m] = mu[m*N+n];
        for(m=0;m<M;m++) mu_tmp[m] = mu[m+M*n];
        
        if ( gibbs != NULL )
          gibbs(M, mu_tmp , 1); 
        
        pvsc_kpm_1D_reconstruction_line (M, mu_tmp, M, spc );
        if (N == 1)
            for(m=0;m<M;m++)  fprintf(out,"%g\t%g\n", E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
        else if ( info == NULL )
          {
            for(m=0;m<M;m++)  fprintf(out,"%d\t%g\t%g\n", n, E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
            fprintf(out,"\n");
          }
        else
          {
            for(m=0;m<M;m++)  fprintf(out,"%g\t%g\t%g\n", info[n], E*(2.*(m+0.5)/M-1.) + shift, scal_spc*M*spc[m]);
            fprintf(out,"\n");
          }

    }
    free(mu_tmp);
    free(spc);
    fclose(out);
    
    return 0;
}

int pvsc_kpm_print_density_level(char * file_name, int M, int N, double * mu , double scale, double shift, double level, double label, double * info, void gibbs (int const , double * , int const ) ){
    int m,n;
    FILE * out = fopen(file_name,"w");
    double * spc = (double *)malloc(sizeof(double)*N);
    double E = 1./scale;
    double scal_spc = 0.5/E;
    
    //double * mu_tmp = (double *)malloc(sizeof(double)*M*N);
    
    //for(m=0; m<M*N; m++) mu_tmp[m] = mu[m];
    //if ( gibbs != NULL ) gibbs( M, mu_tmp , 1);
          
    pvsc_kpm_eval_density_at_engegie(spc, scale*(level-shift), M, N, mu , scale, shift,  gibbs );
    //pvsc_kpm_1D_reconstruction_line_level( M, mu_tmp, N, scale*(level-shift), spc);
    
    for ( n=0;n<N;n++ ){
        fprintf(out,"%d\t%g\t%g\n", n, label, spc[n]);
    }
    fprintf(out,"\n");
    //free(mu_tmp);
    free(spc);
    fclose(out);
    
    return 0;
}




double pvsc_kpm_dos_integal ( double * mu, int M, double a, double b, void gibbs (int const , double * , int const ) ){
//   /     T_n(x)           /  - cos(n*y)                 sin(n*y)        sin(n*arccos(x))
//   | -------------- dx =  | ------------ sin(y) dy = - ---------- = - -------------------
//   /   sqrt(1-x*x)        /     sin(y)                     n                   n
	double * mu_tmp = pvsc_malloc( NULL, sizeof(double), M, true, NULL, 1245 );
	for(int i = 0; i<M; i++) mu_tmp[i] = mu[i];
	gibbs( M, mu_tmp, 1 );
	
	if( b < a) {
		double t = a;
		a = b;
		b = a;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_kpm_dos_integal()  b<a, a<->b\n");
		#endif 
		}
	
	if( a < -1 ){
		a = -1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_kpm_dos_integal()  a<-1, a -> -1\n");
		#endif 
	}
	if( b < -1 ){
		b = -1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_kpm_dos_integal()  b<-1, b -> -1\n");
		#endif 
	}
	if( a >  1 ){
		a =  1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_kpm_dos_integal()  a<-1, a -> 1\n");
		#endif 
	}
	if( b >  1 ){
		b =  1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_kpm_dos_integal()  b>1, b -> 1\n");
		#endif 
	}
	
	if(a == b){
		return 0.;
		}
	
	double acos_a = acos(a);
	double acos_b = acos(b);
	double sum                       =    mu_tmp[0]*( acos_a - acos_b );
	
	for( int n = 1; n<M; n++  ){
		 sum += 2.*mu_tmp[n]*(sin(n*acos_a) - sin(n*acos_b) )/n;
	}
	sum /= M_PI;
	PVSC_FREE(mu_tmp)
	
	return sum;

}

int pvsc_cheb_toolbox_window_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha ){
	
	if( scale < 0. )  return 0;
	
	double a = scale*(lambda_min-shift);
	double b = scale*(lambda_max-shift);
	
	if( b < a) {
		double t = a;
		a = b;
		b = a;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_cheb_toolbox_window_coeffs()  b<a, a<->b\n");
		#endif 
		}
	
	if( a < -1 ){
		a = -1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_cheb_toolbox_window_coeffs()  a<-1, a -> -1\n");
		#endif 
	}
	if( b < -1 ){
		b = -1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_cheb_toolbox_window_coeffs()  b<-1, b -> -1\n");
		#endif 
	}
	if( a >  1 ){
		a =  1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_cheb_toolbox_window_coeffs()  a<-1, a -> 1\n");
		#endif 
	}
	if( b >  1 ){
		b =  1;
		#if PVSC_VERBOSE > 2
		pvsc_printf("pvsc_cheb_toolbox_window_coeffs()  b>1, b -> 1\n");
		#endif 
	}
	
	double x;
	//if( lambda_min > lambda_max ) {x = lambda_min; lambda_min = lambda_max; lambda_max = x;}
	//if( (fabs(scale*(lambda_min-shift)) > 1.) || (fabs(scale*(lambda_max-shift)) > 1.) ) return 0;
	int m;
	//double phi_min = acos(scale*(lambda_min-shift));
	//double phi_max = acos(scale*(lambda_max-shift));
	double phi_min = acos(a);
	double phi_max = acos(b);

	int M =  (int)( alpha*M_PI/(phi_min - phi_max));
	
	if(M>0){   *coeff = (double *) pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
	
	(*coeff)[0] = (phi_min - phi_max)/M_PI;
	for (m = 1; m < M; m++)	(*coeff)[m] = 2.*(sin(m*phi_min)-sin(m*phi_max))/(m*M_PI);
	
	}
	return M;
}

int pvsc_cheb_toolbox_print_filter(char * file_name, double * coeff, int ldc, int M, double scale, double shift, int n ){
	
	FILE * out = fopen(file_name,"w");
	double * y = malloc(sizeof(double)*ldc);
	
	for( int i= 0; i<n; i++){
		
		double x = 2.*(i+0.5)/n - 1.0;
		
		double t[2];
		t[0] = 1.;
		t[1] = x;
		
		for( int j = 0; j<ldc; j++){
			y[j] = coeff[0]*t[0] + coeff[1*ldc]*t[1];
		}
		
		for( int m = 2; m<M; m++ ){
			t[m&1] = 2*t[(m-1)&1]*x - t[(m-2)&1];
			for( int j = 0; j<ldc; j++){
				y[j] += coeff[m*ldc]*t[m&1];
			}
		}
		
		fprintf(out, "%g", x/scale + shift);
		for( int j = 0; j<ldc; j++){
			fprintf(out, "\t%g", y[j]);
		}
		fprintf(out, "\n" );
	}
	free(y);
	fclose(out);
	return 0;
}

double pvsc_cheb_eval_cheb_poly( double * coeff, int M, double x){
	
	double t[2];
	t[0] = 1.;
	t[1] = x;
	
	double y = 0;
	if ( M > 0 ) y  = coeff[0]*t[0];
	if ( M > 1 ) y += coeff[1]*t[1];
	
	for( int m = 2; m<M; m++ ){
		t[m&1] = 2*t[(m-1)&1]*x - t[(m-2)&1];
		y += coeff[m]*t[m&1];
	}
	
	return y;
}

int pvsc_cheb_window_coeff( int M, double *e, double *r, double * c, int n, double scale, double shift){
	
	
	if (M<1) return 1;
	
	for(int j=0; j<n; j++){
		double a = scale*(e[j]-r[j]-shift);
		double b = scale*(e[j]+r[j]-shift);
		if ((a<-1) || (a>1)) return 2;
		if ((b<-1) || (b>1)) return 2;
		
		double phi_l = acos(a);
		double phi_h = acos(b);
		
		                        c[j    ] =   (phi_l - phi_h)/M_PI;
		for (int m=1; m<M; m++) c[j+m*n] = 2.*(sin(m*phi_l)-sin(m*phi_h))/(m*M_PI);
	}
	
	return 0;
}

int pvsc_cheb_toolbox_delta_coeffs(double lambda_min, double lambda_max, double scale, double shift, double ** coeff, double alpha ){
	
	int M_max = 128;
	double * mu = malloc( sizeof(double)*M_max );
	
	double phi_l = acos(scale*(lambda_min-shift));
	double phi_r = acos(scale*(lambda_max-shift));
	double phi_c = 0.5* (phi_l + phi_r);
	
	mu[0] = 1.;
	mu[1] = 2.*cos(phi_c);
	
	double tl[2];
	double tr[2];
	double tc[2];
	
	double xl = cos(phi_l);
	double xr = cos(phi_r);
	double xc = cos(phi_c);
	tl[0] = 1.;
	tr[0] = 1.;
	tc[0] = 1.;
	tl[1] = xl;
	tr[1] = xr;
	tc[1] = xc;
	
	double sum_l = tl[0]*mu[0] + tl[1]*mu[1];
	double sum_r = tr[0]*mu[0] + tr[1]*mu[1];
	double sum_c = tc[0]*mu[0] + tc[1]*mu[1];
	
	int m = 1;
	while( alpha*sum_c < sum_r && alpha*sum_c < sum_l ){
		
		m++;
		
		mu[m] = 2.*cos(m*phi_c);
		
		tl[m&1] = 2*tl[(m+1)&1]*xl - tl[m&1];
		tr[m&1] = 2*tr[(m+1)&1]*xr - tr[m&1];
		tc[m&1] = 2*tc[(m+1)&1]*xc - tc[m&1];
		
		sum_l += mu[m]*tl[m&1];
		sum_r += mu[m]*tr[m&1];
		sum_c += mu[m]*tc[m&1];
		
		
		if( m > M_max-3 ){
			M_max=2*M_max;
			double * mu_new = malloc( sizeof(double)*M_max );
			for(int i=0; i<m+1; i++) mu_new[i] = mu[i];
			free(mu);
			mu = mu_new;
			//mu = realloc(mu, M_max);
		}
	}
	
	* coeff = mu;
return m;
}

int pvsc_cheb_delta_coeff( int M, double *e, double * c, int n, double scale, double shift){
	
	
	
	if (M<1) return 1;
	
	for(int j=0; j<n; j++){
		double a = scale*(e[j]-shift);
		if ((a<-1) || (a>1)) return 2;
		
		double phi = acos(a);
		
		double tmp= 1./(sin(phi)*M);
		                        c[j    ] =    tmp;
		for (int m=1; m<M; m++) c[j+m*n] = 2.*tmp*cos(m*phi);
	}
	
	return 0;
}





int pvsc_cheb_delta_coeff_arg( double * c,  int M, double phi){
	
	double tmp= 1./M;
	//double tmp= 1./(sin(phi)*M;
	//tmp= 1.;
		                    c[0] =    tmp;
	for (int m=1; m<M; m++) c[m] = 2.*tmp*cos(m*phi);
	
	return 0;
}


