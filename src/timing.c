#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int pvsc_timing_stc_num = 0;
int pvsc_timing_stc_alloc = 0;

pvsc_timing * pvsc_timing_stc_ptr = NULL;


int pvsc_timing_init(char * name){
	
	int i;
	pvsc_timing * t = NULL;
	for (i=0; i<pvsc_timing_stc_num; i++){
		if ( !strcmp(pvsc_timing_stc_ptr[i].name, name ) ){
			t = pvsc_timing_stc_ptr + i;
			break;
		}
	}
	
	if (!t){
		if( pvsc_timing_stc_num == pvsc_timing_stc_alloc ){
			if(pvsc_timing_stc_ptr){
				pvsc_timing * tmp_ptr = malloc(sizeof(pvsc_timing)*(pvsc_timing_stc_alloc+64));
				for(i=0;i<pvsc_timing_stc_alloc;i++)  tmp_ptr[i] = pvsc_timing_stc_ptr[i];
				pvsc_timing_stc_alloc =+ 64;
				free(pvsc_timing_stc_ptr);
				pvsc_timing_stc_ptr = tmp_ptr;
			}else{
				pvsc_timing_stc_alloc = 64;
				pvsc_timing_stc_ptr   = malloc(sizeof(pvsc_timing) * pvsc_timing_stc_alloc );
			}
		}
		
		i = pvsc_timing_stc_num;
		t = pvsc_timing_stc_ptr + i;
		pvsc_timing_stc_num++;
		pvsc_timing_stc_ptr[i].name = malloc(sizeof(char)*128);
		strcpy( pvsc_timing_stc_ptr[i].name , name );
		pvsc_timing_stc_ptr[i].counter = 0;
		pvsc_timing_stc_ptr[i].t_max = 0.;
		pvsc_timing_stc_ptr[i].t_min = 0.;
		pvsc_timing_stc_ptr[i].t_acc = 0.;
		pvsc_timing_stc_ptr[i].t_acc10skip = 0.;
	}
	
	gettimeofday(&(t->current_tp), NULL);
	return i;
}

double pvsc_timing_end(int i){
	
	struct timeval tp, tp_start = pvsc_timing_stc_ptr[i].current_tp;
	gettimeofday(&tp, NULL);
	int sec = tp.tv_sec - tp_start.tv_sec;
	if ( sec < 0 ) sec += 86400;
	double t = sec + 1.e-6 *(tp.tv_usec - tp_start.tv_usec );
	pvsc_timing_stc_ptr[i].t_acc += t;
	
	if( (t > pvsc_timing_stc_ptr[i].t_max) || ( pvsc_timing_stc_ptr[i].counter == 0 ) ) pvsc_timing_stc_ptr[i].t_max = t;
	if( (t < pvsc_timing_stc_ptr[i].t_min) || ( pvsc_timing_stc_ptr[i].counter == 0 ) ) pvsc_timing_stc_ptr[i].t_min = t;
	if( pvsc_timing_stc_ptr[i].counter>=10 )                                            pvsc_timing_stc_ptr[i].t_acc10skip += t;
	
	
	pvsc_timing_stc_ptr[i].counter++;
	return t;
}

int pvsc_timing_print(){
	 
	char line[200];

	memset(line,'\0',200);
	memset(line,'=',120);
	pvsc_printf("%s\n", line);
	
	pvsc_printf("%50s :  %7s  %9s | %8s %8s %8s %8s\n", "timing area       ", "calls", "t_acc  ",  "t_avg  " , "t_avg10s" , "t_min  "  ,"t_max  ");
	
	memset(line,'-',120);
	pvsc_printf("%s\n", line);
	
	memset(line + 50,'\0',1);
	
	int * list = malloc(sizeof(int)*pvsc_timing_stc_num);
	
	for(int i=0;i<pvsc_timing_stc_num;i++){
		list[i] = i;
		int j=0;
		while(  (pvsc_timing_stc_ptr[i].t_acc < pvsc_timing_stc_ptr[list[j]].t_acc) && (j<i) ) j++;
		for(int l=i; l>j; l--) list[l] = list[l-1];
		list[j] = i;
	}
	
	
	for(int j=0;j<pvsc_timing_stc_num;j++){
		int i = list[j];
		double tavg_10skip = 0.;
		if ( pvsc_timing_stc_ptr[i].counter > 10 ) tavg_10skip = pvsc_timing_stc_ptr[i].t_acc10skip/( pvsc_timing_stc_ptr[i].counter - 10 );
		strncpy(line, pvsc_timing_stc_ptr[i].name , 50);
		pvsc_printf("%50s :  %7d  %.3e | %.2e %.2e %.2e %.2e\n", line, pvsc_timing_stc_ptr[i].counter, pvsc_timing_stc_ptr[i].t_acc, pvsc_timing_stc_ptr[i].t_acc/pvsc_timing_stc_ptr[i].counter, tavg_10skip,  pvsc_timing_stc_ptr[i].t_min, pvsc_timing_stc_ptr[i].t_max );
	}
	
	memset(line,'=',120);
	pvsc_printf("%s\n", line);
	
	free(list);
	
	return 0;
}



/*
double timing()  // zeitmessung auf Mikrosekunde genau
{
   struct timeval tp;

   gettimeofday(&tp, NULL);
   return (double) (tp.tv_sec + 1.e-6 * tp.tv_usec);

}

double timing_since( struct timeval tp_start )  // zeitmessung auf Mikrosekunde genau
{
   struct timeval tp;

   gettimeofday(&tp, NULL);
   int sec = tp.tv_sec - tp_start.tv_sec;
   if ( sec < 0 ) sec += 86400;

   return (double) ( sec + 1.e-6 * (tp.tv_usec - tp_start.tv_usec ) );
}
*/
