
#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>

pvsc_volume pvsc_init_volume( pvsc_lidx_t * n){
	pvsc_volume v;
	v.offset = 0;
	v.n[0] = n[0];
	v.n[1] = n[1];
	v.n[2] = n[2];
	v.n[3] = n[3];
	v.s[0] = 1;
	v.s[1] = v.s[0]*v.n[0];
	v.s[2] = v.s[1]*v.n[1];
	v.s[3] = v.s[2]*v.n[2];
	
	return v;
}

pvsc_volume pvsc_get_sub_volume(pvsc_lidx_t * p,  pvsc_lidx_t * n,  pvsc_volume w ){
	pvsc_volume v = w;
	v.n[0] = n[0];
	v.n[1] = n[1];
	v.n[2] = n[2];
	v.n[3] = n[3];
	v.offset += p[0]*v.s[0];
	v.offset += p[1]*v.s[1];
	v.offset += p[2]*v.s[2];
	v.offset += p[3]*v.s[3];
	
	return v;
}

int pvsc_vecidx2xyz( pvsc_lidx_t * xyz, pvsc_lidx_t vecidx, pvsc_volume v ){
	
	xyz[3] = vecidx/v.s[3];  vecidx -= xyz[3]*v.s[3];
	xyz[2] = vecidx/v.s[2];  vecidx -= xyz[2]*v.s[2];
	xyz[1] = vecidx/v.s[1];  vecidx -= xyz[1]*v.s[1];
	xyz[0] = vecidx/v.s[0];
	/*
	xyz[3] = vecidx%v.n[0];  vecidx /= v.n[0];
	xyz[2] = vecidx%v.n[1];  vecidx /= v.n[1];
	xyz[1] = vecidx%v.n[2];  vecidx /= v.n[2];
	xyz[0] = vecidx;
	*/
	//xyz[2] = vecidx%(sys->n_z + 2);  vecidx /= (sys->n_z + 2);
	//xyz[1] = vecidx%(sys->n_y + 2);  vecidx /= (sys->n_y + 2);
	//xyz[0] = vecidx;
	
	return 0;
}

int pvsc_global_vecidx2xyz( pvsc_lidx_t * xyz, pvsc_lidx_t vecidx, pvsc_system_geo * sys ){
	
	pvsc_vecidx2xyz( xyz, vecidx, sys->v );
	
	xyz[1] += sys->mpi_rank_z*sys->n_z - sys->b_z;
	xyz[2] += sys->mpi_rank_y*sys->n_y - sys->b_y;
	xyz[3] += sys->mpi_rank_x*sys->n_x - sys->b_x;
	if( xyz[1] <  0                        ) xyz[1] += sys->mpi_size_z*sys->n_z;
	if( xyz[2] <  0                        ) xyz[2] += sys->mpi_size_y*sys->n_y;
	if( xyz[3] <  0                        ) xyz[3] += sys->mpi_size_x*sys->n_x;
	if( xyz[1] >= sys->mpi_size_z*sys->n_z ) xyz[1] -= sys->mpi_size_z*sys->n_z;
	if( xyz[2] >= sys->mpi_size_y*sys->n_y ) xyz[2] -= sys->mpi_size_y*sys->n_y;
	if( xyz[3] >= sys->mpi_size_x*sys->n_x ) xyz[3] -= sys->mpi_size_x*sys->n_x;
	
	
	return 0;
}

int print_sub_volume( char * fname, pvsc_volume v  ){
	
	FILE * out = fopen( fname, "w");
	
	pvsc_lidx_t i[4],idx,xyz[4];
	
	for( i[3]=0; i[3]<v.n[3]; i[3]++)
	for( i[2]=0; i[2]<v.n[2]; i[2]++)
	for( i[1]=0; i[1]<v.n[1]; i[1]++)
	for( i[0]=0; i[0]<v.n[0]; i[0]++){
		idx = v.offset + i[0]*v.s[0] + i[1]*v.s[1] + i[2]*v.s[2] + i[3]*v.s[3];
		pvsc_vecidx2xyz( xyz, idx, v );
		fprintf(out,"%d\t%d\t%d\t%d\t%d\n", idx, xyz[0], xyz[1], xyz[2], xyz[3]);
	}
	
	fclose(out);
	return 0;
}
