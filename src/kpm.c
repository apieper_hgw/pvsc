#define _XOPEN_SOURCE 600

#include "pvsc.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <mkl_lapacke.h>

int pvsc_cheb_dos( double * mu, pvsc_matrix * mat , int R, int nb, int M_in, double scale, double shift, pvsc_vector * y){
	// input      { mat, nb, M_in, scale, shift }
	//   optional { y, R>nb }
	//
	// output     {  mu[0, .. , M_in-1] }
	
	if( !mat ) {
		pvsc_printf("error: pvsc_cheb_dos(): mat is NULL\n");
		return 1;
		}
	
	if( nb < 1 ) {
		pvsc_printf("error: pvsc_cheb_dos(): nb is < 1\n");
		return 1;
		}
	
	int timing_num = pvsc_timing_init( "pvsc_kpm_ChebDos" );
	int M = M_in >> 1;
	pvsc_printf("Strat cheb dos  nb = %d; R = %d\n", nb, R );
	int Iter = R/nb;
	if(Iter == 0) Iter=1;
	R = nb*Iter;
	
	pvsc_vector x[2] = {  PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
	int i,m,iter;
	if ( y ){
		Iter = 1;
		R = y->nb;
		nb = R;
	}
	
	
	pvsc_create_vector( x+0, nb, mat->sys);
	pvsc_create_vector( x+1, nb, mat->sys);
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->dot_allreduce = false;
	
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cheb_dos() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cheb_dos() spmvm_conf->shift_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cheb_dos() spmvm_conf->scale_z", 0 );
	
	for(i=0; i<nb; i++) spmvm_conf->shift_h[i] =  shift;
	
	
	double * mu_tmp = pvsc_malloc( NULL, sizeof(double), R*M_in, false, "pvsc_cheb_dos() mu_tmp",  0 );
	for(i=0; i<R*M_in; i++) mu_tmp[i] = 0.;
	
	for(iter=0;iter<Iter;iter++){
		for(i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale;
		for(i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
		
		if( y )
			pvsc_vector_copy( x+0, y);
		else
			pvsc_vector_from_func( x+0, pvsc_rand_vec, NULL);
		
		int timing_num_loop = pvsc_timing_init( "pvsc_kpm_ChebDos_loop" );
		spmvm_conf->x = x+0;
		spmvm_conf->y = x+1;
		spmvm_conf->z = NULL;
		//spmvm_conf->z = spmvm_conf->y;
		spmvm_conf->dot_xx = mu_tmp + iter*nb + 0*R;
		spmvm_conf->dot_xy = mu_tmp + iter*nb + 1*R;
		mat->ham->spmvm_kernel( spmvm_conf );
		m=0;
		
		for(i=0; i<nb; i++) spmvm_conf->scale_h[i] *=  2.;
		for(i=0; i<nb; i++) spmvm_conf->scale_z[i]  = -1.;
		
		for( m=1; m<M; m++ ){
			spmvm_conf->x = x + ( m   &1);
			spmvm_conf->y = x + ((m+1)&1);
			spmvm_conf->z = x + ((m+1)&1);
			//spmvm_conf->z = spmvm_conf->y;
			spmvm_conf->dot_xx = mu_tmp + iter*nb + (2*m  )*R;
			spmvm_conf->dot_xy = mu_tmp + iter*nb + (2*m+1)*R;
			if( (m+1==M) && (M_in & 1) )
			  spmvm_conf->dot_yy = mu_tmp + iter*nb + (2*m+2)*R;
			mat->ham->spmvm_kernel( spmvm_conf );
		}
		spmvm_conf->dot_yy = NULL;
		pvsc_timing_end(timing_num_loop);
	}
	
	pvsc_print_performance_statistic( spmvm_conf );
	
	pvsc_destroy_vector(x  );
	pvsc_destroy_vector(x+1);
	
	for( m=0; m<M_in; m++ ){
		mu[m] = 0.;
		for(i=0; i<R; i++) mu[m] += mu_tmp[R*m+i];
	}
	
	
#ifdef PVSC_HAVE_MPI
	MPI_Allreduce( MPI_IN_PLACE, mu, M_in, MPI_DOUBLE, MPI_SUM, mat->sys->mpi_comm);
#endif
	
	//pvsc_vector_write( "kpm_vec.vec", x  );
	
	pvsc_dot_products_2_cheb_moments( mu, M_in, 1 );
	
	// pvsc_printf("pvsc_cheb_dos FLOPs per Process per spmvm: %g\n", pvsc_spmvm_dot_axpby_flops_pre_process( spmvm_conf ) );

	
	
	PVSC_FREE(mu_tmp)
	
	PVSC_FREE(spmvm_conf->scale_h)
	PVSC_FREE(spmvm_conf->shift_h)
	PVSC_FREE(spmvm_conf->scale_z)
	
	pvsc_timing_end(timing_num);
	return 0;
	}





int pvsc_cheb_filter_block(  pvsc_matrix * mat ,double scale, double shift, int M, double * coeff,  pvsc_lidx_t ldcoeff, pvsc_vector * y, pvsc_vector * y_in, double * mu){
	// input      { mat, scale, shift, M, coeff, y }
	//   
	// output     { y }
	//   optional { mu [0 , 2*M*nb] }
	
	if( !mat ) {
		pvsc_printf("error: pvsc_cheb_dos(): mat is NULL\n");
		return 1;
		}
	
	int timing_num = pvsc_timing_init( "pvsc_cheb_filter_block" );
	
	pvsc_lidx_t nb = y->nb;
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = mat;
	spmvm_conf->dot_allreduce = false;
	
	pvsc_malloc((void**) &(spmvm_conf->scale_h), sizeof(double), nb, true, "pvsc_cheb_filter() spmvm_conf->scale_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->shift_h), sizeof(double), nb, true, "pvsc_cheb_filter() spmvm_conf->shift_h", 0 );
	pvsc_malloc((void**) &(spmvm_conf->scale_z), sizeof(double), nb, true, "pvsc_cheb_filter() spmvm_conf->scale_z", 0 );
	
	double *one = pvsc_malloc(NULL,  sizeof(double), nb, true, "pvsc_cheb_filter() one",  0 );
	
	
	for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->shift_h[i] =  shift;
	
	int blocksize_axpy = 32;
	pvsc_vector *x = pvsc_malloc( NULL, sizeof(pvsc_vector), blocksize_axpy, false, "pvsc_cheb_tp() y", 0);
	for(pvsc_lidx_t i=0; i<blocksize_axpy; i++) pvsc_create_vector( x+i, nb, y->sys);
	double *tmp = pvsc_malloc(NULL,  sizeof(double), nb*blocksize_axpy, true, "pvsc_cheb_filter() tmp",  0 );
	for(pvsc_lidx_t i=0; i<nb; i++) one[i] = 1.;
	
	for(pvsc_lidx_t m=0; m<M; m+=blocksize_axpy ){
		int block = blocksize_axpy;
		if( blocksize_axpy+m > M ) block = M-m;
		for(pvsc_lidx_t mm=0; mm<block; mm++){
			if( ldcoeff == 1 ) for(pvsc_lidx_t i=0; i<nb; i++) tmp[mm*nb+i] = coeff[ m+mm            ];
			else               for(pvsc_lidx_t i=0; i<nb; i++) tmp[mm*nb+i] = coeff[(m+mm)*ldcoeff +i];
			
			if( !(mm+m) ){
				if( y_in ) pvsc_vector_copy( x+0, y_in);
				else       pvsc_vector_copy( x+0, y);
				pvsc_vector_from_func(y, pvsc_zero_vec, NULL );
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_h[i] =  scale;
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_z[i] =  0.;
				spmvm_conf->z = x+1;
				spmvm_conf->x = x+0;
				//pvsc_vector_axpby( one, y, tmp + mm*nb, x+mm );
				
				continue;
			}
			if(mu){ spmvm_conf->dot_xx = mu + (2*(mm+m)-2)*nb;
			        spmvm_conf->dot_xy = mu + (2*(mm+m)-1)*nb;  }
			spmvm_conf->y = x + mm;
			mat->ham->spmvm_kernel( spmvm_conf );
			
			spmvm_conf->z = spmvm_conf->x;
			spmvm_conf->x = spmvm_conf->y;
			//pvsc_vector_axpby( one, y, tmp + mm*nb, x+mm );
			
			if( (mm+m)==1 ){
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_h[i] =  2.*scale;
				for(pvsc_lidx_t i=0; i<nb; i++) spmvm_conf->scale_z[i] = -1.;        }
		
		}
		pvsc_vector_axpy_block( y, tmp, x, block);
	}
	for(pvsc_lidx_t i=0; i<blocksize_axpy; i++) pvsc_destroy_vector(x+i);
	PVSC_FREE( x )
	
	
	if(mu){
		#ifdef PVSC_HAVE_MPI
		MPI_Allreduce( MPI_IN_PLACE, mu, 2*M*nb, MPI_DOUBLE, MPI_SUM, mat->sys->mpi_comm);
		#endif
		pvsc_dot_products_2_cheb_moments( mu, 2*M, nb );
	}
	
	//pvsc_print_performance_statistic( spmvm_conf );
	
	
	PVSC_FREE(tmp)
	PVSC_FREE(one)
	
	PVSC_FREE(spmvm_conf->scale_h)
	PVSC_FREE(spmvm_conf->shift_h)
	PVSC_FREE(spmvm_conf->scale_z)
	
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_cheb_filter(  pvsc_matrix * mat , double scale, double shift, int M, double * coeff, pvsc_vector * y, pvsc_vector * y_in, bool * apply,  double * mu){
	int timing_num = pvsc_timing_init( "pvsc_cheb_filter" );
	
	#if PVSC_OPT_BLOCK_SIZE > 0
	pvsc_lidx_t nb = PVSC_OPT_BLOCK_SIZE;
	#else
	pvsc_lidx_t nb = y->nb;
	#endif
	
	if( nb < y->nb ){
		
		pvsc_vector x[1];
		pvsc_create_vector( x, nb, mat->sys);
		double * mu_tmp = NULL;
		if( mu ) pvsc_malloc( (void **) &(mu_tmp), sizeof(double), nb*2*M, true, "pvsc_cheb_filter() mu", 0);
		
		
		pvsc_lidx_t x_idx[PVSC_OPT_BLOCK_SIZE];
		pvsc_lidx_t y_idx[PVSC_OPT_BLOCK_SIZE];
		
		for(pvsc_lidx_t j=0; j<PVSC_OPT_BLOCK_SIZE; j++) x_idx[j] = j;
		
		pvsc_lidx_t i=0;
		while( i< y->nb ){
			
			pvsc_lidx_t nb_tmp=0;
			
			while( (i < y->nb) && (nb_tmp<nb) ){
				if ( apply ) { if( apply[i] ){  y_idx[nb_tmp] = i; nb_tmp++; } }
				else {                          y_idx[nb_tmp] = i; nb_tmp++;   }
				i++;
			}
			
			if( !nb_tmp ) break;
			
			if( nb_tmp < nb ){
				pvsc_destroy_vector(x);
				pvsc_create_vector( x, nb_tmp, mat->sys);
			}
			
			if ( y_in ) pvsc_vector_copy_select( nb_tmp, x, x_idx, y_in, y_idx );
			else        pvsc_vector_copy_select( nb_tmp, x, x_idx, y   , y_idx );
			pvsc_cheb_filter_block(  mat , scale, shift, M, coeff , 1, x, NULL, mu_tmp);
			pvsc_vector_copy_select( nb_tmp, y, y_idx, x, x_idx );
			
			if( mu ){
				for(pvsc_lidx_t m=0; m<2*M; m++)
				for(pvsc_lidx_t j=0; j<nb_tmp; j++)
					//mu[ m ]                  += mu_tmp[nb_tmp*m + j];
					mu[y->nb*m + y_idx[j] ] = mu_tmp[nb_tmp*m + j];
				}
		}
		
		
		pvsc_destroy_vector(x);
		
		if( mu ) { PVSC_FREE(mu_tmp) }
		
	}else{
		pvsc_cheb_filter_block(  mat , scale, shift, M, coeff, 1, y, y_in, mu);
	}
	
	//PVSC_FREE(coeff_delta)
	pvsc_timing_end(timing_num);
	return 0;
}

const pvsc_cheb_fd_handle_t PVSC_CHEB_FD_HANDLE_INITIALIZER = {
	.mat = NULL,
	.lambda_min = -1.,
	.lambda_max =  1.,
	.res_eps   = 1.e-10,
	
	.vec = NULL,
	.eig = NULL,
	.res = NULL,
	
	.dos_export = NULL,
	
	.iter_max       = 20,
	
	
	.auto_nb         =  PVSC_OPT_BLOCK_SIZE,
	
	.auto_rescale    = true,
	.rescale_ld      = 20,
	.scale           = 0.1,
	.shift           = 0.,
	.auto_subspace   = true,
	.subspace        = 32,
	.subspace_ddf    = 8.,
	.searchspace_reduction = true,
	.subspace_reduction    = false,
	
	.cheap_init    = true,
	
	.auto_polydegree   = true,
	.polydegree_factor = 6.,
	.polydegree        = 0,
	
	.overpopulation    = 4.
};


int pvsc_cheb_fd(  pvsc_cheb_fd_handle_t * fdh  ){
	int timing_num = pvsc_timing_init( "pvsc_cheb_fd" );
	
	//pvsc_printf("call pvsc_cheb_fd\n");
	
	pvsc_printf("set shift = %g, scale =%g\n",fdh->shift, fdh->scale);
	
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = fdh->mat;
	spmvm_conf->dot_allreduce = false;
	
	pvsc_system_geo * sys = fdh->mat->sys;
	
	if( fdh->auto_rescale ){
		pvsc_printf(" calc eigenvalues wiht lanczos:\n");
		pvsc_vector vec[1] = {PVSC_VECTOR_INITIALIZER};
		pvsc_create_vector( vec, fdh->auto_nb, spmvm_conf->mat->sys);
		pvsc_vector_from_func( vec, pvsc_rand_vec, NULL);
		
		double extemal_eig[2];
		
		pvsc_lanczos( fdh->mat , NULL, fdh->rescale_ld,  vec, extemal_eig, PVSC_KRYLOV_DEFAULT);
		
		fdh->scale = 0.9*2.0/( extemal_eig[1] - extemal_eig[0] );
		fdh->shift =     0.5*( extemal_eig[1] + extemal_eig[0] );
		pvsc_printf("extemal eigenvalues:  %g : %g\n", extemal_eig[0], extemal_eig[1]);
		
		pvsc_destroy_vector(vec);
	}
	pvsc_printf("  set shift = %g, scale =%g\n",fdh->shift, fdh->scale);
	
	if( fdh->auto_subspace ){
		
		int M = (int) (fdh->subspace_ddf/(fdh->scale*(fdh->lambda_max - fdh->lambda_min)));
		if(M<16) M=16; 
		
		pvsc_printf("auto_subspace: Polydegree = %d \n", M  );
		
		double * mu = pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
		
		pvsc_cheb_dos( mu, fdh->mat, fdh->auto_nb, fdh->auto_nb, M, fdh->scale, fdh->shift, NULL );
		
		pvsc_gidx_t dim = sys->mpi_size*((pvsc_gidx_t)(sys->n_x*sys->n_y*sys->n_z*sys->n_o));
		
		double subspace = sys->mpi_size*((pvsc_gidx_t)(sys->n_x*sys->n_y*sys->n_z*sys->n_o))*
		                    pvsc_kpm_dos_integal ( mu, M, fdh->scale*(fdh->lambda_min-fdh->shift), fdh->scale*(fdh->lambda_max-fdh->shift), &pvsc_gibbs_Jackson );
		pvsc_printf(" assume  %g eigen pairs in interval [%g:%g]\n", subspace, fdh->lambda_min, fdh->lambda_max  );
		fdh->subspace = (int)(fdh->overpopulation*subspace);
		//fdh->subspace = (int)(10*subspace);
		
		if( fdh->subspace & 1 ) fdh->subspace += 1;
		if( fdh->subspace & 2 ) fdh->subspace += 2;
		//if( (fdh->subspace & 4) && (fdh->subspace > 8) ) fdh->subspace += 4;
		pvsc_printf("subspace  = %d / %ld \n", fdh->subspace, dim );
		PVSC_FREE(mu);
	}
	
	pvsc_timing_print();
	
	//return 0;
	
	//fdh->subspace = 8;
	//fdh->polydegree = (int) (4./(fdh->scale*(fdh->lambda_max - fdh->lambda_min)));
	
	
	
	double * coeff_init = NULL;
	double * coeff_iter = NULL;
	fdh->polydegree = pvsc_cheb_toolbox_window_coeffs(fdh->lambda_min, fdh->lambda_max, fdh->scale, fdh->shift, &coeff_iter, fdh->polydegree_factor );
	
	
	double delta_init = 0.25 * (fdh->lambda_max - fdh->lambda_min);
	
	int polydegree_init = pvsc_cheb_toolbox_window_coeffs(fdh->lambda_min - delta_init, fdh->lambda_max + delta_init, fdh->scale, fdh->shift, &coeff_init, fdh->polydegree_factor/fdh->overpopulation );
	
	//double * mu     = pvsc_malloc(NULL, sizeof(double),2*fdh->polydegree*fdh->subspace, true, "mu", 0);
	//double * mu_sum = pvsc_malloc(NULL, sizeof(double),2*fdh->polydegree, true, "mu_sum", 0);
	
	
	pvsc_gibbs_Lanczos (fdh->polydegree, 2., coeff_iter, 1);
	pvsc_gibbs_Lanczos (polydegree_init, 2., coeff_init, 1);
	//pvsc_gibbs_Jackson (polydegree_init, coeff_init, 1);
	
	pvsc_printf("pvsc_cheb_fd Polydegree = %d \n", fdh->polydegree  );
	
	pvsc_cheb_toolbox_print_filter( "filter_iter.dat",coeff_iter, 1, fdh->polydegree,  fdh->scale, fdh->shift, 10000 );
	pvsc_cheb_toolbox_print_filter( "filter_init.dat",coeff_init, 1, polydegree_init,  fdh->scale, fdh->shift, 10000 );
	
	
	pvsc_vector vec[2] = {PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
	pvsc_create_vector( vec, fdh->subspace, spmvm_conf->mat->sys);
	pvsc_create_vector( vec+1, fdh->subspace, spmvm_conf->mat->sys);
	pvsc_vector_from_func( vec, pvsc_rand_vec, NULL);
	
	pvsc_rayleigh_ritz_handle_t rrh[1] = {PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER};
	
	
	//char fname_fd_spec[128];
	
	rrh->mat     = fdh->mat;
	pvsc_malloc( (void **) &(rrh->eig), sizeof(double), fdh->subspace, true, "eig", 0);
	pvsc_malloc( (void **) &(rrh->res), sizeof(double), fdh->subspace, true, "res", 0);
	
	bool * vec_apply = pvsc_malloc(NULL, sizeof(bool), fdh->subspace, true, "vec_apply", 0);
	int * eig_info = pvsc_malloc(NULL, sizeof(int), fdh->subspace, true, "mu", 0);
	for(int i=0; i<fdh->subspace; i++) vec_apply[i] = true;
	
	for(int iter=0; iter<fdh->iter_max; iter++){
		
		rrh->vec_in  = vec+((iter  )&1);
		rrh->vec_eig = vec+((iter+1)&1);
		rrh->vec_res = rrh->vec_in;
		
		
		double * coeff = NULL;
		int polydegree;
		if( !iter  && fdh->cheap_init ){
			polydegree = polydegree_init;
			coeff      = coeff_init;
		} else {
			polydegree = fdh->polydegree;
			coeff      = coeff_iter;
		}
		
		pvsc_cheb_filter(  fdh->mat , fdh->scale, fdh->shift, polydegree, coeff, rrh->vec_in, NULL, vec_apply,  NULL);
		
		//for(int m=0;m<2*polydegree;m++){
		//	mu_sum[m] = 0;
		//	for(int i=0;i<fdh->subspace;i++)
		//		mu_sum[m] += mu[m*fdh->subspace+i];
		//	}
		//sprintf(fname_fd_spec,"fd_spec_%02d.dat",iter);
		//pvsc_kpm_print_density( fname_fd_spec, 2*polydegree, fdh->subspace, mu , fdh->scale, fdh->shift, NULL, &pvsc_gibbs_Jackson );
		//sprintf(fname_fd_spec,"fd_spec_sum_%02d.dat",iter);
		//pvsc_kpm_print_density( fname_fd_spec, 2*polydegree, 1, mu_sum , fdh->scale, fdh->shift, NULL, &pvsc_gibbs_Jackson );
		
		
		//pvsc_lanczos( fdh->mat , rrh->eig, 200,  rrh->vec_in, NULL, PVSC_KRYLOV_MIN_EIG_VEC|PVSC_KRYLOV_SQUARE_MAT);
		//pvsc_kacz( fdh->mat ,    rrh->eig, 40, NULL, rrh->vec_in, 1.);
		//for(int j=0;j<10;j++) pvsc_cg_eig( fdh->mat ,  rrh->eig,  50, rrh->vec_in );
		pvsc_rayleigh_ritz( rrh );
		
		
		int eig_L0 = 0;
		int eig_L1 = 0;
		int eig_L2 = 0;
		int tag_idx = -1;
		int tag_idx_l = -1;
		int tag_idx_h = -1;
		
		for(int i=0; i<fdh->subspace; i++) {
			eig_info[i] = 0;
			if( fdh->searchspace_reduction ) vec_apply[i] = false;
			if(     ( fdh->lambda_min < rrh->eig[i] ) && (rrh->eig[i]  < fdh->lambda_max) ) {
				eig_info[i] += 1;
				vec_apply[i] = true;
				eig_L0++;
				if(tag_idx_l == -1) tag_idx_l = i;
				else                tag_idx_h = i;
			}
			
			if(     ( fdh->lambda_min < rrh->eig[i] + rrh->res[i] ) && (rrh->eig[i] - rrh->res[i] < fdh->lambda_max) ) {
				eig_info[i] += 2;
				eig_L1++;
				if( ( fdh->lambda_min < rrh->eig[i] - rrh->res[i] ) && (rrh->eig[i] + rrh->res[i] < fdh->lambda_max) ) {
					eig_info[i] += 4;
					eig_L2++;
					if(tag_idx == -1) tag_idx = i;
				}
			}
		}
		
		
		for(int i=0; i<fdh->subspace; i++){
			pvsc_printf("#%02d    %+.15e +/- %.4e  ", i, rrh->eig[i], rrh->res[i] );
			if( eig_info[i] & 1 ) pvsc_printf("i");
			if( eig_info[i] & 2 ) pvsc_printf("I");
			if( eig_info[i] & 4 ) pvsc_printf("I");
			pvsc_printf("\n");
		}
		pvsc_printf(" eig_L0 =  %d, eig_L1 =  %d, eig_L2 =  %d \n", eig_L0, eig_L1, eig_L2 );
		
		int break_test = 1;
		if( eig_L2 ){
			for(int i=0; i<fdh->subspace; i++) {
				if( (eig_info[i] & 4) && ( rrh->res[i] > fdh->res_eps) ){
					break_test = 0;
					pvsc_printf("kill breake by %d\n", i);
					break;
				}
			}
		} else {
			break_test = 0;
			pvsc_printf("kill breake by non eig_L2\n");
		}
		
		if(break_test) {
			pvsc_printf("brake after %d Iterations\n",iter);
			break;
		}
		
		if( tag_idx_l-1 > -1            ) vec_apply[tag_idx_l-1] = true;
		if( tag_idx_h+1 < fdh->subspace ) vec_apply[tag_idx_h+1] = true;
		
		
		
		if( fdh->subspace_reduction ){
			int tag_l,tag_h;
			for( tag_l = tag_idx; tag_l > 0;               tag_l-- ) if( !(eig_info[tag_l-1] & 2) ) break;
			for( tag_h = tag_idx; tag_h < fdh->subspace-1; tag_h++ ) if( !(eig_info[tag_h+1] & 2) ) break;
			
			
			pvsc_printf(" tag_l =  %d, tag_h =  %d \n", tag_l, tag_h );
			pvsc_lidx_t new_subspace = tag_h-tag_l+1;
			pvsc_lidx_t * old_idx = pvsc_malloc(NULL, sizeof(pvsc_lidx_t),new_subspace, true, "old_idx", 0);
			pvsc_lidx_t * new_idx = pvsc_malloc(NULL, sizeof(pvsc_lidx_t),new_subspace, true, "new_idx", 0);
			
			for(pvsc_lidx_t i=0; i<new_subspace; i++) new_idx[i] = i;
			
			if( new_subspace < fdh->subspace ){
				for(pvsc_lidx_t i=0; i<new_subspace; i++) old_idx[i] = i+tag_l;
			}else{
				new_subspace = 0;
				for(pvsc_lidx_t i=0; i<fdh->subspace; i++) {
					if( eig_info[i] & 4 ) {
						old_idx[new_subspace] = i;
						new_subspace++;
					}
				}
			}
			
			// ********* zum reduzieren des Suchraums  **************
			if( new_subspace < fdh->subspace ){
				pvsc_vector vec_new[2] = {PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
				
				pvsc_create_vector( vec_new,   new_subspace, spmvm_conf->mat->sys);
				pvsc_create_vector( vec_new+1, new_subspace, spmvm_conf->mat->sys);
				
				pvsc_vector_copy_select( new_subspace, vec_new  , new_idx,  vec  , old_idx );
				pvsc_vector_copy_select( new_subspace, vec_new+1, new_idx,  vec+1, old_idx );
				
				fdh->subspace = new_subspace;
				for(pvsc_lidx_t i=0; i<new_subspace; i++) {
					rrh->eig[i] = rrh->eig[old_idx[i]];
					rrh->res[i] = rrh->res[old_idx[i]];
					eig_info[i] = eig_info[old_idx[i]];
				}
				
				pvsc_destroy_vector(vec  );
				pvsc_destroy_vector(vec+1);
				
				vec[0] = vec_new[0];
				vec[1] = vec_new[1];
			}
			
			PVSC_FREE(old_idx)
			PVSC_FREE(new_idx)
		}
		//pvsc_cg( fdh->mat , rrh->eig, 20, NULL,  rrh->vec_in, PVSC_KRYLOV_DEFAULT|PVSC_KRYLOV_SQUARE_MAT);

	}
	
	/*
	int M = 258;
	double * mu = pvsc_malloc(NULL, sizeof(double),M, false, "mu", 0);
	pvsc_cheb_dos( mu, fdh->mat, fdh->subspace, fdh->subspace, M, fdh->scale, fdh->shift, vec );
	if( !sys->mpi_rank ) pvsc_kpm_print_density( "fd_dos.dat", M, 1, mu , fdh->scale, fdh->shift, NULL, &pvsc_gibbs_Jackson );
	PVSC_FREE(mu);
	*/
	
	//PVSC_FREE(mu_sum)
	//PVSC_FREE(mu)
	PVSC_FREE(vec_apply)
	PVSC_FREE(eig_info)
	PVSC_FREE(rrh->eig)
	PVSC_FREE(rrh->res)
	PVSC_FREE(coeff_iter)
	PVSC_FREE(coeff_init)
	pvsc_destroy_vector(vec  );
	pvsc_destroy_vector(vec+1);
	
	
	//pvsc_printf("end pvsc_cheb_fd\n");
	pvsc_timing_end(timing_num);
	return 0;
}


int pvsc_cheb_fd_trl(  pvsc_cheb_fd_handle_t * fdh  ){
	int timing_num = pvsc_timing_init( "pvsc_cheb_fd_trl" );
	
	//pvsc_printf("call pvsc_cheb_fd\n");
	
	pvsc_printf("set shift = %g, scale =%g\n",fdh->shift, fdh->scale);
	
	pvsc_spmvm_handel_t spmvm_conf[1] = {PVSC_SPMVM_HANDEL_INITIALIZER};
	spmvm_conf->mat = fdh->mat;
	spmvm_conf->dot_allreduce = false;
	
	pvsc_system_geo * sys = fdh->mat->sys;
	
	if( fdh->auto_rescale ){
		pvsc_printf(" calc eigenvalues wiht lanczos:\n");
		pvsc_vector vec[1] = {PVSC_VECTOR_INITIALIZER};
		pvsc_create_vector( vec, fdh->auto_nb, spmvm_conf->mat->sys);
		pvsc_vector_from_func( vec, pvsc_rand_vec, NULL);
		
		double extemal_eig[2];
		
		pvsc_lanczos( fdh->mat , NULL, fdh->rescale_ld,  vec, extemal_eig, PVSC_KRYLOV_DEFAULT);
		
		fdh->scale = 0.9*2.0/( extemal_eig[1] - extemal_eig[0] );
		fdh->shift =     0.5*( extemal_eig[1] + extemal_eig[0] );
		pvsc_printf("extemal eigenvalues:  %g : %g\n", extemal_eig[0], extemal_eig[1]);
		
		pvsc_destroy_vector(vec);
	}
	pvsc_printf("  set shift = %g, scale =%g\n",fdh->shift, fdh->scale);
	
	if( fdh->auto_subspace ){
		
		int M = (int) (fdh->subspace_ddf/(fdh->scale*(fdh->lambda_max - fdh->lambda_min)));
		if(M<16) M=16; 
		
		pvsc_printf("auto_subspace: Polydegree = %d \n", M  );
		
		double * mu = pvsc_malloc(NULL, sizeof(double),M, true, "mu", 0);
		
		pvsc_cheb_dos( mu, fdh->mat, fdh->auto_nb, fdh->auto_nb, M, fdh->scale, fdh->shift, NULL );
		
		
		pvsc_gidx_t dim = sys->mpi_size*((pvsc_gidx_t)(sys->n_x*sys->n_y*sys->n_z*sys->n_o));
		
		double subspace = sys->mpi_size*((pvsc_gidx_t)(sys->n_x*sys->n_y*sys->n_z*sys->n_o))*
		                    pvsc_kpm_dos_integal ( mu, M, fdh->scale*(fdh->lambda_min-fdh->shift), fdh->scale*(fdh->lambda_max-fdh->shift), &pvsc_gibbs_Jackson );
		
		if( !fdh->mat->sys->mpi_rank && fdh->dos_export ){
			pvsc_printf(" export dos in: %s\n", fdh->dos_export  );
			pvsc_kpm_print_density( fdh->dos_export, M, 1, mu, fdh->scale, fdh->shift, NULL, &pvsc_gibbs_Jackson );
		}
		pvsc_printf(" assume  %g eigen pairs in interval [%g:%g]\n", subspace, fdh->lambda_min, fdh->lambda_max  );
		fdh->subspace = (int)(fdh->overpopulation*subspace);
		//fdh->subspace = (int)(10*subspace);
		
		if( fdh->subspace & 1 ) fdh->subspace += 1;
		if( fdh->subspace & 2 ) fdh->subspace += 2;
		//if( (fdh->subspace & 4) && (fdh->subspace > 8) ) fdh->subspace += 4;
		pvsc_printf("subspace  = %d / %ld \n", fdh->subspace, dim );
		
		
		PVSC_FREE(mu);
	}
	
	pvsc_timing_print();
	
	double filter_cut     = 0.5;
	double filter_boerder = 0.25;
	int w_nb    = 1;
	int gs_iter = 1;
	
	double * coeff_iter = NULL;
	
	if( !fdh->mat->sys->mpi_rank){
		fdh->polydegree = pvsc_cheb_toolbox_delta_coeffs(fdh->lambda_min , fdh->lambda_max , fdh->scale , fdh->shift, &coeff_iter, filter_cut);
		pvsc_gibbs_Lanczos (fdh->polydegree, 2., coeff_iter, 1);
		//pvsc_gibbs_Jackson ( fdh->polydegree, coeff_iter, 1);
	}
	#ifdef PVSC_HAVE_MPI
			MPI_Bcast(  &(fdh->polydegree),  1,  MPI_INT, 0, fdh->mat->sys->mpi_comm);
			if( fdh->mat->sys->mpi_rank ) coeff_iter = malloc( sizeof(double)*fdh->polydegree );
			MPI_Bcast( coeff_iter, fdh->polydegree,  MPI_DOUBLE, 0, fdh->mat->sys->mpi_comm);
	#endif
	pvsc_printf("pvsc_cheb_fd Polydegree = %d \n", fdh->polydegree  );
	pvsc_cheb_toolbox_print_filter( "filter_iter.dat",coeff_iter, 1, fdh->polydegree,  fdh->scale, fdh->shift, 10000 );
	
	double lanc_barrier_min = pvsc_cheb_eval_cheb_poly( coeff_iter, fdh->polydegree, fdh->scale*(fdh->lambda_min-fdh->shift) );
	double lanc_barrier_max = pvsc_cheb_eval_cheb_poly( coeff_iter, fdh->polydegree, fdh->scale*(fdh->lambda_max-fdh->shift) );
	double lanc_barrier = lanc_barrier_min;
	if ( lanc_barrier > lanc_barrier_max ) lanc_barrier = lanc_barrier_max;
	//pvsc_printf(" lanc_barrier = %g \n",  lanc_barrier );
	lanc_barrier *= (1.0-filter_boerder);
	
	
	
	pvsc_vector vec[2] = {PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
	pvsc_vector   w[2] = {PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
	pvsc_dense_redundant_matrix lanc_mat[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
	pvsc_rayleigh_ritz_handle_t ot[1]  = {PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER};
	pvsc_rayleigh_ritz_handle_t rrh[1] = {PVSC_RAYLEIGH_RITZ_HANDLE_INITIALIZER};
	rrh->mat     = fdh->mat;
	
	pvsc_lidx_t * idx_s    = pvsc_malloc( NULL,                  sizeof(pvsc_lidx_t), fdh->subspace, true, "idx_s", 0);
	pvsc_lidx_t * idx_d    = pvsc_malloc( NULL,                  sizeof(pvsc_lidx_t), fdh->subspace, true, "idx_d", 0);
	int         * eig_info = pvsc_malloc( NULL,                  sizeof(int),         fdh->subspace, true, "eig_info", 0);
	double      * tmp0     = pvsc_malloc( NULL,                  sizeof(double),      fdh->subspace, true, "tmp0", 0);
	                         pvsc_malloc( (void **) &(rrh->eig), sizeof(double),      fdh->subspace, true, "eig", 0);
	                         pvsc_malloc( (void **) &(rrh->res), sizeof(double),      fdh->subspace, true, "res", 0);
	pvsc_create_dense_redundant_matrix( lanc_mat, fdh->subspace, fdh->subspace );
	
	pvsc_create_vector( vec,   fdh->subspace, fdh->mat->sys);
	pvsc_create_vector( vec+1, fdh->subspace, fdh->mat->sys);
	
	pvsc_create_vector( w,   w_nb, fdh->mat->sys);
	if (w_nb>1) 
	  pvsc_create_vector( w+1, w_nb, fdh->mat->sys);
	pvsc_vector * w_cur = w+0;
	pvsc_vector * w_new = w+1;
	
	pvsc_vector_from_func( w_cur, pvsc_rand_vec, NULL);
	int eig_L0_o = 0, eig_L1_o = 0, eig_L2_o = 0;
	int li=0;
	
	for(int iter=0; iter<fdh->iter_max; iter++){
		
		
		while( li<fdh->subspace ){
			for(int i=0; i<gs_iter; i++) pvsc_vector_class_gram_schmidt( w_cur, vec+0, li );
			if( w_nb == 1 ){ 
				pvsc_vector_normalized(w_cur);
				/*pvsc_vector_dot( tmp0, w_cur, w_cur);
				tmp0[0] = 1./ sqrt( tmp0[0] );
				pvsc_printf("  norm:  %+.15e", tmp0[0] );
				pvsc_vector_scale(w_cur, tmp0);*/
			}
			else{	ot->vec_in  = w_cur;
					ot->vec_eig = w_new;
					pvsc_rayleigh_ritz( ot );
					pvsc_vector * w_tmp = w_cur; w_cur = w_new; w_new = w_tmp;
			}
			
			for(int i=0; i<w_nb; i++) idx_s[i] = i;
			for(int i=0; i<w_nb; i++) idx_d[i] = i + li;
			int n_copy = w_nb;
			if( n_copy + li >= fdh->subspace ) n_copy = fdh->subspace - li;
			
			pvsc_vector_copy_select( n_copy, vec+0, idx_d, w_cur, idx_s );
			pvsc_cheb_filter_block( fdh->mat , fdh->scale, fdh->shift, fdh->polydegree, coeff_iter, 1, w_cur, NULL, NULL);
			
			pvsc_vector_copy_select( n_copy, vec+1, idx_d, w_cur, idx_s );
			
			li+=w_nb;
		}
		for(int i=0; i<gs_iter; i++) pvsc_vector_class_gram_schmidt( w_cur, vec+0, li );
		if( w_nb == 1 ) pvsc_vector_normalized(w_cur);
		else{	ot->vec_in  = w_cur;
				ot->vec_eig = w_new;
				pvsc_rayleigh_ritz( ot );
				pvsc_vector * w_tmp = w_cur; w_cur = w_new; w_new = w_tmp;
		}
		
		pvsc_vector_M_eq_axy( lanc_mat->val, lanc_mat->ld_col, 1., vec+1, vec+0 );
		
		li = 0;
		LAPACKE_dsyev( LAPACK_COL_MAJOR, 'V' , 'U', lanc_mat->ld_col, lanc_mat->val, lanc_mat->ld_col, tmp0 );
		for(int i=0; i<fdh->subspace; i++){
			//pvsc_printf("#%02d    %+.15e\n", i, tmp0[i] );
			if( tmp0[i] > lanc_barrier ) { idx_s[li] = i; idx_d[li]=li; li++; }
		}
		
		#ifdef PVSC_HAVE_MPI
			MPI_Bcast(  lanc_mat->val,  lanc_mat->ld_col*lanc_mat->ld_col,  MPI_DOUBLE, 0, fdh->mat->sys->mpi_comm);
		#endif
		
		
		pvsc_vector_xApby( 0, vec+0, vec+0, lanc_mat->val, lanc_mat->ld_col );
		pvsc_vector_xApby( 0, vec+1, vec+1, lanc_mat->val, lanc_mat->ld_col );
		
		pvsc_vector vec_rrh[2] = {PVSC_VECTOR_INITIALIZER, PVSC_VECTOR_INITIALIZER};
		pvsc_create_vector( vec_rrh+0, li, fdh->mat->sys);
		pvsc_create_vector( vec_rrh+1, li, fdh->mat->sys);
		rrh->vec_in  = vec_rrh+0;
		rrh->vec_eig = vec_rrh+1;
		
		pvsc_dense_redundant_matrix transform_mat[1] = {PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER};
		rrh->transform_mat = transform_mat;
		
		
		if( 0 ){
			pvsc_vector_copy_select( li, rrh->vec_in, idx_d, vec+0, idx_s );
			
			pvsc_rayleigh_ritz( rrh );
			
			pvsc_vector_copy_select( li, vec+0, idx_d, rrh->vec_eig, idx_d );
			
			pvsc_vector_copy_select( li, vec_rrh+0, idx_d, vec+1, idx_s );
			pvsc_vector_xApby( 0, vec_rrh+1, vec_rrh+0, transform_mat->val, transform_mat->ld_col );
			pvsc_vector_copy_select( li, vec+1, idx_d,    vec_rrh+1, idx_d );
		}else{
			pvsc_vector_copy_select( li, rrh->vec_in, idx_d, vec+1, idx_s );
			pvsc_vector_copy_select( li, vec+0, idx_d, vec+0, idx_s );
			pvsc_vector_copy_select( li, vec+1, idx_d, vec+1, idx_s );
			
			pvsc_rayleigh_ritz( rrh );
		}
		
		
		pvsc_destroy_dense_redundant_matrix( transform_mat );
		
		int eig_L0 = 0;
		int eig_L1 = 0;
		int eig_L2 = 0;
		int tag_idx = -1;
		int tag_idx_l = -1;
		int tag_idx_h = -1;
		
		for(int i=0; i<rrh->vec_in->nb; i++) {
			eig_info[i] = 0;
			//if( fdh->searchspace_reduction ) vec_apply[i] = false;
			if(     ( fdh->lambda_min < rrh->eig[i] ) && (rrh->eig[i]  < fdh->lambda_max) ) {
				eig_info[i] += 1;
				//vec_apply[i] = true;
				eig_L0++;
				if(tag_idx_l == -1) tag_idx_l = i;
				else                tag_idx_h = i;
			}
			
			if(   ( fdh->lambda_min < rrh->eig[i] + fdh->res_eps ) && (rrh->eig[i] - fdh->res_eps < fdh->lambda_max)
			    &&  !(( fdh->lambda_min > rrh->eig[i] - rrh->res[i]  ) && (rrh->eig[i] + rrh->res[i] > fdh->lambda_max)  ) ) {
			//if(   ( fdh->lambda_min < rrh->eig[i] + rrh->res[i] )  && (rrh->eig[i] - rrh->res[i] < fdh->lambda_max) ) {
				eig_info[i] += 2;
				eig_L1++;
				if(  rrh->res[i] < fdh->res_eps   ) {
					eig_info[i] += 4;
					eig_L2++;
					if(tag_idx == -1) tag_idx = i;
				}
			}
		}
		
		int j=0;
		for(int i=0; i<rrh->vec_in->nb; i++){
			pvsc_printf("#%02d    %+.15e +/- %.4e ", i, rrh->eig[i], rrh->res[i] );
			if( eig_info[i] & 1 )  pvsc_printf("i");
			if( eig_info[i] & 2 ){ pvsc_printf("I");    idx_s[j] = i; idx_d[j]=j; j++;}
			if( eig_info[i] & 4 )  pvsc_printf("I");
			pvsc_printf("\n");
		}
		pvsc_printf(" eig_L0 =  %d, eig_L1 =  %d, eig_L2 =  %d \n", eig_L0, eig_L1, eig_L2 );
		
		pvsc_destroy_vector(vec_rrh+0);
		pvsc_destroy_vector(vec_rrh+1);
		
		if( ( eig_L0 == eig_L0_o ) &&
		    ( eig_L1 == eig_L1_o ) &&
		    ( eig_L2 == eig_L2_o )    ) break;
		
		eig_L0_o = eig_L0;
		eig_L1_o = eig_L1;
		eig_L2_o = eig_L2;
	}
	
	
	
	pvsc_destroy_dense_redundant_matrix( lanc_mat );
	
	pvsc_destroy_vector(w+0);
	if (w_nb>1) 
	  pvsc_destroy_vector(w+1);
	
	free(coeff_iter);
	
	PVSC_FREE(idx_s)
	PVSC_FREE(idx_d)
	PVSC_FREE(eig_info)
	PVSC_FREE(tmp0)
	PVSC_FREE(rrh->eig)
	PVSC_FREE(rrh->res)
	
	pvsc_destroy_vector(vec  );
	pvsc_destroy_vector(vec+1);
	
	//pvsc_printf("end pvsc_cheb_fd\n");
	pvsc_timing_end(timing_num);
	return 0;
}





