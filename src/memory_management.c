#define _XOPEN_SOURCE 600

#define PVSC_ALIGNMENT_256BIT  0x20
#define PVSC_ALIGNMENT_512BIT  0x40
#define PVSC_ALIGNMENT_1024BIT 0x80
#define PVSC_ALIGNMENT PVSC_ALIGNMENT_1024BIT
//#define PVSC_ALIGNMENT 0x100

#define PVSC_MEM_LIST_N_INIT 64

//#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "pvsc/util.h"


/*
typedef enum{
    PVSC_ = 0,
    PVSC_ = 1
}pvsc_men_info_flags;
*/

typedef struct 
{
	void * ptr;
	size_t e_size;
	size_t n_size;
	bool   alloced;
	bool   lock;
	bool   aligned;
	char * comment;
	unsigned int tag;
}pvsc_men_info_t;


const pvsc_men_info_t PVSC_MEM_INFO_INITIALIZER = {  
	.ptr     = NULL,
	.e_size  = 0,
	.n_size  = 0,
	.alloced = false,
	.lock    = false,
	.aligned = false,
	.comment = NULL,
	.tag     = 0
	};

//#define PVSC_MEM_INFO_DEFAULT {  .ptr = NULL, .e_size = 0, .n_size = 0, .alloced = false, .lock = false, .aligned = false, .comment = NULL }

pvsc_men_info_t * men_list = NULL;
int men_list_n = 0;

void * pvsc_malloc( void ** ptr, size_t e_size, size_t n_size, bool aligned, char * comment,unsigned int tag ){

	void * p;

	#ifdef PVSC_HAVE_ERROR_CHECK
	
	int i;
	if( !men_list_n ){
		men_list_n = PVSC_MEM_LIST_N_INIT;
		#if PVSC_VERBOSE > 0
		pvsc_printf("INFO: pvsc_malloc(): init men_list with %d elements\n", men_list_n);
		#endif
		men_list = malloc(sizeof(pvsc_men_info_t)*men_list_n);
		for( i=0; i<men_list_n; i++){
			men_list[i] = PVSC_MEM_INFO_INITIALIZER;
		}
	}
	
	if( ptr && *ptr )
		for( i=0; i<men_list_n; i++)
			if( ( * ptr == men_list[i].ptr ) &&  men_list[i].alloced  ){
				#if PVSC_VERBOSE > 0
				pvsc_printf("WARNING pvsc_malloc: pointer is allocated (new tag: %d; old tag %d)\n", tag, men_list[i].tag);
				if(             comment ) pvsc_printf("  new pointer comment: %s\n", comment);
				if( men_list[i].comment ) pvsc_printf("  old pointer comment: %s\n", men_list[i].comment);
				#endif
				break;
			}
	
	
	for( i=0; i<men_list_n; i++)
		if( !men_list[i].lock && !men_list[i].alloced  ) break;
	
	if( i == men_list_n ){
		men_list_n = men_list_n << 1;
		#if PVSC_VERBOSE > 0
		pvsc_printf("INFO: pvsc_malloc(): extent men_list to %d elements\n", men_list_n);
		#endif
		men_list = realloc( men_list, sizeof(pvsc_men_info_t)*men_list_n );
		for( i=men_list_n>>1; i<men_list_n; i++){
			men_list[i] = PVSC_MEM_INFO_INITIALIZER;
		}
		i = men_list_n>>1;
	}
	#endif
	
	
	if( aligned ){
		posix_memalign((void **)(&p), PVSC_ALIGNMENT, e_size*n_size);
		//void *aligned_alloc( size_t alignment, size_t size );	(since C11)
	} else {
		p = malloc ( e_size*n_size );
	}
	
	if( ptr ) *ptr = p;
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	
	men_list[i].ptr = p;
	
	men_list[i].e_size  = e_size;
	men_list[i].n_size  = n_size;
	men_list[i].alloced = true;
	men_list[i].lock    = true;
	men_list[i].aligned = aligned;
	men_list[i].tag     = tag;
	
	if(      men_list[i].comment ) {
		free(men_list[i].comment);
		men_list[i].comment = NULL;
	}
	
	if( comment ){
		men_list[i].comment = malloc ( sizeof(char)*(strlen(comment)+1) );
		strcpy( men_list[i].comment, comment);
		}
		
	#endif
	
	return p;
}

void * pvsc_free( void * ptr, bool lock, char * comment ){
	
	#ifdef PVSC_HAVE_ERROR_CHECK
	
	int count = 0;
	
	if( ptr ){
		for(int i=0; i<men_list_n; i++)
			if( ptr == men_list[i].ptr ){
				if( men_list[i].alloced ) {
					men_list[i].alloced = false;
					if( !lock ){
						men_list[i].lock = false;
						//men_list[i].ptr  = NULL;
					}
	#endif
					free(ptr);
					return NULL;
	#ifdef PVSC_HAVE_ERROR_CHECK
				}
				count++;
			}
		
		pvsc_printf("ERROR pvsc_free(): pointer for dealloc not found\n");
		if (comment) pvsc_printf("  comment: %s\n", comment);
		if ( count ) pvsc_printf("   but is %d times in the history\n", count);
		
		return ptr;
	}
	#if PVSC_VERBOSE > 0
	pvsc_printf("WARNING pvsc_free(): try to dealloc NULL pointer\n");
	#endif
	return ptr;
	#endif
}



int pvsc_mem_manag_clear( ){
#ifdef PVSC_HAVE_ERROR_CHECK
	#if PVSC_VERBOSE > 0
	pvsc_printf("INFO: pvsc_mem_manag_clear(): clear memory\n");
	#endif
	
	if( men_list ){
		size_t sum = 0;
		for(int i=0; i<men_list_n; i++){
			
			if( men_list[i].ptr && men_list[i].alloced ){
				free(men_list[i].ptr);
				men_list[i].ptr = NULL;
				men_list[i].alloced = false;
				sum += men_list[i].e_size*men_list[i].n_size;
				#if PVSC_VERBOSE > 1
				pvsc_printf("  #%d tag: %d  size: %zu * %zu byte (%g MB)\n", i, men_list[i].tag, men_list[i].n_size, men_list[i].e_size, men_list[i].n_size*9.5367e-07*men_list[i].e_size );
				if( men_list[i].comment )  pvsc_printf("       comment: %s\n", men_list[i].comment );
				
				#endif
			}
			if (men_list[i].comment){
				free(men_list[i].comment);
				men_list[i].comment = NULL;
			}
		}
		
		#if PVSC_VERBOSE > 0
		if( sum ) pvsc_printf("    %g MB dealloced by pvsc_mem_manag_clear()\n    Use -D PVSC_VERBOSE>1 for more information\n", sum*9.5367e-07);
		#endif
		free( men_list );
		men_list = NULL;
		men_list_n = 0;
		
		return 0;
	}
	#if PVSC_VERBOSE > 0
	pvsc_printf("pvsc_mem_manag_clear(): men_list was not using\n");
	#endif
#endif
	return 1;
}

int pvsc_aligned_check(void * ptr, char * info){
	
	if( !ptr ){
		pvsc_printf("%s is NULL pointer (ERROR)\n", info);
		return 2;
		}
	
	if( (uintptr_t)ptr & (PVSC_ALIGNMENT-1) ){
		pvsc_printf("%s is not aligned (%p)\n", info, ptr);
		return 1;
		}
	#if PVSC_VERBOSE > 3
	pvsc_printf("%s is aligned (%p)\n", info, ptr);
	#endif
	return 0;
}
