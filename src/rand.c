#include <stdint.h>
#include "pvsc/rand.h"

double rcp64 = 1. / 0xFFFFFFFFFFFFFFFFULL;
double rcp32 = 1. / 0xFFFFFFFFU;
// http://vigna.di.unimi.it/ftp/papers/xorshiftplus.pdf


uint64_t s128[2] = { 0x775095e9382798c3ULL, 0xc39cf74baa73568fULL};
uint64_t s64     =   0x775095e9382798c3ULL;
uint32_t s32     =   0x775095e4;

uint64_t * pvsc_rand_seed128( uint64_t s0, uint64_t s1 ) {
	s128[0] ^= s0;
	s128[1] ^= s1;
	return  s128;
}

uint64_t * pvsc_rand_seed64( uint64_t s ) {
	s64 ^= s;
	return  &s64;
}

uint32_t * pvsc_rand_seed32( uint32_t s ) {
	s32 ^= s;
	return  &s32;
}

uint8_t xorshift128plus_triples[8][3]={
	{ 23, 17, 26},
	{ 26, 19,  5},
	{ 23, 18,  5},
	{ 41, 11, 34},
	{ 23, 31, 18},
	{ 21, 23, 28},
	{ 21, 16, 37},
	{ 20, 21, 11}};

#define XORS128PT 0


uint64_t pvsc_rand_l_xorshift128plus(void) {
	uint64_t       x = s128[0];
	uint64_t const y = s128[1];
	s128[0] = y;
	//x ^= x << xorshift128plus_triples[XORS128PT][0]; s128[1] = x ^ y ^ (x >> xorshift128plus_triples[XORS128PT][1]) ^ (y >> xorshift128plus_triples[XORS128PT][2]);
	x ^= x << 23; s128[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
	return  s128[1] + y;
}


double pvsc_rand_d_xorshift128plus(void) {
	uint64_t       x = s128[0];
	uint64_t const y = s128[1];
	s128[0] = y;
	x ^= x << 23; s128[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
    double r;
    uint64_t * pr   = (uint64_t *)&r;
    *pr = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & ( s128[1] + y ) );
    return r - 1.;
}

uint64_t pvsc_rand_ls_xorshift128plus( uint64_t * s) {
	uint64_t       x = s[0];
	uint64_t const y = s[1];
	s[0] = y;
	x ^= x << 23; s[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
	return  s[1] + y;
}


double pvsc_rand_ds_xorshift128plus( uint64_t * s) {
	uint64_t       x = s[0];
	uint64_t const y = s[1];
	s[0] = y;
	x ^= x << 23; s[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
    double r;
    uint64_t * pr   = (uint64_t *)&r;
    *pr = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & ( s[1] + y ) );
    return r - 1.;

}


void pvsc_rand_ds_xorshift128plus_simd(double * a, int n, void * s_, double b) {
	b= 1. - b;
	uint64_t * s = (uint64_t *)s_;
	uint64_t * s0 = s;
	uint64_t * s1 = s+PVSC_XORS128PLUSSIMDW;
	uint64_t x[PVSC_XORS128PLUSSIMDW];
	uint64_t y[PVSC_XORS128PLUSSIMDW];
	
	uint64_t * ai = (uint64_t *)a;
	
	for(int i=0; i<n; i+=PVSC_XORS128PLUSSIMDW){
		#if PVSC_XORS128PLUSSIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_XORS128PLUSSIMDW; j++){
		#else
		#define j 0
		#endif
			x[j] = s0[j];
			y[j] = s1[j];
			x[j] ^= x[j] << 23;  s1[j] = x[j] ^ s1[j] ^ (x[j] >> 17) ^ (s1[j] >> 26); 
			//ai[i+j] = s1[j] + y[j];
			//a[i+j]= rcp64*(s1[j] + y[j]);
			ai[i+j] = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & (s1[j] + y[j]));
			a[i+j] -= b;
		#if PVSC_XORS128PLUSSIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}

// http://vigna.di.unimi.it/ftp/papers/xorshift.pdf


uint32_t xorshift64_triples[4][3]={
	{ 18, 31, 11},
	{ 19, 29,  8},
	{  8, 29, 19},
	{ 11, 31, 18}};
#define XORS64PT 0
//#define XORS64STAR 0x2545f4914f6cdd1dULL


void pvsc_rand_ds_xorshift64star_simd(double *  a, int n, void * s_, double b) {
	uint64_t * s = (uint64_t *)s_;
	b= 1. - b;
	uint64_t * ai = (uint64_t *)a;
	
	for(int i=0; i<n; i+=PVSC_XORS64STARSIMDW){
		#if PVSC_XORS64STARSIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_XORS64STARSIMDW; j++){
		#else
		#define j 0
		#endif
			s[j] ^= s[j] << 18; s[j] ^= s[j] >>  31; s[j] ^= s[j] << 11;
			//ai[i+j] = s[j];
			//a[i+j] = rcp32*s[j];
			ai[i+j] = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & (s[j]));
			a[i+j] -= b;
		#if PVSC_XORS64STARSIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}


void pvsc_rand_ds_xorshift32_simd(double *  a, int n, void * s_, double b) {
	//b= 1. - b;
	uint32_t * s = (uint32_t *)s_;
	uint64_t * ai = (uint64_t *)a;
	
	for(int i=0; i<n; i+=PVSC_XORS32SIMDW){
		
		#if PVSC_XORS32SIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_XORS32SIMDW; j++){
		#else
		#define j 0
		#endif
			s[j] ^= s[j] << 12; s[j] ^= s[j] >>  5; s[j] ^= s[j] << 11;
			//ai[i+j] = s[j];
			a[i+j] = rcp32*s[j] - b;
			//ai[i+j] =  0x3ff0000000000000 |((uint64_t)(s[j]) << 20 );
			//a[i+j] -= b;
		#if PVSC_XORS32SIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}

 
void pvsc_rand_ds_xorshift128_simd(double *  a, int n, void * s_, double b) {
	//b= 1. - b;
	uint32_t * s = (uint32_t *)s_;
	uint32_t * x = s + PVSC_XORS128SIMDW;
	uint32_t * y = x + PVSC_XORS128SIMDW;
	uint32_t * z = y + PVSC_XORS128SIMDW;
	uint32_t t[PVSC_XORS128SIMDW];
	
	uint64_t * ai = (uint64_t *)a;
	
	for(int i=0; i<n; i+=PVSC_XORS128SIMDW){
		
		#if PVSC_XORS128SIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_XORS128SIMDW; j++){
		#else
		#define j 0
		#endif
			t[j] = x[j] ^ (x[j] << 11);
			x[j] = y[j]; y[j] = z[j]; z[j] = s[j];
			s[j] ^= (s[j] >> 19) ^ t[j] ^ (t[j] >> 8);
			//ai[i+j] = s[j];
			a[i+j] = rcp32*s[j] - b;
			//ai[i+j] =  0x3ff0000000000000 |((uint64_t)(s[j]) << 20 );
			//a[i+j] -= b;
		#if PVSC_XORS32SIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}


double pvsc_rand_d_lcg32(void) {
	s32 = 1664525*s32 + 1013904223;
	//return rcp32*s32;
	double r;
	uint64_t * pr   = (uint64_t *)&r;
	*pr = 0x3ff0000000000000 |((uint64_t)(s32) << 20 );
	return r - 1.;
}

double pvsc_rand_ds_lcg32( uint32_t * s ) {
	s[0] = 1664525*s[0] + 1013904223;
	//return rcp32*s[0];
	double r;
	uint64_t * pr   = (uint64_t *)&r;
	*pr = 0x3ff0000000000000 |((uint64_t)(s[0]) << 20 );
	return r - 1.;
}

void pvsc_rand_ds_lgc32_simd(double *  a, int n, void * s_, double b) {
	//b= 1. - b;
	uint32_t * s = (uint32_t *)s_;
	uint64_t * ai = (uint64_t *)a;
	
	for(int i=0; i<n; i+=PVSC_LGC32SIMDW){
		
		#if PVSC_LGC32SIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_LGC32SIMDW; j++){
		#else
		#define j 0
		#endif
			s[j] = 1664525*s[j] + 1013904223;
			//ai[i+j] = s[j];
			a[i+j] = rcp32*s[j] - b;
			//ai[i+j] =  0x3ff0000000000000 |((uint64_t)(s[j]) << 20 );
			//a[i+j] -= b;
		#if PVSC_LGC32SIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}

double pvsc_rand_d_lcg64(void) {
	s64 = 6364136223846793005ULL*s64 + 1442695040888963407ULL;
	double r;
	uint64_t * pr   = (uint64_t *)&r;
	*pr = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & s64 );
	return r - 1.;
}

double pvsc_rand_ds_lcg64( uint64_t * s ) {
	s[0] = 6364136223846793005ULL*s[0] + 1442695040888963407ULL;
	double r;
	uint64_t * pr   = (uint64_t *)&r;
	*pr = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & s[0] );
	return r - 1.;
}



void pvsc_rand_ds_lgc64_simd(double *  a, int n, void * s_, double b) {
	b= 1. - b;
	uint64_t * ai = (uint64_t *)a;
	uint64_t * s = (uint64_t *)s_;
	for(int i=0; i<n; i+=PVSC_LGC64SIMDW){
		
		#if PVSC_LGC64SIMDW > 1
		#pragma simd
		for(int j=0; j<PVSC_LGC64SIMDW; j++){
		#else
		#define j 0
		#endif
			s[j] = 6364136223846793005ULL*s[j] + 1442695040888963407ULL;
			//ai[i+j] = s[j];
			//a[i+j] = rcp64*s[j] - b;
			ai[i+j] = 0x3FF0000000000000 | ( 0x000FFFFFFFFFFFFF & (s[j]));
			a[i+j] -= b;
		#if PVSC_LGC64SIMDW > 1
		}
		#else
		#undef j 
		#endif
	}
}
