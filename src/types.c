#define _XOPEN_SOURCE 600
#include "pvsc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
#include "pvsc/spmvm_stencil_graphene.h"
#include "pvsc/spmvm_stencil_graphene_short.h"
//#include "pvsc/spmvm_stencil_graphene2b.h"
//#include "pvsc/spmvm_stencil_tri.h"
#include "pvsc/spmvm_stencil_2dlattice.h"
#include "pvsc/spmvm_stencil_3dlattice.h"
//#include "pvsc/spmvm_stencil_3dlattice4b.h"
//#include "pvsc/spmvm_stencil_vectriade.h"
#include "pvsc/spmvm_stencil_topi.h"
*/
#include "pvsc_stencil_kernels/stencil_repos.h"

const pvsc_matrix PVSC_MATRIX_INITIALIZER = {
	.ham = NULL,
	.sys = NULL,
	.ham_scalars = NULL,
	.ham_array = NULL,
	.rand_simd_func_ptr = &pvsc_rand_ds_xorshift128_simd
};

const pvsc_vector PVSC_VECTOR_INITIALIZER = {
	.sys                           = NULL,
	.nb                            = 0,
	.val                           = NULL,
	.n_data_trans_independent      = 0,
	.data_trans_independent_handle = NULL,
	.levels_data_trans_dependent   = 0,
	.n_data_trans_dependent        = NULL,
	.data_trans_dependent_handle   = NULL
};

const pvsc_dense_redundant_matrix PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER = {
	.val       = NULL, 
	.n_row     = 0,
	.n_col     = 0,
	.ld_col    = 0,
	.redundant = true,
	.pernet_matrix = NULL
};

const pvsc_spmvm_handel_t PVSC_SPMVM_HANDEL_INITIALIZER = { 
	.y                      = NULL,
	.x                      = NULL,
	.z                      = NULL,
	.individual_scale_shift = true,
	.scale_h                = NULL,
	.shift_h                = NULL,
	.scale_z                = NULL,
	//.zscale_y               = NULL,
	//.zscale_z               = NULL,
	.dependent_data_trans   = false,
	.mat                    = NULL,
	//.mat_scalars            = NULL,
	//.mat_arrays             = NULL,
	//.ld_arrays              = 0,
	.dot_allreduce          = true,
	.dot_yy                 = NULL,
	.dot_xy                 = NULL,
	.dot_xx                 = NULL,
	.nb_block               = 16,
	.kacz                   = false,
	.call_counter           = 0,
	.call_time              = 0.,
	.call_time_compute      = 0.
	//.call_time_datatrans    = 0.
};


const pvsc_wp_handel_t PVSC_WP_HANDEL_INITIALIZER = { 
	.n       = 32,
	.wp_deep = 32,
	.v0 = NULL,
	.v1 = NULL,
	.w  = NULL,
	.scale_h = NULL,
	.shift_h = NULL,
	.scale_v = NULL,
	.coeff_w = NULL,
	.mat     = NULL,
	.dot_allreduce = false,
	.dot_vv  = NULL,
	.dot_vnv = NULL,
	
	.call_counter = 0,
	.call_time = 0.,
	.call_time_compute = 0.
};

extern const pvsc_vec_data_trans_handle PVSC_VEC_DATA_TRANS_HANDLE_INITIALIZER = {
	//#ifdef PVSC_HAVE_MPI
	//.s_request,
	//.s_status,
	//.r_request,
	//.r_status,
	//#endif
	.vec              = NULL,
	.inter_porc       = false,
	.bcomm            = NULL,
	.n                = 0,
	.buff_send_local  = NULL,
	.buff_recv_remote = NULL
};

int pvsc_init_matrix_from_args (pvsc_matrix * mat,  MPI_Comm comm, int argc, char * argv[] ){
	
	if( !mat->sys ) { mat->sys  = malloc(sizeof(pvsc_system_geo));         }
	if( !mat->ham ) { mat->ham  = malloc(sizeof(pvsc_system_hamiltonian)); }
	int i;
	
	if ( pvsc_get_hamiltionian( mat->ham,  argc, argv ) ) {
		pvsc_printf("pvsc_init_matrix_from_args(): error in pvsc_get_hamiltionian()\n");
		return 1;
	}
	/*
	i = pvsc_read_args("-stencil" , argc, argv);
	if( i ){
		if(       !strcmp(argv[i+1], "graphene_short" ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene_short();
		else if ( !strcmp(argv[i+1], "graphene"  ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene();
		else if ( !strcmp(argv[i+1], "2dlattice"      ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_2dlattice();
		else if ( !strcmp(argv[i+1], "3dlattice"      ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_3dlattice();
	//	else if ( !strcmp(argv[i+1], "3dlattice4b"      ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_3dlattice4b();
	//	else if ( !strcmp(argv[i+1], "graphene2b"     ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene2b();
	//	else if ( !strcmp(argv[i+1], "vectriade"      ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_vectriade();
		else if ( !strcmp(argv[i+1], "topi"           ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_topi();
	//	else if ( !strcmp(argv[i+1], "graphene"       ) ) *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene();
		else                                              *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene();
	}
	else *(mat->ham) = pvsc_make_stencil_hamiltionian_spmvm_stencil_graphene();
	*/
	
	mat->sys->finalised=false;
	
	
	int mpi_xyz[3];
	if ( pvsc_read_mpi_grid( mpi_xyz, argc, argv ) ){
		pvsc_printf("pvsc_init_matrix_from_args(): error read_mpi_grid\n");
		return 1;
	}
	
	
	if ( pvsc_init_system_geo_mpicomm( mat->sys,  comm, mpi_xyz[0], mpi_xyz[1], mpi_xyz[2] ) )
	 {
		 pvsc_printf("pvsc_init_matrix_from_args(): error init_system_geo_mpicomm\n");
		 return 1;
	 }
	
	i = pvsc_read_args("-pbc" , argc, argv); if(i){mat->sys->pbc_x =  true;mat->sys->pbc_y =  true;mat->sys->pbc_z =  true;}
	i = pvsc_read_args("-obc" , argc, argv); if(i){mat->sys->pbc_x = false;mat->sys->pbc_y = false;mat->sys->pbc_z = false;}
	
	i = pvsc_read_args("-pbc_x" , argc, argv);	if(i)	mat->sys->pbc_x = true;
	i = pvsc_read_args("-pbc_y" , argc, argv);	if(i)	mat->sys->pbc_y = true;
	i = pvsc_read_args("-pbc_z" , argc, argv);	if(i)	mat->sys->pbc_z = true;
	
	i = pvsc_read_args("-obc_x" , argc, argv);	if(i)	mat->sys->pbc_x = false;
	i = pvsc_read_args("-obc_y" , argc, argv);	if(i)	mat->sys->pbc_y = false;
	i = pvsc_read_args("-obc_z" , argc, argv);	if(i)	mat->sys->pbc_z = false;
	
	
	if ( pvsc_read_sys_conf( mat->sys, argc, argv ) ){
		pvsc_printf("pvsc_init_matrix_from_args(): error read_sys_conf\n");
		return 1;
	}
	
	
	
	mat->sys->n_o = mat->ham->n_o;
	if ( pvsc_set_ghostlayer_for_system_hamiltonian( mat->sys, mat->ham, 1 ) ){
		pvsc_printf("pvsc_init_matrix_from_args(): error set_ghostlayer_for_system_hamiltonian\n");
		return 1;
	}
	
	if ( pvsc_finalise_system_geo( mat->sys ) ) {
		pvsc_printf("pvsc_init_matrix_from_args(): error finalise_system_geo\n");
		return 1;
	}
	
	if ( pvsc_create_matrix( mat, mat->sys, mat->ham) ) {
		return 1;
	}
	
	i = pvsc_read_args("-rand_simd" , argc, argv);
	if(i){
		if      ( !strcmp( argv[i+1] , "lgc32"   ) )  mat->rand_simd_func_ptr = &pvsc_rand_ds_lgc32_simd;
		else if ( !strcmp( argv[i+1] , "xors32"  ) )  mat->rand_simd_func_ptr = &pvsc_rand_ds_xorshift32_simd;
		else if ( !strcmp( argv[i+1] , "xors128" ) )  mat->rand_simd_func_ptr = &pvsc_rand_ds_xorshift128_simd;
		else if ( !strcmp( argv[i+1] , "xors64s" ) )  mat->rand_simd_func_ptr = &pvsc_rand_ds_xorshift64star_simd;
		else if ( !strcmp( argv[i+1] , "xors128p") )  mat->rand_simd_func_ptr = &pvsc_rand_ds_xorshift128plus_simd;
		else                                            mat->rand_simd_func_ptr = NULL;
		
		if( !mat->rand_simd_func_ptr )  pvsc_printf("   unset rand_simd_func_ptr  [NULL]\n");
		else                            pvsc_printf("     set rand_simd_func_ptr to %s\n",  argv[i+1] );
		
	}
	 
	
	i = pvsc_read_args("-seed" , argc, argv);
	if(i) srand( atoi(argv[i+1]) + 113*mat->sys->mpi_rank );
	else  srand( time(NULL)      + 113*mat->sys->mpi_rank );
	
	#if PVSC_VERBOSE>0
	pvsc_printf("pvsc_init_matrix_from_args() success\n");
	#endif
	
	return 0;
}


int pvsc_init_system_geo_mpicomm (pvsc_system_geo *sys,  MPI_Comm comm, int mpi_size_x, int mpi_size_y, int mpi_size_z)
{	
	int rank=0,size=1;
#ifdef PVSC_HAVE_MPI
	MPI_Comm_rank( comm, &rank );
#endif
	if ( sys->finalised ){
		pvsc_printf("WARNING pvsc_init_system_geo_mpicomm: pvsc_system_geo is finalised\n");
		pvsc_printf("                                      set finalised = False \n");
		sys->finalised=false;
	}
#ifdef PVSC_HAVE_MPI
	MPI_Comm_size( comm, &size );
#endif
	pvsc_printf("   size = %d, mpi_size_xyz = %d * %d * %d\n", size, mpi_size_x,mpi_size_y,mpi_size_z);
	if ( size != mpi_size_x*mpi_size_y*mpi_size_z){
		pvsc_printf("ERROR pvsc_init_system_geo_mpicomm: mpi size do not match with grid size\n");
		pvsc_printf("   size = %d, mpi_size_xyz = %d * %d * %d\n", size, mpi_size_x,mpi_size_y,mpi_size_z);
		
		return 1;
	}
	sys->mpi_comm   =     comm;
	sys->mpi_size   =     size;
	sys->mpi_size_x = mpi_size_x;
	sys->mpi_size_y = mpi_size_y;
	sys->mpi_size_z = mpi_size_z;
	sys->mpi_rank   =     rank;
	sys->mpi_rank_z = rank%mpi_size_z;  rank /= mpi_size_z;
	sys->mpi_rank_y = rank%mpi_size_y;  rank /= mpi_size_y;
	sys->mpi_rank_x = rank;
	
	sys->b_x = 0;
	sys->b_y = 0;
	sys->b_z = 0;
	
	sys->n_x = 1;
	sys->n_y = 1;
	sys->n_z = 1;
	
	sys->pbc_x = false;
	sys->pbc_y = false;
	sys->pbc_z = false;
	
	sys->b_zu.enable_send = true;
	sys->b_zd.enable_send = true; 
	sys->b_zu.enable_recv = true;
	sys->b_zd.enable_recv = true; 
	sys->b_yu.enable_send = true;
	sys->b_yd.enable_send = true;
	sys->b_yu.enable_recv = true;
	sys->b_yd.enable_recv = true;
	sys->b_xu.enable_send = true;
	sys->b_xd.enable_send = true;
	sys->b_xu.enable_recv = true;
	sys->b_xd.enable_recv = true;
	
	//pvsc_printf("pvsc_init_system_geo_mpicomm: ( rank; x, y, z) = ( %d; %d, %d, %d)\n", sys->mpi_rank, sys->mpi_rank_x, sys->mpi_rank_y, sys->mpi_rank_z);
	
	#if PVSC_VERBOSE>0
	pvsc_printf("pvsc_init_system_geo_mpicomm() success\n");
	#endif
	
	return 0;
}


int pvsc_set_ghostlayer_for_system_hamiltonian( pvsc_system_geo *sys, pvsc_system_hamiltonian * ham, int max_wavefront_iter ){
	
	if (sys->finalised) {
		pvsc_printf("WARNING pvsc_set_ghostlayer_for_system_hamiltonian(), sys is already finalised and will be disabled. Call pvsc_finalise_system_geo() again!");
		sys->finalised=false;
		}
	
	pvsc_lidx_t b_x = 0;
	pvsc_lidx_t b_y = 0;
	pvsc_lidx_t b_z = 0;
	int i;
	for( i=0; i<ham->m; i++){
		if( b_x < abs(ham->dx[i]) ) b_x = abs(ham->dx[i]);
		if( b_y < abs(ham->dy[i]) ) b_y = abs(ham->dy[i]);
		if( b_z < abs(ham->dz[i]) ) b_z = abs(ham->dz[i]);
	}
	
	if( max_wavefront_iter*b_x > sys->b_x ) sys->b_x = max_wavefront_iter*b_x;
	if( max_wavefront_iter*b_y > sys->b_y ) sys->b_y = max_wavefront_iter*b_y;
	if( max_wavefront_iter*b_z > sys->b_z ) sys->b_z = max_wavefront_iter*b_z;
	
	if( !(sys->b_x) ) {
			sys->b_xu.enable_send = false;
			sys->b_xd.enable_send = false;
			sys->b_xu.enable_recv = false;
			sys->b_xd.enable_recv = false; }
	if( !(sys->b_y) ) {
			sys->b_yu.enable_send = false;
			sys->b_yd.enable_send = false;
			sys->b_yu.enable_recv = false;
			sys->b_yd.enable_recv = false; }
	if( !(sys->b_z) ) {
			sys->b_zu.enable_send = false;
			sys->b_zd.enable_send = false; 
			sys->b_zu.enable_recv = false;
			sys->b_zd.enable_recv = false; } 
	
	#if PVSC_VERBOSE>0
	pvsc_printf("pvsc_set_ghostlayer_for_system_hamiltonian() success\n");
	#endif
	
	return 0;
}

int pvsc_finalise_system_geo( pvsc_system_geo *sys ){
	
	//int nn_rank;
	
	if( sys->mpi_rank_z != 0 )                  sys->b_zd.rank_send = sys->mpi_rank - 1;
	else                                        sys->b_zd.rank_send = sys->mpi_rank - 1 + sys->mpi_size_z;
	
	if( sys->mpi_rank_y != 0 )                  sys->b_yd.rank_send = sys->mpi_rank -                       sys->mpi_size_z;
	else                                        sys->b_yd.rank_send = sys->mpi_rank - (1 - sys->mpi_size_y)*sys->mpi_size_z;
	
	if( sys->mpi_rank_x != 0 )                  sys->b_xd.rank_send = sys->mpi_rank -                       sys->mpi_size_z*sys->mpi_size_y;
	else                                        sys->b_xd.rank_send = sys->mpi_rank - (1 - sys->mpi_size_x)*sys->mpi_size_z*sys->mpi_size_y;
	
	if( sys->mpi_rank_z != sys->mpi_size_z -1 ) sys->b_zu.rank_send = sys->mpi_rank + 1;
	else                                        sys->b_zu.rank_send = sys->mpi_rank + 1 - sys->mpi_size_z;
	
	if( sys->mpi_rank_y != sys->mpi_size_y -1 ) sys->b_yu.rank_send = sys->mpi_rank +                       sys->mpi_size_z;
	else                                        sys->b_yu.rank_send = sys->mpi_rank + (1 - sys->mpi_size_y)*sys->mpi_size_z;
	
	if( sys->mpi_rank_x != sys->mpi_size_x -1 ) sys->b_xu.rank_send = sys->mpi_rank +                       sys->mpi_size_z*sys->mpi_size_y;
	else                                        sys->b_xu.rank_send = sys->mpi_rank + (1 - sys->mpi_size_x)*sys->mpi_size_z*sys->mpi_size_y;
	
	sys->b_zu.rank_recv = sys->b_zd.rank_send;
	sys->b_zd.rank_recv = sys->b_zu.rank_send;
	sys->b_yu.rank_recv = sys->b_yd.rank_send;
	sys->b_yd.rank_recv = sys->b_yu.rank_send;
	sys->b_xu.rank_recv = sys->b_xd.rank_send;
	sys->b_xd.rank_recv = sys->b_xu.rank_send;
	
	sys->b_zd.tag = 1; 
	sys->b_zu.tag = 6;
	sys->b_yd.tag = 2;
	sys->b_yu.tag = 5;
	sys->b_xd.tag = 3;
	sys->b_xu.tag = 4;
	
	if( !sys->pbc_z ){
		if( sys->mpi_rank_z == 0                  ) { sys->b_zd.enable_send = false; sys->b_zu.enable_recv = false;}
		if( sys->mpi_rank_z == sys->mpi_size_z -1 ) { sys->b_zu.enable_send = false; sys->b_zd.enable_recv = false;} }
	if( !(sys->pbc_y) ){
		if( sys->mpi_rank_y == 0                  ) { sys->b_yd.enable_send = false; sys->b_yu.enable_recv = false;}
		if( sys->mpi_rank_y == sys->mpi_size_y -1 ) { sys->b_yu.enable_send = false; sys->b_yd.enable_recv = false;} }
	if( !sys->pbc_x ){
		if( sys->mpi_rank_x == 0                  ) { sys->b_xd.enable_send = false; sys->b_xu.enable_recv = false;}
		if( sys->mpi_rank_x == sys->mpi_size_x -1 ) { sys->b_xu.enable_send = false; sys->b_xd.enable_recv = false;} }
	/*
	sys->b_zu.enable_send = false;
	sys->b_zd.enable_send = false; 
	sys->b_zu.enable_recv = false;
	sys->b_zd.enable_recv = false; 
	sys->b_xu.enable_send = false;
	sys->b_xd.enable_send = false;
	sys->b_xu.enable_recv = false;
	sys->b_xd.enable_recv = false;
*/
	
	
	sys->b_xu.sys = sys;
	sys->b_xd.sys = sys;
	sys->b_yu.sys = sys;
	sys->b_yd.sys = sys;
	sys->b_zu.sys = sys;
	sys->b_zd.sys = sys;
	
	sys->s_x = sys->n_x + 2*sys->b_x;
	sys->s_y = sys->n_y + 2*sys->b_y;
	sys->s_z = sys->n_z + 2*sys->b_z;
	
	pvsc_lidx_t n[4], p[4];
	
	n[0]=sys->n_o; 
	n[1]=sys->n_z + 2*sys->b_z;
	n[2]=sys->n_y + 2*sys->b_y;
	n[3]=sys->n_x + 2*sys->b_x; 
	sys->v_mem = pvsc_init_volume( n);
	
	n[1]=sys->n_z;
	n[2]=sys->n_y;
	n[3]=sys->n_x;
	p[0]=0; 
	p[1]=sys->b_z;
	p[2]=sys->b_y;
	p[3]=sys->b_x;
	sys->v = pvsc_get_sub_volume( p, n,  sys->v_mem );
	
	sys->b_zd_depend = sys->b_zd;
	sys->b_zu_depend = sys->b_zu;
	
	n[1]=sys->b_z;
	p[1]=sys->b_z;            sys->b_zd.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );  sys->b_zd_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[1]=0;                   sys->b_zu.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );  sys->b_zu_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[1]=sys->s_z-2*sys->b_z; sys->b_zu.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );  sys->b_zu_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[1]=sys->s_z-  sys->b_z; sys->b_zd.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );  sys->b_zd_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	n[1]=sys->n_z;
	p[1]=sys->b_z; 
	
	n[2]=sys->b_y;
	p[2]=sys->b_y;            sys->b_yd.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=0;                   sys->b_yu.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=sys->s_y-2*sys->b_y; sys->b_yu.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=sys->s_y-  sys->b_y; sys->b_yd.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	sys->b_yd_depend = sys->b_yd;
	sys->b_yu_depend = sys->b_yu;
	n[1]=sys->n_z+2*sys->b_z;
	p[1]=0; 
	p[2]=sys->b_y;            sys->b_yd_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=0;                   sys->b_yu_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=sys->s_y-2*sys->b_y; sys->b_yu_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[2]=sys->s_y-  sys->b_y; sys->b_yd_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	n[2]=sys->n_y;
	p[2]=sys->b_y;
	n[1]=sys->n_z;
	p[1]=sys->b_z;
	
	n[3]=sys->b_x;
	p[3]=sys->b_x;            sys->b_xd.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=0;                   sys->b_xu.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=sys->s_x-2*sys->b_x; sys->b_xu.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=sys->s_x-  sys->b_x; sys->b_xd.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	sys->b_xd_depend = sys->b_xd;
	sys->b_xu_depend = sys->b_xu;
	n[1]=sys->n_z+2*sys->b_z;
	p[1]=0;
	n[2]=sys->n_y+2*sys->b_y;
	p[2]=0;
	p[3]=sys->b_x;            sys->b_xd_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=0;                   sys->b_xu_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=sys->s_x-2*sys->b_x; sys->b_xu_depend.v_local  = pvsc_get_sub_volume( p, n,  sys->v_mem );
	p[3]=sys->s_x-  sys->b_x; sys->b_xd_depend.v_remote = pvsc_get_sub_volume( p, n,  sys->v_mem );
	n[3]=sys->n_x;
	p[3]=sys->b_x;
	n[1]=sys->n_z;
	p[1]=sys->n_z;
	n[2]=sys->n_y;
	p[2]=sys->n_y;
	
	#if PVSC_VERBOSE > 0
	pvsc_printf("pvsc_finalise_system_geo():\n");
	pvsc_printf("    x_dim: mpi_size = %2d   local_size = %4d total_size = %6d  (border 2x %d)\n", sys->mpi_size_x, sys->n_x, sys->n_x*sys->mpi_size_x, sys->b_x );
	pvsc_printf("    y_dim: mpi_size = %2d   local_size = %4d total_size = %6d  (border 2x %d)\n", sys->mpi_size_y, sys->n_y, sys->n_y*sys->mpi_size_y, sys->b_y );
	pvsc_printf("    z_dim: mpi_size = %2d   local_size = %4d total_size = %6d  (border 2x %d)\n", sys->mpi_size_z, sys->n_z, sys->n_z*sys->mpi_size_z, sys->b_z );
	pvsc_printf("    o_dim: size = %d\n", sys->n_o );
	
	pvsc_printf("   boundary conditions:");
	if (sys->pbc_x)  pvsc_printf(" pbc_x,"); else pvsc_printf(" obc_x,");
	if (sys->pbc_y)  pvsc_printf(" pbc_y,"); else pvsc_printf(" obc_y,");
	if (sys->pbc_z)  pvsc_printf(" pbc_z,"); else pvsc_printf(" obc_z,");
	pvsc_printf("\n");
	
	#endif
	
	#if PVSC_VERBOSE > 1
	pvsc_printf("    communicator: ( my_rank; xd, xu, yd, yu, zd, zu) = ( %d; %d, %d, %d, %d, %d, %d)\n", 
	     sys->mpi_rank, sys->b_xd.rank_send, sys->b_xu.rank_send, sys->b_yd.rank_send, sys->b_yu.rank_send, sys->b_zd.rank_send, sys->b_zu.rank_send);
	#endif
	
	
	sys->finalised=true;
	return 0;
}


int pvsc_create_matrix( pvsc_matrix * mat, pvsc_system_geo * sys, pvsc_system_hamiltonian * ham )
{
	// todo some checks
	
	mat->sys = sys;
	mat->ham = ham;
	
	mat->ham_array = NULL;
	mat->ham_scalars = NULL;
	if( ham->n_scalars ) pvsc_malloc((void**) &(mat->ham_scalars), sizeof(double),ham->n_scalars                          , true, NULL, 64);
	if( ham->n_arrays  ) pvsc_malloc((void**) &(mat->ham_array),   sizeof(double),ham->n_arrays*sys->s_z*sys->s_y*sys->s_x, true, NULL, 64);
	
	for(pvsc_lidx_t i=0; i<ham->n_scalars; i++) mat->ham_scalars[i] = ham->n_scalars_default[i];
	#pragma omp parallel
	{
	#pragma omp for collapse(3)
	for(pvsc_lidx_t x=0; x<sys->s_x; x++) 
	for(pvsc_lidx_t y=0; y<sys->s_y; y++) 
	for(pvsc_lidx_t z=0; z<sys->s_z; z++) 
	for(pvsc_lidx_t k=0; k<ham->n_arrays; k++)
	mat->ham_array[ k + ham->n_arrays * ( z + sys->s_z * ( y + sys->s_y * x ))] =0.;
	}
	
	#if PVSC_VERBOSE>0
	pvsc_printf("pvsc_create_matrix() success\n");
	#endif
	
	return 0;
}

int pvsc_destroy_matrix( pvsc_matrix * mat ){
	
	mat->sys = NULL;
	mat->ham = NULL;
	
	PVSC_FREE(mat->ham_scalars)
	PVSC_FREE(mat->ham_array)
	
	return 0;
}


int pvsc_print_performance_statistic( pvsc_spmvm_handel_t * p ){
	pvsc_system_geo * sys = p->mat->sys;
	
	double n_xyz = sys->n_x * sys->n_y * sys->n_z;
	double s_xyz = sys->s_x * sys->s_y * sys->s_z;
	
	int flop_per_lup = p->mat->ham->flop_per_lup;
	
	if( p->dot_xx || p->dot_xy || p->dot_yy )
	    flop_per_lup += 6 * sys->n_o;
	
	
	
	double flop = flop_per_lup * n_xyz  * p->x->nb;
	
	
	
	int    store_per_lup  = sys->n_o * p->y->nb;
	int     load_per_lup  = sys->n_o *(p->x->nb + p->y->nb)+ p->mat->ham->n_arrays;
	if( (p->y != p->z) && p->z )
	        load_per_lup += sys->n_o * p->z->nb;
	
	store_per_lup *= sizeof(double);
	 load_per_lup *= sizeof(double);
	 
	double  lup    = n_xyz  * (p->x->nb * sys->n_o * p->call_counter * 1.e-9);
	double  store  = s_xyz * store_per_lup * p->call_counter * 1.e-9;
	double   load  = s_xyz *  load_per_lup * p->call_counter * 1.e-9;
	flop  *=                  p->call_counter * 1.e-9;
	
	pvsc_printf( " ---  pvsc_print_performance_statistic ---\n" );
	pvsc_printf( "    call_counter = %d\n",      p->call_counter);
	pvsc_printf( "    call_time    = %g sec\n",  p->call_time   );
	pvsc_printf( "    call_time_c  = %g sec  ( %g )\n",  p->call_time_compute,  p->call_time_compute/p->call_time  );
	pvsc_printf( "     nb = %d, n_o = %d\n",  p->y->nb, sys->n_o );
	pvsc_printf( "     FLOP/LUP = %g ",  flop_per_lup*1./sys->n_o );
	if( p->dot_xx || p->dot_xy || p->dot_yy )
		pvsc_printf( " (with dots)\n" );
	else
		pvsc_printf( " (with out dots)\n" );
	pvsc_printf( "     BYTE/LUP = %g (load %g, store %g)\n", (load_per_lup + store_per_lup)*1./(p->y->nb*sys->n_o), load_per_lup*1./(p->y->nb*sys->n_o),  store_per_lup*1./(p->y->nb*sys->n_o) );
	pvsc_printf( "     CB = %g\n", flop_per_lup*1./(load_per_lup+store_per_lup)*p->y->nb);
	pvsc_printf( "    P   = %g ( %g ) GLUP/sec   ( %g GLUP )\n",   lup/p->call_time,            lup/p->call_time_compute, lup    );
	pvsc_printf( "    P   = %g ( %g ) GFLOP/sec  ( %g GFLOP )\n", flop/p->call_time,           flop/p->call_time_compute, flop   );
	pvsc_printf( "    B   = %g ( %g ) GB/sec     ( %g GB    )\n", (store + load)/p->call_time, (store + load)/p->call_time_compute, store + load   );
	pvsc_printf( " ------------------------------------------\n" );
	
	
	return 0;
	}

int pvsc_create_dense_redundant_matrix( pvsc_dense_redundant_matrix * mat, pvsc_lidx_t rows, pvsc_lidx_t cols )
{
	*mat = PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER;
	
	mat->val = pvsc_malloc( NULL, sizeof(double), rows*cols, true, NULL, 38);
	
	for(pvsc_lidx_t i = 0; i<rows*cols; i++ ) mat->val[i] = 0;
	
	mat->n_row  = rows;
	mat->n_col  = cols;
	mat->ld_col = cols;
	mat->redundant     = true;
	mat->pernet_matrix = NULL;
	
	return 0;
}

int pvsc_destroy_dense_redundant_matrix( pvsc_dense_redundant_matrix * mat )
{
	
	if( mat->pernet_matrix ){
		pvsc_printf( "pvsc_destroy_dense_redundant_matrix(): error: destroying a dense_redundant_matrix view is not possible\n" );
		return 1;
	}
	
	PVSC_FREE(mat->val)
	
	*mat = PVSC_DENSE_REDUNDANT_MATRIX_INITIALIZER;
	
	return 0;
}

