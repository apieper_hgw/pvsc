
#include "pvsc.h"
#include "pvsc/dot_array.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef _OPENMP
#include <omp.h>
#endif


const pvsc_dot_array PVSC_DOT_ARRAY_INITIALIZER = {
	.n_x = 1,
	.n_y = 1,
	.n_z = 1,
	.Rx = 0.,
	.Ry = 0.,
	.Rz = 0.,
	.Dx = 0.,
	.Dy = 0.,
	.Dz = 0.,
	.V  = 0.,
	
	.offset_x = 0.,
	.offset_y = 0.,
	.offset_z = 0.,
	
	.n = 0,
	.dot = NULL
};


int pvsc_get_gradually_changing_dot_array(pvsc_dot_array * da, double dx, double dy, double dz, double dVz ){
	
	da->n = da->n_y*da->n_z;
	da->dot = malloc(sizeof(pvsc_dot)*da->n);
	
	//double px = da->offset_x;
	double py = da->offset_y;
	double diff_y = da->Dy;
	for(int y=0; y<da->n_y; y++){
		double pz     = da->offset_z;
		double diff_z = da->Dz;
		double V = da->V;
		
		for(int z=0; z<da->n_z; z++){
			int ind = da->n_z*y+z;
			
			da->dot[ind].V = V;
			//da->dot[ind].Rx = da->Rx;
			da->dot[ind].Ry = da->Ry;
			da->dot[ind].Rz = da->Rz;
			//da->dot[ind].rx = px;
			da->dot[ind].ry = py;
			da->dot[ind].rz = pz;
			
			pz += diff_z;
			//pvsc_printf("diff_z = %g\n", diff_z);
			if( 2*z<da->n_z-3 )       diff_z += dz;
			else if ( 2*z>da->n_z-3 ) diff_z -= dz;
			
			if( 2*z<da->n_z-3 )       V += dVz;
			else if ( 2*z>da->n_z-3 ) V -= dVz;
		}
		py += diff_y;
		if( 2*(y+0)<da->n_y )  diff_y += dy;
		else                   diff_y -= dy;
	}
	
	return 0;
}

double pvsc_func_dot_array(pvsc_gidx_t * n, pvsc_lidx_t k, void * parm , void * seed){
	pvsc_dot_array * da = parm;
	double val=0.;
	
	if( da->dot ){
		
		for(int z=0; z<da->n_z; z++)
		for(int y=0; y<da->n_y; y++){
			int ind = da->n_z*y+z;
			double diff_z = da->dot[ind].rz - n[1];
			double diff_y = da->dot[ind].ry - n[2];
			double R2y = da->dot[ind].Ry*da->dot[ind].Ry;
			double R2z = da->dot[ind].Rz*da->dot[ind].Rz;
			double R4  = R2y*R2z;
			
			double diff_r4 = R4 - R2y*diff_z*diff_z - R2z*diff_y*diff_y;
			if( diff_r4 > 0. ) {
				val = da->dot[ind].V;
				break;
			}
		}
		
	}else{

		double R2y = da->Ry*da->Ry;
		double R2z = da->Rz*da->Rz;
		double R4  = R2y*R2z;
		
		
		for(int z=0; z<da->n_z; z++)
		for(int y=0; y<da->n_y; y++){
			double diff_z = z*da->Dz + da->offset_z - n[1];
			double diff_y = y*da->Dy + da->offset_y - n[2];
			double diff_r4 = R4 - R2y*diff_z*diff_z - R2z*diff_y*diff_y;
			if( diff_r4 > 0. ) {
				val = da->V;
				break;
			}
		}
	}
	
	
	return val;
}




